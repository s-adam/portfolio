<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package idvetmp
 */
//
//$fields = get_fields(get_the_ID());
?>
<!doctype html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">

        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favico/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favico/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favico/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favico/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favico/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favico/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favico/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favico/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favico/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_stylesheet_directory_uri(); ?>/img/favico/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favico/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favico/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favico/favicon-16x16.png">
        <link rel="manifest" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favico/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff ">
        <meta name="msapplication-TileImage" content="<?php echo get_stylesheet_directory_uri(); ?>/img/favico/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff ">

				<!-- Globalny tag witryny (gtag.js) - Google Analytics -->
				
				<?php
        if (get_field('google_analytics', 'option')) {
            ?>
<script async src="https://www.googletagmanager.com/gtag/js?id=<?= get_field('google_analytics', 'option')?>"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', '<?= get_field('google_analytics', 'option')?>');
</script>
<?php
        }
            ?>
			
			
			<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-120440877-1"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-120440877-1');
</script>
		
        <link href="https://fonts.googleapis.com/css?family=Cantata+One|Libre+Franklin:400,700&amp;subset=latin-ext" rel="stylesheet">
        <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
        <link href="<?php echo get_stylesheet_directory_uri(); ?>/hamburgers.css" rel="stylesheet">
        <link type="text/css" media="print" href="<?php echo get_stylesheet_directory_uri(); ?>/print.css" rel="stylesheet">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
        <link type="text/css" rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
        <link type="text/css" rel="stylesheet" href="http://gregfranko.com/jquery.selectBoxIt.js/css/jquery.selectBoxIt.css" />
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
        <div class="pre-loader pre-loader-on"></div>
        <img class="hide-always" src="<?php echo get_stylesheet_directory_uri(); ?>/img/modal-bg.jpg">
        <div id="page" class="site">
            <header id="masthead" class="site-header">
                <nav class="navbar navbar-default navbar-fixed-top menu-scroll">
                    <div class="container">
                        <div class="">
                            <div class="hide-modal-close hamburger hamburger--spring js-hamburger" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <div class="hamburger-box">
                                    <div class="hamburger-inner"></div>
                                </div>
                            </div>
                            <!--brand-->
                            <div class="row">
                                <span class="search-submit search-submit-open-modal"></span>
                                <div class="col-md-4 hide-md">
                                    <ul class="socials-menu">
					<li><a href="/"><i class="fas fa-home"></i></a></li>

                                        <?php
                                        if (get_field('facebook', 'option')) {
                                            ?>
                                            <li><a target="_blank" href="<?php echo the_field('facebook', 'option'); ?>"><i class="fab fa-facebook-f"></i></a></li>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                        if (get_field('instagram', 'option')) {
                                            ?>
                                            <li><a target="_blank" href="<?php the_field('instagram', 'option') ?>"><i class="fab fa-instagram"></i></a></li>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                        if (get_field('youtube', 'option')) {
                                            ?>
                                            <li><a target="_blank" href="<?php the_field('youtube', 'option') ?>"><i class="fab fa-youtube"></i></a></li>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                        if (get_field('linkedin', 'option')) {
                                            ?>
                                            <li><a target="_blank" href="<?php the_field('linkedin', 'option') ?>"><i class="fab fa-linkedin-in"></i></a></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>

                                </div>
                                <div class="col-md-4 text-center">
                                    <a class="text-center" href="/">
                                        <img id="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/logo.svg" alt="">
                                    </a>
                                </div>
                                <div class="col-md-4 hide-md">
                                    <form role="search" method="get" class="search-form" action="/">
                                        <label>
                                            <input class="search-field" placeholder="SZUKAJ PRODUKTU" value="" name="s" type="search">
                                            <input class="search-submit" value="" type="submit">
                                        </label>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div id="navbar" class="navbar-collapse collapse">
                            <?php
                            wp_nav_menu(array(
                                'theme_location' => 'menu-1',
                                'menu_id' => 'primary-menu',
                                'container_class' => '',
                                'menu_class' => 'nav navbar-nav',
                                'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
                                'walker' => new wp_bootstrap_navwalker())
                            );
                            ?> 
                        </div>
                    </div>
                </nav> 

                <?php include 'searchMobileModal.php'; ?>
                <div class="modal fade" id="exampleModal"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <span class="back">
                                                <img id="back-arrow" src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/arrow-red-next.svg" alt="">
                                            </span>
                                            <a class="text-center" href="/">
                                                <img id="logo-black" src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-white.png" alt="">
                                            </a>
                                            <span></span>
                                            <div class="close hamburger hamburger--spring js-hamburger is-active">
                                                <div class="hamburger-box">
                                                    <div class="hamburger-inner"></div>
                                                </div>
                                            </div>
                                            <!--                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                            <span aria-hidden="true"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/close.svg" alt=""></span>
                                                                                        </button>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-body">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-6 borderl">
                                            <div class="menu-products">
                                                <?php
                                                $a = getRootParent(41);

                                                $taxonomyN = get_term($a, 'produkty');
                                                ?>
                                                <span class="menu-products-title"><a href="/produkty/<?= $taxonomyN->slug ?>/"><?= $taxonomyN->name ?></a></span>
                                                <div class="row">
                                                    <?php
                                                    $taxonomyName = "produkty";
                                                    $taxonomies = get_terms(array(
                                                        'taxonomy' => 'produkty',
                                                        'hide_empty' => false
                                                    ));

                                                    if (!empty($taxonomies)) :
                                                        $output = '<ul class="first-ul">';
                                                        $i = 0;


                                                        foreach ($taxonomies as $category) {
                                                            if ($category->parent == 0) {

                                                                foreach ($taxonomies as $subcategory) {

                                                                    if ($subcategory->parent == 41) {
                                                                        if ($i == 0) {
                                                                            $output .= '<div class="col-md-6">';
                                                                        }
                                                                        $i++;
                                                                        $output .= '<li class="first-level"><a href="' . get_term_link($subcategory) . '">' . esc_html($subcategory->name) . '</a></li>';
                                                                        $taxonomies = get_terms($taxonomyName, array('parent' => $subcategory->term_id, 'orderby' => 'DESC', 'hide_empty' => false));
                                                                        if (!empty($taxonomies)) {
                                                                            $output .= '<ul class="second-ul">';
                                                                            foreach ($taxonomies as $term) {

                                                                                $output .= '<li class="second-level"><a href="' . get_term_link($term) . '">' . $term->name . '</a></li>';
                                                                            }
                                                                            $output .= "</ul>";
                                                                        }

                                                                        if ($i == 9) {
                                                                            $output .= '</div>';
                                                                            $i = 0;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }

                                                        $output .= '</ul>';
                                                        echo $output;
                                                    endif;
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <div class="menu-products">
                                                <?php
                                                $a = getRootParent(39);

                                                $taxonomyN = get_term($a, 'produkty');
                                                ?>
                                                <span class="menu-products-title"><a href="/produkty/<?= $taxonomyN->slug ?>/"><?= $taxonomyN->name ?></a></span>
                                                <?php
                                                $taxonomyName = "produkty";
                                                $taxonomies = get_terms(array(
                                                    'taxonomy' => 'produkty',
                                                    'hide_empty' => false
                                                ));
                                                if (!empty($taxonomies)) :
                                                    $output = '<ul class="first-ul">';
                                                    foreach ($taxonomies as $category) {
                                                        if ($category->parent == 0) {
                                                            foreach ($taxonomies as $subcategory) {
                                                                if ($subcategory->parent == 39) {
                                                                    $output .= '<li class="first-level"><a href="' . get_term_link($subcategory) . '">' . esc_html($subcategory->name) . '</a></li>';
//                                    
                                                                    $taxonomies = get_terms($taxonomyName, array('parent' => $subcategory->term_id, 'orderby' => 'DESC', 'hide_empty' => false));
                                                                    $output .= '<ul class="second-ul">';
                                                                    foreach ($taxonomies as $term) {
//                                            
                                                                        $output .= '<li class="second-level"><a href="' . get_term_link($term) . '">' . $term->name . '</a></li>';
                                                                    }
                                                                    $output .= "</ul>";
                                                                }
                                                            }
//                            
                                                        }
                                                    }
                                                    $output .= '</ul>';
                                                    echo $output;
                                                endif;
                                                ?>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>
                </div>



            </header><!-- #masthead -->
            <div id="content" class="site-content">

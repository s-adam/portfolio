<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*
  Template Name: Współpraca
 */
get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>

<div class="main-wrapper main-wrapper-single-page wspolpraca-wrapper add-blur">

    <div class="container">
        <div class="row title-hidden">
            <div class="col-md-12">
                <div class="section-title-archive">
                    <h2><?= the_title(); ?></h2>
                </div>
                <div class="our-products">
                    <?php
                    if ($fields['sekcja_-_wspołpraca']) {
                        foreach ($fields['sekcja_-_wspołpraca'] as $field) {
                            ?>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="text-section">
                                        <?= $field['opis'] ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="photo-section">
                                        <img src="<?= $field['zdjęcie']['sizes']['produkty-img-list'] ?>">
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>			 
            </div>
        </div>
    </div>




    <div class="why-us text-center">
        <div class="section-title">
            <h6><?= $fields['dlaczego_my'] ?></h6>
        </div>

        <div class="container">
            <?php
            if ($fields['dlaczego_my_-_kafelki']) {
                $a = 1;
                $i = 0;
                foreach ($fields['dlaczego_my_-_kafelki'] as $field) {

                    if ($i == 0) {
                        echo '<div class="row">';
                    }
                    ?>

                    <div class="col-md-3">
                        <div class="single-cooperation-wrapper">
                            <img class="text-center" src="<?= $field['ikona']['sizes']['ikona-wspolpraca']; ?>">
                            <?= $field['opis']; ?>
                        </div>
                    </div>
                    <?php
                    $i++;
                    if ($i == 4) {
                        echo "</div>";
                    }
                }
                ?>




            <?php } ?>

            <?php
            ?>

        </div>
    </div>
    <div class="cooperation-contact-section">
        <div class="container">
            <?php
            if ($fields['wspołpraca_-_kontakt']) {
                $a = 1;
                $i = 0;
				$j = 0;
                foreach ($fields['wspołpraca_-_kontakt'] as $field) {

                    if ($i == 0) {
                        echo '<div class="row">';
                    }
                    ?>
                    <div class="col-md-4">
                        <div class="single-contact-wrapper">
                            <div class="header-section">
                                <?php
                                if ($field['tytuł_+_zdjęcie']) {
                                    foreach ($field['tytuł_+_zdjęcie'] as $fieldy) {
                                        ?>
                                        <h4><?= $fieldy['tytuł_sekcji']; ?></h4>
                                        <img class="header-section-img" src="<?= $fieldy['zdjęcie_obok_tytułu_sekcji']['sizes']['produkty-img-list']; ?>">
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                            <div class="under-header-section">
                                <?= $field['dane_osoby_kontakowej']; ?>



                                <div class="contact-section">
                                    <ul class="menu-contact-section">
                                        <?php
                                        if ($field['dane_kontaktowe']) {
                                            foreach ($field['dane_kontaktowe'] as $fieldy) {
                                                ?>
                                                <li>
                                                    <i class="<?= $fieldy['ikona']; ?>"></i><a target="_blank" href="<?php echo ($fieldy['ikona'] == 'fas fa-at') ? 'mailto:' : 'tel:';?><?= $fieldy['telefonemailfax']; ?>"><?= $fieldy['telefonemailfax']; ?></a>
                                                </li>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $i++;
					$j++;
                    if ($i == 3 || count($fields['wspołpraca_-_kontakt']) == $j) {
                        echo "</div>";
						$i = 0;
                    }
                }
                ?>




            <?php } ?>

            <?php
            ?>
            <?php
            if ($fields['działy']) {
                foreach ($fields['działy'] as $field) {
                    ?>
                    <div class="section-department">
                        <h4><?=$field['nazwa_działu']?></h4>
                        <ul class="menu-contact-section">
                            <?php
                            if ($field['dane_kontaktowe']) {
                                foreach ($field['dane_kontaktowe'] as $fieldy) {
                                    ?>
                                    <li>
                                        <i class="<?= $fieldy['ikona']; ?>"></i><a target="_blank" href="tel:<?= $fieldy['telefonemailfax']; ?>"><?= $fieldy['telefonemailfax']; ?></a>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </div>

</div>

<?php
get_footer();

<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*

 */



get_header('');
//the_post();

$fields = get_fields(get_the_ID());
?>

<?php include 'postModal.php'; ?>

<div class="recipes recipes-produkt-list add-blur">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <?php
                $a = getRootParent(get_queried_object()->term_id);
				
                $taxonomyN = get_term($a, 'produkty');
                ?>

                <script>
                    var menuActive = <?= $a; ?>
                </script>

                <span class="menu-products-title"><a href="/produkty/<?= $taxonomyN->slug ?>"><?= $taxonomyN->name ?></a></span>
                
                
                <?php
                
                
                
                $taxonomyName = "produkty";
                $taxonomies = get_terms(array(
                    'taxonomy' => 'produkty',
                    'hide_empty' => false
                ));
                if (!empty($taxonomies)) :
                    $output = '<ul class="first-ul">';
                    foreach ($taxonomies as $category) {
                        if ($category->parent == 0) {
                            $i = 0;
                            foreach ($taxonomies as $subcategory) {
                                if ($subcategory->parent == $a) {
                                    $active = get_queried_object()->term_id == $subcategory->term_id ? 'active' : '';
                                    $output .= '<li class="first-level"><a class="' . $active . '" href="' . get_term_link($subcategory) . '">' . esc_html($subcategory->name) . '</a></li>';
                                    $taxonomies = get_terms($taxonomyName, array('parent' => $subcategory->term_id, 'orderby' => 'DESC', 'hide_empty' => false));
                                    $output .= '<ul class="second-ul">';
                                    foreach ($taxonomies as $term) {
                                        $active = get_queried_object()->term_id == $term->term_id ? 'active' : '';

                                        $output .= '<li class="second-level"><a class="' . $active . '" href="' . get_term_link($term) . '">' . $term->name . '</a></li>';
                                    }
                                    $output .= "</ul>";
                                }
                                $i++;
                            }
                        }
                    }
                    $output .= '</ul>';
                    echo $output;
                endif;
                ?>
            </div>
            <div class="col-md-9 border-left-class">
                <?php
                global $query_string;
//                query_posts("{$query_string}&posts_per_page=3");
                $term_id = get_queried_object()->slug;
				
                ?>

                <?php
//				$paged = (get_query_var('page_val') ? get_query_var('page_val') : 1);
                //$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

                $the_query = new WP_Query(array(
                    'post_type' => 'produkt',
                    //'posts_per_page' => '2',
                    'paged' => $paged,
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'produkty',
                            'field' => 'slug',
                            'terms' => $term_id
                        )
                    ),
                ));
				

                if (function_exists('pagination')) {
                    global $wp_query;
                    pagination($wp_query->max_num_pages);
                }

                $i = 0;

                $p = $paged - 1;
                if ($p < 0) {
                    $p = 0;
                }
                if ($p == 0) {
                    $i = 0;
                } else {
                    $i = ($p * 6) + 1;
                }




                if ($the_query->have_posts() == true) {
                    while ($the_query->have_posts()) :
                        $the_query->the_post();

                        $fields = get_fields(get_the_ID());
                        ?>
                        <div class="news-post__wrapper">        
                            <div class="row txt">
                                <div class="col-md-6">
                                    <?php
                                    if (!empty($fields['nowość'])) {
                                        ?>
                                        <div class="news-post__photo nowosc">
                                            <?php
                                        } else {
                                            ?>
                                            <div class="news-post__photo">
                                                <?php
                                            }
                                            ?>

                                            <a href="#produkt-<?= $i ?>" data-value="<?= $i ?>"><?php the_post_thumbnail('produkty-img'); ?></a>
                                            <a href="#produkt-<?= $i ?>" class="btn btn-rwd" data-value="<?= $i ?>"><i class="fas fa-search"></i></a>
                                            <?php
                                            if (!empty($fields['nowość'])) {
                                                ?>
                                                <div class="new-label">
                                                    <span>NOWOŚĆ</span>
                                                </div>
                                                <?php
                                            }
                                            ?>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="news-post">
                                            <div class="news-post__archive_title">
                                                <a href="#produkt-<?= $i ?>" data-value="<?= $i ?>"><?php the_title(); ?></a>
                                            </div>
                                            <span class="news-post__archive_date"><?= $fields['kod'] ?></span>

                                            <ul class="produkty-list">
                                                <!--
                                                //<?php if ($fields['ilośćwaga']) { ?>
                                                            <li><img src="//<?php echo get_stylesheet_directory_uri(); ?>/img/waga.png"><?= $fields['ilośćwaga'] ?></li>
                                                        //<?php } ?>
                                                //<?php if ($fields['termin_spożycia']) { ?>
                                                            <li><img src="//<?php echo get_stylesheet_directory_uri(); ?>/img/lodowka.png"><?= $fields['termin_spożycia'] ?></li>
                                                        //<?php } ?>
                                                //<?php if ($fields['temperatura_przechowywania']) { ?>
                                                            <li><img src="//<?php echo get_stylesheet_directory_uri(); ?>/img/termometr.png"><?= $fields['temperatura_przechowywania'] ?>&deg;C</li>
                                                        //<?php } ?>
                                                //<?php if ($fields['rodzaj_mięsa']) { ?>
                                                            <li class="d-none"><img src="//<?php echo get_stylesheet_directory_uri(); ?>/img/svg/<?= $fields['rodzaj_mięsa'] ?>.svg"></li>
                                                        //<?php } ?>
                                                -->
                                                <?php if ($fields['ilośćwaga']) { ?>
                                                    <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/icon-scale.svg"><p><?= $fields['ilośćwaga'] ?></p></li>
                                                <?php } ?>
                                                <?php if ($fields['termin_spożycia']) { ?>
                                                    <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/icon-fridge.svg"><?= $fields['termin_spożycia'] ?></li>
                                                <?php } ?>
                                                <?php if ($fields['temperatura_przechowywania']) { ?>
                                                    <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/icon-temp.svg"><?= $fields['temperatura_przechowywania'] ?>&deg;C</li>
                                                <?php } ?>
                                                <?php if ($fields['rodzaj_mięsa'] == "brak") {
                                                    ?><?php } else {
                                                    ?>
                                                    <li class="d-none"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/<?= $fields['rodzaj_mięsa'] ?>.svg"></li>
                                                <?php } ?>

                                            </ul>
                                            <div class="news-post__archive_text">
                                                <?php the_content(); ?>
                                            </div>
                                            <a href="#produkt-<?= $i ?>" class="btn btn-lg" data-value="<?= $i ?>"><i class="fas fa-search"></i></a>

                                        </div>
                                    </div>

                                </div>
                            </div>
                            <?php
                            $i++;
                        endwhile;
                    } else {
                        ?>
                        <div class="no-products text-center">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/empty-list.svg">
                            <h1>BRAK PRODUKTÓW</h1>
                        </div>
                        <?php
                    }
                    ?>

                    <?php
                    $args = [
                        'query' => $the_query
                    ];
                    numeric_posts_nav();
                    ?>

                </div>
            </div>

        </div>
    </div>



    <?php
    get_footer();
    
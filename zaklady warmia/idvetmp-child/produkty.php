<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*
  Template Name: Kategoria Produkty
  Template Post Type: produkty
 */
get_header('');
the_post();
$fields = get_fields(get_the_ID());
?>



<div class="produkty-wrapper add-blur">

    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="category-wrapper">
                    <!--<h2>TEST START</h2>-->
                    <?php
                        $args = array(
                        'child_of'                 => 0,
                        'parent'                   => '',
                        'order'                    => 'ASC',
                        'hide_empty'               => 0,
                        'hierarchical'             => 0,
                        'taxonomy'                 => 'produkty',
                        'pad_counts'               => false );
                        $categories = get_categories($args);
                        echo '<ul>';

                        foreach ($categories as $category) {
                          $url = get_term_link($category);?>
                           <li><a href="<?php echo $url;?>"><?php echo $category->name; ?></a></li>
                          <?php
                        }
                        echo '</ul>';
                    ?>
                    <!--<h2>TEST END</h2>-->
                    <?php
                    $args = array(
                        'post_type' => 'produkty',
                        'posts_per_page' => -1,
                        'post_parent' => wp_get_post_parent_id(get_the_ID()),
                        'order' => 'ASC',
                        'orderby' => 'menu_order'
                    );
                    $parent = new WP_Query($args);
                    ?>
                    <ul>
                        <?php if ($parent->have_posts()) : ?>
                            <?php
                            while ($parent->have_posts()) : $parent->the_post();
                                ?>
                                <li><a href="<?php the_permalink(); ?>" class="<?= get_the_ID() == $currentID ? 'active' : '' ?>" >
                                        <?php the_title(); ?>
                                        <!--START-->
                                        <?php
                                        if (has_children()) {
                                            ?>
                                            <?php
                                            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                                            $args = array(
                                                'post_type' => 'produkty',
                                                'posts_per_page' => 5,
                                                'post_parent' => get_the_ID(),
                                                'order' => 'ASC',
                                                'paged' => $paged
                                            );

                                            $wp_query = null;

                                            $wp_query = new WP_Query($args);
                                            ?>
                                            <?php
                                            if ($wp_query->have_posts() == true) {
                                                if ($wp_query->have_posts()) :
                                                    ?>
                                                    <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

                                                        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

                                                        <?php
                                                    endwhile;

                                                    numeric_posts_nav();

                                                endif;
                                            }else {
                                                ?>
                                                <div class="no-products">
                                                    <h1>BRAK PRODUKTÓW</h1>
                                                </div>
                                                <?php
                                            }
                                            ?>



                                            <?php wp_reset_query(); ?>           

                                            <?php
                                        }
                                        ?>
                                        <!--END-->
                                    </a></li>
                                <?php
                            endwhile;

                        else :
                        endif;
                        ?>
                        <?php wp_reset_query(); ?>
                    </ul>
                </div>
            </div>
            <div class="col-md-9">

                <div class="single-category-product">
                    <?php
                    
                    
                    global $wp_query;
                    $categories = get_the_category();
                    
                    $args = array(
                        'posts_per_page' => 6,
                        'cat' => '28'
                    );

                   

                    $wp_query = new WP_Query($args);
                    ?>
                    <?php
                    if ($wp_query->have_posts() == true) {
                        if ($wp_query->have_posts()) :
                            ?>
                            <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                                <div class="produkt">
                                    <div class="row">
                                        <?php $fields = get_fields(get_the_ID()); ?>
                                        <div class="col-md-5">
                                            <div class="produkt__photo">
                                                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('produkty-img-list'); ?></a>
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="produkt__description">
                                                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                                <div class="producent"><p><a href="<?php the_permalink($post->post_parent) ?>"><?php echo get_the_title($post->post_parent); ?></a></p></div>
                                                <div class="col-md-4 sidebar">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php
                            endwhile;

                            numeric_posts_nav();

                        endif;
                    }else {
                        ?>
                        <div class="no-products">
                            <h1>BRAK PRODUKTÓW</h1>

                        </div>
                        <?php
                    }
                    ?>



                    <?php wp_reset_query(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();

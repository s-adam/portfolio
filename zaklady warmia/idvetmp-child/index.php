<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*
  Template Name: Strona Główna
 */
get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>

<div class="main-wrapper add-blur">
    <img class="left-img hide-md" src="<?php echo get_stylesheet_directory_uri(); ?>/img/ziola-warmia.png">
    <img class="right-img hide-md" src="<?php echo get_stylesheet_directory_uri(); ?>/img/lisc-laurowy.png">

    <div class="container">
        <div class="row first-home-section">
            <div class="col-md-5">
                <?= $fields['sekcja_pierwsza'] ?>
            </div>

            <div class="col-md-7">
                <div class="meat-section">
                    <img class="meat-warmia" src="<?= $fields['sekcja_pierwsza_-_zdjęcie']['sizes']['mainImg'] ?>">
                    <span class="start hvr-sweep-to-right"><p>START</p></span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="our-products">
    <div class="section-title">
        <h6><?= $fields['nasze_produkty-title'] ?></h6>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="featured-image-wrapper left-45">

                    <a class=""><img src="<?= $fields['nasze_produkty_-_zdjęcie_po_lewej']['sizes']['produkty-img-list'] ?>"></a>
                </div>
                <div class="content-wrapper">
                    <?= $fields['nasze_produkty_-_tekst_po_lewej'] ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="featured-image-wrapper featured-image-wrapper-right">
                    <a class=""><img src="<?= $fields['nasze_produkty_-_zdjęcie_po_prawej']['sizes']['produkty-img-list'] ?>"></a>
                </div>
                <div class="content-wrapper content-wrapper-right">
                    <?= $fields['nasze_produkty_-_tekst_po_prawej'] ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="news-slider">
    <img class="right-img hide-md" src="<?php echo get_stylesheet_directory_uri(); ?>/img/czosnek.png">
    <div class="container">
        <div class="section-title" style="background: url(<?php echo get_stylesheet_directory_uri(); ?>/img/svg/warmia-section-title-dark.svg) center center no-repeat">
            <h6>Nowości</h6>
        </div>
        <div class="home-swiper swiper-container">
            <!-- Additional required wrapper -->
            <div class="swiper-wrapper">
                <!-- Slides -->
                <?php
                if ($fields['nowości_-_slider']) {

                    foreach ($fields['nowości_-_slider'] as $field) {
                        ?>
                        <div class="swiper-slide">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="hide-on-small">
                                        <div class="slide-title">
                                            <?= $field['opis'] ?>
                                        </div>
                                        <div class="describe">
                                            <ul>
                                                <?php if ($field['rodzaj_mięsa']) {
                                                    ?>
                                                    <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/<?= $field['rodzaj_mięsa'] ?>.svg"></li>
                                                <?php }
                                                ?>
                                                <?php if ($field['waga']) {
                                                    ?>
                                                    <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/icon-scale.svg"></li>
                                                    <li><span class="little_desc"><?= $field['waga'] ?></span></li>
                                                <?php }
                                                ?>
                                                <?php if ($field['termin_przydatności_do_spożycia']) {
                                                    ?>
                                                    <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/icon-fridge.svg"></li>
                                                    <li><span class="little_desc"><?= $field['termin_przydatności_do_spożycia'] ?></span></li>
                                                <?php }
                                                ?>
                                            </ul>
                                            <a class="btn">ZOBACZ WSZYSTKIE</a>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-7">
                                    <div class="slider-inside-wrapper">
                                        <img class="slide-photo" src="<?= $field['zdjęcie']['sizes']['glowna-news-img'] ?>">

                                        <div class="only-small">
                                            <div class="slide-title">
                                                <?= $field['opis'] ?>
                                            </div>
                                            <div class="describe">
                                                <ul>
                                                    <?php if ($field['rodzaj_mięsa']) {
                                                        ?>
                                                        <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/<?= $field['rodzaj_mięsa'] ?>.svg"></li>
                                                    <?php }
                                                    ?>
                                                    <?php if ($field['waga']) {
                                                        ?>
                                                        <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/icon-scale.svg"></li>
                                                        <li><span class="little_desc"><?= $field['waga'] ?></span></li>
                                                    <?php }
                                                    ?>
                                                    <?php if ($field['termin_przydatności_do_spożycia']) {
                                                        ?>
                                                        <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/icon-fridge.svg"></li>
                                                        <li><span class="little_desc"><?= $field['termin_przydatności_do_spożycia'] ?></span></li>
                                                    <?php }
                                                    ?>
                                                </ul>
                                                <a class="btn">ZOBACZ WSZYSTKIE</a>
                                            </div>
                                        </div>

                                        <div class="swiper-button-prev"></div>
                                        <div class="swiper-button-next"></div>
                                        <div class="swiper-pagination"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php
                    }
                }
                ?>
                <a class="btn show-on-small">ZOBACZ WSZYSTKIE</a>
                <span id="count-slides"><?= count($fields['nowości_-_slider']); ?></span>

            </div>
            <!-- If we need pagination -->


            <!-- If we need navigation buttons -->


        </div>
    </div>
    <img class="left-img hide-md" src="<?php echo get_stylesheet_directory_uri(); ?>/img/pietruszka.png">
</div>
<div class="recipes">
    <div class="container">
        <div class="section-title">
            <h6>Przepisy</h6>
        </div>
        <div class="row">

            <?php
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

            query_posts(array('posts_per_page' => 2, 'post_type' => 'przepisy', 'order' => 'ASC', 'paged' => $paged));

            if (have_posts()) :
                ?>
                <?php
                // Start the Loop.

                while (have_posts()) : the_post();
                    ?>
                    <?php
                    $fields = get_fields(get_the_ID());
                    $row = false;
                    ?>
                    <?php if (has_post_thumbnail()) : ?>

                    <?php endif;
                    ?>
                    <div class="col-md-6">
                        <div class="latest-news__single-col">
                            <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('news-img'); ?></a>
                            <a class="btn hvr-bounce-to-right" href="<?php the_permalink(); ?>">ZOBACZ</a>
                            <span><?php
                                $terms = get_the_terms($post->ID, 'przepis');
                                foreach ($terms as $term) {
                                    ?>
                                    <a href="/przepis/<?= $term->slug; ?>"><?= $term->name; ?></a>
                                    <?php
                                }
                                ?></span>
                            <div class="wrapper-outer">
                                <div class="row">
                                    <div class="col-md-9 col-xs-9">
                                        <div class="wrapper-for-title">
                                            <a class="latest-news__post-title" href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-xs-3">
                                        <div class="clock-section">
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/time.svg">
                                            <span class="time"><?= $fields['czas_przygotowania']; ?></span>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                endwhile;

            endif;
            ?>
        </div>
    </div>
    <div class="read-more-posts-wrapper">
        <a class="read-more-posts" href="/przepisy">
            Zobacz wszystkie przepisy
        </a>
    </div>
</div>
<div class="latest-news">
    <img class="right-img hide-md" src="<?php echo get_stylesheet_directory_uri(); ?>/img/chili.png">
    <div class="container">
        <div class="section-title" style="background: url(<?php echo get_stylesheet_directory_uri(); ?>/img/svg/warmia-section-title-dark.svg) center center no-repeat">
            <h6>Aktualności</h6>
        </div>


        <div class="row">
            <?php
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

            query_posts(array('posts_per_page' => 2, 'post_type' => 'aktualnosci', 'order' => 'ASC', 'paged' => $paged));

            if (have_posts()) :
                ?>
                <?php
                // Start the Loop.

                while (have_posts()) : the_post();
                    ?>
                    <?php
                    $row = false;
                    ?>
                    <?php if (has_post_thumbnail()) : ?>

                    <?php endif; ?>
                    <div class="col-md-6">
                        <div class="latest-news__single-col">
                            <span><?php the_time("j F Y"); ?></span>
                            <div class="wrapper-for-title">
                                <div class="latest-news__post-title">
                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                </div>
                            </div>

                            <div class="latest-news__content">
                                <?php the_excerpt(); ?>
                            </div>
                        </div>
                    </div>
                    <?php
                endwhile;

            endif;
            ?>

        </div>

    </div> 

    <div class="read-more-posts-wrapper">
        <a class="read-more-posts" href="/aktualnosci">
            Zobacz wszystkie aktualności
        </a>
    </div>


</div>

<?php
get_footer();

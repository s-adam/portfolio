<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*
  Template Name: Przepis
 */
get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>

<div class="main-wrapper main-wrapper-single-page add-blur">

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="main-wrapper-single-page-title"><?= get_the_title(); ?></h1>
                <span class="category">

                    <?php
                    $terms = get_the_terms($post->ID, 'przepis');
                    foreach ($terms as $term) {
                        echo $term->name;
                    }
                    ?>
                </span>

                <?php
                if ($fields['zdjęcia']) {
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="triple-slider">
                                <!-- Slider main container -->

                                <div class="swiper-container swiper-triple-news">
                                    <!-- Additional required wrapper -->
                                    <div class="swiper-wrapper">
                                        <?php
                                        foreach ($fields['zdjęcia'] as $field) {
                                            ?>
                                            <div class="swiper-slide">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="slide-photo text-center">
                                                            <img src="<?= $field['zdjęcie']['sizes']['single-news-img'] ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        ?>

                                    </div>
                                    <?php $ile = count($fields['zdjęcia']); ?>
                                    <!-- If we need navigation buttons -->
                                    <?php
                                    if ($ile > 1) {
                                        ?>
                                        <div class="swiper-button-prev triple-btn-prev"></div>
                                        <div class="swiper-button-next triple-btn-next"></div>
                                        <div class="swiper-pagination"></div>
                                        <?php
                                    }
                                    ?>
                                    

                                </div>

                            </div>

                        </div>
                    </div>
                    <?php
                }
                ?>

                <div class="recipe-details">
                    <?php $fields = get_fields(get_the_ID()); ?>
                    <ul>
                        <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/time.svg"><?= $fields['czas_przygotowania']; ?></li>

                        <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/people.svg"><?= $fields['liczba_osob']; ?></li>

                        <li><img class="drukuj" src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/drukarka.svg"><span class="drukuj">Drukuj przepis</span></li>

                        <li class="dont"><img class="dont" src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/facebook.svg"><a href="https://www.facebook.com/sharer/sharer.php?u=<?= get_permalink() ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">Udostępnij</a></li>
                    </ul>
                </div>


            </div>
        </div>
        <div class="row recipe-styling">
            <div class="col-md-4">
                <span class="recipe-headers">SKŁADNIKI</span>

                <div class="recipe-headers-ul">
                    <?= $fields['składniki'] ?>
                </div>
            </div>
            <div class="col-md-8">
                <div class="simple-wrapper-single-recipe">
                    <span class="recipe-headers">SPOSÓB PRZYGOTOWANIA</span>
                    <?= $fields['przepis'] ?>
                </div>
            </div>
        </div>
    </div>


</div>

<?php
get_footer();

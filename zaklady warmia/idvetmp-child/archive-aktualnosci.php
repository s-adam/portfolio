<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>



<div class="recipes add-blur">
    <div class="container">
        <div class="section-title-archive">
            <h2>Aktualności</h2>
        </div>

        <?php
        //juz
        global $query_string;
        query_posts("{$query_string}&posts_per_page=3");

        $args = array(
            'posts_per_page' => 3,
            'post_type' => 'aktualnosci',
            'paged' => get_query_var('paged')
        );
        $wp_query = new WP_Query($args);
        while ($wp_query->have_posts()) : $wp_query->the_post();

            $fields = get_fields(get_the_ID());
            ?>
            <div class="news-post__wrapper">
                <div class="row">
                    <div class="col-md-5">
                        <div class="news-post__photo">
                            <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('news-photo'); ?></a>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="news-post">
                            <span class="news-post__archive_date"><?php the_time('d F Y'); ?></span>
                            <div class="news-post__archive_title">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </div>

                            <div class="news-post__archive_text">
                                <?php the_excerpt(); ?>
                            </div>
                            <a href="<?php the_permalink(); ?>" class="btn single-btn-arch hvr-bounce-to-right">CZYTAJ</a>
                        </div>
                    </div>

                </div>
            </div>
            <?php
        endwhile;
        ?>


        <?php
        numeric_posts_nav();
        /* the_posts_pagination( array(
          'mid_size' => 3,
          'prev_text' => __( '&laquo', '' ),
          'next_text' => __( '&raquo', '' ),
          ) );
         */
        ?>
    </div>
</div>



<?php
//get_sidebar();
get_footer();

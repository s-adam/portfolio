<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*
  Template Name: Sieć Sklepów
 */
get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>

<?php
$url = $_SERVER['REQUEST_URI'];
$mapa = '/siec-sklepow/?id=mapa';
$sklep = '/siec-sklepow/?id=lista-sklepow';
?>

<div class="siec-sklepow add-blur">
    <div class="container">
        <div class="select-map">
            <div class="row">
                <div class="col-md-6">
                    <div class="section-title-archive">
                        <h2><?php the_title(); ?></h2>
                    </div>

                </div>
                <div class="col-md-6">
                    <div class="list-right text-right">
                        <ul>
                            <li><a class="<?php if ($url == $mapa) echo "active" ?>" href="?id=mapa">MAPA</a></li>
                            <li><a class="<?php if ($url == $sklep) echo "active" ?>" href="?id=lista-sklepow">LISTA SKLEPÓW</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php
                if ($url == $mapa) {
                    echo '<div id="map"></div>';
                    echo '<style>.siec-sklepow-lista{display:none;}</style>';
                }
                if ($url == $sklep) {
                    echo '<style>#map{display:none;}</style>';
                }
                ?>
            </div>
        </div>

        <div class="siec-sklepow-lista">
            <div class="row">
                <div class="col-md-6">
                    <script>
                        var locations = [
<?php
//var_dump($fields['mapa']);
$i = 0;
foreach ($fields['mapa'] as $field) {
    echo '[', $field['znacznik_na_mapie']['lat'], ', ' . $field['znacznik_na_mapie']['lng'], '],';
}
?>
]
                    </script>
                    
                    <script>
                    
//                                            var contentString = [
//<?php
//var_dump($fields['mapa']);
$i = 0;
foreach ($fields['mapa'] as $field) {
//    echo '[', $field['adres'], '],';
}
?>
                        ]
                        
                       
                        
                        
                    </script>
                    <script>  
    <?php 
        $php_array = $fields['mapa'];
        $code_array = json_encode($php_array);
    ?>  
    window.array_code = <?= $code_array ?>
//    const address_list = array_code.map(item => item.znacznik_na_mapie.address);
//    console.log(address_list);
</script>


                </div>
                <div class="col-md-6 text-right">
                    <span id="select-woj"><span class="move-up">Wybierz województwo</span>
                        <div class="select-box">
                            <select id="wojewodztwo" name="carlist" form="carform">
                                <option data-value="Wybierz">Wybierz</option>
                                <option data-value="Dolnośląskie">Dolnośląskie</option>
                                <option data-value="Kujawsko-Pomorskie">Kujawsko-Pomorskie</option>
                                <option data-value="Lubelskie">Lubelskie</option>
                                <option data-value="Lubuskie">Lubuskie</option>
                                <option data-value="Łódzkie">Łódzkie</option>
                                <option data-value="Małopolskie">Małopolskie</option>
                                <option data-value="Mazowieckie">Mazowieckie</option>
                                <option data-value="Opolskie">Opolskie</option>
                                <option data-value="Podkarpackie">Podkarpackie</option>
                                <option data-value="Podlaskie">Podlaskie</option>
                                <option data-value="Pomorskie">Pomorskie</option>
                                <option data-value="Śląskie">Śląskie</option>
                                <option data-value="Świętokrzyskie">Świętokrzyskie</option>
                                <option data-value="Warmińsko-Mazurskie">Warmińsko-Mazurskie</option>
                                <option data-value="Wielkopolskie">Wielkopolskie</option>
                                <option data-value="Zachodniopomorskie">Zachodniopomorskie</option>
                            </select>
                        </div>
                    </span>
                </div>
            </div>
            <?php
            echo '<div class="row">';

            $i = 0;
            $j = 0;
            foreach ($fields['sklep'] as $field) {
                ?> 
                <div class="col-md-6 single-shop-outer" data-page="1" data-woj="<?php echo $field['wojewodztwo']; ?>">
                    <div class="single-shop" >
                        <?= $field['adres_sklepu']; ?>

                        <div class="row small-space">
                            <div class="col-md-5">
                                <span class="godz-otwarcia">GODZINY OTWARCIA</span>
                                <div class="row">
                                    <div class="col-md-5 col-xs-6 col-sm-6">
                                        <?= $field['dni_tygodnia']; ?>
                                    </div>
                                    <div class="col-md-7 col-xs-6 col-sm-6">
                                        <?= $field['godzina_otwarcia']; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <ul class="menu-contact-section">
                                    <?php
                                    if ($field['numer_kontaktowy']) {
                                        ?>
                                        <i class="fas fa-phone"></i><a target="_blank" href="tel:<?= $field['numer_kontaktowy']; ?>"><?= $field['numer_kontaktowy']; ?></a>
                                        <?php
                                    }
                                    ?>
                                    <?php
                                    if ($field['email']) {
                                        ?>
                                        <i class="fas fa-at"></i><a target="_blank" href="mailto:<?= $field['email']; ?>"><?= $field['email']; ?></a>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>



                <?php
                $i++;
                $j++;
            }
            echo '</div>';
            ?>
            <div class="row">
                <div class="col-md-12 doradcy-empty no-products text-center" style="">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/empty-list.svg">
                    <h5 style="padding: 20px">Nie znaleziono sklepów w tym regionie.</h5>
                </div>
            </div>
        </div>

    </div>
</div>

<?php
get_footer();

<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*
  Template Name: Przepisy
 */
get_header('');
the_post();
//global $query_string;
//query_posts("{$query_string}&posts_per_page=1");
$fields = get_fields(get_the_ID());
?>

<div class="archive-main-wrapper add-blur">

</div>
<div class="recipes add-blur">
    <div class="container">

        <div class="produkty-lista">

            <div class="row">
                <div class="section-title-archive">
                    <h2>Przepisy</h2>
                    
                </div>
                <div class="col-md-6">


                </div>
                <div class="col-md-6 text-right">
                    <span id="select-woj">
                        <div class="select-box">
                            <select id="wojewodztwo" name="carlist" form="carform" onchange="if (this.value)
                                        window.location.href = this.value">
                                <option data-value="Wybierz">WSZYSTKIE</option>

                                <?php
                                $wcatTerms = get_terms('przepis', array('hide_empty' => 0, 'parent' => 0));
                                foreach ($wcatTerms as $wcatTerm) :
                                    ?>

                                    <option value="/przepis/<?php echo $wcatTerm->slug; ?>" data-value="/<?php echo $wcatTerm->slug; ?>">
                                        <?php echo $wcatTerm->name; ?>

                                    </option>

                                    <?php
                                endforeach;
                                wp_reset_postdata();
                                ?>



                            </select>

                        </div>
                    </span>
                </div>
                <span class="search-h2 text-center"><?php
                        $terms = get_the_terms($post->ID, 'przepis');
                        foreach ($terms as $term) {
                            echo $term->name;
                        }
                        ?></span>
            </div>
        </div>

        <?php
        global $query_string;
        $term_id = get_queried_object()->name;
        ?>

        <?php
//        $a = query_posts($the_query);
//        var_dump($a);
        wp_reset_query();
        $the_query = new WP_Query(array(
            'post_type' => 'przepisy',
            'paged' => get_query_var('paged'),
            'tax_query' => array(
                array(
                    'taxonomy' => 'przepis',
                    'field' => 'slug',
                    'terms' => $term_id
                )
            ),
        ));
//        if (function_exists('pagination')) {
//            global $wp_query;
//            pagination($wp_query->max_num_pages);
//        }


        $i = 0;
        $j = 0;

        while ($the_query->have_posts()) :
            $the_query->the_post();
            if ($i == 0) {
                echo '<div class="row">';
            }
            $fields = get_fields(get_the_ID());
            ?>
            <div class="col-md-6">
                <div class="latest-news__single-col">
                    <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('news-img'); ?></a>
                    <a class="btn " href="<?php the_permalink(); ?>">ZOBACZ</a>

                    <span>
                        <?php
                        $terms = get_the_terms($post->ID, 'przepis');
                        foreach ($terms as $term) {
                            echo $term->name;
                        }
                        ?>
                    </span>
                    <div class="wrapper-outer">
                        <div class="row">
                            <div class="col-md-9">
                                <a class="latest-news__post-title" href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                            </div>
                            <div class="col-md-3">
                                <div class="clock-section">
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/timer.png">
                                    <span class="time"><?= $fields['czas_przygotowania']; ?></span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <?php
            $i++;
            $j++;
//            var_dump($the_query->post_count);
            if ($i == 2 || $the_query->post_count == $j) {

                echo '</div>';

                $i = 0;
            }
            ?>
            <?php
        endwhile;
        wp_reset_postdata()
        ?>

        <?php
        $args = [
            'query' => $the_query
        ];
        numeric_posts_nav();
        ?>
        <div class="row">
            <div class="col-md-12 doradcy-empty" style="">
                <h5 style="padding: 20px" class="alert alert-danger">Nie znaleziono przepisów w tej kategorii.</h5>
            </div>
        </div>
    </div>

</div>


<?php
//get_sidebar();
get_footer();

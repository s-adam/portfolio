var swiper = new Swiper('.home-swiper', {
    pagination: {
        el: '.swiper-pagination',
        type: 'fraction',
    },
    dots: false,
    effect: 'fade',
    allowTouchMove: false,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});
var mySwiper5 = new Swiper('.swiper-awards', {
    autoplay: 5000,
    // loop: true,
    slidesPerView: 1,
    navigation: {
        nextEl: '.awards-btn-next',
        prevEl: '.awards-btn-prev'
    },
    effect: 'fade'
});

//$(".close").click(function(){
//    window.location.href = "#";
//});


//$("a[href^=#]").on("click", function(e) {
//    e.preventDefault();
//    history.pushState({}, "", this.href);
//});

var mySwiperModal = new Swiper('.modal-swiper', {
    // Optional parameters
    observer: true,
    observeParents: true,
    loop: true,
    draggable: false,
    slidesPerView: 1,
    centeredSlides: true,
    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
    },

    // Navigation arrows
    navigation: {
        nextEl: '.swiper-button-next1',
        prevEl: '.swiper-button-prev1',
    }

    // And if we need scrollbar
})

var mySwiper6 = new Swiper('.swiper-history', {
    autoplay: 5000,
    // loop: true,
    slidesPerView: 1,
    navigation: {
        nextEl: '.history-btn-next',
        prevEl: '.history-btn-prev'
    },
    effect: 'fade'
});

var mySwiper7 = new Swiper('.swiper-triple-news', {
    autoplay: 5000,
    loop: true,
    speed: 500,
    centeredSlides: true,
    slidesPerView: 1,
    pagination: {
        el: '.swiper-pagination',
    },
    navigation: {
        nextEl: '.triple-btn-next',
        prevEl: '.triple-btn-prev'
    },
    effect: 'fade'
});



//Menu Scroll
$(window).scroll(function (event) {
    var scroll = $(window).scrollTop();
    if (scroll > 0) {
        $(".navbar-default").addClass('bg-click');
    } else {
        $(".navbar-default").removeClass('bg-click');
    }
});

$(window).scroll(function (event) {
    var scroll = $(window).scrollTop();
    if (scroll > 0) {
        $(".navbar-default").addClass('menu-admin');
    } else {
        $(".navbar-default").removeClass('menu-admin');
    }
});

$(document).ready(function () {
    var scroll = $(window).scrollTop();
    if (scroll > 5) {
        $(".navbar-default").addClass('menu-admin');
    } else {
        $(".navbar-default").removeClass('menu-admin');
    }

    if (scroll > 5) {
        $(".navbar-default").addClass('bg-click');
    } else {
        $(".navbar-default").removeClass('bg-click');
    }

    replaceHeadlings();
});

$('.txt').click(function (clicked) {
    var link = $(clicked.currentTarget).find('.news-post').html();
    var photo = $(clicked.currentTarget).find('.news-post__photo').html();
    console.log(link);
    console.log(photo);
    $('.tu').append(link);
});



$('.search-submit-open-modal').click(function () {
    $('#searchMobileModal').modal('toggle');
});

$('#searchMobileModal').on('shown.bs.modal', function (e) {
    $("#search-field").focus();
})

$('.border-left-class .news-post .btn').click(function () {
    $('#postModal').modal('toggle');
    $('#searchModal').modal('toggle');
    $('.tu').empty();
});

$('.border-left-class .news-post__archive_title a').click(function () {
    $('#postModal').modal('toggle');
    $('#searchModal').modal('toggle');
    $('.tu').empty();
});

$('.border-left-class .news-post__photo a').click(function () {
    $('#postModal').modal('toggle');
    $('#searchModal').modal('toggle');
    $('.tu').empty();
});

$('#menu-item-227').click(function () {
    $('#exampleModal').modal('toggle');
    $('.tu').empty();
});

$('.news-slider .btn').click(function () {
    $('#exampleModal').modal('toggle');
    $('.tu').empty();
});

$('.close').click(function () {
    $('#postModal').modal('hide');
    $('#exampleModal').modal('hide');
    $('#searchModal').modal('hide');
    $('#searchMobileModal').modal('hide');

    $('.tu').empty();
});



$('#back-arrow').click(function () {
    $('#postModal').modal('hide');
    $('#exampleModal').modal('hide');
    $('#searchModal').modal('hide');
    $('#searchMobileModal').modal('hide');

    $('.tu').empty();
    $(".hide-modal-close").trigger("click");


    $('.navbar').removeClass('blur');
//    $('.navbar').addClass('blur');
    $('.search-wrapper').addClass('blur');
    $('.add-blur').each(function () {
        $(this).addClass('blur');
    });
});


$(document).keyup(function (e) {
    if (e.keyCode == 27) { // escape key maps to keycode `27`
        $('#postModal').modal('hide');
        $('#exampleModal').modal('hide');
        $('#searchModal').modal('hide');
        $('#searchMobileModal').modal('hide');

        $('.tu').empty();
    }
});


$(".start").click(function () {
    $('html, body').animate({
        scrollTop: $(".our-products").offset().top - 106
    }, 1500);
})

/* Print this page script v 1.0.0 */
jQuery(document).ready(function ($) {
    //setup click function for button target the image class
    $('.drukuj').click(function () {
        window.print();
        return false;
    });
});

function replaceHeadlings() {
    const allBigHeadlings = [...document.querySelectorAll(".single-cooperation-wrapper h3")];
    const allSmallHeadlings = [...document.querySelectorAll(".single-cooperation-wrapper h3")];

    allBigHeadlings.forEach((headling, i) => {
        allSmallHeadlings[i].innerHTML = headling.innerHTML;
        if (allSmallHeadlings[i].offsetWidth > 205) {
            allSmallHeadlings[i].style.fontSize = "22px";
        }
    })
}


//Filtrowanie
var doradcy = null;
var przepisy = null;

function show_doradcy() {

    var doradca = $('.siec-sklepow-lista #wojewodztwo').val();

    var i = 0;

    $('.doradcy-empty').fadeOut(400);
    $(".single-shop-outer").each(function (index) {



        if ($(this).attr('data-woj') == doradca || doradca == 'Wybierz') {
            $(this).fadeIn(400);

            i = i + 1;

        } else {
            $(this).fadeOut(400);
        }


    });





    if (i == 0) {
        $('.doradcy-empty').fadeIn(400);
    }


}

function sortDoradcy() {
    var doradca = $('.siec-sklepow-lista #wojewodztwo').val();

    j = 0;

    doradcyArray = [];


    $(doradca).each(function (index, val) {
        doradcyArray.push(parseInt(val))
    });


    $(".latest-news__single-col").each(function (index) {
        showRow = true;


        if (doradcyArray > 0 && showRow) {

            if (jQuery.inArray($(this).data('woj'), doradcyArray) < 0) {
                showRow = false;
            }
        }

    });


}




$(function () {
    $('.siec-sklepow-lista #wojewodztwo').change(function () {
        show_doradcy();

    });
//    $('.produkty-lista #wojewodztwo').change(function () {
//        show_przepisy();
//        
//    });
//    sortPrzepisy();
    sortDoradcy();

});

//KONIEC
//function show_przepisy() {
//
//    var przepisy = $('.produkty-lista #wojewodztwo').val();
//    
//    var i = 0;
//
//    $('.doradcy-empty').fadeOut(400);
//    $(".wrapper-out").each(function (index) {
//        if ($(this).attr('data-przepis') == przepisy) {
//            $(this).fadeIn(400);
//
//            i = i + 1;
//
//        } else {
//            $(this).fadeOut(400);
//        }
//
//
//    });
//
//    if (przepisy == "wszystkie") {
//        $('.doradcy-empty').fadeIn(400);
//    }
//
//    if (i == 0) {
//        $('.doradcy-empty').fadeIn(400);
//    }
//
//
//}




//function sortPrzepisy() {
//    var przepisy = $('.produkty-lista #wojewodztwo').val();
//
//    j = 0;
//
//    przepisyArray = [];
//
//
//    $(przepisy).each(function (index, val) {
//        przepisyArray.push(parseInt(val))
//    });
//
//
//    $(".wrapper-out").each(function (index) {
//        showRow = true;
//
//
//        if (przepisyArray > 0 && showRow) {
//
//            if (jQuery.inArray($(this).data('przepis'), przepisyArray) < 0) {
//                showRow = false;
//            }
//        }
//
//    });
//
//
//}


function initMap() {
//   alert(lat);
    var bounds = new google.maps.LatLngBounds();


    if (typeof (lat) == "undefined") {
        lat = 53.7671353;
    }
    if (typeof (lng) == "undefined") {
        lng = 20.499327;
    }

    var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: lat, lng: lng},
        scrollwheel: false,
        zoom: 8,
        styles:
                [
                    {
                        "featureType": "all",
                        "elementType": "all",
                        "stylers": [
                            {
                                "lightness": "29"
                            },
                            {
                                "invert_lightness": true
                            },
                            {
                                "hue": "#008fff"
                            },
                            {
                                "saturation": "-73"
                            }
                        ]
                    },
                    {
                        "featureType": "all",
                        "elementType": "labels",
                        "stylers": [
                            {
                                "saturation": "-72"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative",
                        "elementType": "all",
                        "stylers": [
                            {
                                "lightness": "32"
                            },
                            {
                                "weight": "0.42"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative",
                        "elementType": "labels",
                        "stylers": [
                            {
                                "visibility": "on"
                            },
                            {
                                "lightness": "-53"
                            },
                            {
                                "saturation": "-66"
                            }
                        ]
                    },
                    {
                        "featureType": "landscape",
                        "elementType": "all",
                        "stylers": [
                            {
                                "lightness": "-86"
                            },
                            {
                                "gamma": "1.13"
                            }
                        ]
                    },
                    {
                        "featureType": "landscape",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "hue": "#006dff"
                            },
                            {
                                "lightness": "4"
                            },
                            {
                                "gamma": "1.44"
                            },
                            {
                                "saturation": "-67"
                            }
                        ]
                    },
                    {
                        "featureType": "landscape",
                        "elementType": "geometry.stroke",
                        "stylers": [
                            {
                                "lightness": "5"
                            }
                        ]
                    },
                    {
                        "featureType": "landscape",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "weight": "0.84"
                            },
                            {
                                "gamma": "0.5"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "visibility": "off"
                            },
                            {
                                "weight": "0.79"
                            },
                            {
                                "gamma": "0.5"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "simplified"
                            },
                            {
                                "lightness": "-78"
                            },
                            {
                                "saturation": "-91"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "labels.text",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            },
                            {
                                "lightness": "-69"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "lightness": "5"
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "lightness": "10"
                            },
                            {
                                "gamma": "1"
                            }
                        ]
                    },
                    {
                        "featureType": "road.local",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "lightness": "10"
                            },
                            {
                                "saturation": "-100"
                            }
                        ]
                    },
                    {
                        "featureType": "transit",
                        "elementType": "all",
                        "stylers": [
                            {
                                "lightness": "-35"
                            }
                        ]
                    },
                    {
                        "featureType": "transit",
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "all",
                        "stylers": [
                            {
                                "saturation": "-97"
                            },
                            {
                                "lightness": "-14"
                            }
                        ]
                    }
                ]
    });



//    var map = new google.maps.Map(document.getElementById('map'), {
//      zoom: 10,
//      center: new google.maps.LatLng(-33.92, 151.25),
//      mapTypeId: google.maps.MapTypeId.ROADMAP
//    });



    var infowindow = new google.maps.InfoWindow();

    var marker, i;
    var marker2;
//    center: {lat: lat, lng: lng},
    marker2 = new google.maps.Marker({
        position: new google.maps.LatLng(lat, lng),
        map: map,
        icon: '/wp-content/themes/idvetmp-child/img/marker.png'
    });


//    window.array_code;

    const address_list = window.array_code.map(item => item.znacznik_na_mapie.address);

    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][0], locations[i][1]),
            map: map,
            icon: '/wp-content/themes/idvetmp-child/img/marker.png'
        });

        bounds.extend(marker.position);

        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                infowindow.setContent(locations[i][0]);
                infowindow.setContent(address_list[i]);
                infowindow.open(map, marker);
            }
        })(marker, i));
    }

    map.fitBounds(bounds);


//    for (var i = 0; i < markers.length; i++) {
//
//        markers[i] = new google.maps.Marker({
//            position: {lat: lat[i][0], lng: lng[i][1]},
//            map: map,
//            icon: '/wp-content/themes/idvetmp/img/marker.png'
//        });

//    }
//    var markers = [];

//    for (var i,j = 0; i < markers.length; i++,j+2) {
//
//        markers[i] = new google.maps.Marker({
//            position: {lat: lat, lng: lng},
//            map: map,
//            icon: '/wp-content/themes/idvetmp/img/marker.png'
//        });
//
//    }

}


//Paralax

$(document).scroll(function () {

    var $this = $(this);

    if ($this.scrollTop() > 0) {

        var ile = $this.scrollTop();

        var a = (ile / 5);
        b = (ile / 2);
        c = ile / 6;
        d = ile / 11;
        e = ile / 15;
        $('.right-img').css({"transform": "translateY(" + 0 + c + "px)"});
        $('.left-img').css({"transform": "translateY(" + 0 + c + "px)"});
        $('.news-slider .right-img').css({"transform": "translateY(" + 0 + d + "px)"});
        $('.left-img').css({"transform": "translateY(" + 0 + d + "px)"});
        $('.latest-news .right-img').css({"transform": "translateY(" + 0 + e + "px)"});
    }

});

//Submneu
function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}

function myFunction2() {
    var x = document.getElementById("myTopnav2");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}

function openSubMenu() {
    var x = document.getElementById("submenuNav");
    if (x.hidden === true) {
        x.hidden = false;
    } else {
        x.hidden = true;
    }
}




//TRIPLE SLIDER

$(document).ready(function () {

    var szerokosc2 = $(".main-wrapper").innerWidth();

    if (szerokosc2 >= 1200) {
        $('.triple-btn-next').click(function () {
            var next = $('.swiper-slide-next .slide-photo img').attr('src');
            var prev = $('.swiper-slide-prev .slide-photo img').attr('src');

            $('.triple-btn-prev').css('background', 'url(' + prev + ')');
            $('.triple-btn-prev').css('background-size', 'cover');
            $('.triple-btn-prev').css('background-position-x', 'left');
            $('.triple-btn-next').css('background', 'url(' + next + ')');
            $('.triple-btn-next').css('background-size', 'cover');
            $('.triple-btn-next').css('background-position', '99%');
        });

        $('.triple-btn-prev').click(function () {
            var next = $('.swiper-slide-next .slide-photo img').attr('src');
            var prev = $('.swiper-slide-prev .slide-photo img').attr('src');

            $('.triple-btn-prev').css('background', 'url(' + next + ')');
            $('.triple-btn-prev').css('background-size', 'cover');
            $('.triple-btn-prev').css('background-position-x', 'right');
            $('.triple-btn-next').css('background', 'url(' + prev + ')');
            $('.triple-btn-next').css('background-size', 'cover');
            $('.triple-btn-next').css('background-position', '99%');
        });



        var next = $('.swiper-slide-next .slide-photo img').attr('src');
        var prev = $('.swiper-slide-prev .slide-photo img').attr('src');
        $('.triple-btn-prev').css('background', 'url(' + next + ')');
        $('.triple-btn-prev').css('background-size', 'cover');
        $('.triple-btn-prev').css('background-position-x', 'left');
        $('.triple-btn-next').css('background', 'url(' + prev + ')');
        $('.triple-btn-next').css('background-size', 'cover');
        $('.triple-btn-next').css('background-position-x', 'right');

    }
});


//MODAL SLIDER


$(document).ready(function () {
    var szerokosc = $("#postModal").innerWidth();

    if (szerokosc >= 1200) {


        $('.swiper-button-prev1').click(function () {
            var prevTitle = $('.swiper-slide-prev .news-post__archive_title').text();
            $(".prev-title").text(prevTitle);

            var nextTitle = $('.swiper-slide-next .news-post__archive_title').text();
            $(".next-title").text(nextTitle);
        });


        $('.swiper-button-next1').click(function () {
            var nextTitle = $('.swiper-slide-next .news-post__archive_title').text();
            $(".next-title").text(nextTitle);
            var prevTitle = $('.swiper-slide-prev .news-post__archive_title').text();
            $(".prev-title").text(prevTitle);
        });




    }
});

$('#postModal').on('shown.bs.modal', function (e) {
    var prevTitle = $('.swiper-slide-prev .news-post__archive_title').text();
    $(".prev-title").text(prevTitle);
    var nextTitle = $('.swiper-slide-next .news-post__archive_title').text();
    $(".next-title").text(nextTitle);


})

$('#exampleModal').on('shown.bs.modal', function (e) {
    $(".hide-modal-close").trigger("click");
    $('.add-blur').addClass('blur');
    $('.navbar').addClass('blur');
    $('.search-wrapper').addClass('blur');
})

//here
$('#searchModal').on('shown.bs.modal', function (e) {
    var prevTitle = $('.swiper-slide-prev .news-post__archive_title').text();
    $(".prev-title").text(prevTitle);
    var nextTitle = $('.swiper-slide-next .news-post__archive_title').text();
    $(".next-title").text(nextTitle);
})



//$(document).ready(function () {
//    if (window.location.hash) {
//        var url2 = window.location.hash;
//        var res = url2.substr(9);
//        res = parseInt(res) + 1;
//        $('#searchModal').modal('toggle');
//        mySwiperModal.slideTo(res, 1000, false);
//    }
//});
//



function openModalProduct(slide) {
    //var link = $(this).attr('data-value');
    link = parseInt(slide) + 1;
//    alert(link);
    mySwiperModal.slideTo(link, 1000, false);
    $('a[data-toggle="modal"]').click(function () {

        window.location.hash = $(this).attr('href');
    });
}

$('.border-left-class .news-post__archive_title a').click(function () {
    $('.add-blur').addClass('blur');

    $('.navbar').addClass('blur');
    openModalProduct($(this).attr('data-value'));

});

$('.recipes-produkt-list .border-left-class .btn').click(function () {
    $('.add-blur').addClass('blur');
    $('.navbar').addClass('blur');
    $('.search-wrapper').addClass('blur');
    openModalProduct($(this).attr('data-value'));

});


$(document).keyup(function (e) {
    if (e.keyCode == 27) { // escape key maps to keycode `27`
        $('.add-blur').removeClass('blur');
        $('.navbar').removeClass('blur');
        $('.search-wrapper').removeClass('blur');
    }
});

$('.border-left-class .news-post__photo a').click(function () {
    $('.navbar').addClass('blur');
    $('.add-blur').addClass('blur');
    openModalProduct($(this).attr('data-value'));
});
$('.border-left-class .news-post__photo .btn').click(function () {
    $('.navbar').addClass('blur');
    $('.add-blur').addClass('blur');
    openModalProduct($(this).attr('data-value'));
});
$('.border-left-class .btn').click(function () {
    $('.navbar').addClass('blur');
    $('.add-blur').addClass('blur');
    openModalProduct($(this).attr('data-value'));
});

$(document).ready(function () {
    if (window.location.hash) {
        var url2 = window.location.hash;
        var res = url2.substr(9);
        res = parseInt(res) + 1;
        $('#postModal').modal('toggle');
        mySwiperModal.slideTo(res, 1000, false);
    }
    var zeroC = $('.swiper-pagination-current').text();
    var zeroT = $('#count-slides').text();


//    alert(zeroC);



    zeroCurrent = 1;
    zeroTotal = zeroT;
    $(".swiper-pagination-current").text("0" + zeroCurrent);
    $(".swiper-pagination-total").text("0" + zeroTotal);

    $('.home-swiper .swiper-button-prev').click(function () {
        zeroCurrent = zeroCurrent - 1;

        $(".swiper-pagination-current").text("0" + zeroCurrent);
        $(".swiper-pagination-total").text("0" + zeroTotal);
    });
    $('.home-swiper .swiper-button-next').click(function () {
        zeroCurrent = zeroCurrent + 1;

        $(".swiper-pagination-current").text("0" + zeroCurrent);
        $(".swiper-pagination-total").text("0" + zeroTotal);
    });
});

$('.close').click(function () {
    // Remove the # from the hash, as different browsers may or may not include it
    var hash = location.hash.replace('#', '');

    if (hash != '') {
        // Show the hash if it's set

        // Clear the hash in the URL
        location.hash = '';
    }
});

$(document).ready(function () {
    if (typeof (menuActive) != "undefined") {
        if (menuActive == 39 || menuActive == 41) {
            $("#menu-item-227").addClass('menuActive');
        } else {
            $("#menu-item-227").removeClass('menuActive');
        }
    }


});



var hamburger = document.querySelector(".hamburger");
var search = document.querySelector(".search-submit-open-modal");
var close = document.querySelector(".close");
var bodys = document.querySelector(".page");
// On click
hamburger.addEventListener("click", function () {
    // Toggle class "is-active"
    hamburger.classList.toggle("is-active");
    search.classList.toggle("search-z-index");
    close.classList.toggle("close-z-index");
    bodys.classList.toggle("ohidden");
//    bodys.addClass('ohidden');
});





//Executes your code when the DOM is ready.  Acts the same as $(document).ready().
$(function () {
    //Calls the selectBoxIt method on your HTML select box.
    $("#wojewodztwo").selectBoxIt();
});

$('.search-submit-open-modal').click(function () {
    $('.navbar').addClass('blur');
    $("#search-field").click();
    $('.add-blur').each(function () {
        $(this).addClass('blur');
    });
});

$('.close').click(function () {

    if ($(".add-blur").hasClass("blur")) {
        $('.navbar').removeClass('blur');
        $('.search-wrapper').removeClass('blur');
        $('.add-blur').each(function () {
            $(this).removeClass('blur');
        });
    } else {
        $('.navbar').addClass('blur');
        $('.search-wrapper').addClass('blur');
        $('.add-blur').each(function () {
            $(this).addClass('blur');
        });
    }


});
//$(window).load(function () {
//    $(".search-field").val("");
//
//    $(".search-field").focusout(function () {
//        if ($(this).val() != "") {
//            $(this).addClass("has-content");
//        } else {
//            $(this).removeClass("has-content");
//        }
//    })
//});


$('.hamburger').click(function () {
    $('#logo').toggleClass('blur');
    $('.add-blur').toggleClass('blur');
//    $('.add-blur').each(function () {
//        $(this).toggleClass('blur');
//    });
});

$('.close').click(function () {
    $('#logo').removeClass('blur');
    $('.add-blur').each(function () {
        $(this).removeClass('blur');
    });
});

$(document).ready(function () {
    sizeTheVideo();
    $(window).resize(function () {
        sizeTheVideo();
    });
});

function sizeTheVideo() {
    var aspectRatio = 1.78;
    var YT = $('body');

    var video = $('iframe');
    var videoWidth = video.outerWidth();
    var test = YT.outerWidth();

    var newHeight = test / aspectRatio;
    if (videoWidth >= test) {
        video.css({"width": test - 30 + "px", "left": "auto", "height": newHeight + "px"});
    }

}



//$('.pre-loader-on').delay(50000).hide();

setTimeout(function () {
    $('.pre-loader-on').addClass('pre-loader-off');
    setTimeout(function () {
        $('.pre-loader-on').hide();
    }, 1000);
}, 1000);


setTimeout(function () {
    var video = $('body');
    var bodyWidth = video.outerWidth();

    if (bodyWidth > 991) {
        var time = 100;
        $('#primary-menu').find('li').each(function (index) {
            var $this = $(this);
            function delayed() {
                $this.addClass("op1");
                $this.addClass("animated fadeInUp");

            }
            setTimeout(delayed, time);
            time += 100;
        })
    }



}, 700);


//setTimeout(function () {
//
//
//    setTimeout(function () {
//        $("#primary-menu").find("li").each(function (i) {
//            $(this).delay(2000 * i).addClass('test');
//        });
//
//    }, 1000);
//
//
//}, 2000);

$('.hamburger').click(function () {
    setTimeout(function () {

        var time = 100;
        $('#primary-menu').find('li').each(function (index) {
            var $this = $(this);
            function delayed() {
                $this.addClass("animated fadeInUp");

            }
            setTimeout(delayed, time);
            time += 100;
        })
    }, 1);
});


$('#menu-item-227').click(function () {
//   $(".hamburger").click();
});

//$("#search-field").focus();
<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
the_post();
get_header(); ?>

<div class="main-wrapper single-aktualnosc-wrapper single-custom-page-template add-blur">

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><?= the_title(); ?></h1>
                <span class="category"></span>
            </div>
        </div>

        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <div class="text-under-slider">
                    <?= the_content(); ?>
                </div>
            </div>
        </div>
    </div>


</div>
<?php
get_footer();

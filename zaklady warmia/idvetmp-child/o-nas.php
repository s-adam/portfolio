<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*
  Template Name: O nas
 */
get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>



<div class="main-wrapper main-wrapper-single-page wspolpraca-wrapper o-nas add-blur">

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title-archive">
                    <h2><?= the_title(); ?></h2>
                </div>
                <div class="o-nas-first-section">


                    <div class="container">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="text">
                                    <?= $fields['tekst'] ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="photo">
                                    <img src="<?= $fields['zdjęcie']['sizes']['news-img'] ?>">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>			 
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title">
                    <h6><?= $fields['historia_-_tytuł'] ?></h6>
                </div>
                <div class="history-section">



                    <!-- Slider main container -->
                    <div class="swiper-container swiper-history">
                        <!-- Additional required wrapper -->
                        <div class="swiper-wrapper">
                            <?php
                            if ($fields['historia_-_slider']) {
                                foreach ($fields['historia_-_slider'] as $field) {
                                    ?>
                                    <div class="swiper-slide">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="year text-center">
                                                    <?= $field['rok'] ?>
                                                </div>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="text">
                                                    <?= $field['opis'] ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>

                        </div>

                        <!-- If we need navigation buttons -->
                        <div class="swiper-button-prev history-btn-prev"></div>
                        <div class="swiper-button-next history-btn-next"></div>


                    </div>

                </div>			 
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="red-box">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="text">
                                <?= $fields['czerwony_box_-_tekst_obok_certyfikatu']; ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="photo text-center">
                                <img src="<?= $fields['zdjęcie_certyfikatu']['sizes']['news-img'] ?>">
                            </div>
                        </div>
                    </div>
                    <div class="section-two">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="photo-small text-center">
                                    <img src="<?= $fields['zdjęcie_po_lewej_stronie']['sizes']['news-img'] ?>">
                                </div>

                            </div>
                            <div class="col-md-9">
                                <div class="text">
                                    <?= $fields['czerwony_box_-_tekst_po_prawej_stronie']; ?>
                                    <?php
                                    if ($fields['czerwony_box_-_pliki_do_pobrania']) {
                                        foreach ($fields['czerwony_box_-_pliki_do_pobrania'] as $field) {
                                            ?>
                                            <p><a class="btn" href="<?= $field['adres_url'] ?>" target="_blank"><span class="btn-border"><?= $field['nazwa'] ?></span></a></p>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title">
                    <h6><?= $fields['nagrody_-_tytuł_sekcji'] ?></h6>
                </div>
                <div class="awards">
                    <div class="swiper-container swiper-awards">
                        <!-- Additional required wrapper -->
                        <div class="swiper-wrapper">
                            <?php
                            if ($fields['nagrody']) {
                                foreach ($fields['nagrody'] as $field) {
                                    ?>
                                    <div class="swiper-slide">
                                        <div class="row">
                                            <div class="awards-inside-wrapper">
                                                <div class="col-md-8">
                                                    <div class="text-wrapper">
                                                        <div class="text text-center">
                                                            <?= $field['tekst'] ?>
                                                        </div>

                                                    </div>

                                                </div>
                                                <div class="col-md-4 text-center">
                                                    <div class="photo">
                                                        <img src="<?= $field['nagroda_-_logo']['sizes']['news-img'] ?>">
                                                        <div class="swiper-button-prev awards-btn-prev"></div>
                                                        <div class="swiper-button-next awards-btn-next"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>

                        </div>			 
                    </div>

                </div>			 
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title">
                    <h6><?= $fields['misja-title'] ?></h6>
                </div>
                <div class="o-nas-first-section o-nas-first-section-bottom">


                    <div class="container">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="photo">
                                    <img src="<?= $fields['misja_-_zdjęcie']['sizes']['o-nas-img'] ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="text">
                                    <?= $fields['misja_-_tekst'] ?>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>			 
            </div>
        </div>
    </div>

</div>

<?php
get_footer();

<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*
  Template Name: Aktualnosci
 */
get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>

<div class="main-wrapper single-aktualnosc-wrapper add-blur">

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><?= the_title(); ?></h1>
                <span class="category"><?php the_date('j F Y') ?></span>
            </div>
        </div>

        <?php
        if ($fields['slider-aktualnosci']) {
            ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="triple-slider">
                        <!-- Slider main container -->

                        <div class="swiper-container swiper-triple-news">
                            <!-- Additional required wrapper -->
                            <div class="swiper-wrapper">
                                <?php
                                if ($fields['slider-aktualnosci']) {
                                    foreach ($fields['slider-aktualnosci'] as $field) {
                                        ?>
                                        <div class="swiper-slide">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="slide-photo text-center">
                                                        <img src="<?= $field['zdjęcie']['sizes']['single-news-img'] ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>

                            </div>
                            <?php $ile = count($fields['slider-aktualnosci']); ?>
                            <?php
                            if ($ile > 1) {
                                ?>
                                <div class="swiper-button-prev triple-btn-prev"></div>
                                <div class="swiper-button-next triple-btn-next"></div>
                                <div class="swiper-pagination"></div>
                                <?php
                            }
                            ?>

                        </div>
                    </div>

                </div>
            </div>
            <?php
        }
        ?>
        <div class="row">
            <div class="col-md-offset-2 col-md-8">
                <div class="text-under-slider">
                    <?= the_content(); ?>
                    <?php
                    if ($fields['pliki_do_pobrania']) {
                        foreach ($fields['pliki_do_pobrania'] as $field) {
                            ?>
                    <p><a class="btn" href="<?= $field['adres_url'] ?>" target="_blank"><span class="btn-border"><?= $field['nazwa'] ?></span></a></p>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>


</div>

<?php
get_footer();

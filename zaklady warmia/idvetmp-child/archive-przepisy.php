<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*
  Template Name: Przepisy
 */
get_header('');
the_post();
global $query_string;
query_posts("{$query_string}&posts_per_page=6");
$fields = get_fields(get_the_ID());
?>

<div class="archive-main-wrapper add-blur">
    
</div>
<div class="recipes add-blur">
    <div class="container">

        <div class="produkty-lista">

            <div class="row">
                <div class="section-title-archive">
                    <h2>Przepisy</h2>
                </div>
                <div class="col-md-6">


                </div>
                <div class="col-md-6 text-right">
                    <span id="select-woj">
                        <div class="select-box">
                            <select id="wojewodztwo" name="carlist" form="carform" onchange="if (this.value)
                                        window.location.href = this.value">
                                <option data-value="Wybierz">WSZYSTKIE</option>

                                <?php
                                $wcatTerms = get_terms('przepis', array('hide_empty' => 0, 'parent' => 0));
                                foreach ($wcatTerms as $wcatTerm) :
                                    ?>

                                    <option value="/przepis/<?php echo $wcatTerm->slug; ?>" data-value="/<?php echo $wcatTerm->slug; ?>">
                                        <?php echo $wcatTerm->name; ?>

                                    </option>

                                    <?php
                                endforeach;
                                wp_reset_postdata();
                                ?>



                            </select>

                        </div>
                    </span>
                </div>
            </div>
        </div>
        <?php
        $i = 0;
        $j = 0;
        $args = array(
            'posts_per_page' => 6,
            'post_type' => 'przepisy',
            'paged' => get_query_var('paged'),
        );
        $a = query_posts($args);
        $wp_query = new WP_Query($args);
        while ($wp_query->have_posts()) : $wp_query->the_post();
            if ($i == 0) {
                echo '<div class="row">';
            }
            $fields = get_fields(get_the_ID());
            ?>
            <div class="wrapper-out" data-przepis="<?php
            $terms = get_the_terms($post->ID, 'kategorie_przepisow');
            foreach ($terms as $term) {
                echo $term->name;
            }
            ?>">
                <div class="col-md-6">
                    <div class="latest-news__single-col">
                        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('news-img'); ?></a>
                        <a class="btn hvr-bounce-to-right" href="<?php the_permalink(); ?>">ZOBACZ</a>

                        <span>
                            <?php
                            $terms = get_the_terms($post->ID, 'kategorie_przepisow');
                            foreach ($terms as $term) {
                                echo $term->name;
                            }
                            ?>
                        </span>
                        <div class="wrapper-outer">
                            <div class="row">
                                <div class="col-md-9 col-xs-9">
                                    <a class="latest-news__post-title" href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                                </div>
                                <div class="col-md-3 col-xs-3">
                                    <div class="clock-section">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/time.svg">
                                        <span class="time"><?= $fields['czas_przygotowania']; ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <?php
            $i++;
            $j++;
            if ($i == 2 || count($a) == $j) {

                echo '</div>';

                $i = 0;
            }
            ?>
            <?php
        endwhile;
        wp_reset_postdata()
        ?>



        <!--</div>-->
        <?php
        numeric_posts_nav();
        /* the_posts_pagination( array(
          'mid_size' => 3,
          'prev_text' => __( '&laquo', '' ),
          'next_text' => __( '&raquo', '' ),
          ) );
         */
        ?>
        <div class="row">
                <div class="col-md-12 doradcy-empty" style="">
                    <h5 style="padding: 20px" class="alert alert-danger">Nie znaleziono przepisów w tej kategorii.</h5>
                </div>
            </div>
    </div>

</div>
<?php
//get_sidebar();
get_footer();

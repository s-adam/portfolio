<?php
get_header();
?>

<div class="recipes recipes-produkt-list search-wrapper">
    <div class="container">
        <div class="col-md-12 border-left-class">
            <?php
            global $query_string;
            $GET_SEARCH = $_GET["s"];
//                echo $GET_SEARCH;
            ?>
            <div class="results-type">
                <?php
                /* translators: %s: search query. */
                printf(esc_html__('Wyniki wyszukiwania dla: %s', 'idvetmp'), '<span>' . get_search_query() . '</span>');
                ?>
            </div>
            <?php
            //$the_query = new WP_Query(array(
            //    'post_type' => 'produkt',
            //    's' => $GET_SEARCH,
            //    'posts_per_page' => 6
            //));


            $i = 0;
            if (have_posts() == true) {
                while (have_posts()) :
                    the_post();

                    $fields = get_fields(get_the_ID());
                    ?>
                    <div class="news-post__wrapper">        
                        <div class="row txt">
                            <div class="col-md-6">
                                <div class="news-post__photo" data-value="<?= $i ?>">
                                    <a data-value="<?= $i ?>" href="#produkt-<?= $i ?>"><?php the_post_thumbnail('produkty-img'); ?></a>
                                    <?php
                                    if (isset($fields['nowość']) && (!empty($fields['nowość']))) {
                                        ?>
                                        <div class="new-label">
                                            <span>NOWOŚĆ</span>
                                        </div>
                                        <?php
                                    }
                                    ?>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="news-post">
                                    <div class="news-post__archive_title">
                                        <a href="#produkt-<?= $i ?>" data-value="<?= $i ?>"><?php the_title(); ?></a>
                                    </div>
                                    <span class="news-post__archive_date" data-value="<?= $i ?>"><?= $fields['kod'] ?></span>

                                    <ul class="produkty-list">

                                        <?php if ($fields['ilośćwaga']) { ?>
                                            <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/icon-scale.svg"><?= $fields['ilośćwaga'] ?></li>
                                        <?php } ?>
                                        <?php if ($fields['termin_spożycia']) { ?>
                                            <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/icon-fridge.svg"><?= $fields['termin_spożycia'] ?></li>
                                        <?php } ?>
                                        <?php if ($fields['temperatura_przechowywania']) { ?>
                                            <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/icon-temp.svg"><?= $fields['temperatura_przechowywania'] ?>&deg;C</li>
                                        <?php } ?>
                                        <?php if ($fields['rodzaj_mięsa']) { ?>
                                            <li class="d-none"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/<?= $fields['rodzaj_mięsa'] ?>.png"></li>
                                        <?php } ?>
                                    </ul>
                                    <div class="news-post__archive_text">
                                        <?php the_content(get_post()->ID); ?>
                                    </div>
                                    <a href="#produkt-<?= $i ?>" class="btn" data-value="<?= $i ?>"><i class="fas fa-search"></i></a>

                                </div>
                            </div>

                        </div>
                    </div>
                    <?php
                    $i++;
                endwhile;
            } else {
                ?>
                <div class="no-products text-center">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/empty-list.svg">
                    <h1>Brak wyników wyszukiwania. Spróbuj ponownie zmieniając kryteria. </h1>
                </div>
                <?php
            }
            ?>


            <?php
            numeric_posts_nav();
            ?>

        </div>

    </div>
</div>
<?php include 'searchModal.php'; ?>


<?php
get_footer();

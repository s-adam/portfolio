<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package idvetmp
 */
?>

</div><!-- #content -->

<footer class="site-footer site-footer2 add-blur">
    <ul class="socials-menu">
        
        <?php
        if (get_field('facebook', 'option')) {
            ?>
            <li><a target="_blank" href="<?php echo the_field('facebook', 'option'); ?>"><i class="fab fa-facebook-f"></i></a></li>
            <?php
        }
        ?>
        <?php
        if (get_field('instagram', 'option')) {
            ?>
            <li><a target="_blank" href="<?php the_field('instagram', 'option') ?>"><i class="fab fa-instagram"></i></a></li>
            <?php
            }
        ?>
        <?php
        if (get_field('youtube', 'option')) {
            ?>
            <li><a target="_blank" href="<?php the_field('youtube', 'option') ?>"><i class="fab fa-youtube"></i></a></li>
            <?php
            }
        ?>
        <?php
        if (get_field('linkedin', 'option')) {
            ?>
            <li><a target="_blank" href="<?php the_field('linkedin', 'option') ?>"><i class="fab fa-linkedin-in"></i></a></li>
            <?php
            }
        ?>
    </ul>
    <div class="container">
        <div class="mt">
            <div class="row">
                <div class="col-md-3">
                    <?php the_field('lista_odnośnikow_w_stopce', 'option'); ?>
                    
                </div>
                <div class="col-md-3">

                    <?php the_field('przetworstwo', 'option'); ?>
                </div>

                <div class="col-md-3">
                    <p><b>Sekretariat</b></p>
                    <ul class="menu-contact-section">
                        <li>
                            <i class="fas fa-phone"></i><a target="_blank" href="tel:<?php the_field('numer_telefonu', 'option'); ?>"><?php the_field('numer_telefonu', 'option'); ?></a>
                        </li>
                        <li>
                            <i class="fas fa-at"></i><a target="_blank" href="mailto:<?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3 text-right footer-text footer-autor">
                    Copyright &copy <?= date('Y'); ?></br> Zakłady Mięsne "Warmia"
                    <a href="https://ideative.pl">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/by-ideative.svg" alt="ideative.pl">
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 text-center footer-last-image">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/zaklady-miesne-warmia-stek.png" alt="">
            </div>
        </div> 
    </div>

</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>


<?php if ((get_the_ID() == 15) || (get_the_ID() == 19)) {
    ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAr3MdEhoxISN2To7Ok4osIwqpOlR_VMmA&callback=initMap"
    async defer></script>
    <?php
}
?>
<script src='https://www.google.com/recaptcha/api.js'></script>
</body>
</html>

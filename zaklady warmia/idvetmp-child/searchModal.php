<div class="modal fade" id="searchModal"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <span></span>
                            <div class="close hamburger hamburger--spring js-hamburger is-active">
                                                <div class="hamburger-box">
                                                    <div class="hamburger-inner"></div>
                                                </div>
                                            </div>
<!--                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/close.svg" alt=""></span>
                            </button>-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="news-slider2">


                        <div class="modal-swiper swiper-container">

                            <div class="swiper-wrapper">



                                <?php		

                                $i = 0;
                                
                                if (have_posts() == true) {
                                    while (have_posts()) :
                                        the_post();

                                        $fields = get_fields(get_the_ID());
                                        $i = 0;
                                        ?>
                                        <div class="swiper-slide slide<?= $i++ ?>">
                                            <div class="news-post__wrapper">        
                                                <div class="row txt">
                                                    <div class="col-md-6">
                                                        <div class="news-post__photo">
                                                            <?php the_post_thumbnail('produkty-img'); ?>
                                                            <?php
                                                            if (isset($fields['nowość']) && (!empty($fields['nowość']))) {
                                                                ?>
                                                                <div class="new-label">
                                                                    <span>NOWOŚĆ</span>
                                                                </div>
                                                                <?php
                                                            }
                                                            ?>

                                                        </div>
                                                    </div>
                                                    <?php if ($fields['rodzaj_mięsa'] == "brak") { ?>
                                                        <div class="col-md-6 bl bl-red">
                                                        <?php } else {
                                                            ?>
                                                            <div class="col-md-6 bl">
                                                            <?php }
                                                            ?>
                                                            <div class="news-post">
                                                                <div class="news-post__archive_title">
                                                                    <?php the_title(); ?>
                                                                </div>
                                                                <span class="news-post__archive_date"><?= $fields['kod'] ?></span>
                                                                <div class="news-post__archive_text">
                                                                    <?php the_content(); ?>
                                                                </div>
                                                                <ul class="produkty-list">
                                                                    <?php if ($fields['rodzaj_mięsa'] == "brak") {
                                                                        ?><?php } else {
                                                                        ?>
                                                                        <li class="d-none"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/<?= $fields['rodzaj_mięsa'] ?>.svg"></li>
                                                                    <?php } ?>
                                                                    <?php if ($fields['ilośćwaga']) { ?>
                                                                        <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/icon-scale.svg"><?= $fields['ilośćwaga'] ?></li>
                                                                    <?php } ?>
                                                                    <?php if ($fields['termin_spożycia']) { ?>
                                                                        <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/icon-fridge.svg"><?= $fields['termin_spożycia'] ?></li>
                                                                    <?php } ?>
                                                                    <?php if ($fields['temperatura_przechowywania']) { ?>
                                                                        <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/svg/icon-temp.svg"><?= $fields['temperatura_przechowywania'] ?>&deg;C</li>
                                                                    <?php } ?>

                                                                </ul>



                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            $i++;
                                        endwhile;
                                    } else {
                                        ?>
                                        <div class="no-products">
                                            <h1>Brak wyników wyszukiwania. Spróbuj ponownie zmieniając kryteria. </h1>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                








                                </div>

                                
                                <?php
                                $countPost = $the_query->post_count;

                                if ($countPost > 1) {
                                    ?>
                                    <div class="swiper-pagination"></div>


                                    <div class="swiper-button-prev swiper-button-prev1">

                                    </div>
                                    <span class="prev-title"></span>
                                    <span class="next-title"></span>
                                    <div class="swiper-button-next swiper-button-next1">

                                    </div>

                                    <?php
                                }
                                ?>
                                
                                
                                
<!--                                <div class="swiper-pagination"></div>


                                <div class="swiper-button-prev swiper-button-prev1">

                                </div>
                                <span class="prev-title"></span>
                                <span class="next-title"></span>
                                <div class="swiper-button-next swiper-button-next1">

                                </div>-->


                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div> 
</div>
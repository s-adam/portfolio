<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*
  Template Name: Kontakt
 */
get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>

<div class="contact add-blur">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title-archive">
                    <h2>Kontakt</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="contact-form">
        <div class="container">
            <div class="row">
                <div class="col-md-6">

                    <div class="info-section">
                        <?= $fields['glowny_adres'] ?>
                    </div>
                    <div class="sekretariat-info">
                        <p><b>Sekretariat</b></p>
                        <ul class="menu-contact-section">
                            <li>
                                <i class="fas fa-phone"></i><a target="_blank" href="tel:<?php the_field('numer_telefonu', 'option'); ?>"><?php the_field('numer_telefonu', 'option'); ?></a>
                            </li>
                            <li>
                                <i class="fas fa-at"></i><a target="_blank" href="mailto:<?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a>
                            </li>
                        </ul>
                    </div>

                </div>
                <div class="col-md-6">
                    <?php echo do_shortcode('[contact-form-7 id="137" title="Formularz kontaktowy"]'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="sections">
        <div class="container">

            <?php
            if ($fields['dane_kontaktowe_zakładow']) {
                foreach ($fields['dane_kontaktowe_zakładow'] as $field) {
                    ?>

                    <div class="row">
                        <?php
                        if ($field['zakład2']) {
                            foreach ($field['zakład2'] as $field_zakład2) {
                                ?>
                                <div class="col-md-8">
                                    <span class="section-name">
                                        <?= $field_zakład2['nazwa_zakładu'] ?>
                                    </span>
                                    <!--START-->

                                    <?php
                                    $i = 0;
                                    $j = 0;
                                    foreach ($field_zakład2['dział'] as $field_dzial2) {
                                        if ($i == 0) {
                                            echo '<div class="row">';
                                        }
                                        ?>
                                        <div class="col-md-6">
                                            <span class="section-name-inner">
                                                <?= $field_dzial2['nazwa_działu']; ?>

                                            </span>
                                            <ul class="menu-contact-section">
                                                <?php
                                                    foreach ($field_dzial2['dane'] as $field_dane2) {
                                                        ?>
                                                        <li>
                                                            <?php
                                                            if (strpos($field_dane2['pole_kontakt'], '@') == true) {
                                                                ?>
                                                                
                                                                <i class="<?= $field_dane2['ikona'] ?>"></i><a target="_blank" href="mailto:<?= $field_dane2['pole_kontakt'] ?>"><?= $field_dane2['pole_kontakt'] ?></a>
                                                            <?php } else { ?>
                                                                <i class="<?= $field_dane2['ikona'] ?>"></i><a target="_blank" href="tel:<?= $field_dane2['pole_kontakt'] ?>"><?= $field_dane2['pole_kontakt'] ?></a>
                                                                <?php  } ?>
                                                            </li>

                                                            <?php
                                                        }
                                                        ?>
                                            </ul>
                                        </div>
                                        <?php
                                        $i++;
                                        $j++;
                                        if ($i == 2 || count($field_zakład2['dział']) == $j) {

                                            echo '</div>';

                                            $i = 0;
                                        }
                                    }
                                    ?>

                                    <!--END-->
                                </div>
                                <?php
                            }
                        }
                        ?>
                        <?php
                        if ($field['zakład1']) {
                            foreach ($field['zakład1'] as $field_zakład1) {
                                ?>
                                <div class="col-md-4">
                                    <span class="section-name">
                                        <?= $field_zakład1['nazwa_zakładu'] ?>
                                    </span>
                                    <?php
                                    foreach ($field_zakład1['dział'] as $field_dzial1) {
                                        ?>
                                        <div class="row">

                                            <div class="col-md-12">
                                                <span class="section-name-inner">
                                                    <?= $field_dzial1['nazwa_działu']; ?>
                                                </span>
                                                <ul class="menu-contact-section">
                                                    <?php
                                                    foreach ($field_dzial1['dane'] as $field_dane1) {
                                                        ?>
                                                        <li>
                                                            <?php
                                                            if (strpos($field_dane1['pole_kontakt'], '@') == true) {
                                                                ?>
                                                                
                                                                <i class="<?= $field_dane1['ikona'] ?>"></i><a target="_blank" href="mailto:<?= $field_dane1['pole_kontakt'] ?>"><?= $field_dane1['pole_kontakt'] ?></a>
                                                            <?php } else { ?>
                                                                <i class="<?= $field_dane1['ikona'] ?>"></i><a target="_blank" href="tel:<?= $field_dane1['pole_kontakt'] ?>"><?= $field_dane1['pole_kontakt'] ?></a>
                                                                <?php  } ?>
                                                            </li>

                                                            <?php
                                                        }
                                                        ?>
                                                    </ul>
                                                </div>

                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>


                        <?php
                    }
                }
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <div id="map">
                            <?= $fields ['google_maps']['lat'] ?></br>
                            <?= $fields ['google_maps']['lng'] ?>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>

    <script>
        var lat = <?= $fields ['google_maps']['lat'] ?>;
        var lng = <?= $fields ['google_maps']['lng'] ?>;
    </script>

    <?php
    get_footer();

    
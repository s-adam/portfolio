<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package idvetmp
 */
get_header();
?>

<div id="primary" class="content-area">
    <main id="main" class="site-main">

        <div class="error404">

            <div class="class-not-found">
                <h2>Nie znaleziono strony!</h2>
            </div>

        </div>

    </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();

<?php

function idvetmpchild_scripts() {
    //wp_enqueue_style( 'fonty', 'https://fonts.googleapis.com/css?family=Cantata+One|Libre+Franklin:400,700&amp;subset=latin-ext" rel="stylesheet', array( 'idvetmp-style' ));
    wp_enqueue_style('swiper', get_template_directory_uri() . '/library/swiper/swiper.min.css', array('idvetmp-style'));
    wp_enqueue_style('idvetmp-style', get_stylesheet_directory_uri() . '/main.css');
}

add_action('wp_enqueue_scripts', 'idvetmpchild_scripts');

function idvetmp_scriptschild() {
    //wp_enqueue_script('TeenMax', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.1/TweenMax.min.js', array('jquery'), '2018', true);
    //wp_enqueue_script('scroll-magic', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js', array('jquery'), '2018', true);

    //wp_enqueue_script('TeenMax', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js', array('jquery'), '2018', true);
//wp_enqueue_script( 'google_js', 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyAr3MdEhoxISN2To7Ok4osIwqpOlR_VMmA', '', '' );
    wp_enqueue_script('swiper-js', get_template_directory_uri() . '/library/swiper/swiper.min.js', array('jquery'), '20151215', true);
    wp_enqueue_script('jquery-ui', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js', array('jquery', 'bootstrap-js'), '20151215', true);
    wp_enqueue_script('select-box-js', get_stylesheet_directory_uri() . '/js/jquery.selectBoxIt.min.js', array('jquery', 'bootstrap-js'), '20151215', true);
    wp_enqueue_script('main-js', get_stylesheet_directory_uri() . '/js/main.js', array('jquery', 'bootstrap-js'), '20151215', true);
}

add_action('wp_enqueue_scripts', 'idvetmp_scriptschild');


add_image_size('blog-img-thumb', 350, 258, true); //wpisu na blogu
add_image_size('mainImg', 764, 787, false); //wpisu na blogu
add_image_size('news-img', 555, 417, true); //wpisu na blogu
add_image_size('o-nas-img', 606, 373, false); //wpisu na blogu
add_image_size('produkty-img', 555, 370, false); //wpisu na blogu
add_image_size('produkty-img-list', 360, 240, false);
add_image_size('single-news-img', 800, 534, false);
add_image_size('glowna-news-img', 890, 516, false);
add_image_size('news-photo', 458, 306, false); //wpisu na blogu
add_image_size('wspolpraca-img', 405, 262, false); //wpisu na blogu
add_image_size('ikona-wspolpraca', 112, 112, true); //wpisu na blogu


function my_acf_init() {
	
	acf_update_setting('google_api_key', 'AIzaSyAr3MdEhoxISN2To7Ok4osIwqpOlR_VMmA');
}

add_action('acf/init', 'my_acf_init');

function numeric_posts_nav() {

    if (is_singular())
        return;

    global $wp_query;

    /** Stop execution if there's only 1 page */
    if ($wp_query->max_num_pages <= 1)
        return;

    $paged = get_query_var('paged') ? absint(get_query_var('paged')) : 1;
    $max = intval($wp_query->max_num_pages);

    /**    Add current page to the array */
    if ($paged >= 1)
        $links[] = $paged;

    /**    Add the pages around the current page to the array */
    if ($paged >= 3) {
        $links[] = $paged - 1;
//        $links[] = $paged - 2;
    }

    if (( $paged + 3 ) <= $max) {
//        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo '<div class="navigation"><ul>' . "\n";

    /**    Previous Post Link */
    if (get_previous_posts_link())
        printf('<li>%s</li>' . "\n", get_previous_posts_link('&laquo;'));

    /**    Link to first page, plus ellipses if necessary */
    if (!in_array(1, $links)) {
        $class = 1 == $paged ? ' class="active"' : '';
        $pp = zeroise($max, 2);
        printf('<li%s><a href="%s">' . '01' . '</a></li>' . "\n", $class, esc_url(get_pagenum_link(1)), '1');

        if (!in_array(2, $links))
            echo '<li class="up-to-row"><a href="%s">' . '...' . '</a></li>';
    }

    /**    Link to current page, plus 2 pages in either direction if necessary */
    sort($links);
    foreach ((array) $links as $link) {
        $pp = zeroise($link, 2);
        $class = $paged == $link ? ' class="active"' : '';
        printf('<li%s><a href="%s">' . $pp . '</a></li>' . "\n", $class, esc_url(get_pagenum_link($link)), $link);
    }

    /**    Link to last page, plus ellipses if necessary */
    if (!in_array($max, $links)) {

        if (!in_array($max - 1, $links))
            echo '<li><a href="">...</a></li>' . "\n";
        $pp = zeroise($max, 2);
        $class = $paged == $max ? ' class="active"' : '';
        printf('<li%s><a href="%s">' . $pp . '</a></li>' . "\n", $class, esc_url(get_pagenum_link($max)), $max);
    }

    /**    Next Post Link */
    if (get_next_posts_link())
        printf('<li>%s</li>' . "\n", get_next_posts_link('»'));

    echo '</ul></div>' . "\n";

//    var_dump($links);
}

function leading_zero_wp_link_pages_link($i) {
    $i = zeroise($i, 2);
    return $i;
}

add_filter('wp_link_pages_link', 'leading_zero_link_pages_link');

function has_children() {
	global $post;
	return count( get_posts( array('post_parent' => $post->ID, 'post_type' => $post->post_type) ) );
}

//TINY MC BUTTON

add_action('admin_head', 'add_my_tc_button');

function add_my_tc_button() {
    global $typenow;

    if ( !current_user_can('edit_posts') && !current_user_can('edit_pages') ) {
        return;
    }


    if ( get_user_option('rich_editing') == 'true') {
        add_filter("mce_external_plugins", "add_tinymce_plugin");
        add_filter('mce_buttons', 'register_my_tc_button');
    }
}

function add_tinymce_plugin($plugin_array) {
    $plugin_array['gavickpro_tc_button'] =  '/wp-content/themes/idvetmp-child/js/tinymce-buttons.js';
    return $plugin_array;
}

function register_my_tc_button($buttons) {
    array_push($buttons, "gavickpro_tc_button");
    return $buttons;
}


function getRootParent($id){
    $taxonomyCheck = get_term( $id, 'produkty');
    if($taxonomyCheck->parent === 0){
		return $taxonomyCheck->term_id;
    }else{
        return getRootParent($taxonomyCheck->parent);
    }
}


function get_tax_by_search($search_text){

$args = array(
    'taxonomy'      => array( 'my_tax' ), // taxonomy name
    'orderby'       => 'id', 
    'order'         => 'ASC',
    'hide_empty'    => true,
    'fields'        => 'all',
    'name__like'    => $search_text
); 

$terms = get_terms( $args );

 $count = count($terms);
 if($count > 0){
     echo "<ul>";
     foreach ($terms as $term) {
       echo "<li><a href='".get_term_link( $term )."'>".$term->name."</a></li>";

     }
     echo "</ul>";
 }


}

function searchfilter($query) {
 
    if ($query->is_search && !is_admin() ) {
        $query->set('post_type',array('produkt'));
    }
 
return $query;
}
 
add_filter('pre_get_posts','searchfilter');



add_action( 'pre_get_posts', function ( $q ) 
{
    if(    !is_admin() 
        && $q->is_main_query() 
        && $q->is_post_type_archive( 'aktualnosci' ) 
    ) {
        $q->set( 'posts_per_page', 3          );
        $q->set( 'orderby',        'modified' );
    }
});
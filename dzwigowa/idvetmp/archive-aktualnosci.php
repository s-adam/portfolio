<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package idvetmp
 *
 */

get_header();
?>




	<div class="main-container">
		<div class="aktualnosci">
			<div class="container">
				<div class="row white-bg mt">
					<div class="col-md-12">

						<div class="title-wrapper">
							<h3>Aktualności</h3>
							<span class="grey-line"></span>
						</div>
						<?php
						if ( have_posts() ) : ?>
							<?php
							while ( have_posts() ) : the_post();
								?>
						<div class="row">
								<div class="col-md-5">
									<div class="aktualnosci-photo">

										<img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id(), 'aktualnosc-img'); ?>" alt="">
									</div>
								</div>
								<div class="col-md-7">
									<div class="aktualnosci-wrapper">
										<div class="aktualnosc-wrapper">
											<span class="square"></span>
											<div class="text">
												<a href=""><strong><h2><?php the_title(); ?></h2></strong></a>
												<p class="date"><?php the_time('n F Y'); ?></p>
												<p>
													<?php
														the_content();
													?>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
								<span class="grey-line"></span>


								<?php
							endwhile;
							?>
								<?php the_posts_pagination( array(
									'mid_size' => 2,
									'prev_text' => __( '&laquo', 'textdomain' ),
									'next_text' => __( '&raquo', 'textdomain' ),
								) ); ?>
							<?php
						else :
						endif;
						?>

					</div>

				</div>

			</div>

		</div>

	</div>
	<div class="mt50"></div>
<?php
get_template_part('kontaktBox');
?>
<?php
get_footer();

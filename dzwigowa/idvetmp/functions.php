<?php
/**
 * idvetmp functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package idvetmp
 */

require get_template_directory() . '/wp_bootstrap_navwalker.php';

if ( ! function_exists( 'idvetmp_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function idvetmp_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on idvetmp, use a find and replace
	 * to change 'idvetmp' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'idvetmp', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'idvetmp' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'idvetmp_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'idvetmp_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function idvetmp_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'idvetmp_content_width', 640 );
}
add_action( 'after_setup_theme', 'idvetmp_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function idvetmp_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'idvetmp' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'idvetmp' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'idvetmp_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function idvetmp_scripts() {
	wp_enqueue_style( 'idvetmp-style', get_stylesheet_uri() );

	wp_enqueue_script( 'idvetmp-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'idvetmp-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'idvetmp_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';



//more fn


add_action('acf/input/admin_footer', 'my_acf_input_admin_footer');


//ukrywanie pozycji z menu
function remove_menus(){
    global $current_user;
    if($current_user->user_login != 'ideative'){
        remove_menu_page( 'edit-comments.php' );          //Comments
        remove_menu_page( 'themes.php' );                 //Appearance
        remove_menu_page( 'plugins.php' );                //Plugins
        remove_menu_page( 'users.php' );                  //Users
        remove_menu_page( 'tools.php' );                  //Tools
        remove_menu_page( 'options-general.php' );        //Settings
        remove_menu_page( 'edit.php' );                   //Posts
    }    
}
//add_action( 'admin_menu', 'remove_menus' );


//ukrywanie dla redaktora

add_action('admin_head', 'sleek_hide_acf_section_fields_css');

function sleek_hide_acf_section_fields_css () {

    global $current_user;
    if($current_user->user_login != 'ideative'){
        echo '<style>';
        echo '.acf-field-flexible-content .acf-actions .acf-button{display:none}';
        echo '.acf-fc-layout-controlls, .hidden-option{display:none}';
        echo '.btn {background:red}';    
        echo '</style>';
        
    }else{
        
        // włączenie sortowania pol elastycznych
        function my_acf_input_admin_footer() {
        ?>
        <script type="text/javascript">
        (function($) {
            $( ".values" ).sortable({
              connectWith: ".values"
            })
        })(jQuery); 
        </script>
        <?php
        }
        add_action('acf/input/admin_footer', 'my_acf_input_admin_footer');
        // włączenie sortowania pol elastycznych END
    }
    
}    

add_action( 'admin_enqueue_scripts', 'load_admin_styles' );
      function load_admin_styles() {
        wp_enqueue_style( 'admin_css_foo', get_template_directory_uri() . '/css/admin-style.css', false, '1.0.0' );
      }  
function adminJS( $hook ) {
    wp_enqueue_script( 'admin_js',  '/wp-content/themes/idvetmp/js/admin.js' );
}

add_action('admin_footer', 'adminJS');


if( function_exists('acf_add_options_page') ) {	
    
    
    global $current_user;
    
	acf_add_options_page(array(
		'page_title' 	=> 'Ustawienia szablonu',
		'menu_title'	=> 'Ustawienia szablonu',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
        
        
}

function custom_loginlogo() {
	echo '<style type="text/css">
h1 a {background-image: url('.get_bloginfo('template_directory').'/img/logo2.png) !important;
 width: 200px;}
</style>';
}
add_action('login_head', 'custom_loginlogo');


add_image_size('usluga-max-bg', 352, 352, true );
add_image_size('flat-bg', 585, 385, false);
add_image_size('inwestycje-img', 451, 451, true );
add_image_size('aktualnosc-img', 460, 306, true );
add_image_size('mapa-img', 1182, 607, true );
add_image_size('slider-photo', 844, 475, true );



function my_acf_init() {
	acf_update_setting('google_api_key', 'AIzaSyDr2cyBjdMRnXIk7B2LdABRhMnpkVBgPlI');
}
add_action('acf/init', 'my_acf_init');

//dodanie własnych guzików 
add_action('admin_head', 'add_my_tc_button');

function add_my_tc_button() {
    global $typenow;
    
    if ( !current_user_can('edit_posts') && !current_user_can('edit_pages') ) {
    return;
    }
    
    
    if ( get_user_option('rich_editing') == 'true') {
        add_filter("mce_external_plugins", "add_tinymce_plugin");
        add_filter('mce_buttons', 'register_my_tc_button');
    }
}

function add_tinymce_plugin($plugin_array) {
    $plugin_array['gavickpro_tc_button'] =  '/wp-content/themes/idvetmp/js/tinymce-buttons.js';
    return $plugin_array;
}

function register_my_tc_button($buttons) {
   array_push($buttons, "gavickpro_tc_button");
   return $buttons;
}



// generowanie styli


remove_action('wp_head', 'wp_generator');

function my_secure_generator( $generator, $type ) {
	return '';
}
add_filter( 'the_generator', 'my_secure_generator', 10, 2 );

function my_remove_src_version( $src ) {
	global $wp_version;

	$version_str = '?ver='.$wp_version;
	$offset = strlen( $src ) - strlen( $version_str );

	if ( $offset >= 0 && strpos($src, $version_str, $offset) !== FALSE )
		return substr( $src, 0, $offset );

	return $src;
}
add_filter( 'script_loader_src', 'my_remove_src_version' );
add_filter( 'style_loader_src', 'my_remove_src_version' );


// function custom_title($title_parts) {
    
//     global $post;
//     $fields = get_fields($post->ID);
    
//     if($fields['seo_title'] && !empty($fields['seo_title'])){
//         $title_parts['title'] = $fields['seo_title'];
//     }
//     return $title_parts;
// }
// add_filter( 'document_title_parts', 'custom_title' );


// function custom_description() {
//     global $post;
//     $fields = get_fields($post->ID);
//     if($fields['seo_description'] && !empty($fields['seo_description'])){
//        $description = '<meta name="description" content="'.$fields['seo_description'].'">';
//     }
//     echo $description;
// }
// add_action('wp_head','custom_description');



add_filter( 'wp_nav_menu_items', 'wti_loginout_menu_link', 10, 2 );

function wti_loginout_menu_link( $items, $args ) {
	if ($args->theme_location == 'MenuLogin') {
		if (is_user_logged_in()) {
			$items .= '<li><a href="'. wp_logout_url() .'">Wyloguj</a></li>';
		} else {
			$items .= '<li><a href="'. wp_login_url(get_permalink()) .'">Log In</a></li>';
		}
	}
	return $items;
}

function wpbeginner_numeric_posts_nav() {

	if( is_singular() )
		return;

	global $wp_query;

	/** Stop execution if there's only 1 page */
	if( $wp_query->max_num_pages <= 1 )
		return;

	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );

	/**	Add current page to the array */
	if ( $paged >= 1 )
		$links[] = $paged;

	/**	Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	echo '<div class="navigation"><ul>' . "\n";

	/**	Previous Post Link */
	if ( get_previous_posts_link() )
		printf( '<li>%s</li>' . "\n", get_previous_posts_link() );

	/**	Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? ' class="active"' : '';

		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

		if ( ! in_array( 2, $links ) )
			echo '<li>…</li>';
	}

	/**	Link to current page, plus 2 pages in either direction if necessary */
	sort( $links );
	foreach ( (array) $links as $link ) {
		$class = $paged == $link ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
	}

	/**	Link to last page, plus ellipses if necessary */
	if ( ! in_array( $max, $links ) ) {
		if ( ! in_array( $max - 1, $links ) )
			echo '<li>…</li>' . "\n";

		$class = $paged == $max ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
	}

	/**	Next Post Link */
	if ( get_next_posts_link() )
		printf( '<li>%s</li>' . "\n", get_next_posts_link() );

	echo '</ul></div>' . "\n";

}

/**
 * Redirect user after successful login.
 *
 * @param string $redirect_to URL to redirect to.
 * @param string $request URL the user is coming from.
 * @param object $user Logged user's data.
 * @return string
 */
function my_login_redirect( $redirect_to, $request, $user ) {
	//is there a user to check?
	if ( isset( $user->roles ) && is_array( $user->roles ) ) {
		//check for admins
		if ( in_array( 'administrator', $user->roles ) ) {
			// redirect them to the default place
			return $redirect_to;
		} else {
			return home_url('/komunikaty/');
		}
	} else {
		return $redirect_to;
	}
}

add_filter( 'login_redirect', 'my_login_redirect', 10, 3 );
add_filter('show_admin_bar', '__return_false');


add_action('admin_menu','yoursite_admin_menu');
function yoursite_admin_menu() {
	global $submenu;
	// This needs to be set to the URL for the admin menu section (aka "Menu Page")
	$menu_page = 'tools.php';
	// This needs to be set to the URL for the admin menu option to remove (aka "Submenu Page")
	$taxonomy_admin_page = 'tools.php?page=bbp-repair';
	// This removes the menu option but doesn't disable the taxonomy
	foreach($submenu[$menu_page] as $index => $submenu_item) {
		if ($submenu_item[2]==$taxonomy_admin_page) {
			unset($submenu[$menu_page][$index]);
		}
	}
}
//add_action('init','yoursite_init');


function the_excerpt_max_charlength($charlength) {
    $excerpt = get_the_excerpt();
    $charlength++;

    if ( mb_strlen( $excerpt ) > $charlength ) {
        $subex = mb_substr( $excerpt, 0, $charlength - 5 );
        $exwords = explode( ' ', $subex );
        $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
        if ( $excut < 0 ) {
            echo mb_substr( $subex, 0, $excut );
        } else {
            echo $subex;
        }
        echo '[...]';
    } else {
        echo $excerpt;
    }
}

?>
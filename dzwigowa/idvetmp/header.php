<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package idvetmp
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,900&subset=latin-ext" rel="stylesheet">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/img/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/img/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/img/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/img/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/img/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/img/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/img/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/img/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/img/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri(); ?>/img/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri(); ?>/img/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/img/favicon-16x16.png">
    <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/img/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/img/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">




<?php wp_head(); ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/magnific-popup.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700|Roboto:300,400,700&amp;subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/animate.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap-multiselect.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/jPages.css">
    <link rel="stylesheet" href="https://refreshless.com/noUiSlider/distribute/nouislider.css?v=1000">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

</head>

<body <?php body_class(); ?>>

<div class="container-effect">
    <ul><li><span class="inner"></span></li><li><span class="inner"></span></li><li><span class="inner"></span></li><li><span class="inner"></span></li><li><span class="inner"></span></li><li><span class="inner"></span></li><li><span class="inner"></span></li><li><span class="inner"></span></li><li><span class="inner"></span></li><li><span class="inner"></span></li></ul>
</div>

<nav class="navbar navbar-default nav-down navbar-fixed-top del">
    <div class="container-fluid">
        <div class="row">
            <div class="her1 full-size">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="<?php echo get_template_directory_uri(); ?>/img/logo_1.svg" alt=""></a>
                </div>
            </div>
            <div class="her2">
                <div id="navbar" class="navbar-collapse collapse">

                    <ul class="strefa strefa-show">
                        <li><a href="tel:<?php the_field('telefon1', 'option'); ?>"><i class="fa fa-phone" aria-hidden="true"></i>+48 <?php the_field('telefon1', 'option'); ?></a></li>
                        <li><a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i><?php the_field('tekst_pod_telefonem_nr_1', 'option'); ?></a></li>
                    </ul>
                    <div class="wrapper-menu" id="menu-menu-gorne">
                        <ul class="strefa" style="display: none;">
                            <li><a href="tel:<?php the_field('telefon1', 'option'); ?>"><i class="fa fa-phone" aria-hidden="true"></i>+48 <?php the_field('telefon1', 'option'); ?></a></li>
                            <li><a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>+48 <?php the_field('tekst_pod_telefonem_nr_1', 'option'); ?></a></li>
                        </ul>

                        <?php
                        wp_nav_menu(array(
                                'menu' => 'Menu Górne',
                                'theme_location' => 'primary',
                                'depth' => 3,
                                'container' => false,
                                'container_class' => 'collapse navbar-collapse',
                                'menu_class' => 'nav navbar-nav',
                                'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
                                'walker' => new wp_bootstrap_navwalker())
                        );
                        ?>

                        <a class="facebook" href="<?php the_field('facebook_link', 'option'); ?>" target="_blank"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                        <a class="g-plus" href="<?php the_field('google+_-_link', 'option'); ?>" target="_blank"><i class="fa fa-google-plus-official" aria-hidden="true"></i></a>

                    </div>

                </div><!--/.nav-collapse -->
            </div>
        </div>



    </div><!--/.container-fluid -->

</nav>


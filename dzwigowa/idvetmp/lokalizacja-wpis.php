<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package idvetmp
 *
 */
/*
Template Name: Lokalizacja Tekst
*/
get_header();
the_post();
$fields = get_fields(get_the_ID());

?>
<div class="main-container">
    <div class="lokalizacja lokalizacja-wpis">
        <div class="container separate-class">
            <div class="row">
                <div class="col-md-12">
                    <div class="bg-nav-photo" style="background: url(<?= $fields['zdjecie_-_naglowek']?>)">

                    </div>
                    <div class="page-wrapper">
                        <div class="small-wrapper">
                            <div class="page-name">
                                <img src='<?php echo get_template_directory_uri(); ?>/img/logo-small-white.svg' class="page-logo">
                                <h6><?php the_title(); ?></h6>
                            </div>
                            <div class="page-desc">
                                <?= $fields ['tekst'] ?>
                                </div>
                            <div class="page-photo">
                                <img src='<?= $fields['zdjecie_prawa_strona']?>'>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <?php
        get_template_part('kontaktBox');
        ?>
    </div>
</div>
<?php
get_footer();

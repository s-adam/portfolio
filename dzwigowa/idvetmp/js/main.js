function initMap() {


    var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: lat, lng: lng},
        scrollwheel: false,
        zoom: 16,
        styles:
            [
                {
                    "featureType": "all",
                    "elementType": "all",
                    "stylers": [
                        {
                            "lightness": "29"
                        },
                        {
                            "invert_lightness": true
                        },
                        {
                            "hue": "#008fff"
                        },
                        {
                            "saturation": "-73"
                        }
                    ]
                },
                {
                    "featureType": "all",
                    "elementType": "labels",
                    "stylers": [
                        {
                            "saturation": "-72"
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "all",
                    "stylers": [
                        {
                            "lightness": "32"
                        },
                        {
                            "weight": "0.42"
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "labels",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "lightness": "-53"
                        },
                        {
                            "saturation": "-66"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "all",
                    "stylers": [
                        {
                            "lightness": "-86"
                        },
                        {
                            "gamma": "1.13"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "hue": "#006dff"
                        },
                        {
                            "lightness": "4"
                        },
                        {
                            "gamma": "1.44"
                        },
                        {
                            "saturation": "-67"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "lightness": "5"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "weight": "0.84"
                        },
                        {
                            "gamma": "0.5"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "labels.text.stroke",
                    "stylers": [
                        {
                            "visibility": "off"
                        },
                        {
                            "weight": "0.79"
                        },
                        {
                            "gamma": "0.5"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "simplified"
                        },
                        {
                            "lightness": "-78"
                        },
                        {
                            "saturation": "-91"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "labels.text",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        },
                        {
                            "lightness": "-69"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "lightness": "5"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "lightness": "10"
                        },
                        {
                            "gamma": "1"
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "lightness": "10"
                        },
                        {
                            "saturation": "-100"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "all",
                    "stylers": [
                        {
                            "lightness": "-35"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "labels.text.stroke",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": "-97"
                        },
                        {
                            "lightness": "-14"
                        }
                    ]
                }
            ]
    });

    var marker = new google.maps.Marker({
        position: {lat: lat, lng: lng},
        map: map,
        icon: '/wp-content/themes/idvetmp/img/marker.png'

    });
}

var time = 100;
$('#menu-menu-gornes').find('.menu-item').children().each(function(index){
    var $this  = $(this);
    var count = $('#menu-menu-gorne').find('.menu-item').size();
    function delayed() {
        $this.addClass("animated fadeInUp");

        c = index + 1;
        if(c == count){
            startPosition = $('.navbar-nav .active');
            p = startPosition.offset();
            $('.menu-hover').css('left',p.left);

            setTimeout(
                function()
                {
                    $('.menu-hover').css('opacity',1);
                }, 1000);
        }
    }
    setTimeout( delayed , time );
    time += 100;
});

$(document).ready(function () {

    $('.home .her1').addClass('col-md-2');
    $('.home .her2').addClass('col-md-10');
    if (screen.width > 1) {

        $('.anim').viewportChecker();

    }else{
    }

});

// $(".multiselect-native-select").text("Your text here");
// $( ".invest-numbers .text" ).append( "dowolny" ).last();
// $( ".invest-numbers .text" ).prepend( "dowolnys" );
// $("#tagscloud span").text("Your text here");
$(document).ready(function () {

    jQuery(window).scroll(function () {
        var scrollPos = jQuery(window).scrollTop();
        if (scrollPos > 1){
            // $('.navbar').addClass('scrolled-nav');
            $('.nav-down').css('background','rgba(255,255,255,0.9)');
            $('.home .nav-down').css('background','transparent');
        }
        if (scrollPos > 900){
            $('.select-section').addClass('select-fixed');
            $('.mieszkania .grey-line').css('margin-bottom','30px');
        }

        else
        {
            if ($( window ).width() > 767){
                    $('.select-section').removeClass('select-fixed');

            }
        }
    })

});
// $('.wrapper1').addClass('litera');
$(document).ready(function () {

    $('.lista').viewportChecker({

        callbackFunction: function(elem, action){

            setTimeout(
                function()
                {
                    $('.timer').countTo();
                }, 500);
        }
    });
});



setTimeout(function () {
    $('.one').show().addClass('animated fadeInUp');}, 300
);
setTimeout(function () {
    $('.two').show().addClass('animated fadeInUp');}, 600
);
setTimeout(function () {
    $('.tri').show().css('opacity','1'); }, 1200
);

$(".szukaj-fixed").click(function() {

    $(".select-section").toggleClass('select-block');
    $(".select-section").addClass('off');
    $('.select-block').css('z-index','999999999999');
    $('html, body').animate({
        scrollTop: $(".flat-background").offset().top - 100
    }, 1000);
});
$(".szukaj-close").click(function() {
    $(".select-block").toggleClass('select-block');
    $(".select-section").toggleClass('off');
});
$(".x").click(function() {
    $(".select-block").toggleClass('select-block');
    $(".select-section").toggleClass('off');
});
// $(document).on('click', '.show-search', function(e) {
//     $(".select-fixed").trigger('click');
//     $('.select-fixed').addClass('select-block');
//     $('.select-fixed').css('position','fixed');
// });
$(".fig1").click(function() {
    $(".literaC").toggleClass('accept');
    $(".multiselect-container li.c .checkbox").trigger('click');
    $('html, body').animate({
        scrollTop: $(".grey-line").offset().top - 100
    }, 1000);
});

$(".fig2").click(function() {
    $(".literaB").toggleClass('accept');
    $(".multiselect-container li.b .checkbox").trigger('click');
    $('html, body').animate({
        scrollTop: $(".grey-line").offset().top - 100
    }, 1000);
});

$(".fig3").click(function() {
    $(".literaA").toggleClass('accept');
    $(".multiselect-container li.a .checkbox").trigger('click');
    $('html, body').animate({
        scrollTop: $(".grey-line").offset().top - 100
    }, 1000);
});
$(".literaA").click(function() {
    $(".literaA").toggleClass('accept');
    $(".multiselect-container li.a .checkbox").trigger('click');
    $('html, body').animate({
        scrollTop: $(".grey-line").offset().top - 100
    }, 1000);
});
$(".literaB").click(function() {
    $(".literaB").toggleClass('accept');
    $(".multiselect-container li.b .checkbox").trigger('click');
    $('html, body').animate({
        scrollTop: $(".grey-line").offset().top - 100
    }, 1000);
});
$(".literaC").click(function() {
    $(".literaC").toggleClass('accept');
    $(".multiselect-container li.c .checkbox").trigger('click');
    $('html, body').animate({
        scrollTop: $(".grey-line").offset().top - 100
    }, 1000);
});


$(".navbar-toggle").click(function() {
    $('.navbar .container-fluid').toggleClass('foo');
    // $('.nav-down').toggleClass('nav-down-relative');
});

$(document).ready(function(){

    $(".owl-carousel").owlCarousel({
        nav: true,
        rewindNav : true,
        loop: true,
        navText: ["<img src='/wp-content/themes/idvetmp/img/arrow-left.svg'>","<img src='/wp-content/themes/idvetmp/img/arrow-left.svg'>"],
        responsive:{
            320:{
                items:1,
                center: false
            },

            768:{
                items:1,
                center: false
            },

            960:{
                items:1,
                center: false
//                    center: true
            },
            1200:{
                items:1,
                center: false
//                    center: true
            }
        },
        onChange : owlpagination
    });


    $('.container-effect .inner').addClass('inner-open');

    setTimeout(
        function()
        {
            $('.container-effect').hide();
        }, 1000);

});

function owlpagination(event){
    var totalItems = event.item.count;
    var ff = event.page.index;
    var currentIndex = ff + 1;

    $('#first-l').html(currentIndex+1);
    $('#second-l').html('/'+totalItems);

}

$( ".fig1" ).hover(
    function() {
        $( '.literaC' ).addClass( "hover-btn" );
    }, function() {
        $( '.literaC' ).removeClass( "hover-btn" );
    }
);

$( ".fig2" ).hover(
    function() {
        $( '.literaB' ).addClass( "hover-btn" );
    }, function() {
        $( '.literaB' ).removeClass( "hover-btn" );
    }
);

$( ".fig3" ).hover(
    function() {
        $( '.literaA' ).addClass( "hover-btn" );
    }, function() {
        $( '.literaA' ).removeClass( "hover-btn" );
    }
);

$(document).ready(function(){
    $(".hamburger").click(function(){
        $(this).toggleClass("is-active");
    });
});
var supports3DTransforms =  document.body.style['webkitPerspective'] !== undefined ||
    document.body.style['MozPerspective'] !== undefined;

function linkify( selector ) {
    if( supports3DTransforms ) {

        var nodes = document.querySelectorAll( selector );

        for( var i = 0, len = nodes.length; i < len; i++ ) {
            var node = nodes[i];

            if( !node.className || !node.className.match( /roll/g ) ) {
                node.className += ' cl-effect-4';
            }
        };
    }
}

linkify( '.menu-item' );


$(document).ready(function(){
    $(".navbar-toggle").click(function(){
        $(this).toggleClass("is-active");
    });
});
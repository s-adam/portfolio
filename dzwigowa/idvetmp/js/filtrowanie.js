var skipSlider = document.getElementById('slid');
var select = document.getElementById('slid');

// Append the option elements
for ( var i = -20; i <= 40; i++ ){

    var option = document.createElement("option");
    option.text = i;
    option.value = i;

}

noUiSlider.create(skipSlider, {
    range: {
        'min': 0,
        'max': 100
    },
    step: 1,
    decimal: 0,
    start: [20, 100]
});


var skipValues = [
    document.getElementById('min-val'),
    document.getElementById('max-val')
];

skipSlider.noUiSlider.on('update', function( values, handle ) {
    skipValues[handle].innerHTML = values[handle];

});


skipSlider.noUiSlider.on('end', function( values, handle ) {
    sortRooms();
});



$(document).ready(function() {
    $('#chkveg').multiselect();
    $('#rooms').multiselect();
    $('#sektors').multiselect();
});


//Paginacja


//Filtrowanie

var rooms = null;
var floor = null;
var pages = 0;


$('#chkveg').on('change', function() {
    floor = this.value;
    sort();
});

function sort(){

    var floor = $('#chkveg').val();

    floorArray = [];

    $( floor ).each(function( index, val ) {
        floorArray.push(parseInt(val))
    });

    $( ".each-flat" ).each(function( index ) {
        showRow = true;
        if(floorArray > 0 && showRow){

            if(jQuery.inArray(parseInt($(this).data('floor')),floorArray) < 0){
                showRow = false;
            }
        }

        if(showRow){
            $(this).fadeIn(400);
        }else{
            $(this).fadeOut(400);
        }
    });



}

function availableApartments(){
    $('#switchName').click(function(){
        if(this.checked){
            THIS.available = true;
        }else{
            THIS.available = false;
        }


    });
}


var page = 1;
var apartamentPerPage = 2;
var iP = 0;
function sortRooms(){
    var floor = $('#chkveg').val();
    var rooms = $('#rooms').val();
    var sektors = $('#sektors').val();
    j = 0;
    page = 1;
    iP = 0;
    roomArray = [];
    floorArray = [];
    sektorArray = [];

    surfaceFrom = skipSlider.noUiSlider.get()[0];
    surfaceTo = skipSlider.noUiSlider.get()[1];



    $( rooms ).each(function( index, val ) {
        roomArray.push(parseInt(val))
    });
    $( floor ).each(function( index, val ) {
        floorArray.push(parseInt(val))
    });
    $( sektors ).each(function( index, val ) {
        sektorArray.push(val)
    });

    $( ".each-flat" ).each(function( index ) {
        showRow = true;

        if(roomArray > 0 && showRow){
            if(jQuery.inArray(parseInt($(this).data('rooms')),roomArray) < 0){
                showRow = false;
            }
        }

        if(sektorArray.length > 0 && showRow){

            if(jQuery.inArray($(this).data('segment'),sektorArray) < 0){
                showRow = false;
            }
        }
        if(floorArray > 0 && showRow){

            if(jQuery.inArray(parseInt($(this).data('floor')),floorArray) < 0){
                showRow = false;
            }
        }

        if($('#switchName').prop( "checked" ) == false){
            if($(this).data('avaliable') != 'wolny'){
                showRow = false;
            }
        }

        if($(this).data('surface') < surfaceFrom){
            showRow = false;
        }
        if($(this).data('surface') > surfaceTo){
            showRow = false;
        }

        if (iP == 0){
            $('.pagination').hide();
            $('.apartament-empty').fadeIn(400);
        }

        if(showRow){
            j++;
            iP++;
            $(this).attr('data-page', page);
            if(j == apartamentPerPage){
                page++;
                j=0;
            }

        }else{
            $(this).fadeOut(1);
            $(this).attr('data-page', '9999');
        }
    });


    pages = Math.ceil(iP / apartamentPerPage);

    pagination();


    $('.page-count').html(pages);

}


function pagination(status = ''){
    page = parseInt($('.pagination').attr('data-page'));


    if(status == 'prev'){
        currentPage = page - 1;
        if(currentPage < 1){
            return;
        }
    }else if(status == 'next'){
        currentPage = page + 1;
        if(currentPage > pages){
            return;
        }
    }
    else{
        currentPage = page;
        if(currentPage > pages){
            return;
        }
    }


    $('.pagination-main').html('');
    for (i = 1; i <= pages; i++) {

        c = '';
        if(currentPage == i) {
            c = 'active';
        }
        $('.pagination-main').append('<span class="paginationPage '+c+'" data-paginationPage="'+i+'">'+i+'</span>');

        if(currentPage == 1) {
            $('#prev-page').hide();
        }
        else{
            $('#prev-page').show();
        }
        if(currentPage == pages) {
            $('#next-page').hide();
        }else{
            $('#next-page').show()
        }


    }



    $('.current-page').html(currentPage);

    $('.pagination').attr('data-page',currentPage);

    $( ".each-flat" ).fadeOut(1);

    $( ".each-flat" ).each(function( index ) {
        if($(this).attr('data-page') == currentPage){
            $(this).fadeIn(400);
        }
    });

}


$(function() {

    $('#prev-page').click(function(){
        pagination('prev');

        $('html, body').animate({
            scrollTop: $(".flat-background").offset().top - 100
        }, 1000);
    });

    $('#next-page').click(function(){
        pagination('next');
        $('html, body').animate({
            scrollTop: $(".flat-background").offset().top - 100
        }, 1000);
    });



    $('#rooms').multiselect({

        includeSelectAllOption: true

    });

    $('#sektors').multiselect({

        includeSelectAllOption: true

    });

    $('#chkveg').multiselect({

        includeSelectAllOption: true

    });
    $('#rooms').change(function() {
        sortRooms();
    })
    $('#sektors').change(function() {
        sortRooms();
    })
    $('#chkveg').change(function() {
        sortRooms();
    })
    $('#switchName').change(function() {
        //availableApartments();
        sortRooms();
    });

    sortRooms();

});

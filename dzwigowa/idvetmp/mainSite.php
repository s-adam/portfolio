<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package idvetmp
 *
 */
/*
Template Name: Strona Główna
*/
get_header();
the_post();
$fields = get_fields(get_the_ID());

?>

<div class="main-container">

    <div class="strona-glowna">
        <div class="container-fluid">
            <div class="row">
                <div class="section-main">
                    <div class="mar">
<!--                        <div class="col-md-2">-->
<!--                            <div class="left-site">-->
<!---->
<!--                            </div>-->
<!--                        </div>-->
                        <div class="col-md-6">
                            <div class="right-site">
                                <div class="site-wrapper">
                                    <div class="main-title">
                                        <h6 class="anim one" data-vp-add-class="animated fadeIn"><?= $fields ['sekcja_glowna_-_szary_napis'] ?></h6>
                                        <h1 class="anim two" style="display:none"><?= $fields ['sekcja_glowna_-_duzy_tekst'] ?></h1>
                                        <?= $fields ['przycisk'] ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-site-logo-brand">
            <div class="social-icons">
                <a class="facebook" href="<?php the_field('facebook_link', 'option'); ?>" target="_blank"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                <a class="g-plus" href="<?php the_field('google+_-_link', 'option'); ?>" target="_blank"><i class="fa fa-google-plus-official" aria-hidden="true"></i></a>
            </div>
            <a href="https://ideative.pl/" target="_blank" class="logo-idv"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-png.png" alt=""></a>
        </div>
    </div>

</div>



<?php get_footer(); ?>



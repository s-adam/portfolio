<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package idvetmp
 */
get_header();
the_post();
$fields = get_fields(get_the_ID());

?>
<div class="main-container">
    <div class="kariera">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="special-title special-title-archive">
                        
                    </div>
                    <div class="kariera-wrapper aktualnosci-wrapper">
                        <div class="kariera-each-wrapper aktualnosc-wrapper">
                            <p class="post-title"><strong><?php the_title(); ?></strong></p>
                            <p class="date"><?php get_the_date(); ?></p>
                            <div class="text-wrapper">
                            <?php the_content(); ?>
                            </div>
                        </div>
                        

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();

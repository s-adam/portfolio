<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package idvetmp
 *
 */
/*
Template Name: Kontakt
*/
get_header();
the_post();
$fields = get_fields(get_the_ID());

?>
<div class="page-map">
    <div class="main-container page-map">
        <div class="lokalizacja lokalizacja-wpis">
            <div class="container mb">
                <div class="row mt">
                    <div class="col-md-12">
                        <div class="bg-nav-photo" id="map">

                        </div>
                        <div class="page-wrapper">
                            <div class="small-wrapper kontakt-small-wrapper">
                                <div class="page-name">

                                </div>
                                <div class="page-desc kontakt-page-desc">
                                    <?= $fields ['sekcja_naglowek'] ?>

                                    <div class="row" id="contact-wrapper">
                                        <div class="col-md-5 col-sm-12">
                                            <?php the_content(); ?>
                                        </div>
                                        <div class="col-md-7 col-sm-12">
                                            <div class="contact-info">
                                                <span class="square"></span>

                                                <ul>
                                                    <li>+48<span><a href="tel:<?php the_field('telefon1', 'option'); ?>"><?php the_field('telefon1', 'option'); ?></a></span></li>
                                                    <li><a href="mailto:<?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a></li>
                                                </ul>
                                                <?= $fields ['sekcja_-_informacje_kontaktowe'] ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="page-photo kontakt-page-photo hidden-xs hidden-sm">
                                    <img src='<?php echo get_template_directory_uri(); ?>/img/zdjss.jpg'>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <?php
            get_template_part('kontaktBox');
            ?>
        </div>
    </div>
</div>

    <script>

        var lat =  <?=$fields ['google_maps']['lat']?>;
        var lng =  <?=$fields ['google_maps']['lng']?>;

    </script>
<?php
get_footer();

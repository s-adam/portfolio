<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package idvetmp
 */
?>
<footer class="footer-show">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                    <ul>
                        <?php while( have_rows('logotypy_firm', 'option') ): the_row(); ?>
                            <li><a href="<?php the_sub_field('adres_www'); ?>" target="_blank"><img src="<?php the_sub_field('logo'); ?>"></a></li>
                        <?php endwhile; ?>
                    </ul>

            </div>
            <div class="col-md-4">
                <p class="text-center">Wszelkie prawa zastrzeżone &copy <?php the_time('Y')?> Belvedere</p>
            </div>
            <div class="col-md-4">
                <a href="https://ideative.pl/" target="_blank" class="logo-idv"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-png.png" alt=""></a>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script type="text/javascript" src="https://refreshless.com/noUiSlider/distribute/nouislider.js?v=1000"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.viewportchecker.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.countTo.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/bootstrap-multiselect.js"></script>

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap-multiselect.css" type="text/css"/>
<!--<script src="//code.jquery.com/jquery-1.10.2.js"></script>-->
<script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>

    <script src="<?php echo get_template_directory_uri(); ?>/js/filtrowanie.js"></script>



<script src="<?php echo get_template_directory_uri(); ?>/js/modernizr.custom.js"></script>

<?php if(get_the_ID() == '1061') {
    ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDr2cyBjdMRnXIk7B2LdABRhMnpkVBgPlI&callback=initMap"
            async defer></script>
    <?php
}
?>
</body>
</html>
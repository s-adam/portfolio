<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package idvetmp
 *
 */
/*
Template Name: Lokalizacja
*/
get_header();
the_post();
$fields = get_fields(get_the_ID());

?>
<div class="main-container">
    
    <div class="main-photo">
        <div class="container">
            <div class="row mt">
                <div class="col-md-12">
                    <div class="bg-nav-photo" style="background: url(<?= $fields['zdjecie_-_naglowek']?>)">

                    </div>
                </div>    
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="after-main-photo">

                </div>
            </div>
        </div>
    </div>

    
    <div class="lokalizacja lokalizacja-mampa">
        
        <div class="container ">
            <div class="row">
                <div class="col-md-9">
                    <div class="main-photo-text">
                        <div>
                            <img src='<?php echo get_template_directory_uri(); ?>/img/logo-small-white.svg' class="page-logo">
                            <h6><?php the_title(); ?></h6>
                        </div>
                        <div class="lokalizacja-oryginal">
                            <div class="page-desc">
                                <?= $fields ['tekst'] ?>
                                </div>
                        </div>
                    </div>    
                </div>
            </div>
        </div>
        
        <div class="container mb">
            <div class="row">
                <div class="col-md-12">

                    <div class="page-wrapper white-map">

                        <div class="page-map small-map-hide">
                            <img src="<?php echo $fields['mapa_-_zdjecie']['sizes']['mapa-img']; ?>" alt="">
                        </div>
                        <div class="page-map small-map-show">
                            <img src='<?php echo get_template_directory_uri(); ?>/img/small-map.jpg'>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        get_template_part('kontaktBox');
        ?>
    </div>
</div>
<?php
get_footer();

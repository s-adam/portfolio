<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package idvetmp
 *
 */
/*
Template Name: Inwestycja
*/
get_header();
the_post();
$fields = get_fields(get_the_ID());

?>



<div class="main-container">
    <div class="lokalizacja lokalizacja-wpis inwestycja">
        <div class="container pb">
            <div class="mt">
                <div class="row">
                    <div class="col-md-12">
                        <div class="bg-nav-photo" style="background: url(<?= $fields['zdjecie_-_naglowek']?>);">

                        </div>
                        <div class="white-bg">
                            <div class="page-wrapper">
                                <div class="small-wrapper">
                                    <div class="page-name">
                                        <img src='<?php echo get_template_directory_uri(); ?>/img/logo-small-white.svg' class="page-logo">
                                        <h6><?php the_title(); ?></h6>
                                    </div>
                                    <div class="page-desc">
                                        <?= $fields ['tekst'] ?>
                                        <div class="sliders">
                                            <div class="owl-carousel text-center">
                                            <?php
                                            $images = $fields['zdjecia_-_slider'];
                                            if($images){

                                                foreach ($images as $field) {
                                                    ?>

                                                    <img class="text-center" src="<?= $field['sizes']['slider-photo']  ?>" alt="">
                                                    <?php
                                                }
                                            }

                                            ?>

                                            </div>
                                            <div class="num"><span id="first-l"></span><span id="second-l"></span></div>
                                            <div class="under-slider"><img src='<?php echo get_template_directory_uri(); ?>/img/under_slider.jpg'></div>
                                        </div>
                                    </div>
                                    <div class="white-bg page-photo invest-numbers">
                                        <ul>

                                        <?php

                                        if($fields['liczby']){

                                            foreach ($fields['liczby'] as $tym) {
                                                ?>
                                            <li class="li-s"><div class="lista"><span class="numbers timer" data-from="0" data-to="<?= $tym ['liczba'] ?>" data-speed="1500"><strong></strong></span></br><span class="text">mieszkań</span></div> </li>
                                            <li ><div class="lista"><span class="numbers timer" data-from="0" data-to="<?= $tym ['miejsc_postojowych'] ?>" data-speed="1500"><strong></strong></span></br><span class="text">miejsc postojowych</span></div> </li>
                                            <?php

                                            }
                                        }
                                        ?>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                            <div class="short-desc">
                                <?= $fields ['tekst_-_sekcja_druga'] ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="single-box white-bg">

                    <?php

                    if($fields['boxy_-_sekcja_trzecia']){
                        $i = 0;
                        $j = 0;
                        foreach ($fields['boxy_-_sekcja_trzecia'] as $tym) {
                            if ($i == 0) {
                                echo '<div class="row">';
                            }
                            ?>
                        <div class="col-md-6">
                            <div class="single-wrapper">
                                <?= $tym ['tekst'] ?>
                                <img src="<?php echo $tym['zdjecie']['sizes']['inwestycje-img']; ?> " alt="">
                            </div>
                        </div>

                            <?php
                            $i++;
                            $j++;
                            if ($i == 2 || count($fields['boxy_-_sekcja_trzecia']) == $j) {

                                echo '</div>';

                                $i = 0;
                            }
                        }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>

    <?php
        get_template_part('kontaktBox');
        ?>
    </div>
<!--</div>-->
<div class="mt50"></div>
<?php
get_footer();

<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package idvetmp
 *
 */

get_header();
the_post();
$fields = get_fields(get_the_ID());
$mieszkania = get_posts(
    array(
        'posts_per_page' => -1,
        'post_type' => 'mieszkania',
        'order' => 'ASC',
        'orderby' => 'menu_order'
    )

);

$countApartaments = 0;
$countApartamentsFree = 0;
if ($mieszkania) {
    foreach ($mieszkania as $mieszkanie) {
        $fields = get_fields($mieszkanie->ID);

        if($mieszkanie->status == 'wolny'){
            $countApartamentsFree++;
        }
        $countApartaments++;
    }
}
?>
<div>
    <div class="main-container">
        <div class="mieszkania">
            <div class="container separate-class">
                <div class="map-section">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="wrapp">
                        <span class="square">
                        <div class="square-wrapper">
                            <span class="numbers"><?=$countApartamentsFree?></span>
                        </div>
                    </span>
                                <span class="mieszkania-square-text">jeszcze dostępnych mieszkań</span>
                            </div>
                            <div class="short-text-flat">
                                <?php the_field('mieszkania_-_tekst', 'option'); ?>
                            </div>
                            <span class="grey-line"></span>
                        </div>
                        <div class="col-md-7">
                            <div class="flats-map">
                                <img class="fig-map" src="<?php echo get_template_directory_uri(); ?>/img/mieszkania-schemat.jpg" alt="">
                                <div class="wrapper1"><span class="litera literaC">C</span><img class="fig1" src="<?php echo get_template_directory_uri(); ?>/img/bud-c.svg" alt=""></div>
                                <div class="wrapper1"><span class="litera literaB">B</span><img class="fig2" src="<?php echo get_template_directory_uri(); ?>/img/bud-b.svg" alt=""></div>
                                <div class="wrapper1"><span class="litera literaA">A</span><img class="fig3" src="<?php echo get_template_directory_uri(); ?>/img/bud-a.svg" alt=""></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="select-wrapper-outer">
                            <div class="select-section text-center margin-center">
                                <div class="x"></div>
                                <div class="col-md-2 col-md-offset-1">
                                    <div class="simple-item">
                                        <p>TYLKO DOSTĘPNE</p>
                                        <label class="switch" for="switchName">
                                            <input type="checkbox" id="switchName" name="switchName" value="switchValue">
                                            <span class="slider round"></span>
                                        </label>

                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="simple-item">
                                        <p>ILOŚC POKOI</p>
                                        <select class="rooms" id="rooms" multiple="multiple">
                                            <option data-value="dowolne">dowolne</option>
                                            <option data-value="1" value="1">1</option>
                                            <option data-value="2" value="2">2</option>
                                            <option data-value="3" value="3">3</option>
                                            <option data-value="4" value="4">4</option>
                                            <option data-value="5" value="5">5</option>
                                            <option data-value="6" value="6">6</option>
                                            <option data-value="7" value="7">7</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="simple-item">
                                        <p>PIĘTRO</p>
                                        <select id="chkveg" multiple="multiple">
                                            <option data-value="dowolne">dowolne</option>
                                            <option data-value="0" value="0">0</option>
                                            <option data-value="1" value="1">1</option>
                                            <option data-value="2" value="2">2</option>
                                            <option data-value="3" value="3">3</option>
                                            <option data-value="4" value="4">4</option>
                                            <option data-value="5" value="5">5</option>
                                            <option data-value="6" value="6">6</option>
                                            <option data-value="7" value="7">7</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="simple-item">
                                        <p>METRAŻ</p>
                                        <span id="min-val"> - </span>
                                        <span id="max-val"></span>
                                        <div id="slid">

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="simple-item">
                                        <p>BUDYNEK</p>
                                        <select class="sektors" id="sektors" multiple="multiple">
                                            <option data-value="dowolne">dowolne</option>
                                            <option class="a" data-value="a" value="a">A</option>
                                            <option class="b" data-value="b" value="b">B</option>
                                            <option class="c" data-value="c" value="c">C</option>
                                        </select>
                                        </br>

                                        <span class="szukaj-close">Szukaj</span>
                                    </div>


                                </div>
                            </div>

                        </div>
                        <span class="szukaj szukaj-fixed">Szukaj</span>
                    </div>
                </div>
                <div class="row apartament-empty text-center">
                    <div class="col-md-12">
                        <p>Brak mieszkań spełniających wybrane parametry.</p>
                        <p>Spróbuj jeszcze raz.</p>
                    </div>
                </div>
                <div class="flat-background">
                    <?php
                    $i=1;
                    if ($mieszkania) {
                        foreach ($mieszkania as $mieszkanie) {
                            $fields = get_fields($mieszkanie->ID);

                            ?>

                            <div class="each-flat" data-page="1" data-segment="<?php echo $fields['segment']; ?>" data-rooms="<?php echo $fields['liczba_pokoi']; ?>" data-avaliable="<?=$fields['status']?>" data-surface="<?php echo $fields['metraz']; ?>" data-floor="<?php echo $fields['pietro']; ?>">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6 text-right">
                                                <div class="photo-simple-wrapper">
                                                    <img src="<?php echo $fields['zdjecie']['sizes']['flat-bg']; ?> " alt="">
                                                </div>
                                            </div>
                                            <div class="col-md-6 mieszkanie-simple-wrapper">
                                                <p class="mieszkanie <?= $mieszkanie->status ?>"><?php echo $mieszkanie->post_title ?></p>
                                                <p class="cena"><?= $fields ['cena'] ?></p>
                                                <ul>
                                                    <li><span class="first-span"><?= $fields ['liczba_pokoi'] ?></span></br><span class="text-flat">pokoje</span> </li>
                                                    <li><span class="first-span"><?= $fields ['pietro'] ?></span></br><span class="text-flat">piętro</span></li>
                                                    <li><span class="first-span"><?= $fields ['metraz'] ?></span></br><span class="text-flat">m<sup>2</sup></span></li>
                                                </ul>
                                                <a href="<?= $fields ['pobierz_rzut'] ?>" target="_blank" class="rzut">Pobierz rzut</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <span class="grey-line grey-centering"></span>
                            </div>

                            <?php
                        }
                    }
                    ?>
                    <div class="pagination col-md-12" data-page="1">
                        <span class="grey-line"></span>
<span id="prev-page" class="navigation">
                            </span>
                        <div class="pagination-main">

                        </div>

                        <span id="next-page" class="navigation">
                            </span>
                    </div>
                </div>
            </div>

            <?php
            get_template_part('kontaktBox');
            ?>


        </div>




    </div>
</div>

<div class="mt50"></div>
<?php
get_footer();
?>

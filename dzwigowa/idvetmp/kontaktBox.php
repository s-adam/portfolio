<div class="container">
    <div class="contact-wrapper-file">
        <div class="row">
            <div class="col-md-8 col-sm-6">
                <img src='<?php echo get_template_directory_uri(); ?>/img/logo-small.svg' class="contact-file-logo">
              
                <?php the_field('banner_-_zadbaj_o_swoja_przyszlosc', 'option'); ?>
            </div>
            <div class="col-md-4 col-sm-6">
                <ul>
                    <li><a href="tel:<?php the_field('telefon1', 'option'); ?>"><i class="fa fa-phone" aria-hidden="true"></i>+48 <?php the_field('telefon1', 'option'); ?></a></li>
                    <a href="/kontakt" class="btn">Formularz kontaktowy</a>
                </ul>
            </div>
        </div>
    </div>
</div>
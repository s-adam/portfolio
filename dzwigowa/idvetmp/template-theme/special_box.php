<section class="<?=$content['acf_fc_layout']?>" style="<?=$content['section_style']?>">
    <div class="<?=!$content['full_width']?'container':'container-fluid'?>">
        <div class="row">
            <div class="special-box-container">
                <div class="col-md-1 text-center">
                    <?=wp_get_attachment_image( $content['special_box_icon'],'large');?>

                </div>
                <div class="col-md-11">
                    <?= $content['special_box_text'];?>
                </div>
            </div>    
        </div>
    </div>
</section>
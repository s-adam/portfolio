
<section class="<?=$content['acf_fc_layout']?>" style="<?=$content['section_style']?>">

    <div class="<?=!$content['full_width']?'container':'container-fluid'?>">
        <div class="row">
            
            <div class="carousel-nav">
                <span class="prev-slide">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/left.jpg" alt="prev">
                </span>
                
                <span class="next-slide">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/right.jpg" alt="next">
                </span> 
            </div>
            
            <div class="owl-carousel-single owl-carousel history-carousel">
               
                <?php
                    if($content['wpis']){
                        foreach ($content['wpis'] as $item) {
                            ?>
                            <div class="item">
                                <div class="text-center">
                                    <p class="history-data"><?=$item['data']?></p>
                                </div>
                                <?=$item['tekst']?>
                                
                            </div>    
                                
                            <?php
                        }
                    }
                
                ?>

            </div>
        </div>    
    </div>
</section>    
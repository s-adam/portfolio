<?php
/**
 * szablon dla nagłówków podstron korzystających z pól własne pola -> Nagłówek podstrony
 *
 */
?>

<header  style="<?=$fields['subpage_header']['section_style']?>">
    <div class="<?=!$fields['subpage_header']['full_width']?'container':'container-fluid'?>"> 
    <?php
    if (!empty($fields['subpage_header']['subpage_header_bg'])) {
        ?>
        <div class="subpage-header-bg" style="background: url(<?=$fields['subpage_header']['subpage_header_bg']['sizes']['subpage-bg'] ?>)">    
                <?php
            }
            ?>
                        
                <div class="row">
                    <div class="col-md-12">
                        <?= $fields['subpage_header']['subpage_header_text'] ?>
                        <?php
                        if (!empty($fields['subpage_header']['subpage_header_bg'])) {
                            ?>
                    </div>
                </div>    
                    <?php
                }
                ?>
            

    <?php if (!empty($fields['subpage_header']['subpage_header_bg'])) {?>            

    </div>
    <?php } ?>
    </div>    
</header>
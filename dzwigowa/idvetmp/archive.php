<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">


		</main>
	</div>

	<div class="main-container">

		<div class="container">
			<div class="row">
				<div class="main-wrapper darken-color col-md-12 ">
					<div class="contact">
						<div class="row">
							<div class="berk-title big-title">
								<?php the_title(); ?>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>

	</div>
<?php
get_footer();

<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package idvetmp
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&amp;subset=latin-ext" rel="stylesheet">
        <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
        <link href="<?php echo get_stylesheet_directory_uri(); ?>/js/animate.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css">
        <link href="<?php echo get_stylesheet_directory_uri(); ?>/hamburgers.css" rel="stylesheet">

        <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.29.0/mapbox-gl.js'></script>
        <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.29.0/mapbox-gl.css' rel='stylesheet' />


        <script src="http://www.openlayers.org/api/OpenLayers.js"></script>



        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favico/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favico/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favico/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favico/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favico/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favico/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favico/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favico/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favico/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_stylesheet_directory_uri(); ?>/img/favico/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favico/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favico/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favico/favicon-16x16.png">
        <link rel="manifest" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favico/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?php echo get_stylesheet_directory_uri(); ?>/img/favico/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">



        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
        <header id="masthead" class="site-header">
            <div class="wrapper-for-nav">
                <nav class="navbar navbar-default navbar-fixed-top">
                    <div class="wrapper-for-navbar">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <div class="hamburger hamburger--spring js-hamburger" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                    <div class="hamburger-box">
                                        <div class="hamburger-inner"></div>
                                    </div>
                                </div>
                                <a class="text-left" href="/">
                                    <img id="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" alt="">
                                </a>
                            </div>
                            <!--                    <div class="row">
                                                    <div class="col-md-12">-->

                            <!--                        </div>
                                                </div>-->
                            <div id="navbar" class="navbar-collapse collapse">
                                <ul class="socials-menu socials-menu-right">
                                    <?php
                                    if (get_field('numer_telefonu', 'option')) {
                                        ?>
                                        <li><a class="phone-number" target="_blank" href="tel:<?php echo the_field('numer_telefonu', 'option'); ?>">
                                                <?php
                                                $from = get_field('numer_telefonu', 'option');
//                                        echo $from;
                                                $to = sprintf("%s %s %s", substr($from, 0, 3), substr($from, 3, -3), substr($from, 8));
                                                echo $to;
                                                ?>
                                                <i class="fas fa-phone"></i></a></li>
                                        <?php
                                    }
                                    ?>
                                    <?php
                                    if (get_field('facebook', 'option')) {
                                        ?>
                                        <li><a target="_blank" href="<?php echo the_field('facebook', 'option'); ?>"><i class="fab fa-facebook"></i></a></li>
                                        <?php
                                    }
                                    ?>
                                    <?php
                                    if (get_field('google_plus', 'option')) {
                                        ?>
                                        <li><a target="_blank" href="<?php the_field('google_plus', 'option') ?>"><i class="fab fa-google-plus-square"></i></a></li>
                                        <?php
                                    }
                                    ?>
                                    <?php
                                    if (get_field('youtube', 'option')) {
                                        ?>
                                        <li><a target="_blank" href="<?php the_field('youtube', 'option') ?>"><i class="fab fa-youtube"></i></a></li>
                                        <?php
                                    }
                                    ?>
                                    <?php
                                    if (get_field('linkedin', 'option')) {
                                        ?>
                                        <li><a target="_blank" href="<?php the_field('linkedin', 'option') ?>"><i class="fab fa-linkedin-in"></i></a></li>
                                                <?php
                                            }
                                            ?>
                                </ul>
                                <?php
                                wp_nav_menu(array(
                                    'theme_location' => 'menu-1',
                                    'menu_id' => 'primary-menu',
                                    'container_class' => '',
                                    'menu_class' => 'nav navbar-nav navbar-right',
                                    'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
                                    'walker' => new wp_bootstrap_navwalker())
                                );
                                ?> 
                            </div>
                        </div>
                    </div>
                </nav>
            </div>            
        </header><!-- #masthead -->
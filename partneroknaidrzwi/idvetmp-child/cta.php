<div class="cta-wrapper anim" data-vp-add-class="animated fadeIn">
    <div class="container">
        <div class="cta-box">
            <div class="text">
                <?php
                if (get_field('call_to_action_-_tekst_na_gorze', 'option')) {
                    ?>
                    <img class="rwd-photo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/cta-photo.jpg">
                    <?php
                }
                ?>
                <?php
                if (get_field('call_to_action_-_tekst_na_gorze', 'option')) {
                    ?>
                    <span class="small"><?php echo the_field('call_to_action_-_tekst_na_gorze', 'option'); ?></span>
                    <?php
                }
                ?>
                <?php
                if (get_field('call_to_action_-_tekst_na_dole', 'option')) {
                    ?>
                    <span class="big"><?php echo the_field('call_to_action_-_tekst_na_dole', 'option'); ?></span>
                    <?php
                }
                ?>
                <?php
                if (get_field('call_to_action_-_przycisk_-_url', 'option')) {
                    ?>
                    <a class="btn" href="<?php echo the_field('call_to_action_-_przycisk_-_url', 'option'); ?>"><?php echo the_field('call_to_action_-_przycisk_-_tekst', 'option'); ?></a>
                    <?php
                }
                ?>

            </div>
            <?php
            if (get_field('call_to_action_-_przycisk_-_url', 'option')) {
                ?>
                <a class="btn" href="<?php echo the_field('call_to_action_-_przycisk_-_url', 'option'); ?>"><?php echo the_field('call_to_action_-_przycisk_-_tekst', 'option'); ?></a>
                <?php
            }
            ?>
        </div>
    </div>
</div>





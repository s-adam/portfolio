<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */

get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>



<div class="single-page-wrapper realizacje">
    <div class="header-wrapper" style="background: url(<?= $fields['header-photo']['sizes']['blog-img-big'] ?>) top 142px center no-repeat; background-size: cover;">
        <div class="wrapper-for-header-info">
            <div class="container">
                <div class="section-title-archive">
                    <h2><?= the_title(); ?></h2>
                    <p>Skontaktuj się z nami, z chęcią odpowiemy na Twoje pytania</p>
                </div>
            </div>
        </div>
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/effect-single-page.png" class="single-page-effect hide-now">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/effekt-single-page-small.png" class="single-page-effect single-page-effect-small">
    </div>
    <div class="content-single-page">
        <div class="container">
            <div class="sortowanie">
                <ul class="sort-menu text-center">
                    <li><a class="active" href="#">Wszystkie</a></li>
                    <li><a href="#">Okna</a></li>
                    <li><a href="#">Drzwi</a></li>
                    <li><a href="#">Bramy</a></li>
                    <li><a href="#">Rolety</a></li>
                    <li><a href="#">Parapetys</a></li>
                </ul>
            </div>
            <div class="content-inside-wrapper">

                <div class="posts-content-realizacje">
                    <?php
                    if ($fields['sekcja_druga_-_parapety']) {
                        $i = 0;
                        $j = 0;
                        foreach ($fields['sekcja_druga_-_parapety'] as $field) {
                            if ($i == 0) {
                                echo '<div class="row">';
                            }
                            ?>

                            <div class="col-md-3">
                                <div class="one-box-content">
                                    <a href=""><?= wp_get_attachment_image($field['zdjecie']['ID'], 'oferta-img'); ?></a>

                                    <div class="box-content">

                                        <?php if ($field['nazwa']) {
                                            ?>
                                            <a href="">
                                                <p><?= $field['nazwa'] ?></p>
                                            </a>
                                        <?php }
                                        ?>

                                    </div>

                                    <div class="triangle-gallery">
                                        
                                    </div>
                                </div>
                            </div>

                            <?php
                            $i++;
                            $j++;
                            if ($i == 4 || count($fields['sekcja_druga_-_parapety']) == $j) {

                                echo '</div>';

                                $i = 0;
                            }
                        }
                    }
                    ?>
                </div>
            </div>   
        </div>
    </div>

    <?php include_once "partnerzy.php"; ?>

</div>

<?php
get_footer();

<div class="slider-swiper swiper-container">
    <!--Additional required wrapper--> 
    <div class="swiper-wrapper text-center">
        <!--Slides--> 
        <?php
        $variable = get_field('slider', 'option');
        if ($variable) {
            foreach ($variable as $field) {
                ?>
                <div class="swiper-slide">
                    <a href="<?= $field['adres_url']; ?>"><?= wp_get_attachment_image($field['logo']['ID'], 'slider'); ?></a>
                </div>
                <?php
            }
        }
        ?>
    </div>
</div>
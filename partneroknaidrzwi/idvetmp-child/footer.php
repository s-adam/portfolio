<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package idvetmp
 */
?>
<footer class="site-footer" style="background: url(<?php echo get_stylesheet_directory_uri(); ?>/img/footer.jpg) no-repeat; background-size: cover;">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <?php
                wp_nav_menu(array(
                    'menu' => 'footer-menu',
                    'theme_location' => 'footer-menu',
                    'depth' => 3,
                    'container' => false,
                    'container_class' => 'collapse navbar-collapse ',
                    'menu_class' => 'nav navbar-nav menu-footer',
                    'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
                    'walker' => new wp_bootstrap_navwalker())
                );
                ?>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="address">
                    <?php
                    if (get_field('adres_firmy', 'option')) {
                        ?>
                        <?php echo the_field('adres_firmy', 'option'); ?>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">

                <ul class="footer-contact">
                    <?php
                    if (get_field('numer_telefonu_-_stacjonarny', 'option')) {

                        $from = get_field('numer_telefonu_-_stacjonarny', 'option');

                        function clean($string) {
                            $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.

                            return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
                        }
                        ?>
                        <li><a class="phone-number" target="_blank" href="tel:<?php echo clean($from); ?>"><i class="fas fa-phone"></i>
                                <?php
                                $from = get_field('numer_telefonu_-_stacjonarny', 'option');
                                


//                                        echo $from;
//                                $to = sprintf("%s/ %s %s %s", substr($from, 0, 2), substr($from, 3, -5), substr($from, 5, -3), substr($from, 9));
//                                $to = str_replace("/", " ", $from);
                                echo $from;
                                ?>
                            </a></li>
                        <?php
                    }
                    ?>
                    <?php
                    if (get_field('numer_telefonu', 'option')) {
                        ?>
                        <li><a class="phone-number" target="_blank" href="tel:<?php echo the_field('numer_telefonu', 'option'); ?>"><i class="fas fa-phone"></i>
                                <?php
                                $from = get_field('numer_telefonu', 'option');
//                                        echo $from;
//                                $to = sprintf("%s %s %s", substr($from, 0, 3), substr($from, 4, -5), substr($from, 6, 8));
                                echo $from;
                                ?>
                            </a></li>
                        <?php
                    }
                    ?>
                    <?php
                    if (get_field('email', 'option')) {
                        ?>
                        <li><a class="email" target="_blank" href="mailto:<?php echo the_field('email', 'option'); ?>"><i class="fas fa-at"></i><?php echo the_field('email', 'option'); ?></a></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
            <div id="cookie-div">
                <span id="closeCookies" onclick="readcookie();">&times;</span>
                <div>
                    
                        <?php echo get_field('cookies_-_tekst', 'option') ?>
                        <!--<span  id="close">Zamknij</span>-->
                        <a href="" onclick="readcookie();" class="btn">Zamknij</a>
                    
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <p class="ll">Social media</p>
                <ul class="socials-menu">
                    <?php
                    if (get_field('numer_telefonu', 'option')) {
                        ?>
                        <li><a class="phone-number" target="_blank" href="tel:<?php echo the_field('numer_telefonu', 'option'); ?>">
                                <?php
                                $from = get_field('numer_telefonu', 'option');
//                                        echo $from;
                                $to = sprintf("%s %s %s", substr($from, 0, 3), substr($from, 3, -3), substr($from, 6));
                                echo $to;
                                ?>
                                <i class="fas fa-phone"></i></a></li>
                        <?php
                    }
                    ?>
                    <?php
                    if (get_field('facebook', 'option')) {
                        ?>
                        <li><a target="_blank" href="<?php echo the_field('facebook', 'option'); ?>"><i class="fab fa-facebook"></i></a></li>
                        <?php
                    }
                    ?>
                    <?php
                    if (get_field('google_plus', 'option')) {
                        ?>
                        <li><a target="_blank" href="<?php the_field('google_plus', 'option') ?>"><i class="fab fa-google-plus-square"></i></a></li>
                        <?php
                    }
                    ?>
                    <?php
                    if (get_field('youtube', 'option')) {
                        ?>
                        <li><a target="_blank" href="<?php the_field('youtube', 'option') ?>"><i class="fab fa-youtube"></i></a></li>
                        <?php
                    }
                    ?>
                    <?php
                    if (get_field('linkedin', 'option')) {
                        ?>
                        <li><a target="_blank" href="<?php the_field('linkedin', 'option') ?>"><i class="fab fa-linkedin-in"></i></a></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 text-center footer-text">
                Wszelkie prawa zastrzeżone &copy; <?php echo date('Y') ?> PARTNER OKNA I DRZWI
            </div>
        </div>         
        <div class="row">
            <div class="col-md-12 text-center footer-autor">
                <a href="https://ideative.pl">
                    <img src="<?= get_template_directory_uri() . '/img/by-ideative.svg' ?>" alt="ideative.pl">
                </a>
            </div>
        </div>    
    </div>

</footer><!-- #colophon -->


<?php wp_footer(); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/classie.js"></script>



<script type='text/javascript' src='https://www.google.com/recaptcha/api.js'></script>


<?php if ((get_the_ID() == 90)) {
    ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDT5ONMSnq3pV_L5VlenZYFv4I-UBlVHDo"></script>
            <!--<script src='https://www.google.com/recaptcha/api.js'></script>-->
    <?php
}
?>

</body>
</html>

<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*

 */
get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>



<div class="single-page-wrapper aktualnosc">
    <?php $image = get_field('zdjecie_w_naglowku_-_aktualnosci', 'option'); ?>
    <div class="header-wrapper" style="background: url(<?=$image['sizes']['blog-img-big']?>) top 0 center no-repeat; background-size: cover;">
        <div class="wrapper-for-header-info">
            <div class="container">
                <div class="section-title-archive">
                    <h2>AKTUALNOŚCI</h2>
                    <ul class="breadcrumbs">
                        <li>
                            <a href="/">Strona Główna</a>
                            <span>»</span>
                        </li>
                        <li>
                            <a href="/aktualnosci">Aktualności</a>
                            <span>»</span>
                        </li>
                        <li>
                            <?= the_title(); ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/effect-single-page.png" class="single-page-effect hide-now">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/effekt-single-page-small.png" class="single-page-effect single-page-effect-small">
    </div>
    <div class="content-single-page">
        <div class="container">
            <div class="content-inside-wrapper">
                <div class="info-wrapper text-center">
                    <h2><?= the_title() ?></h2>
                </div>
                <?php the_post_thumbnail('lightbox-img'); ?>

                <div class="row">
                    <div class="col-md-12">
                        <div class="text-wrapper-for-single-post">
                            <?php
                            the_content();
                            ?>
                        </div>
                    </div>
                </div>
                <div class="wrapper-for-swiper-width">
                    <?php
                    $showSlider = false;
                    $variable = $fields['slider'];
                    if ($variable) {
                        ?>
                        <div class="fourth-swiper swiper-container text-center">
                            <!-- Additional required wrapper -->
                            <div class="swiper-wrapper">
                                <!-- Slides -->

                                <?php
                                foreach ($variable as $field) {
                                    if(!empty($field['zdjecie'])){
                                    ?>
                                    <div class="swiper-slide">
                                        <a href='<?= $field['zdjecie']['sizes']['lightbox-img']; ?>' data-toggle="lightbox" data-gallery="example-gallers887877y" data-footer="<?= $field['nazwa']; ?>"><?= wp_get_attachment_image($field['zdjecie']['ID'], 'slider-img'); ?></a>
                                        <a href='<?= $field['zdjecie']['sizes']['lightbox-img']; ?>' data-toggle="lightbox" data-gallery="example-galleryx123123" data-footer="<?= $field['nazwa']; ?>"><span><?= $field['nazwa']; ?></span></a>
                                    </div>
                                    <?php
                                    $showSlider = true;
                                    }
                    }}
                                ?>
                            </div>
                            <?php
                            if ($showSlider) {
                                ?>
                                <div class="swiper-button-next z1"></div>
                                <div class="swiper-button-prev z2"></div>
                                <?php
                            }
                            ?>
                        </div>

                        
                </div>
            </div>

        </div>

    </div>
</div>

<?php include_once "call-to-action.php"; ?>
<?php include_once "partnerzy.php"; ?>

</div>

<?php
get_footer();

<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*
  Template Name: Kontakt
 */
get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>



<div class="single-page-wrapper kontakt">
    <div class="header-wrapper" style="background: url(<?= $fields['header-photo']['sizes']['blog-img-big'] ?>) top 0px center no-repeat; background-size: cover;">
        <div class="wrapper-for-header-info">
            <div class="container">
                <div class="section-title-archive">
                    <h2><?= the_title(); ?></h2>
                    <ul class="breadcrumbs">
                        <li>
                            <a href="/">Strona Główna</a>
                            <span>»</span>
                        </li>
                        <li>
                            <?= get_the_title(); ?>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/effect-single-page.png" class="single-page-effect hide-now">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/effekt-single-page-small.png" class="single-page-effect single-page-effect-small">
    </div>
    <div class="content-single-page">
        <div class="container">
            <div class="content-inside-wrapper">
                <div class="row">
                    <div class="col-md-6">
                        <?php
                        the_content();
                        ?>
                        <ul>
                            <?php
                            if (get_field('numer_telefonu_-_stacjonarny', 'option')) {
                                ?>
                                <li><a target="_blank" href="tel:<?php echo the_field('numer_telefonu_-_stacjonarny', 'option'); ?>"><i class="fas fa-phone"></i><?php echo the_field('numer_telefonu_-_stacjonarny', 'option'); ?></a></li>
                                <?php
                            }
                            ?>
                            <?php
                            if (get_field('numer_telefonu_-_stacjonarny_2', 'option')) {
                                ?>
                                <li><a target="_blank" href="tel:<?php echo the_field('numer_telefonu_-_stacjonarny_2', 'option'); ?>"><i class="fas fa-phone"></i><?php echo the_field('numer_telefonu_-_stacjonarny_2', 'option'); ?></a></li>
                                <?php
                            }
                            ?>
                            <?php
                            if (get_field('numer_telefonu', 'option')) {
                                ?>
                                <li><a target="_blank" href="tel:<?php echo the_field('numer_telefonu', 'option'); ?>"><i class="fas fa-phone"></i><?php echo the_field('numer_telefonu', 'option'); ?></a></li>
                                <?php
                            }
                            ?>
                            <?php
                            if (get_field('adres_firmy', 'option')) {
                                ?>
                                <li><a target="_blank" href="mailto:<?php echo the_field('email', 'option'); ?>"><i class="fas fas fa-envelope"></i><?php echo the_field('email', 'option'); ?></a></li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>

                    <div class="col-md-6">
                        <section class="content">
                            <!--                            <div role="form" class="wpcf7" id="wpcf7-f107-p90-o1" lang="pl-PL" dir="ltr">
                                                            <div class="screen-reader-response"></div>
                                                            <form action="/kontakt/#wpcf7-f107-p90-o1" method="post" class="wpcf7-form" novalidate="novalidate">
                                                                <div style="display: none;">
                                                                    <input type="hidden" name="_wpcf7" value="107">
                                                                    <input type="hidden" name="_wpcf7_version" value="5.0.1">
                                                                    <input type="hidden" name="_wpcf7_locale" value="pl_PL">
                                                                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f107-p90-o1">
                                                                    <input type="hidden" name="_wpcf7_container_post" value="90">
                                                                </div>
                                                                <span class="input input--hoshi">
                                                                    <input class="input__field input__field--hoshi" name="your-name" type="text" id="input-4" />
                                                                    <label class="input__label input__label--hoshi input__label--hoshi-color-2" for="input-4">
                                                                        <span class="input__label-content input__label-content--hoshi">IMIĘ</span>
                                                                    </label>
                                                                </span>
                                                                <span class="input input--hoshi">
                                                                    <input class="input__field input__field--hoshi" name="your-email" type="text" id="input-5" />
                                                                    <label class="input__label input__label--hoshi input__label--hoshi-color-2" for="input-5">
                                                                        <span class="input__label-content input__label-content--hoshi">EMAIL LUB TELEFON</span>
                                                                    </label>
                                                                </span>
                                                                <span class="input input--hoshi">
                                                                    <input class="input__field input__field--hoshi" name="your-message" type="textarea" id="input-6" />
                                                                    <label class="input__label input__label--hoshi input__label--hoshi-color-3" for="input-6">
                                                                        <span class="input__label-content input__label-content--hoshi">WIADOMOŚĆ</span>
                                                                    </label>
                                                                </span>
                                                                <div class="wrapper-for-captcha">
                                                                    <div data-sitekey="6Ld_e14UAAAAAOn-HU6kx9hagCqJPyz85PPOGM9m" class="wpcf7-form-control g-recaptcha wpcf7-recaptcha"><div style="width: 304px; height: 78px;"><div><iframe src="https://www.google.com/recaptcha/api2/anchor?ar=1&amp;k=6Ld_e14UAAAAAOn-HU6kx9hagCqJPyz85PPOGM9m&amp;co=aHR0cDovL3BhcnRuZXJva25haWRyendpLmdhOjgw&amp;hl=pl&amp;v=v1528135568984&amp;size=normal&amp;cb=n5kirucaiebj" width="304" height="78" role="presentation" frameborder="0" scrolling="no" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox"></iframe></div><textarea id="g-recaptcha-response" name="g-recaptcha-response" class="g-recaptcha-response" style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;  display: none; "></textarea></div></div>
                                                                </div>
                                                                <button type="submit" value="Wyślij" class="wpcf7-form-control wpcf7-submit btn">WYŚLIJ</button></p>
                                                                <div class="wpcf7-response-output wpcf7-display-none"></div>
                                                            </form>
                                                            
                                                        </div>   -->

                            <div role="form" class="wpcf7" id="wpcf7-f107-p90-o1" lang="pl-PL" dir="ltr">
                                <div class="screen-reader-response"></div>
                                <form action="/kontakt/#wpcf7-f107-p90-o1" method="post" class="wpcf7-form" novalidate="novalidate">
                                    <div style="display: none;">
                                        <input type="hidden" name="_wpcf7" value="107" />
                                        <input type="hidden" name="_wpcf7_version" value="5.0.2" />
                                        <input type="hidden" name="_wpcf7_locale" value="pl_PL" />
                                        <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f107-p90-o1" />
                                        <input type="hidden" name="_wpcf7_container_post" value="90" />
                                    </div>
                                    <span class="input input--hoshi">
                                        <input class="input__field input__field--hoshi" name="your-name" type="text" id="input-4" />
                                        <label class="input__label input__label--hoshi input__label--hoshi-color-2" for="input-4">
                                            <span class="input__label-content input__label-content--hoshi">IMIĘ</span>
                                        </label>
                                    </span>
                                    <span class="input input--hoshi">
                                        <input class="input__field input__field--hoshi" name="your-email" type="text" id="input-5" />
                                        <label class="input__label input__label--hoshi input__label--hoshi-color-2" for="input-5">
                                            <span class="input__label-content input__label-content--hoshi">EMAIL LUB TELEFON</span>
                                        </label>
                                    </span>
                                    <span class="input input--hoshi">
                                        <input class="input__field input__field--hoshi" name="your-message" type="textarea" id="input-6" />
                                        <label class="input__label input__label--hoshi input__label--hoshi-color-3" for="input-6">
                                            <span class="input__label-content input__label-content--hoshi">WIADOMOŚĆ</span>
                                        </label>
                                    </span>
                                    <div class="wrapper-for-captcha">
                                        <div class="wpcf7-form-control-wrap">
                                            <div data-sitekey="6Ld_e14UAAAAAOn-HU6kx9hagCqJPyz85PPOGM9m" class="wpcf7-form-control g-recaptcha wpcf7-recaptcha">

                                            </div>
                                            <noscript>
                                            <div class="new-test">
                                                <div>
                                                    <div>
                                                        <iframe src="https://www.google.com/recaptcha/api/fallback?k=6Ld_e14UAAAAAOn-HU6kx9hagCqJPyz85PPOGM9m" frameborder="0" scrolling="no" style="width: 302px; height:422px; border-style: none;">
                                                        </iframe>
                                                    </div>
                                                    <div>
                                                        <textarea id="g-recaptcha-response" name="g-recaptcha-response" class="g-recaptcha-response" style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;">
                                                        </textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            </noscript>
                                        </div><br />
                                    </div>
                                    <button type="submit" value="Wyślij" class="wpcf7-form-control wpcf7-submit btn">WYŚLIJ</button></p>
                                    <div class="wpcf7-response-output wpcf7-display-none"></div></form></div> 
                        </section>

                    </div>
                </div>
            </div>
            <div id='map'></div>
            <script>

                var lat = <?= $fields ['google_maps']['lat'] ?>;
                var lng = <?= $fields ['google_maps']['lng'] ?>;
            </script>


        </div>
    </div>
    <?php include_once "partnerzy.php"; ?>
</div>

<?php
get_footer();

<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*
 * Template Name: Realizacjez
 */
get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>

<?php
$url = $_SERVER['REQUEST_URI'];
?>

<div class="single-page-wrapper realizacje t">
    <div class="header-wrapper" style="background: url(<?= get_the_post_thumbnail_url(); ?>) top 0 center no-repeat; background-size: cover;">
        <div class="wrapper-for-header-info">
            <div class="container">
                <div class="section-title-archive">

                    <?php
                    $term1 = get_queried_object()->name;

                    if ($term1 == 'galeria') {
                        ?>
                        <h2><?= get_the_title(155); ?></h2>
                        <ul class="breadcrumbs">
                            <li>
                                <a href="/">Strona Główna</a>
                                <span>»</span>

                            </li>
                            <li>
                                <a href="<?= the_permalink(); ?>"><?= get_the_title(155); ?></a>
                            </li>
                        </ul>
                        <?php
                    } else {
                        ?>
                        <h2><?= the_title(); ?></h2>
                        <ul class="breadcrumbs">
                            <li>
                                <a href="/">Strona Główna</a>
                                <span>»</span>

                            </li>
                            <li>
                                <a href="<?= the_permalink(); ?>"><?= the_title(); ?></a>
                            </li>
                        </ul>
                        <?php
                    }
                    ?>


                </div>
            </div>
        </div>
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/effect-single-page.png" class="single-page-effect hide-now">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/effekt-single-page-small.png" class="single-page-effect single-page-effect-small">
    </div>
    <div class="content-single-page">
        <div class="container">
            <div class="sortowanie">
                <ul class="sort-menu text-center">
                    <li><a class="active" href="/">Wszystkie</a></li>
                    <?php
                    $wcatTerms = get_terms('realizacje', array('hide_empty' => 0, 'parent' => 0));
                    foreach ($wcatTerms as $wcatTerm) :
                        ?>

                        <li>
                            <a class="<?php $term = get_queried_object()->name == $wcatTerm->name ? "active" : "" ?>" href="<?php echo get_term_link($wcatTerm->slug, $wcatTerm->taxonomy); ?>"><?php echo $wcatTerm->name; ?></a>

                        </li>

                        <?php
                    endforeach;
                    wp_reset_query();
                    ?>
                </ul>

            </div>

            <div class="content-inside-wrapper">

                <div class="posts-content-realizacje">

                    <?php
                    $args = array('post_type' => 'galeria',
                        'posts_per_page' => 16,
                        'paged' => ( get_query_var('paged') ? get_query_var('paged') : 1)
                    );

                    $a = query_posts($args);
                    $loop = new WP_Query($args);
                    if ($loop->have_posts()) {
                        $i = 0;
                        $j = 0;

                        while ($loop->have_posts()) : $loop->the_post();
                            if ($i == 0) {
                                echo '<div class="row">';
                            }
                            ?>
                            <div class="col-md-3">
                                <div class="wrapper-for-realizacje">
                                    <div class="wrapper-photo">
                                        <a href="<?= the_post_thumbnail_url('lightbox-img') ?>" data-toggle="lightbox" data-gallery="example-gallerypp" data-footer="<?= get_the_title() ?>"><img src="<?= the_post_thumbnail_url('realizacje-img') ?>"></a>
                                        <div id="triangle-bottomleft"></div>
                                    </div>
                                    <a href="<?= the_post_thumbnail_url('lightbox-img') ?>" data-toggle="lightbox" data-gallery="example-gallery" data-footer="<?= get_the_title() ?>"><?= get_the_title() ?></a>

                                </div>
                            </div>
                            <?php
                            $i++;
                            $j++;

                            if ($i == 4 || count($a) == $j) {

                                echo '</div>';

                                $i = 0;
                            }
                        endwhile;
                    }
                    ?>

                    <?php
                    numeric_posts_nav();
                    ?>
                </div>
            </div>    
        </div>
    </div>

    <?php include_once "partnerzy.php"; ?>

</div>

<?php
get_footer();

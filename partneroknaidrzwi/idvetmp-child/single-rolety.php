<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*
 * Template Name: Rolety - Rodzaje
 * Template Post Type: rolety
 */
get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>



<div class="single-page-wrapper rolety">
    <div class="header-wrapper" style="background: url(<?= $fields['header-photo']['sizes']['blog-img-big'] ?>) top 0 center no-repeat; background-size: cover;">
        <div class="wrapper-for-header-info">
            <div class="container">
                <div class="section-title-archive">
                    <h2><?= the_title(); ?></h2>
                    <ul class="breadcrumbs">
                        <li>
                            <a href="/">Strona Główna</a>
                            <span>»</span>
                        </li>
                        <li>
                            <a href="<?= the_permalink(281); ?>"><?= get_the_title(281); ?></a>
                            <span>»</span>
                        </li>
                        <li>
                            <?= the_title(); ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/effect-single-page.png" class="single-page-effect">
    </div>
    <div class="content-single-page">
        <div class="container">
            <div class="content-inside-wrapper">
                <div class="section-second">
                    
                    <div class="parapety-section">
                        <?php
                        if ($fields['drzwi']) {
                            $i = 0;
                            foreach ($fields['drzwi'] as $field) {
                                ?>

                                <div class="row">
                                    <div class="normal-wrapper">
                                        <div class="col-md-3">
                                            <a href="<?= $field['zdjecie']['sizes']['lightbox-img'] ?>" data-toggle="lightbox" data-gallery="drzwi1-example-gallery"><?= wp_get_attachment_image($field['zdjecie']['ID'], 'oferta-img'); ?></a>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="wrao">
                                                <?=$field['tekst']?>
                                            </div>
                                        </div>   
                                    </div> 
                                </div>

                                <?php
                                $i++;
                            }
                        }
                        ?>
                    </div>
                    <div class="show-more text-center">
                        <!--<a href="/rolety" class="btn">ZOBACZ WIĘCEJ</a>-->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include_once "call-to-action.php"; ?>
    <?php include_once "partnerzy.php"; ?>

</div>

<?php
get_footer();

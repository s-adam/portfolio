<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*
 * Template Name: Okna - Główna
 * Template Post Type: okna
 */
get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>



<div class="single-page-wrapper okna">
    <div class="header-wrapper" style="background: url(<?= $fields['header-photo']['sizes']['blog-img-big'] ?>) top 142px center no-repeat; background-size: cover;">
        <div class="wrapper-for-header-info">
            <div class="container">
                <div class="section-title-archive">
                    <h2><?= the_title(); ?></h2>
                    <p>Skontaktuj się z nami, z chęcią odpowiemy na Twoje pytania</p>
                </div>
            </div>
        </div>
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/effect-single-page.png" class="single-page-effect hide-now">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/effekt-single-page-small.png" class="single-page-effect single-page-effect-small">
    </div>
    <div class="content-single-page">
        <div class="container">
            <div class="content-inside-wrapper">
                <div class="section-first">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="left">
                                <?php if ($fields['sekcja_pierwsza_-_tekst']) {
                                    ?>
                                    <?= $fields['sekcja_pierwsza_-_tekst'] ?>
                                <?php }
                                ?>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="right">
                                <?php if ($fields['sekcja_pierwsza_-_zdjecie']) {
                                    ?>
                                    <?= wp_get_attachment_image($fields['sekcja_pierwsza_-_zdjecie']['ID'], 'oferta-img'); ?>
                                <?php }
                                ?>
                                <div class="triangle">

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="section-second">
                    <div class="title-section">
                        <?php if ($fields['sekcja_druga_-_tytul']) {
                            ?>
                            <?= $fields['sekcja_druga_-_tytul'] ?>
                        <?php }
                        ?>
                    </div>
                    <div class="row anim" data-vp-add-class="animated fadeIn">
                <?php
                //juz

                $args = array('post_type' => 'okna', 'posts_per_page' => 4);
                $loop = new WP_Query($args);
                while ($loop->have_posts()) : $loop->the_post();

                    $fields = get_fields(get_the_ID());
                    ?>
                    
                        <div class="normal-wrapper">
                            <div class="col-md-3">
                                <a href="<?php the_permalink();?>">
                                <?php
                                the_post_thumbnail();
                                ?>
                                </a>
                            </div>
<!--                            <div class="col-md-8">
                                <div class="wrao">
                                    <span><?= the_time("j F Y"); ?></span>
                                    <a href="<?php the_permalink();?>"><h2><?= the_title() ?></h2></a>
                                    <?php
                                    the_excerpt();
                                    ?>
                                    <a class="btn" href="<?php the_permalink();?>">ZOBACZ</a>
                                </div>
                            </div>   -->
                        </div> 
                    </div>
                    <?php
                endwhile;
                ?>
                
                    
                    </div>

                    <?php if ($fields['porownywarka_-_tekst']) { ?>
                        <div class="text-area-porownaj">
                            <?=$fields['porownywarka_-_tekst']?>
                            <span>
                                <a class="btn" href="<?=$fields['porownywarka_-_przycisk_url']?>">
                                
                                <?=$fields['porownywarka_-_przycisk_tekst']?>
                            </a>
                            </span>
                        </div>
                    <?php } ?>

                    
                    <div class="section-first section-first-news">
                        <div class="row">

                            <div class="col-md-5">
                                <div class="right">
                                    <?php if ($fields['sekcja_druga_-_zdjecie']) {
                                        ?>
                                        <?= wp_get_attachment_image($fields['sekcja_druga_-_zdjecie']['ID'], 'oferta-img'); ?>
                                    <?php }
                                    ?>
                                    <div class="triangle-after">

                                    </div>

                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="left">
                                    <?php if ($fields['sekcja_druga_-_tekst']) {
                                        ?>
                                        <?= $fields['sekcja_druga_-_tekst'] ?>
                                    <?php }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <?php include_once "call-to-action.php"; ?>
    <?php include_once "partnerzy.php"; ?>

</div>

<?php
get_footer();

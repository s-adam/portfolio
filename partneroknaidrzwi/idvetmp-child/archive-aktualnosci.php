<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*

 */
get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>



<div class="single-page-wrapper aktualnosci">
    <?php $image = get_field('zdjecie_w_naglowku_-_aktualnosci', 'option'); ?>
    <div class="header-wrapper" style="background: url(<?=$image['sizes']['blog-img-big']?>) top 0 center no-repeat; background-size: cover;">
        <div class="wrapper-for-header-info anim" data-vp-add-class="animated fadeIn">
            <div class="container">
                <div class="section-title-archive">
                    <h2>AKTUALNOŚCI</h2>
                    <ul class="breadcrumbs">
                        <li>
                            <a href="/">Strona Główna</a>
                            <span>»</span>
                        </li>
                        <li>
                            Aktualności
                        </li>
                        
                    </ul>
                </div>
            </div>
        </div>
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/effect-single-page.png" class="single-page-effect hide-now">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/effekt-single-page-small.png" class="single-page-effect single-page-effect-small">
    </div>
    <div class="content-single-page">
        <div class="container">
            
            <div class="content-inside-wrapper">


                <?php
                //juz

                $args = array(
                    'posts_per_page' => 3,
                    'order' => 'ASC',
                    'post_type' => 'aktualnosci',
                    'paged' => ( get_query_var('paged') ? get_query_var('paged') : 1),
                );
                $loop = new WP_Query($args);
                while ($loop->have_posts()) : $loop->the_post();

                    $fields = get_fields(get_the_ID());
                    ?>
                    <div class="row anim" data-vp-add-class="animated fadeIn">
                        <div class="normal-wrapper">
                            <div class="col-md-4">
                                <a href="<?php the_permalink();?>">
								<?php the_post_thumbnail('lightbox-img');?>
                                
                                </a>
                            </div>
                            <div class="col-md-8">
                                <div class="wrao">
                                    <!--<span><?= the_time("j F Y"); ?></span>-->
                                    <a href="<?php the_permalink();?>"><h2><?= the_title() ?></h2></a>
                                    <?php
                                    the_excerpt();
                                    ?>
                                    <a class="btn" href="<?php the_permalink();?>">ZOBACZ</a>
                                </div>
                            </div>   
                        </div> 
                    </div>
                    <?php
                endwhile;
                ?>
                <?php
                numeric_posts_nav();
                ?>


            </div>
        </div>
    </div>

    <?php include_once "partnerzy.php"; ?>

</div>

<?php
get_footer();

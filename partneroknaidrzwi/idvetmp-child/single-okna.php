<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*
 * Template Name: Okna - Rodzaje
 * Template Post Type: okna
 */
get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>



<div class="single-page-wrapper okna-rodzaje">
    <div class="header-wrapper" style="background: url(<?= $fields['header-photo']['sizes']['blog-img-big'] ?>) top 0 center no-repeat; background-size: cover;">
        <div class="wrapper-for-header-info">
            <div class="container">
                <div class="section-title-archive">
                    <h2><?= the_title(); ?></h2>
                    <ul class="breadcrumbs">
                        <li>
                            <a href="/">Strona Główna</a>
                            <span>»</span>
                        </li>
                        <li>
                            <a href="<?= the_permalink(279); ?>"><?= get_the_title(279); ?></a>
                            <span>»</span>
                        </li>
                        <li>
                            <?= the_title(); ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/effect-single-page.png" class="single-page-effect hide-now">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/effekt-single-page-small.png" class="single-page-effect single-page-effect-small">
    </div>
    <div class="content-single-page">
        <div class="container">
            <div class="content-inside-wrapper">
                <div class="section-first">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="left text-center">
                                <div class="gallery-top swiper-container text-center">
                                    <!-- Additional required wrapper -->
                                    <div class="swiper-wrapper text-center">
                                        <!-- Slides -->
                                        <?php
                                        $variable = $fields['slider'];
                                        if ($variable) {
                                            foreach ($variable as $field) {
                                                ?>
                                                <div class="swiper-slide text-center">
                                                    <a href="<?= $field['zdjecie']['sizes']['lightbox-img'] ?>" data-toggle="lightbox" data-gallery="example-gallery"><?= wp_get_attachment_image($field['zdjecie']['ID'], 'lightbox-img'); ?></a>

                                                </div>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                    <?php
                                    if ($variable) {
                                        ?>
                                        <!-- Add Arrows -->
                                        <div class="swiper-button-next y1"></div>
                                        <div class="swiper-button-prev y2"></div>
                                        <!--</div>-->
                                        <?php
                                    }
                                    ?>
                                </div>



                                <div class="okna-swiper swiper-container text-center">
                                    <!-- Additional required wrapper -->
                                    <div class="swiper-wrapper">
                                        <!-- Slides -->
                                        <?php
                                        $variable = $fields['slider'];
                                        if ($variable) {
                                            foreach ($variable as $field) {
                                                ?>
                                                <div class="swiper-slide">
                                                    <?= wp_get_attachment_image($field['zdjecie']['ID'], 'lightbox-img'); ?>

                                                </div>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                    <?php
                                    if ($variable) {
                                        ?>
                                        <!-- Add Arrows -->
                                        <div class="swiper-button-next z1"></div>
                                        <div class="swiper-button-prev z2"></div>
                                        <!--</div>-->
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="right">
                                <?php if ($fields['sekcja_pierwsza_-_tekst']) {
                                    ?>
                                    <?= $fields['sekcja_pierwsza_-_tekst'] ?>
                                <?php }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section-second">

                    <div class="row">
                        <div class="wrapper-for-contents">
                            <div class="title-section">
                                <?php if ($fields['sekcja_druga_-_tytul']) {
                                    ?>
                                    <?= $fields['sekcja_druga_-_tytul'] ?>
                                <?php }
                                ?>
                            </div>
                            <div class="col-md-6">
                                <?php if ($fields['sekcja_druga_-_tekst-left']) {
                                    ?>
                                    <?= $fields['sekcja_druga_-_tekst-left'] ?>
                                <?php }
                                ?>
                            </div>
                            <div class="col-md-6">
                                <?php if ($fields['sekcja_druga_-_tekst-right']) {
                                    ?>
                                    <?= $fields['sekcja_druga_-_tekst-right'] ?>
                                <?php }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="section-third">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="left">
                                <?php if ($fields['sekcja_trzecia_-_zdjecie']) {
                                    ?>
                                    <?= wp_get_attachment_image($fields['sekcja_trzecia_-_zdjecie']['ID'], 'main-img'); ?>
                                <?php }
                                ?>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="right">
                                <?php if ($fields['sekcja_trzecia_-_tekst']) {
                                    ?>
                                    <?= $fields['sekcja_trzecia_-_tekst'] ?>
                                <?php }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section-fourth">
                    <div class="okna-bottom-swiper swiper-container text-center">
                        <!-- Additional required wrapper -->
                        <div class="swiper-wrapper">
                            <!-- Slides -->
                            <?php
                            $variable = $fields['slider_bottom'];
                            if ($variable) {
                                foreach ($variable as $field) {
                                    ?>
                                    <div class="swiper-slide">
                                        <a href="<?= $field['zdjecie']['sizes']['lightbox-img'] ?>" data-toggle="lightbox" data-gallery="example-gallessary" data-footer="<?= $field['nazwa']; ?>"><?= wp_get_attachment_image($field['zdjecie']['ID'], 'slider-img'); ?></a>
                                        <a href="<?= $field['zdjecie']['sizes']['lightbox-img'] ?>" data-toggle="lightbox" data-gallery="example-gallessary" data-footer="<?= $field['nazwa']; ?>"><?= $field['nazwa']; ?></a>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <?php
                        if ($variable) {
                            ?>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next s1"></div>
                            <div class="swiper-button-prev s2"></div>
                            <!--</div>-->
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include_once "call-to-action.php"; ?>
    <?php include_once "partnerzy.php"; ?>

</div>

<?php
get_footer();

<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*
 * Template Name: Drzwi - Główna
 */
get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>



<div class="single-page-wrapper drzwi">
    <div class="header-wrapper" style="background: url(<?= $fields['header-photo']['sizes']['blog-img-big'] ?>) top 0 center no-repeat; background-size: cover;">
        <div class="wrapper-for-header-info anim" data-vp-add-class="animated fadeIn">
            <div class="container">
                <div class="section-title-archive">
                    <h2><?= the_title(); ?></h2>
                    <ul class="breadcrumbs">
                        <li>
                            <a href="/">Strona Główna</a>
                            <span>»</span>
                        </li>
                        <li>
                            <?= get_the_title(); ?>
                        </li>
                        
                    </ul>
                </div>
            </div>
        </div>
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/effect-single-page.png" class="single-page-effect hide-now">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/effekt-single-page-small.png" class="single-page-effect single-page-effect-small">
    </div>
    <div class="content-single-page">
        <div class="container">
            <div class="content-inside-wrapper">
                <div class="section-first anim" data-vp-add-class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="left">
                                <?php if ($fields['sekcja_pierwsza_-_tekst']) {
                                    ?>
                                    <?= $fields['sekcja_pierwsza_-_tekst'] ?>
                                <?php }
                                ?>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="right">
                                <?php if ($fields['sekcja_pierwsza_-_zdjecie']) {
                                    ?>
                                    <?= wp_get_attachment_image($fields['sekcja_pierwsza_-_zdjecie']['ID'], 'oferta-img'); ?>
                                <?php }
                                ?>
                                <div class="triangle">

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="section-second anim" data-vp-add-class="animated fadeIn">
                    <div class="title-section">
                        <?php if ($fields['sekcja_druga_-_tytul']) {
                            ?>
                            <?= $fields['sekcja_druga_-_tytul'] ?>
                        <?php }
                        ?>
                    </div>
                    <div class="flex-centering">
                        <div class="row  text-center col-md-12">
                            <?php
                            //juz

                            $args = array('post_type' => 'drzwi', 'posts_per_page' => -1);
                            $loop = new WP_Query($args);
                            while ($loop->have_posts()) : $loop->the_post();

                                $fields = get_fields(get_the_ID());
                                ?>

                                <div class="normal-wrapper">

                                    <a href="<?php the_permalink(); ?>">
                                        <div class="wrapper-for-all">
                                            <?php
                                            the_post_thumbnail('parapety-img');
                                            ?>
                                            <span><?= the_title() ?></span>
                                        </div>
                                    </a>

                                </div> 

                                <?php
                            endwhile;
                            wp_reset_query();
                            ?>
                        </div>
                    </div>
                    <?php
                    $fields = get_fields(get_the_ID());
                    ?>
                    <div class="section-first special-section-first">
                        <div class="row">

                            <div class="col-md-5">
                                <div class="right">
                                    <?php if ($fields['sekcja_druga_-_zdjecie']) {
                                        ?>
                                        <?= wp_get_attachment_image($fields['sekcja_druga_-_zdjecie']['ID'], 'oferta-img'); ?>
                                    <?php }
                                    ?>
                                    <div class="triangle-after">

                                    </div>

                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="left">
                                    <?php if ($fields['sekcja_druga_-_tekst']) {
                                        ?>
                                        <?= $fields['sekcja_druga_-_tekst'] ?>
                                    <?php }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <?php include_once "call-to-action.php"; ?>
    <?php include_once "partnerzy.php"; ?>

</div>

<?php
get_footer();

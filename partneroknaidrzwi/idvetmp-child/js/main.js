

// Get IE or Edge browser version
var version = detectIE();

if (version === false) {

} else if (version >= 12) {
    alert('test2');
} else {

    var hamburgerx1 = document.querySelector(".main-btn h1");
    $('.main-btn h1').addClass('cf');
    $('.third-section__single-wrapper h2 a').addClass('cf-black');
    $('.third-section__single-wrapper h2').addClass('cf-black');
    $('.fourth-section__title a').addClass('cf-black');
    $('.single-wrapper__text-section h1 a').addClass('cf-black');
    $('.single-wrapper__text-section h1').addClass('cf-black');
    $('.call-to-action__wrapper-inside h2').addClass('cf');
    $('.partners .title h2').addClass('cf-black');

}



function detectIE() {
    var ua = window.navigator.userAgent;

    // Test values; Uncomment to check result …

    // IE 10
    // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';

    // IE 11
    // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';

    // Edge 12 (Spartan)
    // ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';

    // Edge 13
    // ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
        // Edge (IE 12+) => return version number
        return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }
    return false;
}


function setCookie(c_name, value, exdays)
{
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = c_name + "=" + c_value;

}


function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');

    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0)
            return c.substring(nameEQ.length, c.length);
    }
    return null;
}
var cook = readCookie('cookies-accepted');

if (($("#cookie-div").length > 0)) {
    if (cook) {
        document.getElementById('cookie-div').style.display = "none";
    } else {
        document.getElementById('cookie-div').style.display = "block";
    }
}
function readcookie() {
    document.getElementById('cookie-div').style.display = "none";
    setCookie('cookies-accepted', 'true', 356);
}


jQuery(function ($) {
    if ($(window).width() > 769) {
        $('.navbar .dropdown').hover(function () {
            $(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();
        }, function () {
            $(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();
        });
        $('.navbar .dropdown > a').click(function () {
            location.href = this.href;
        });
    }
});

$('#porownaj').on('shown.bs.modal', function (e) {

    $(".wrapper-for-scroll-new").niceScroll(".wrapper-for-scroll-inside", {
        cursorcolor: "#d2d2d2",
        cursorwidth: "11px",
        autohidemode: false});


    $(".test1").niceScroll(".test2", {
        cursorcolor: "#d2d2d2",
        cursorwidth: "11px",
        autohidemode: false});

}).on('hide.bs.modal', function (e) {

    $(".wrapper-for-scroll").niceScroll().remove();
//    $(".test1").niceScroll().remove();
    //bModalBody.niceScroll().remove();
});


$(document).ready(function () {
    $('.okna-swiper .swiper-slide img').click(function () {

        var getOkno = $(this).attr('src');

        $(".gallery-top img").attr("src", getOkno);
//        $(".gallery-top .swiper-slide a").attr("href", getOkno);
    });
    $('.dropdown').click(function () {

        if ($(this).hasClass("dropdown")) {

            $(this).removeClass("dropdown");

        }
    });
});

var galleryTop = new Swiper('.gallery-top', {
//    autoplay: 5000,
    // loop: true,
    loop: true,
    slidesPerView: 1,
//    pagination: true,
//    spaceBetween: 10,
//    centeredSlides: true,
//      slidesPerView: 'auto',

    navigation: {
        nextEl: '.y1',
        prevEl: '.y2'
    },
});
var oknaSwiper = new Swiper('.okna-swiper', {
    autoplay: 5000,
    // loop: true,
    loop: true,
    pagination: true,
    initialSlide: 0,
    slidesPerView: 3,
    pagination: true,
    spaceBetween: 10,
    centeredSlides: true,
//      slidesPerView: 'auto',
    touchRatio: 0.2,
    slideToClickedSlide: true,

    navigation: {
        nextEl: '.z1',
        prevEl: '.z2'
    },
    breakpoints: {
        480: {
            slidesPerView: 1,
        },
        640: {
            slidesPerView: 2,
        }
    },
    on: {
        slideChange: function () {
            galleryTop.slideTo(this.realIndex + 1);
        }
    }
});








var swiper = new Swiper('.partners-swiper', {
    dots: false,
    slidesPerView: 7,
    centerSlides: true,
    loop: true,
    autoplay: {
        delay: 2000,
    },
    navigation: {
        nextEl: '.p1',
        prevEl: '.p2',
    },
    breakpoints: {
        // when window width is <= 320px
        480: {
            slidesPerView: 1,
//            spaceBetween: 10
        },
        // when window width is <= 480px
        640: {
            slidesPerView: 2,
//            spaceBetween: 20
        },
        // when window width is <= 640px
        991: {
            slidesPerView: 3,
//            spaceBetween: 30
        },
        1200: {
            slidesPerView: 4,
//            spaceBetween: 30
        },
        1500: {
            slidesPerView: 5,
//            spaceBetween: 30
        }
    }
});
var mySwiper5 = new Swiper('.swiper-awards', {
    autoplay: 5000,
    loop: true,
    pagination: true,
    slidesPerView: 1,
    navigation: {
        nextEl: '.awards-btn-next',
        prevEl: '.awards-btn-prev'
    },
    breakpoints: {
        480: {
            slidesPerView: 1,
        },
        640: {
            slidesPerView: 2,
        },
        980: {
            slidesPerView: 3,
        }
    }
});
var mySwiper22 = new Swiper('.modal-swiper', {
    autoplay: 5000,
    observer: true,
    observeParents: true,
    loop: true,
    pagination: true,
    slidesPerView: 3,
    navigation: {
        nextEl: '.m1',
        prevEl: '.m2'
    },
    breakpoints: {
        550: {
            slidesPerView: 1,
        },
        980: {
            slidesPerView: 2,
        }
    }
});
var mySwiper5 = new Swiper('.fourth-swiper', {
    autoplay: 5000,
    // loop: true,
    slidesPerView: 3,
    navigation: {
        nextEl: '.z1',
        prevEl: '.z2'
    },
    loop: true,
    pagination: true,
    breakpoints: {
        // when window width is <= 320px
        480: {
            slidesPerView: 1,
//            spaceBetween: 10
        },
        // when window width is <= 480px
        640: {
            slidesPerView: 2,
//            spaceBetween: 20
        }
    }
});
var mySwiper5 = new Swiper('.fifth-swiper', {
    autoplay: 5000,
    // loop: true,
    slidesPerView: 4,
    loop: true,
    pagination: true,
    navigation: {
        nextEl: '.b1',
        prevEl: '.b2',
    },
    breakpoints: {
        480: {
            slidesPerView: 1,
        },
        991: {
            slidesPerView: 2,
        }
    }
});
var mySwiper5 = new Swiper('.drzwi-swiper', {
    autoplay: 5000,
    // loop: true,
    slidesPerView: 4,
    loop: true,
    pagination: true,
    navigation: {
        nextEl: '.a1',
        prevEl: '.a2',
    },
    breakpoints: {
        480: {
            slidesPerView: 1,
        },
        640: {
            slidesPerView: 2,
        }
    }
});

var mySwiper5 = new Swiper('.okna-bottom-swiper', {
    autoplay: 5000,
    // loop: true,
    slidesPerView: 4,
    loop: true,
    pagination: true,
    navigation: {
        nextEl: '.s1',
        prevEl: '.s2'
    },
    breakpoints: {
        480: {
            slidesPerView: 1,
        },
        640: {
            slidesPerView: 2,
        },
        980: {
            slidesPerView: 3,
        }
    }
});

var mySwiper5 = new Swiper('.parapety-swiper', {
    autoplay: 5000,
    // loop: true,
    slidesPerView: 4,
    loop: true,
    pagination: true,
    navigation: {
        nextEl: '.pp1',
        prevEl: '.pp2'
    },
    breakpoints: {
        480: {
            slidesPerView: 1,
        },
        640: {
            slidesPerView: 2,
        },
        980: {
            slidesPerView: 3,
        }
    }
});
$(window).scroll(function (event) {
    var scroll = $(window).scrollTop();
    if (scroll > 30) {
        $(".navbar-default").addClass('up');
    } else {
        $(".navbar-default").removeClass('up');
    }
});


function getInternetExplorerVersion()
{
    var rv = -1;
    if (navigator.appName == 'Microsoft Internet Explorer')
    {
        var ua = navigator.userAgent;
        var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
        if (re.exec(ua) != null)
            rv = parseFloat(RegExp.$1);
    } else if (navigator.appName == 'Netscape')
    {
        var ua = navigator.userAgent;
        var re = new RegExp("Trident/.*rv:([0-9]{1,}[\.0-9]{0,})");
        if (re.exec(ua) != null)
            rv = parseFloat(RegExp.$1);
    }
    return rv;
}

console.log('IE version:', getInternetExplorerVersion());
function replaceAttribute() {
//    const attr = 'href';
//    const firstLinks = [].slice.call(document.querySelectorAll('.home .single-wrapper__text-section .btn'));
//    
//    const secondLinks = document.querySelectorAll('#test');
//    const urlList = firstLinks.map(link => link.getAttribute(attr));
//    secondLinks.forEach((link, i) => link.setAttribute(attr, urlList[i])); 
}
$(document).ready(function () {
    var scroll = $(window).scrollTop();
    if (scroll > 35) {
        $(".navbar-default").addClass('up');
    } else {
        $(".navbar-default").removeClass('up');
    }

    if (scroll > 35) {
        $(".navbar-default").addClass('up');
    } else {
        $(".navbar-default").removeClass('up');
    }

    msieversion();
    replaceAttribute();
    _removeClasses();
});


//function replaceHeadlings () {
//    const allBigHeadlings = [...document.getElementByClassName(".home .single-wrapper__text-section .btn").getAttribute("href")];
//    const allSmallHeadlings = [...document.getElementById("test").getAttribute("href")];
//
//  allBigHeadlings.forEach((headling, i) => {
//    allSmallHeadlings[i].toString().getAttribute("href") = setAttribute("href", allBigHeadlings);
//  })
//}






$(document).on('click', '[data-toggle="lightbox"]', function (event) {
    event.preventDefault();
    $(this).ekkoLightbox({
        alwaysShowClose: true
    });
});
$(document).ready(function () {


    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        $('.anim').addClass('anim-mobile');
    } else {
        $('.anim').viewportChecker();
    }

    $('.zero').viewportChecker({
        classToAdd: 'nowa'
    });
    $('.margin-wrapp').viewportChecker({
        classToAdd: 'nowa'
    });
});




var hamburger = document.querySelector(".hamburger");
var hamburger2 = document.querySelector(".navbar");
var hamburger3 = document.querySelector("#navbar");
var hamburger4 = document.querySelector("html");
var hamburger5 = document.querySelector("body");
// On click
hamburger.addEventListener("click", function () {
    hamburger.classList.toggle("is-active");
    hamburger2.classList.toggle("red");
    hamburger3.classList.toggle("red");
    hamburger4.classList.toggle("overflowHidden");
    hamburger5.classList.toggle("overflowVisible");
});



(function () {
    // trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
    if (!String.prototype.trim) {
        (function () {
            // Make sure we trim BOM and NBSP
            var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
            String.prototype.trim = function () {
                return this.replace(rtrim, '');
            };
        })();
    }

    [].slice.call(document.querySelectorAll('input.input__field')).forEach(function (inputEl) {
        // in case the input is already filled..
        if (inputEl.value.trim() !== '') {
            classie.add(inputEl.parentNode, 'input--filled');
        }

        // events:
        inputEl.addEventListener('focus', onInputFocus);
        inputEl.addEventListener('blur', onInputBlur);
    });

    function onInputFocus(ev) {
        classie.add(ev.target.parentNode, 'input--filled');
    }

    function onInputBlur(ev) {
        if (ev.target.value.trim() === '') {
            classie.remove(ev.target.parentNode, 'input--filled');
        }
    }
})();

$(".first-section .btn").click(function () {
    $('html, body').animate({
        scrollTop: $(".second-section").offset().top - 136
    }, 900);
});



mapboxgl.accessToken = 'pk.eyJ1IjoieGVvc25hciIsImEiOiJjamhscDdyYzkwb3l0MzBxa3B3YmJlbTUwIn0.gOexVH7Xc-e-0GNxhd1vSg';

if (typeof (lat) == "undefined") {
    lat = 53.7671353;
}
if (typeof (lng) == "undefined") {
    lng = 20.499327;
}

var map = new mapboxgl.Map({
    container: 'map',
    style: '/wp-content/themes/idvetmp-child/osm.json',
    center: [lng, lat],
    zoom: 16,
    hash: false
});


var geojson = {
    type: 'FeatureCollection',
    features: [{
            type: 'Feature',
            geometry: {
                type: 'Point',
                coordinates: [lng, lat]
            },
            properties: {
                title: 'Mapbox',
                description: 'Washington, D.C.'
            }
        }]
};

// add markers to map
geojson.features.forEach(function (marker) {
    var el = 0;
    // create a HTML element for each feature
    var el = document.createElement('div');
    el.className = 'marker';

    // make a marker for each feature and add to the map
    new mapboxgl.Marker(el)
            .setLngLat(marker.geometry.coordinates)
            .addTo(map);
});




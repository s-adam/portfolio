<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*
 * Template Name: Drzwi - Rodzaje
 * Template Post Type: drzwi
 */
get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>



<div class="single-page-wrapper drzwi-rodzaje">
    <div class="header-wrapper" style="background: url(<?= $fields['header-photo']['sizes']['blog-img-big'] ?>) top 0 center no-repeat; background-size: cover;">
        <div class="wrapper-for-header-info">
            <div class="container">
                <div class="section-title-archive">
                    <h2><?= the_title(); ?></h2>
                    <ul class="breadcrumbs">
                        <li>
                            <a href="/">Strona Główna</a>
                            <span>»</span>
                        </li>
                        <li>
                            <a href="<?= the_permalink(288); ?>"><?= get_the_title(288); ?></a>
                            <span>»</span>
                        </li>
                        <li>
                            <?= the_title(); ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/effect-single-page.png" class="single-page-effect hide-now">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/effekt-single-page-small.png" class="single-page-effect single-page-effect-small">
    </div>
    <div class="content-single-page">
        <div class="container">
            <div class="content-inside-wrapper">
                <div class="section-first">
                    <div class="row">

                        <div class="col-md-12">
                            <div class="left">
                                <?php if ($fields['sekcja_pierwsza_-_tekst']) {
                                    ?>
                                    <?= $fields['sekcja_pierwsza_-_tekst'] ?>
                                <?php }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section-second">


                    <div class="parapety-section">
                        <?php
                        if ($fields['drzwi']) {
                            $i = 0;
                            foreach ($fields['drzwi'] as $field) {
                                ?>

                                <div class="row">
                                    <div class="normal-wrapper">
                                        <div class="col-md-4">
                                            <?= wp_get_attachment_image($field['zdjecie']['ID'], 'small-img'); ?>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="wrao">
                                                <a target="_blank" href="<?= $field['url'] ?>"><h3><?= $field['nazwa'] ?></h3></a>
                                                    <?= $field['tekst'] ?>
                                                <a target="_blank" class="btn" href="<?= $field['url'] ?>">Pobierz Katalog (PDF)</a>
                                            </div>
                                        </div>   
                                    </div> 
                                </div>

                                <?php
                                $i++;
                            }
                        }
                        ?>
                    </div>
                </div>
                <div class="wrapper-for-swiper-width">
                    <div class="section-fourth">
                        <!--<div class="container">-->
                        <div class="drzwi-swiper swiper-container text-center">
                            <!-- Additional required wrapper -->
                            <div class="swiper-wrapper">
                                <!-- Slides -->
                                <?php
                                $variable = $fields['slider'];
                                if ($variable) {
                                    foreach ($variable as $field) {
                                        ?>
                                        <div class="swiper-slide">
                                            <a href="<?= $field['zdjecie']['sizes']['lightbox-img'] ?>" data-toggle="lightbox" data-gallery="drzwi1-example-gallery" data-footer="<?= $field['url']; ?>"><?= wp_get_attachment_image($field['zdjecie']['ID'], 'slider-img'); ?></a>
                                            <span><a href="<?= $field['zdjecie']['sizes']['lightbox-img'] ?>" data-toggle="lightbox" data-gallery="drzwi2-example-gallery" data-footer="<?= $field['url']; ?>"><?= $field['url']; ?></a></span>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <?php
                        if ($variable) {
                            ?>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next a1"></div>
                            <div class="swiper-button-prev a2"></div>
                            <!--</div>-->
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include_once "call-to-action.php"; ?>
    <?php include_once "partnerzy.php"; ?>

</div>

<?php
get_footer();

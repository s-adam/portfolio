<?php

function idvetmpchild_scripts() {
    //wp_enqueue_style( 'font-opensans', 'https://fonts.googleapis.com/css?family=Montserrat:400,600,700&amp;subset=latin-ext" rel="stylesheet', array( 'idvetmp-style' ));
    wp_enqueue_style('swiper', get_template_directory_uri() . '/library/swiper/swiper.min.css', array('idvetmp-style'));
    wp_enqueue_style('idvetmp-style', get_stylesheet_directory_uri() . '/main.css');
}

add_action('wp_enqueue_scripts', 'idvetmpchild_scripts');

function idvetmp_scriptschild() {
    wp_enqueue_script('swiper-js', get_template_directory_uri() . '/library/swiper/swiper.min.js', array('jquery'), '20151215', true);
    
    wp_enqueue_script('nicescroll-js', get_stylesheet_directory_uri() . '/js/jquery.nicescroll.min.js', array('jquery'), '20180515', true);

    wp_enqueue_script('viewportchecker', get_stylesheet_directory_uri() . '/js/jquery.viewportchecker.js', array('jquery', 'bootstrap-js'), '20151215', true);
        wp_enqueue_script('main-js', get_stylesheet_directory_uri() . '/js/main.js', array('jquery', 'bootstrap-js'), '20181215', true);
}

add_action('wp_enqueue_scripts', 'idvetmp_scriptschild');


add_image_size('oferta-img', 380, 270, false); //wpisu na blogu
add_image_size('blog-img-big', 1920, 1200, false); //wpisu na blogu - duża grafika po wejściu na wpis
//add_image_size('blog-img-thumb-mini', 104, 99, true); //miniaturka wpisu na blogu

add_image_size('parapety-img', 230, 160, false); //autor opinii
//add_image_size('parapety-img', 230, 160, false); //autor opinii

add_image_size('lightbox-img', 1200, 800, false); //autor opinii

add_image_size('header-img-desktop', 2500, 1500, false); //nagłówek strony, wersja na komputer



add_image_size('main-img', 549, 497, false); //autor opinii

add_image_size('small-img', 375, 300, false); //autor opinii




add_image_size('slider-img', 151, 150, false); //autor opinii

add_image_size('realizacje-img', 200, 150, true); //autor opinii


function numeric_posts_nav() {

    if (is_singular())
        return;

    global $wp_query;

    /** Stop execution if there's only 1 page */
    if ($wp_query->max_num_pages <= 1)
        return;

    $paged = get_query_var('paged') ? absint(get_query_var('paged')) : 1;
    $max = intval($wp_query->max_num_pages);

    /**    Add current page to the array */
    if ($paged >= 1)
        $links[] = $paged;

    /**    Add the pages around the current page to the array */
    if ($paged >= 3) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if (( $paged + 2 ) <= $max) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo '<div class="navigation"><ul>' . "\n";

    /**    Previous Post Link */
    if (get_previous_posts_link())
        printf('<li class="first">%s</li>' . "\n", get_previous_posts_link('&laquo;'));

    /**    Link to first page, plus ellipses if necessary */
    if (!in_array(1, $links)) {
        $class = 1 == $paged ? ' class="active"' : '';

        printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link(1)), '1');

        if (!in_array(2, $links))
            echo '<li>…</li>';
    }

    /**    Link to current page, plus 2 pages in either direction if necessary */
    sort($links);
    foreach ((array) $links as $link) {
        $class = $paged == $link ? ' class="active"' : '';
        printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link($link)), $link);
    }

    /**    Link to last page, plus ellipses if necessary */
    if (!in_array($max, $links)) {
        if (!in_array($max - 1, $links))
            echo '<li>…</li>' . "\n";

        $class = $paged == $max ? ' class="active"' : '';
        printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link($max)), $max);
    }

    /**    Next Post Link */
    if (get_next_posts_link())
        printf('<li class="last">%s</li>' . "\n", get_next_posts_link('»'));

    echo '</ul></div>' . "\n";
}

//
add_action('pre_get_posts', function ( $q ) {
    if (!is_admin() && $q->is_main_query() && $q->is_post_type_archive('aktualnosci')
    ) {
        $q->set('posts_per_page', 3);
        $q->set('orderby', 'modified');
    }
});


add_action('pre_get_posts', function ( $q ) {
    if (!is_admin() && $q->is_main_query() && $q->is_post_type_archive('galeria')
    ) {
        $q->set('posts_per_page', 16);
        $q->set('orderby', 'modified');
    }
});

//TINY MC BUTTON

add_action('admin_head', 'add_my_tc_button');

function add_my_tc_button() {
    global $typenow;

    if ( !current_user_can('edit_posts') && !current_user_can('edit_pages') ) {
        return;
    }


    if ( get_user_option('rich_editing') == 'true') {
        add_filter("mce_external_plugins", "add_tinymce_plugin");
        add_filter('mce_buttons', 'register_my_tc_button');
    }
}

function add_tinymce_plugin($plugin_array) {
    $plugin_array['gavickpro_tc_button'] =  '/wp-content/themes/idvetmp-child/js/tinymce-buttons.js';
    return $plugin_array;
}

function register_my_tc_button($buttons) {
    array_push($buttons, "gavickpro_tc_button");
    return $buttons;
}


function my_acf_init() {
	
	acf_update_setting('google_api_key', 'AIzaSyDT5ONMSnq3pV_L5VlenZYFv4I-UBlVHDo');
}

add_action('acf/init', 'my_acf_init');


function post_remove ()      //creating functions post_remove for removing menu item
{ 
   remove_menu_page('edit.php');
}

add_action('admin_menu', 'post_remove'); 

/*
function prefix_change_cpt_archive_per_page( $query ) {

    //* for cpt or any post type main archive
    if ( $query->is_main_query() && ! is_admin() && is_post_type_archive( 'galeria' ) ) {
        $query->set( 'posts_per_page', '1' );
    }

}
add_action( 'pre_get_posts', 'prefix_change_cpt_archive_per_page' );
*/

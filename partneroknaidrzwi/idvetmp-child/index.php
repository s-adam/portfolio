<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*
  Template Name: Strona Główna
 */
$fields = get_fields(get_the_ID());
get_header();
?>
<div class="main-wrapper">
    <div class="first-section" style="background: url(<?php echo get_stylesheet_directory_uri(); ?>/img/bg_first_section.jpg)">
        <div class="container">
            <div class="first-section-wrapper">
                <div class="row">
                    <div class="flex-box">
                        <div class="col-md-6">

                            <div class="box-center">
                                <div class="main-btn">
                                    <?= $fields['sekcja_pierwsza_-_tekst'] ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <?= wp_get_attachment_image($fields['sekcja_pierwsza_-_zdjecie']['ID'], 'main-img'); ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="second-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <?php
// check if the flexible content field has rows of data
                    if (have_rows('sekcja_druga_-_oferta')):

                        // loop through the rows of data
                        while (have_rows('sekcja_druga_-_oferta')) : the_row();

                            if (get_row_layout() == 'tekst_+_zdjecie_po_prawej_stronie'):
                                ?>

                                <div class="single-wrapper single-wrapper-text-left">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="single-wrapper__text-section">
                                                <?= the_sub_field('tekst'); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="single-wrapper__photo-section">
                                                <a id="test" href=""><?php
                                                $image = get_sub_field('zdjecie');
                                                echo '<img src="' . $image['sizes']['main-img'] . '" />';
                                                ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>    
                                <?php
                            elseif (get_row_layout() == 'tekst_+_zdjecie_po_lewej_stronie'):
                                ?>
                                <div class="single-wrapper single-wrapper-text-right">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="single-wrapper__photo-section">
                                               <a id="test" href=""><?php
                                                $image = get_sub_field('zdjecie');
                                                echo '<img src="' . $image['sizes']['main-img'] . '" />';
                                                ?></a>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="single-wrapper__text-section">
                                                <?= the_sub_field('tekst'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            endif;

                        endwhile;

                    else :

                    // no layouts found

                    endif;
                    ?>




                </div>
            </div>
        </div>
    </div>
    <div class="third-section" style="background: url(<?php echo get_stylesheet_directory_uri(); ?>/img/bg_second_section.jpg)">
        <div class="container">

            <?php
            if ($fields['sekcja_druga_-_kafelki']) {
                $i = 0;
                $j = 0;
                foreach ($fields['sekcja_druga_-_kafelki'] as $tym) {
                    if ($i == 0) {
                        echo '<div class="row">';
                    }
                    ?>

                    <div class="col-md-4">
                        <div class="third-section__wrapp">
                            <div class="third-section__single-wrapper" style="background: url(<?= $tym['ikona']['sizes']['small-img'] ?>) top right #fff no-repeat">
                                <?= $tym['tresc'] ?>
                                <?= $tym['zdjecie']['sizes']['main-img'] ?>
                            </div>
                        </div>
                    </div>
                    <?php
                    $i++;
                    $j++;
                    if ($i == 3 || count($fields['sekcja_druga_-_kafelki']) == $j) {

                        echo '</div>';

                        $i = 0;
                    }
                }
            }
            ?>


        </div>
    </div>
    <div class="fourth-section">
        <img class="effect" src="<?php echo get_stylesheet_directory_uri(); ?>/img/effect-3.png" alt="">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="fourth-section__single-wrapper">
                        <div class="fourth-section__title">
                            <a href="/realizacje">REALIZACJE</a>
                        </div>
                        <?php
                        $args = array('post_type' => 'galeria', 'posts_per_page' => 1);

                        $loop = new WP_Query($args);
                        if ($loop->have_posts()) {

                            while ($loop->have_posts()) : $loop->the_post();
                                ?>
                                <div class="fourth-section__photo">
                                    <?= the_post_thumbnail('main-img'); ?>
                                    <a class="btn btn-white" href="/realizacje">ZOBACZ WSZYSTKIE</a>
                                </div>
                                <?php
                            endwhile;
                        }
                        ?>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="fourth-section__single-wrapper pull-right">
                        <div class="fourth-section__title">
                            <a href="/aktualnosci">AKTUALNOŚCI</a>
                        </div>
                        <?php
                        $args = array('post_type' => 'aktualnosci', 'posts_per_page' => 1);

                        $loop = new WP_Query($args);
                        if ($loop->have_posts()) {

                            while ($loop->have_posts()) : $loop->the_post();
                                ?>
                                <div class="fourth-section__text-wrapper">
                                    <span class="title"><a href="<?php the_permalink(); ?>"><?= the_title() ?></a></span>
                                    <?php the_excerpt();?>
                                    <p>&nbsp;</p>
                                    <a class="btn" href="/aktualnosci">ZOBACZ WSZYSTKIE</a>
                                </div>
                                <?php
                            endwhile;
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <?php include_once "partnerzy.php"; ?>



</div>
<?php
//get_sidebar();
get_footer();

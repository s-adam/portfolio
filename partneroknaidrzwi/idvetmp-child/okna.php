<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*
 * Template Name: Okna - Główna
 */
get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>



<div class="single-page-wrapper okna">
    <div class="header-wrapper" style="background: url(<?= $fields['header-photo']['sizes']['blog-img-big'] ?>) top 0 center no-repeat; background-size: cover;">
        <div class="wrapper-for-header-info">
            <div class="container">
                <div class="section-title-archive">
                    <h2><?= the_title(); ?></h2>
                    <ul class="breadcrumbs">
                        <li>
                            <a href="/">Strona Główna</a>
                            <span>»</span>
                        </li>
                        <li>
                            <?= get_the_title(); ?>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/effect-single-page.png" class="single-page-effect hide-now">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/effekt-single-page-small.png" class="single-page-effect single-page-effect-small">
    </div>
    <div class="content-single-page">
        <div class="container">
            <div class="content-inside-wrapper">
                <div class="section-first new-section-first">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="left">
                                <?php if ($fields['sekcja_pierwsza_-_tekst']) {
                                    ?>
                                    <?= $fields['sekcja_pierwsza_-_tekst'] ?>
                                <?php }
                                ?>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="right">
                                <?php if ($fields['sekcja_pierwsza_-_zdjecie']) {
                                    ?>
                                    <?= wp_get_attachment_image($fields['sekcja_pierwsza_-_zdjecie']['ID'], 'oferta-img'); ?>
                                <?php }
                                ?>
                                <div class="triangle">

                                </div>

                            </div>
                        </div>
                    </div>
                </div>




                <div class="section-second">
                    <div class="title-section">
                        <?php if ($fields['sekcja_druga_-_tytul']) {
                            ?>
                            <?= $fields['sekcja_druga_-_tytul'] ?>
                        <?php }
                        ?>
                    </div>
                    <div class="flex-centering">
                        <div class="row  text-center col-md-12">
                            <?php
                            //juz

                            $args = array('post_type' => 'okna', 'posts_per_page' => -1);
                            $loop = new WP_Query($args);
                            while ($loop->have_posts()) : $loop->the_post();

                                $fields = get_fields(get_the_ID());
                                ?>

                                <div class="normal-wrapper">

                                    <a href="<?php the_permalink(); ?>">
                                        <div class="wrapper-for-all">
                                            <?php
                                            the_post_thumbnail('parapety-img');
                                            ?>
                                            <span><?= the_title() ?></span>
                                        </div>
                                    </a>

                                </div> 

                                <?php
                            endwhile;
                            wp_reset_query();
                            ?>
                        </div>
                    </div>

                    <?php
                    $fields = get_fields(get_the_ID());
                    ?>
                    <?php if ($fields['porownywarka_-_tekst']) { ?>
                        <div class="text-area-porownaj">
                            <?= $fields['porownywarka_-_tekst'] ?>
                            <span>
                                <a class="btn" href="<?= $fields['porownywarka_-_przycisk_url'] ?>" data-toggle="modal" data-target="#porownaj">

                                    <?= $fields['porownywarka_-_przycisk_tekst'] ?>
                                </a>
                            </span>
                        </div>
                    <?php } ?>

                    <div class="modal fade" id="porownaj" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content" id="modal-content">
                                <div class="modal-header">
                                    <!--                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>-->
                                    <span class="close-now" data-dismiss="modal" aria-label="Close"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/close.png" ></span>
                                </div>

                                <div class="modal-body">


                                    <?php
                                    $fields = get_field('porownywarka_2');

                                    $cechy = array();
                                    $oknaCechy = array();

                                    $idOkno = 0;

                                    if ($fields['okno']) {
                                        foreach ($fields['okno'] as $okno) {

                                            if ($okno['cechy_okna']) {
                                                foreach ($okno['cechy_okna'] as $cecha) {
                                                    $cechy[$cecha['cecha']['value']] = $cecha['cecha']['label'];
                                                    $oknaCechy[$idOkno][$cecha['cecha']['value']] = $cecha['opis_cechy'];
                                                }
                                            }

                                            $idOkno++;
                                        }
                                    }
                                    ?>


                                    <div class="row first-row hide-slider">
                                        <div class="col-md-12">
                                            <div class="modal-swiper swiper-container">
                                                <!-- Additional required wrapper -->
                                                <div class="swiper-wrapper text-center">


                                                    <?php
                                                    $idOkno = 0;
                                                    if ($fields['okno']) {
                                                        foreach ($fields['okno'] as $okno) {
                                                            ?>

                                                            <div class="swiper-slide">
                                                                <div class="row">
                                                                    <div class="simple-wrapper-for-modal">
                                                                        <div class="col-md-12">

                                                                            <div class="row">

                                                                                <div class="col-md-12 text-center">
                                                                                    <div class="ww">
                                                                                        <img src="<?= $okno['zdjecie_okna']['sizes']['slider-img'] ?>">
                                                                                    </div>
                                                                                    <span><?= $okno['nazwa_okna'] ?></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="wrapper-for-scroll-new">
                                                                        <div class="wrapper-for-scroll-inside">

                                                                            <?php
                                                                            if ($cechy) {

                                                                                foreach ($cechy as $key => $c) {
                                                                                    ?>

                                                                                    <div class="wrapper-for-opis-and-content">
                                                                                        <div class="opis">
                                                                                            <?= $c ?>
                                                                                        </div>
                                                                                        <div class="content-in-modal">
                                                                                            <?= $oknaCechy[$idOkno][$key] ?>
                                                                                        </div>
                                                                                    </div>
                                                                                    <?php
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </div>    
                                                                    </div>            
                                                                </div>                                                        
                                                            </div>

                                                            <?php
                                                            $idOkno++;
                                                        }
                                                    }
                                                    ?>


                                                </div>
                                                <div class="swiper-button-next m1"></div>
                                                <div class="swiper-button-prev m2"></div>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="first-row normal-modal">
                                        <div class="simple-wrapper-for-modal">
                                            <div class="col-md-12">

                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <div class="opis">

                                                        </div>
                                                    </div>
                                                    <?php
                                                    if ($fields['okno']) {
                                                        foreach ($fields['okno'] as $tym) {
                                                            ?>
                                                            <div class="col-md-3 text-center">
                                                                <div class="ww">
                                                                    <!--<a id="test" href="">-->
                                                                        <img src="<?= $tym['zdjecie_okna']['sizes']['slider-img']; ?>">
                                                                    <!--</a>-->
                                                                </div>
                                                                <span><?= $tym['nazwa_okna'] ?></span>
                                                            </div>
                                                            <?php
                                                        }
                                                    }
                                                    ?>


                                                </div>
                                            </div>
                                        </div>


                                        <div class="test1" style="width: 100%">
                                            <div class="test2">
                                                <?php
                                                if ($cechy) {
                                                    foreach ($cechy as $key => $cecha) {
                                                        ?>

                                                        <div class="nowy-wrapper">
                                                            <div class="row first-row">

                                                                <div class="col-md-2">
                                                                    <div class="opis">
                                                                        <?= $cecha ?>
                                                                    </div>
                                                                </div>


                                                                <?php
                                                                if ($oknaCechy) {
                                                                    foreach ($oknaCechy as $c) {
                                                                        ?>

                                                                        <div class="col-md-3">
                                                                            <div class="www">
                                                                                <?= $c[$key] ?>
                                                                            </div>
                                                                        </div>

                                                                        <?php
                                                                    }
                                                                }
                                                                ?>



                                                            </div>
                                                        </div>

                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>


                                        <div class="wrapper-for-scroll" style="display: none;">
                                            <div class="wrapper-for-scroll-inside">





                                                <?php
                                                if ($cechy) {
                                                    foreach ($cechy as $key => $cecha) {
                                                        ?>

                                                        <div class="simple-wrapper-for-modal-color">


                                                            <div class="col-md-12">
                                                                <div class="row first-row">

                                                                    <div class="col-md-2">
                                                                        <div class="opis">
                                                                            <?= $cecha ?>
                                                                        </div>
                                                                    </div>


                                                                    <?php
                                                                    if ($oknaCechy) {
                                                                        foreach ($oknaCechy as $c) {
                                                                            ?>

                                                                            <div class="col-md-3">
                                                                                <div class="www">
                                                                                    <?= $c[$key] ?>
                                                                                </div>
                                                                            </div>

                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>



                                                                </div>
                                                            </div>


                                                        </div>

                                                        <?php
                                                    }
                                                }
                                                ?>

                                            </div>
                                        </div>
                                    </div>






                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="section-first section-first-news special-section-first">
                        <div class="row">
                            <?php
                            $fields = get_fields(get_the_ID());
                            ?>
                            <div class="col-md-5">
                                <div class="right">
                                    <?php if ($fields['sekcja_druga_-_zdjecie']) {
                                        ?>
                                        <?= wp_get_attachment_image($fields['sekcja_druga_-_zdjecie']['ID'], 'oferta-img'); ?>
                                    <?php }
                                    ?>
                                    <div class="triangle-after">

                                    </div>

                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="left">
                                    <?php if ($fields['sekcja_druga_-_tekst']) {
                                        ?>
                                        <?= $fields['sekcja_druga_-_tekst'] ?>
                                    <?php }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <?php include_once "call-to-action.php"; ?>
    <?php include_once "partnerzy.php"; ?>
    <script>

        var lat = 2;
        var lng = 23;
    </script>
</div>

<?php
get_footer();

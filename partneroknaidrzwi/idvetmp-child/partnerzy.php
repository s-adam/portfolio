<div class="partners">
    <div class="container">
        <div class="title">
            <h2>PARTNERZY</h2>
        </div>
        <div class="partners-swiper swiper-container">
            <!-- Additional required wrapper -->
            <div class="swiper-wrapper text-center">
                <!-- Slides -->
                <?php
                $variable = get_field('partnerzy', 'option');
                if ($variable) {
                    foreach ($variable as $field) {
                        if (!empty($field['adres_url'])) {
                            ?>
                            <div class="swiper-slide">
                                <a target="_blank"href="<?= $field['adres_url']; ?>"><?= wp_get_attachment_image($field['logo']['ID'], 'slider-img'); ?></a>
                            </div>
                            <?php
                        } else {
                            ?>    

                            <div class="swiper-slide">
                                <?= wp_get_attachment_image($field['logo']['ID'], 'slider-img'); ?>
                            </div>
                            <?php
                        }
                    }
                }
                ?>
            </div>
        </div>
    </div>
</div>
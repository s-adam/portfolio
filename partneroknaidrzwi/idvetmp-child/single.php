<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */

get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>



<div class="single-page-wrapper o-nas">
    <div class="header-wrapper" style="background: url(<?= $fields['header-photo']['sizes']['blog-img-big'] ?>) top 0px center no-repeat; background-size: cover;">
        <div class="wrapper-for-header-info">
            <div class="container">
                <div class="section-title-archive">
                    <h2><?= the_title(); ?></h2>
                    <ul class="breadcrumbs">
                        <li>
                            <a href="/">Strona Główna</a>
                            <span>»</span>
                        </li>
                        <li>
                            <a href=""><?= get_the_title(); ?></a>
                            
                        </li>
                        
                    </ul>
                </div>
            </div>
        </div>
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/effect-single-page.png" class="single-page-effect hide-now">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/effekt-single-page-small.png" class="single-page-effect single-page-effect-small">
    </div>
    <div class="content-single-page">
        <div class="container">
            <div class="content-inside-wrapper">
                <?php
                the_content();
                ?>
            </div>
        </div>
    </div>

    <?php include_once "call-to-action.php"; ?>
    <?php include_once "partnerzy.php"; ?>
    
</div>

<?php
get_footer();

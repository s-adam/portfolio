<?php
if (get_field('reklama_-_gdzie_kupic', 'option')) {
    ?>
    <div class="call-to-action anim" data-vp-add-class="animated fadeIn">
        <div class="container">
            <div class="call-to-action__wrapper-inside">
                <?php echo the_field('reklama_-_gdzie_kupic', 'option'); ?>
            </div>
        </div>
    </div>
    <?php
}
?>
<?php

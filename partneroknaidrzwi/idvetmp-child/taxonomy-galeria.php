<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */

get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>



<div class="single-page-wrapper realizacje">
    <div class="header-wrapper" style="background: url(<?= $fields['header-photo']['sizes']['blog-img-big'] ?>) top 142px center no-repeat; background-size: cover;">
        <div class="wrapper-for-header-info">
            <div class="container">
                <div class="section-title-archive">
                    <h2>Realizacje</h2>
                    <p>Skontaktuj się z nami, z chęcią odpowiemy na Twoje pytania</p>
                </div>
            </div>
        </div>
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/effect-single-page.png" class="single-page-effect hide-now">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/effekt-single-page-small.png" class="single-page-effect single-page-effect-small">
    </div>
    <div class="content-single-page">
        <div class="container">
            <div class="sortowanie">
                <ul class="sort-menu text-center">
                    <li><a class="active" href="#">Wszystkie</a></li>
                    <li><a href="#">Okna</a></li>
                    <li><a href="#">Drzwi</a></li>
                    <li><a href="#">Bramy</a></li>
                    <li><a href="#">Rolety</a></li>
                    <li><a href="#">Parapetys</a></li>
                </ul>
            </div>
            <div class="content-inside-wrapper">

                <div class="posts-content-realizacje">
               
test
                <?php
                        $queried_object = get_queried_object();
                        $term_id = $queried_object->term_id;

                        echo $term_id; 
                        $custom_terms = get_terms('realizacje');

                        foreach ($custom_terms as $custom_term) {
                            wp_reset_query();
                            $args = array('post_type' => 'galeria',
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'realizacje',
                                        'field' => 'slug',
                                        'terms' => $term_id
                                    ),
                                ),
                            );

                            $loop = new WP_Query($args);
                            if ($loop->have_posts()) {

                                while ($loop->have_posts()) : $loop->the_post();
                                    ?>
                                    <div class="col-md-3">
                                        <?= get_the_title() ?>
                                    </div>
                                    <?php
                                endwhile;
                            }
                        }
                        ?>
                </div>
            </div>   
        </div>
    </div>

    <?php include_once "partnerzy.php"; ?>

</div>

<?php
get_footer();

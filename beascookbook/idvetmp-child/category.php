<?php

get_header();
the_post();

$fields = get_fields(get_the_ID());
?>

	<div class="main-wrapper recipes recipes4">
		<div class="container">
			<div class="row">
				<div class="first-section">
					<div class="col-md-6">
						<h2>
							<?php $cat_id = get_query_var('cat');
							?>
							<?php $fields = get_fields(get_the_ID()); ?>
							<?php //echo get_cat_name( $cat_id ) ?>

							<?php if ($cat_id == 545 || $cat_id == 546 || $cat_id == 547 || $cat_id == 544 || $cat_id == 5 ){
								echo get_cat_name(5);
							}else{
								echo get_cat_name(5);
							}?>

						</h2>
					</div>
					<div class="col-md-6">
			
			<?php
                $term = get_queried_object();

		$parent_id = get_queried_object()->category_parent;
				
				
				$c = get_field('opis_kategorii', 'category_'.$parent_id);
				echo $c;
				
				?>
                
                
            </div>
				</div>
			</div>

			<div class="choose-the-time-of-year">
				<?php

				$cat_id = get_query_var('cat');


				$category_id = $cat_id;

				$Spring = get_category_link(545);
				$Summer = get_category_link(544);
				$Winter = get_category_link(546);
				$Autumn = get_category_link(547);
				$x = get_category_link($category_id);

				$aa = 545;
				$bb = 544;
				$cc = 546;
				$dd = 547;
				$ee = 5;
				if ( $aa == $category_id || $ee == $category_id){
					$aa1 = "active";
				}
				if($bb == $category_id){
					$bb1 = "active";
				}
				if($cc == $category_id){
					$cc1 = "active";
				}
				if($dd == $category_id){
					$dd1 = "active";
				}



				if($category_id == $aa || $category_id == $bb || $category_id == $cc || $category_id == $dd){
				?>

				<ul>
					<li><a href="<?=$Spring?>" class="<?=$aa1?>">SPRING</a></li>
					<li><a href="<?=$Summer?>" class="<?=$bb1?>">SUMMER</a></li>
					<li><a href="<?=$Autumn?>" class="<?=$dd1?>">AUTUMN</a></li>
					<li><a href="<?=$Winter?>" class="<?=$cc1?>">WINTER</a></li>
					
				</ul>
				<?php } ?>
			</div>

	<?php

	$args = array(
		'post_type'      => 'post',
		'posts_per_page' => 6,
		'order'          => 'DESC',
		'cat' => $category_id,
		'paged' => ( get_query_var('paged') ? get_query_var('paged') : 1)
	);
	$a = query_posts($args);
	count($a);
	$parent = new WP_Query( $args );
	?>

	<?php

	if ( $parent->have_posts() ) :
		$i=0;
		$j=0;
	?>
		<?php while ( $parent->have_posts() ) : $parent->the_post();

		if ($i == 0) {
			echo '<div class="latest-posts">';
			echo '<div class="row wrapping">';
		}
		?>



				<div class="col-lg-4 col-md-12">
					<div class="latest-posts__single-photo photo-height">
						<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('blog_image_sizet') ?></a>
						<div class="latest-posts__info">
							<span class="data"><?php the_time('d F Y'); ?></span>
							<a href="<?php the_permalink(); ?>" class=""><?php the_title(); ?></a>
						</div>
					</div>
				</div>

		<?php
		$i++;
		$j++;
		if ($i == 3 || $j == count($a)) {


			echo '</div>';
			echo '</div>';


			$i = 0;
		}
		?>



		<?php
	endwhile; ?>
		<div class="container">
			<div class="row paginacja">
				<div class="col-md-12">
					<?php the_posts_pagination( array(
						'mid_size' => 1,
						'prev_text' => __( '&laquo', 'textdomain' ),
						'next_text' => __( '&raquo', 'textdomain' ),
					) ); ?>
				</div>
			</div>
		</div>
	<?php endif; wp_reset_query(); ?>


	<?php require ("slider.php"); ?>


<?php

get_footer();

?>
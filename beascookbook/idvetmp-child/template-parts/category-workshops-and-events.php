<?php

get_header();
the_post();

$fields = get_fields(get_the_ID());
?>

	<div class="main-wrapper recipes" style="margin-top: 200px">
		<div class="container">
			<div class="row">
				<div class="first-section">
					<div class="col-md-6">
						<h2>Recipes</h2>
					</div>
					<div class="col-md-6">
						<p>Food not only nourishes the body, it also feeds the soul.  Let’s cook something together, shall we? </p>
					</div>
				</div>
			</div>

			<div class="choose-the-time-of-year">
				<ul>
					<li><a href="#" class="active">SPRING</a></li>
					<li><a href="#">SUMMER</a></li>
					<li><a href="#">AUTUMN</a></li>
					<li><a href="#">WINTER</a></li>
				</ul>
			</div>

			<?php if($publishable_lite_relatedposts_section == '1') { ?>
				<!-- Start Related Posts -->
				<?php $categories = get_the_category($post->ID);
				if ($categories) { $category_ids = array();
					foreach($categories as $individual_category)
						$category_ids[] = $individual_category->term_id;
					$args=array( 'category__in' => $category_ids,
						'post__not_in' => array($post->ID),
						'ignore_sticky_posts' => 1,
						'showposts'=> 3,
						'orderby' => 'rand' );
					$my_query = new wp_query( $args ); if( $my_query->have_posts() ) {
						echo '<div class="related-posts"><div class="postauthor-top"><h3>'.__('Related Posts', 'publishable-mag').'</h3></div>';
						$pexcerpt=1; $j = 0; $counter = 0;
						while( $my_query->have_posts() ) {
							$my_query->the_post();?>
							<article class="post excerpt  <?php echo (++$j % 3== 0) ? 'last' : ''; ?>">
								<?php if ( has_post_thumbnail() ) { ?>
									<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" id="featured-thumbnail">
										<div class="featured-thumbnail">
											<?php the_post_thumbnail('publishable-mag-related',array('title' => '')); ?>
											<?php if (function_exists('wp_review_show_total')) wp_review_show_total(true, 'latestPost-review-wrapper'); ?>
										</div>
										<header>
											<h4 class="title front-view-title"><?php the_title(); ?></h4>
										</header>
									</a>
								<?php } else { ?>
									<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" id="featured-thumbnail">

										<header>
											<h4 class="title front-view-title"><?php the_title(); ?></h4>
										</header>
									</a>
								<?php } ?>
							</article><!--.post.excerpt-->
							<?php $pexcerpt++;?>
						<?php } echo '</div>'; }} wp_reset_postdata(); ?>
				<!-- End Related Posts -->
			<?php }?>


			<!--
                        <div class="row">

                            <div class="col-md-3 text-center">
                                <a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/photo1.jpg" alt=""></a>
                                <div class="date-category-item">
                                    <span class="text-center">28 July 2018</span>
                                </div>
                                <div class="title-category-content">
                                    <a href="#">Slow mornings with Royal Doulton & Ellen DeGeneres collection + a recipe for PROPER WAFFLES</a>
                                </div>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/photo1.jpg" alt=""></a>
                                <div class="date-category-item text-center">
                                    <span class="text-center">28 July 2018</span>
                                </div>
                                <div class="title-category-content text-center">
                                    <a href="#">Slow mornings with Royal Doulton & Ellen DeGeneres collection + a recipe for PROPER WAFFLES</a>
                                </div>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/photo1.jpg" alt=""></a>
                                <div class="date-category-item">
                                    <span class="text-center">28 July 2018</span>
                                </div>
                                <div class="title-category-content">
                                    <a href="#">Slow mornings with Royal Doulton & Ellen DeGeneres collection + a recipe for PROPER WAFFLES</a>
                                </div>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/photo1.jpg" alt=""></a>
                                <div class="date-category-item">
                                    <span class="text-center">28 July 2018</span>
                                </div>
                                <div class="title-category-content">
                                    <a href="#">Slow mornings with Royal Doulton & Ellen DeGeneres collection + a recipe for PROPER WAFFLES</a>
                                </div>
                            </div>
                        </div>
            -->
		</div>
	</div>


	<?php require ("slider.php"); ?>



<!-- NIE USUWAc
	<div class="main-wrapper workshop">
		<div class="header-post" style="background: url('<?php echo get_stylesheet_directory_uri(); ?>/img/header-photo.jpg') no-repeat">
			<div class="date-and-comments">
				test goof
			</div>
		</div>
	</div>
	-->
<?php

get_footer();

?>
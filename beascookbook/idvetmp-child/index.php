<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*
Template Name: Strona Główna
*/
get_header();
the_post();

$fields = get_fields(get_the_ID());
?>

<div class="main-wrapper r">
	<div class="slider swiper-container">
		<div class="swiper-wrapper text-center">

			<?php

			if($fields['slider']){

				foreach ($fields['slider'] as $field) {
					?>
					<div class="swiper-slide">
						<div class=" slider__item">
							<img src="<?= $field['zdjęcie']['sizes']['slider_home']?>" class="slider-image-p">
						</div>
					</div>
					<?php
				}
			}
			?>


		</div>
		<div class="swiper-pagination"></div>
        <span class="down bounce"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/down.png" alt=""></span>
	</div>
    <div class="kropki">
        <div class="container" id="kro">

                <div class="row">
                    <div class="food">
                        <?php

                        if($fields['sekcja_pierwsza']){
                            foreach ($fields['sekcja_pierwsza'] as $field) {


                                ?>

                                <?php
                                if($field['acf_fc_layout'] == 'tekst_w_2_kolumnach'){
                                    ?>
                                        <div class="col-md-6">
                                            
                                            <div class="left-wrapp" id="left-id">
                                                <?= $field['kol1']?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="right-wrapp">
                                                <?=$field['kol2']?>
                                            </div>
                                        </div>
                                    <?php
                                }
                                ?>

                                <?php

                            }
                        }

                        ?>

                    </div>

                </div>
                <div class="food__slider">
                    <div class="slider-food swiper-container">
                        <div class="swiper-wrapper text-center">
                            <?php

                            $fields = get_fields(get_the_ID());
                            if($fields['sekcja_-_food']){

                                foreach ($fields['sekcja_-_food'] as $field) {
                                    ?>
                                    <div class="swiper-slide">
                                        <div class="slider-box-wrapper">
                                            <p><?= $field['slider-food']?></p>
                                        </div>

                                      
                                    </div>

                                    <?php

                                }
                            }
                            ?>


                        </div>
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
                <div class="quick-sentence">
                    <?=$fields['sekcja_druga'];?>
                </div>
            </div>


        <?php
        $e = 0;
        $i = 1;

        if ($fields['wybierz_kategorię2']){
            foreach ($fields['wybierz_kategorię2'] as $catID) {

                $x = $i % 2;
                $num_padded = sprintf("%02d", $i);
                if ( $x != 1 ) {

                    ?>


                    <?php
                    if ($catID['name']){
                        foreach ($catID['name'] as $catIDs) {
                            ?>
                            <?php $catImage = get_field('zdjęcie_kategorii', 'category_'.$catIDs) ?>
                            <?php $f = get_category_link($catIDs); ?>
                    <div class="category-wrapper category-wrapper-right">
                        <div class="container">
                            <div class="category-wrapper__single-box">
                                <div class="row query-large">

                                    <div class="col-md-5">
                                        <div class="category-wrapper__right-box-edited">

                                            <a href="<?= $f ?>"><img src="<?= $catImage['sizes']['category_img'] ?>" alt=""></a>
                                        </div>
                                    </div>


                                    <div class="col-md-7">
                                        <div class="category-wrapper__left-box category-wrapper__left-box-edited viewport<?=$num_padded?>">
                                            <h1><?= $num_padded ?></h1>
                                            <div class="content-box">
                                                <h2><a href="<?= $f ?>"><?= get_cat_name($catIDs) ?></a></h2>

                                                <?php echo category_description($catIDs); ?>


                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row query-small">
                                    <div class="col-md-6">
                                        <div class="category-wrapper__query-right-box">
                                            <a href="<?= $f ?>"><img src="<?= $catImage['sizes']['category_img'] ?>" alt=""></a>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="category-wrapper__query-left-box">
                                            <h1><?= $num_padded ?></h1>
                                            <div class="content-box">
                                                <h2><a href="<?= $f ?>"><?= get_cat_name($catIDs) ?></a></h2>
                                                <?php echo category_description($catIDs); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a href="<?= $f ?>" class="read-btn read-btn-right">Read</a>
                            </div>
                        </div>
                    </div>
                            <?php
                        }

                    }
                    ?>

                    <?php

                }

                else{
                    ?>
                    <?php

                    if ($catID['name']){
                        foreach ($catID['name'] as $catIDs) {

                    ?>
                    <?php $g = get_category_link($catIDs); ?>

                    <div class="category-wrapper">
                        <div class="container">
                            <div class="category-wrapper__single-box">
                                <div class="row query-large">
                                    <div class="col-md-7">
                                        <div class="category-wrapper__left-box viewport<?=$num_padded?>">
                                            <h1><?=$num_padded?></h1>
                                            <div class="content-box">
                                                <?php
                                                //$catID = $field['wybierz_kategorię'];

                                                ?>

                                                <h2><a href="<?= $g ?>"><?= get_cat_name($catIDs) ?></a></h2>

                                                <?php echo category_description($catIDs); ?>


                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="category-wrapper__right-box">
                                            <?php $fields = get_fields($catIDs);
                                            $catImage = get_field('zdjęcie_kategorii', 'category_'.$catIDs);
                                            ?>

                                            <a href="<?= $g ?>"><img src="<?= $catImage['sizes']['category_img'] ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row query-small">
                                    <div class="col-md-6">
                                        <div class="category-wrapper__query-right-box">
                                            <a href="<?= $g ?>"><img src="<?= $catImage['sizes']['category_img'] ?>" alt=""></a>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="category-wrapper__query-left-box">
                                            <h1><?=$num_padded?></h1>
                                            <div class="content-box">
                                                <h2><a href="<?= $g ?>"><?= get_cat_name($catIDs) ?></a></h2>

                                                <?php echo category_description($catIDs); ?>


                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <a href="<?= $g ?>" class="read-btn">Read</a>
                            </div>
                        </div>
                    </div>
                    <?php
                }

            }
            ?>

                    <?php


                }
                $num_padded++;
                $i++;
                $e++;
            }

        }
        ?>
        </div>


	<div class="latest-posts">
		<div class="latest-posts__title">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/pedzel_small.png" alt="">

            <?php $fields = get_fields(get_the_ID()); ?>
            <?=$fields['latest_posts']; ?>
		</div>

		<div class="container">
			<div class="row wrapping">
				<?php

				$args = array(
					'post_type'      => 'post',
					'posts_per_page' => 3,
					'order'          => 'DESC'

				);


				$parent = new WP_Query( $args );
				?>

				<?php
				if ( $parent->have_posts() ) : ?>

					<?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
				<div class="col-md-4 col-sm-12">
					<div class="latest-posts__single-photo">
						<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('blog_image_sizet') ?></a>
						<div class="latest-posts__info">
							<span class="data"><?php the_time('d F Y'); ?></span>
							<a href="<?php the_permalink(); ?>" class=""><?php the_title(); ?></a>
						</div>
					</div>
				</div>
					<?php endwhile; ?>

				<?php endif; wp_reset_query(); ?>

			</div>
		</div>
	</div>

	<div class="some-class">
		<div class="subscribe-title text-center">
            <?php $fields = get_fields(get_the_ID()); ?>
            <?=$fields['currently_working_on']; ?>
		</div>
        <?php require ("newsletter.php"); ?>
	</div>


	<div class="stay_in_touch">
		<div class="stay_in_touch__title">
			<img class="stay_line" src="<?php echo get_stylesheet_directory_uri(); ?>/img/pedzel_small.png" alt="">
			<h2><?php echo $variable = get_field('stay_in_touch_title', 'option'); ?></h2>
		</div>
		<ul>

            <?php
            $variable = get_field('stay_in_touch', 'option');
            if($variable){

                foreach ($variable as $field) {
                    ?>
                    <?php
					
                    if (strpos($field['link'], '@') == true){
                        ?>
                        <li><a target="_blank" href="mailto:<?=$field['link']; ?>"><img src="<?= $field['ikona']['sizes']['FB_TW'] ?>"></a> </li>
                        <?php
                    }else{
                        ?>
                        <li><a target="_blank" href="<?=$field['link']; ?>"><img src="<?= $field['ikona']['sizes']['FB_TW'] ?>"></a> </li>

                        <?php
                    }
                    ?>

                    <?php
                }}
            ?>
		</ul>
	</div>
    <div class="instagram-section">
        <h6 class="instagram-name"><a href="<?php $variable = get_field('instagram', 'option'); ?>" target="_blank">@bealubas</a></h6>
        <h1 class="instagram-grey-sign">instagram</h1>
        <div class="container-for-inst-photos">
            <?php // do_shortcode("[jr_instagram id=\"2\"]") ?>
			
			<?= do_shortcode("[instashow columns=\"5\" ]") ?>
			
        </div>
            <div class="clear-box" style="clear:both">
        </div>
    </div>
    <?php require ("slider.php"); ?>

</div>
     

<?php
get_footer();

?>



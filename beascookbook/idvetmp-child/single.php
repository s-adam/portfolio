<?php

get_header();
the_post();

$fields = get_fields(get_the_ID());
/*
 * Template Post Type: Events
 */
?>

	<div class="main-wrapper recipes recipe-wrap">
		<?php if($fields['header_photo']['sizes']['header_photo-post'] != null) {?>
			<div class="header-post" style="background: url(<?=$fields['header_photo']['sizes']['header_photo-post'] ?>) center center no-repeat">

			<?}
				else{
					?>
				<div class="header-post" style="background: url(<?php echo get_the_post_thumbnail_url( get_the_ID(), 'header_photo-post' ); ?>) center center no-repeat">
					<?php
				}
			?>
			<div class="date-and-comments">
<!--				--><?//= $fields['header_photo']['sizes']['header_photo-post']?>
			</div>
		</div>
		<div class="single-content">
			<div class="container">
				<div class="row dont">
					<div class="col-md-12">
						<div class="single-title text-center">
							
							
							<?php 
							$categories = get_the_category();
							$category_id = $categories[0]->cat_ID; 
							?>
							
							<?php if($category_id == 543) { ?>
								<p><?php
								$fieldsPost = get_fields(get_the_ID());
								$dateFrom = strtotime($fieldsPost['data_rozpoczęcia']);
								$dateTo = strtotime($fieldsPost['data_zakończenia']);
								?>
								<?php
								if($dateFrom != $dateTo){
									if(date_i18n('m', $dateFrom) == date_i18n('m', $dateTo)){
										echo '<span class="data">'.  date_i18n('j', $dateFrom).'-'.date_i18n( 'j M Y', $dateTo ).'</span>';
									}else{
										echo '<span class="data">'.  date_i18n('j M Y', $dateFrom).'-'.date_i18n( 'j M Y', $dateTo ).'</span>';
									}
								}else{
									echo '<span class="data">'.date_i18n( 'j M Y', $dateFrom ).'</span>';
								}
								?> /  <span><?php comments_number(); ?></span>
							</p>
							<?php }else{
								?>
									<p><?=the_time('j F Y ');?> /  <span><?php comments_number(); ?></span></p>
								<?php
							} ?>
							
							<h2><?php the_title(); ?></h2>
						</div>
					</div>
				</div>

<?php

if($fields['przepis_elastic']){
	foreach ($fields['przepis_elastic'] as $field) {


		if($field['acf_fc_layout'] == 'przepis_na_gorze_strony'){
			?>
			<div class="padding-class">
				<div class="row print-recipe">
					<div class="col-md-8 col-md-offset-2">
						<?=$field['sekcja_pod_recipe']?>
					</div>
				</div>
				<div class="row print-recipe">
					<div class="col-md-3 col-md-offset-2">
						<div class="ingredients">
							<?php var_dump($field['skladniki_elastic']);?>
							<?=$field['skladniki_elastic']?>
						</div>
					</div>
					<div class="col-md-4">
						<div class="preparation">
							<?=$field['przepis_elastics']?>
						</div>
					</div>

				</div>
			</div>
			<div class="padding-class">
				<div class="row dont">
					<div class="col-md-8 col-md-offset-2">
						<?php the_content(); ?>
					</div>
				</div>
			</div>

			<?php

		}
		elseif($field['acf_fc_layout'] == 'przepis_na_dole_strony'){

			?>
			<div class="padding-class">
				<div class="row dont">
					<div class="col-md-8 col-md-offset-2">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
			<div class="padding-class">
				<div class="row print-recipe">
					<div class="col-md-8 col-md-offset-2">
						<?=$field['sekcja_pod_recipe']?>
					</div>
				</div>
				<div class="row print-recipe">
					<div class="col-md-3 col-md-offset-2">
						<div class="ingredients">
							<?=$field['skladniki_elastic']?>
						</div>
					</div>
					<div class="col-md-4">
						<div class="preparation">
							<?=$field['przepis_elastics']?>
						</div>
					</div>

				</div>
			</div>
			
			<?php

		}
	}
}

?>


				<?php if(!isset($fields['przepis_elastic']) || empty($fields['przepis_elastic'])){ ?>
				<div class="padding-class">
				<div class="row dont">
					<div class="col-md-8 col-md-offset-2">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
				<?php }?>
				

				<div class="last-of-content text-center dont">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/small-pedzel.jpg" alt="">
				</div>
				<div class="hhhh dont">
						<div class="container">
							<div class="row">
								<div class="col-md-2 col-md-offset-2">
								<?php if(($fields['przepis_elastic']) != NULL){
									?>
										<div class="print-wrapper">
										<a href="#">Print</a>
									</div>
									<?php
								} ?>
									

								</div>
								<div class="col-md-6">
									<div class="share-wrapper">

										<div class="shre">
											<h3>Share it:</h3>
										</div>

										<ul class="share-section">
											<li>
												<a href="http://twitter.com/intent/tweet?text=<?= get_permalink()?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
													<img class="fb-icon" src="<?=get_stylesheet_directory_uri()?>/img/twitter.jpg" alt="">
												</a>
											</li>

											<li>
												<a href="https://pinterest.com/pin/create/bookmarklet/?url=<?= get_permalink()?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
													<img class="fb-icon" src="<?=get_stylesheet_directory_uri()?>/img/pinterest.jpg" alt="">
												</a>
											</li>

											<li>
												<a href="https://www.facebook.com/sharer/sharer.php?u=<?= get_permalink()?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
													<img class="fb-icon" src="<?=get_stylesheet_directory_uri()?>/img/facebook.jpg" alt="">
												</a>
											</li>
										</ul>
									</div>

								</div>
							</div>
						</div>
					</div>

			</div>
		</div>
			<?php
				$good_date = date('Ymd');
				$fieldsPost = get_fields(get_the_ID());
				$dateFrom = strtotime($fieldsPost['data_rozpoczęcia']);
				$dateTo = strtotime($fieldsPost['data_zakończenia']);
				$now = strtotime($good_date);
				
			?>
			<?php
			
			if($now < $dateTo) {
				?>
				<div class="buynow"><span class="read-btn">Ask more details</span></div>
			<div class="cta">
				<div class="container">
					<div class="row">
						<div class="cta-bg">
						<?php echo do_shortcode( "[contact-form-7 id=\"4603\" title=\"Call to action\"]"); ?>
						</div>
					</div>
				</div>
			</div>
				<?php
			}
				
			?>
			
			
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="comment-wrapper dont">
						<?php
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php require ("newsletter.php"); ?>
<?php require ("latest-posts-recipes.php"); ?>
<?php require ("slider.php"); ?>
<div style="opacity: 0;">
	<?= do_shortcode("[print-me-button]") ?>
</div>

<?php

get_footer();

?>
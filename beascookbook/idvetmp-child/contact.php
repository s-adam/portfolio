<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*
Template Name: Contact
*/
get_header();
the_post();

$fields = get_fields(get_the_ID());
?>

<div class="main-wrapper contact-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="form-wrapper">
                    <h1>Contact</h1>
                    <?php echo do_shortcode( "[contact-form-7 id=\"3373\" title=\"Contact\"]"); ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="contact-content">
                    <?php the_content(); ?>
                    <div class="drop-me-a-line">
                        <h4>DROP ME A LINE</h4>
                        <a href="mailto:<?php the_field('email', 'option') ?>"><?php the_field('email', 'option') ?></a>
                    </div>
                    <div class="stay-in-touch">
                        <h4><?php echo $variable = get_field('stay_in_touch_title', 'option'); ?></h4>
                        <ul>

                            <?php
                            $variable = get_field('stay_in_touch', 'option');
                            if($variable){

                                foreach ($variable as $field) {
                                    ?>
                                    <?php

                                    if (strpos($field['link'], '@') == true){
                                        ?>
                                        <li><a href="mailto:<?=$field['link']; ?>"><img src="<?= $field['ikona']['sizes']['FB_TW'] ?>" alt=""></a> </li>
                                        <?php
                                    }else{
                                        ?>
                                        <li><a href="<?=$field['link']; ?>"><img src="<?= $field['ikona']['sizes']['FB_TW'] ?>" alt=""></a> </li>

                                        <?php
                                    }
                                    ?>

                                    <?php
                                }}
                            ?>
                        </ul>
                    </div>
                    <div class="text-under-socials">
                        <?=$fields['tekst_pod_ikonami_społecznościowymi'];?>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <?php require ("slider.php"); ?>
</div>
<?php
get_footer();

?>

<div class="latest-posts">
	<div class="latest-posts__title" style="margin-bottom: 60px;">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/pedzel_small.png" alt="">
		<h2 style="margin-top: 25px;margin-bottom: 5px;"><?php the_field('latests_posts', 'option') ?></h2>
	</div>

	<div class="container">
<!--		<div class="row wrapping">-->
			<?php

			$category_id = get_query_var('cat');
			

			$args = array(
				'post_type'      => 'post',
				'posts_per_page' => 3,
				'order'          => 'DESC',
				'cat' => $category_id

			);


			$a = query_posts($args);

			$parent = new WP_Query( $args );
			?>

			<?php
			if ( $parent->have_posts() ) :
				$i=0;
				$j=0;
				?>
				<?php while ( $parent->have_posts() ) : $parent->the_post();
			if ($i == 0) {
				echo '<div class="row wrapping">';
					}
					?>
					<div class="col-md-4">
						<div class="latest-posts__single-photo">
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('blog_image_sizet') ?></a>
							<div class="latest-posts__info">
								<span class="data"><?php the_time('d F Y'); ?></span>
								<a href="<?php the_permalink(); ?>" class=""><?php the_title(); ?></a>
							</div>
						</div>
					</div>
				<?php
				$i++;
				$j++;
				if ($i == 3 || $j == count($a)) {


					echo '</div>';



					$i = 0;
				}
				?>
				<?php endwhile; ?>

			<?php endif; wp_reset_query(); ?>

<!--		</div>-->
	</div>
</div>
<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package idvetmp
 */

?>

	</div><!-- #content -->

	<footer class="site-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center footer-text">
                        All rights reserved &copy <?= date('Y');?> Bea's cookbook
                    </div>
                </div>         
                <div class="row">
                    <div class="col-md-12 text-center footer-autor">
                        <a href="https://ideative.rocks/">
                            <img src="<?=get_template_directory_uri().'/img/by-ideative.svg'?>" alt="ideative.rocks">
                        </a>
                    </div>
                </div>    
            </div>
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/up.svg" alt="" class="up">

        <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/typed.min.js"></script>
		<script src="http://www.youtube.com/player_api"></script>


	</footer><!-- #colophon -->

</div><!-- #page -->
<!--<script type="text/javascript" src="--><?php //echo get_stylesheet_directory_uri(); ?><!--/js/hamburgers.js"></script>-->
<?php wp_footer(); ?>

</body>
</html>

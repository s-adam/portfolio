<?php

get_header();
the_post();

$fields = get_fields(get_the_ID());
?>

	<div class="main-wrapper workshops-and-events test">
		<div class="header-post" style="background: url(<?= $fields['header_photo']['sizes']['header_photo-post']?>) center center no-repeat">
			<div class="date-and-comments">

			</div>
		</div>
		<div class="single-content">
			<div class="container">
				<div class="row dont">
					<div class="col-md-12">
						<div class="single-title text-center">
							<p><?=the_time('j F Y ');?> /  <span><?php comments_number(); ?></span></p>
							<h2><?php the_title(); ?></h2>
						</div>
					</div>
				</div>
				<div class="padding-class">
					<div class="row dont">
						<div class="col-md-8 col-md-offset-2">
							<?php the_content(); ?>
						</div>
					</div>
				</div>

				<div class="last-of-content text-center dont">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/small-pedzel.jpg" alt="">
				</div>




				<div class="hhhh dont">
					<div class="container">
						<div class="row">
							<div class="col-md-2 col-md-offset-2">
								<div class="print-wrapper">
									<a href="#">Print</a>
								</div>

							</div>
							<div class="col-md-6">
								<div class="share-wrapper">

									<div class="shre">
										<h3>Share it:</h3>
									</div>

									<ul class="share-section">
										<li>
											<a href="http://twitter.com/intent/tweet?text=<?= get_permalink()?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
												<img class="fb-icon" src="<?=get_stylesheet_directory_uri()?>/img/socials/social-twitter.svg" alt="">
											</a>
										</li>

										<li>
											<a href="https://pinterest.com/pin/create/bookmarklet/?url=<?= get_permalink()?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
												<img class="fb-icon" src="<?=get_stylesheet_directory_uri()?>/img/socials/social-pinterest.svg" alt="">
											</a>
										</li>

										<li>
											<a href="https://www.facebook.com/sharer/sharer.php?u=<?= get_permalink()?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
												<img class="fb-icon" src="<?=get_stylesheet_directory_uri()?>/img/socials/social-facebook.svg" alt="">
											</a>
										</li>
									</ul>
								</div>

							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="comment-wrapper dont">
						<?php
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="comment-wrapper" style="background: black;height: 300px;">
						<svg version="1.1" id="Warstwa_2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 width="874px" height="148px" viewBox="0 0 874 148" enable-background="new 0 0 874 148" xml:space="preserve">
<path fill="none" stroke="#FFFFFF" stroke-width="65" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="d"/>
</svg>

					</div>
				</div>
			</div>
		</div>
	</div>

<?php require ("newsletter.php"); ?>
<?php require ("latest-posts.php"); ?>
<?php require ("slider.php"); ?>

	<div style="display: none;">

			<?= do_shortcode("[print-me-button]") ?>

	</div>


<?php

get_footer();

?>
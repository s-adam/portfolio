var mySwiper8 = new Swiper ('.slider-food', {
    autoplay: {
        delay: 3000,
    },
    loop: true,
    slidesPerView: 1,
    breakpoints: {
        700: {
            slidesPerView: 1,
            spaceBetween: 0
        }
    },
    fade: { crossFade: true },
    virtualTranslate: true,
    // autoplay: 5000,
    speed: 1000,
    effect: 'fade',
    delay: 5000
});

var mySwiper99 = new Swiper ('.last-slider', {
    autoplay: {
        delay: 3000,
    },
    loop: true,
    slidesPerView: 1,
    breakpoints: {
        700: {
            slidesPerView: 1,
            spaceBetween: 0
        }
    },
    fade: { crossFade: true },
    virtualTranslate: true,
    // autoplay: 5000,
    speed: 1000,
    effect: 'fade',
    delay: 5000
});




$( document ).ready(function() {
    $(".es_shortcode_form_email").html("Your Email");
    $("#es_txt_button_pg").attr('value', 'Sign up');
});



$( ".play-btn" ).click(function() {
    x = $(".YT-Player iframe").attr('src', $(".YT-Player iframe").attr('src')+'&autoplay=1');
    $( ".play-btn" ).hide();
    $( ".YT-Player" ).addClass('after');
    f = $(".YT-Player iframe").attr('src');


});
// $( ".YT-Player" ).click(function() {
//     x = $(".YT-Player iframe").attr('src', $(".YT-Player iframe").attr('src')+'&autoplay=0');
//     $( ".play-btn" ).show();
//     f = $(".YT-Player iframe").attr('src');
//
//
// });




$(".up").click(function() {
    $("html, body").animate({ scrollTop: 0 }, "slow");
    return false;
});

/* Print this page script v 1.0.0 */
jQuery(document).ready(function($) {
    //setup click function for button target the image class
    $('img.printme').click(function(){
        window.print();
        return false;
    });
});

$(".down").click(function() {
    $('html, body').animate({
        scrollTop: $("#kro").offset().top-106
    }, 1500);
})

$(".single-content .single-title span").click(function() {
    $('html, body').animate({
        scrollTop: $("#comments").offset().top-180
    }, 1500);
});


//
// $(document).ready(function () {
//
//     //
//     // if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
//     //     $('.anim').addClass('anim-mobile');
//     // }else{
//     //     $('.anim').viewportChecker();
//     // }
//
//     $('.anim').viewportChecker({
//         classToAdd: 'anim-mobile'
//     });
//
// });
//


// var typed2 = new Typed('.left-wrapp h1', {
//     strings: ["food", "Food"],
//     typeSpeed: 1000
// });
var hamburger = document.querySelector(".hamburger");
// On click
hamburger.addEventListener("click", function() {
    // Toggle class "is-active"
    hamburger.classList.toggle("is-active");
    // Do something else, like open/close menu
});

jQuery(document).ready(function($) {

    if (($(".recipes4 .first-section h2").length) > 0){
        var pisaniex = document.querySelector(".values-for-typed").textContent;

        var cat4 = pisaniex;
        var cat3 = pisaniex;
        var cats = [cat3, cat4, pisaniex];


        var typed = new Typed('.recipes4 .first-section h2', {
            strings: cats,
            typeSpeed: 500,
            backSpeed: 1,
            contentType: 'html'
        });


    };
});

if (($(".left-wrapp h1").length) > 0){
    var dd = document.querySelector(".values-for-typed h1").textContent;

    var cat4 = dd;
    var cat3 = dd;
    var cat2 = [cat3, cat4, dd];

    var typed = new Typed('.left-wrapp h1', {
        strings: cat2,
        typeSpeed: 500,
        backSpeed: 1,
        contentType: 'html'
    });

};


/*
$(document).ready(function(){
  sizeTheVideo();
  $(window).resize(function(){
    sizeTheVideo();
  });  
});

function sizeTheVideo(){
  // - 1.78 is the aspect ratio of the video
// - This will work if your video is 1920 x 1080
// - To find this value divide the video's native width by the height eg 1920/1080 = 1.78
  var aspectRatio = 1.78;
  
    var video = $('#videoWithJs iframe');
    var videoHeight = video.outerHeight();
    var newWidth = videoHeight*aspectRatio;
		var halfNewWidth = newWidth/2;
    
  //Define the new width and centrally align the iframe
  video.css({"width":newWidth+"px","left":"50%","margin-left":"-"+halfNewWidth+"px"});
}

*/
$(document).ready(function () {
  
  'use strict';
  
   var c, currentScrollTop = 0,
       navbar = $('.navbar');

   $(window).scroll(function () {
      var a = $(window).scrollTop();
      var b = navbar.height();
     
      currentScrollTop = a;
	  //if( a > 1){
		//navbar.addClass("scrollUp");
	  //}
	  //if( a == 1){
		//  navbar.removeClass("scrollUp");
	  //}
      if (c < currentScrollTop && a > b + b) {
        navbar.addClass("scrollUp");
      } else if (c > currentScrollTop && !(a <= b)) {
        navbar.removeClass("scrollUp");
      }
      c = currentScrollTop;
  });
  
});
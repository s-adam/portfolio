<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*
Template Name: About
*/
get_header();
the_post();

$fields = get_fields(get_the_ID());
?>

<div class="main-wrapper about">
	<div class="YT-Player">
		<iframe width="100%" height="100%" src="https://www.youtube.com/embed/<?php the_field('YT-Video', 'option'); ?>?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        <img class="yt-thumbnail" src="https://i1.ytimg.com/vi/<?php the_field('YT-Video', 'option'); ?>/maxresdefault.jpg">
		<a class="play-btn"><img src="" alt=""></a>
    </div>

    <div class="about-author">
        <div class="container">
			<?php

			if($fields['about_me']){
				foreach ($fields['about_me'] as $field) {


					if($field['acf_fc_layout'] == 'single-column'){
						?>
						<div class="row">
							<div class="col-md-12">
								<div class="single-column">
									<?php echo $field['single-column']; ?>
								</div>
							</div>
						</div>
						<?php

					}

					elseif($field['acf_fc_layout'] == 'double-columns'){

						?>

						<div class="row">
							<div class="double-wrapper">
								<div class="col-md-6">
									<div class="photo">
										<img src="<?=$field['first-column']['sizes']['about_me_img']?>" alt="">
									</div>
								</div>
								<div class="col-md-6 about-author-right-text">
									<?=$field['second-column']?>
								</div>
							</div>
						</div>

						<?php

					}
					elseif($field['acf_fc_layout'] == 'tekst_+_zdjęcie'){

						?>

						<div class="row">
							<div class="double-wrapper">
								<div class="col-md-6 about-author-left-text">
									<?=$field['tekst_+_zdjęcie']?>
								</div>
								<div class="col-md-6">
									<div class="photo">
										<img src="<?=$field['zdjęcie']['sizes']['about_me_img']?>">
									</div>
								</div>
							</div>
						</div>

						<?php

					}
				}
			}

			?>

        </div>
    </div>
</div>


<?php require ("slider.php"); ?>
<?php

get_footer();

?>


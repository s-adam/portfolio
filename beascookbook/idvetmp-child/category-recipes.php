<?php

get_header();
the_post();
global $query_string;
query_posts("{$query_string}&posts_per_page=9");
$fields = get_fields(get_the_ID());
?>

    <div class="main-wrapper recipes mt200">
    <div class="container">
    <div class="row">
        <div class="first-section">
            <div class="col-md-6">
                <h2><?php single_cat_title()?></h2>
            </div>
            <div class="col-md-6">
			
			<?php
                $term = get_queried_object();
                $image = get_field('opis_kategorii', $term);
				?>
                <?=$image;?>
                
            </div>
        </div>
    </div>

    <div class="choose-the-time-of-year">
        <?php


        wp_nav_menu(array(
                'menu' => 'seasons',
                'theme_location' => 'seasons',
                'depth' => 3,
                'container' => false,
                'container_class' => '',
                'menu_class' => ''
            )
        );

        ?>
    </div>

    <?php
	$cat_id = get_query_var('cat');
    $category_id = $cat_id;

    ?>
    <?php 
	if($category_id == 5){
		$args = array(
        'post_type'      => 'post',
        'posts_per_page' => 9,
        'cat' => '544, 545,546,547',
        'paged' => ( get_query_var('paged') ? get_query_var('paged') : 1)
		);
	}else{
		$args = array(
        'post_type'      => 'post',
        'posts_per_page' => 9,
        'cat' => $category_id,
        'paged' => ( get_query_var('paged') ? get_query_var('paged') : 1)
		);
	}
    
	
	
    $a = query_posts($args);
    count($a);
    $parent = new WP_Query( $args );
    ?>

    <?php

    if ( $parent->have_posts() ) :
        $i=0;
        $j=0;
        ?>
        <?php while ( $parent->have_posts() ) : $parent->the_post();

        if ($i == 0) {
            echo '<div class="latest-posts">';
            echo '<div class="row wrapping">';
        }
        ?>



        <div class="col-md-4">
            <div class="latest-posts__single-photo photo-height">
                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('blog_image_sizet') ?></a>
                <div class="latest-posts__info">
                    <span class="data"><?php the_time('d F Y'); ?></span>
                    <a href="<?php the_permalink(); ?>" class=""><?php the_title(); ?></a>
                </div>
            </div>
        </div>

        <?php
        $i++;
        $j++;
        if ($i == 3 || $j == count($a)) {


            echo '</div>';
            echo '</div>';


            $i = 0;
        }
        ?>



        <?php
    endwhile; ?>
        <div class="container">
            <div class="row text-right">
                <div class="col-md-12">
                    <?php the_posts_pagination( array(
                        'mid_size' => 1,
                        'prev_text' => __( '&laquo', 'textdomain' ),
                        'next_text' => __( '&raquo', 'textdomain' ),
                    ) ); ?>
                </div>
            </div>
        </div>
    <?php endif; wp_reset_query(); ?>


    <?php require ("slider.php"); ?>

<?php

get_footer();

?>
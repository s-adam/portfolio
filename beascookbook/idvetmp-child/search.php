<?php

get_header();
the_post();
global $query_string;
query_posts("{$query_string}&posts_per_page=6");
$fields = get_fields(get_the_ID());




$a = query_posts("{$query_string}&posts_per_page=6");
?>

	<div class="main-wrapper recipes mt200">
<!--	--><?php //var_dump(count($a));?>
	<div class="container">
	<div class="row">
		<div class="first-section">
			<div class="col-md-6">
				<h2><?php single_cat_title()?></h2>
			</div>
			<div class="col-md-6">
				<?php
				$term = get_queried_object();

				$image = get_field('kk', $term);
				echo $image;
				?>
			</div>
		</div>
	</div>


	<?php
	$categories = get_the_category();
	$category_id = $categories[0]->cat_ID;

	?>

	<h1 class="search-results" style="padding-bottom: 20px;"><?php printf( __( 'Search Results for: %s', 'shape' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
	
	<?php

	if (have_posts()) :
		$i = 0;
		$j = 0;
		while (have_posts()) : the_post();

			if ($i == 0) {
				echo '<div class="latest-posts">';
				echo '<div class="row wrapping">';
			}
			?>



			<div class="col-md-4">
				<div class="latest-posts__single-photo photo-height">
					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('blog_image_size') ?></a>
					<div class="latest-posts__info">
						<span class="data"><?php the_time('d F Y'); ?></span>
						<a href="<?php the_permalink(); ?>" class=""><?php the_title(); ?></a>
					</div>
				</div>
			</div>

			<?php
			$i++;
			$j++;
			if ($i == 3  || $j == count($a)) {


				echo '</div>';
				echo '</div>';


				$i = 0;
			}
			?>



			<?php
		endwhile; ?>
		<div class="container">
			<div class="row text-right">
				<div class="col-md-12">
					<?php the_posts_pagination( array(
						'mid_size' => 2,
						'prev_text' => __( '&laquo', 'textdomain' ),
						'next_text' => __( '&raquo', 'textdomain' ),
					) ); ?>
				</div>
			</div>
		</div>
	<?php endif; wp_reset_query(); ?>


	<?php require ("slider.php"); ?>


<?php

get_footer();

?>
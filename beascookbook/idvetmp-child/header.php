<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package idvetmp
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
    <link href="https://use.fontawesome.com/releases/v5.0.3/css/all.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Special+Elite" rel="stylesheet">
    <link type="text/css" media="print" rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/print.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/hamburgers.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/animate.css">

    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

	<?php wp_head(); ?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-55715741-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-55715741-1');
</script>


</head>
<body <?php body_class(); ?>>
<div id="page" class="site">
	<header id="masthead" class="site-header">
			<div class="menu-bg"></div>
            <nav class="navbar navbar-default navbar-fixed-top">
              <div class="container">
                <div class="navbar-header">
                    <div class="hamburger hamburger--collapse js-hamburger" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <div class="hamburger-box">
                            <div class="hamburger-inner"></div>
                        </div>
                    </div>

                    <a class="navbar-brand" href="/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.svg" alt=""></a>
                </div>

                  <div class="search-wrap rwd-off">
                      <ul>
                          <?php

                          if (get_field('instagram', 'option')){
                              ?>
                              <li><a href="<?php the_field('instagram', 'option') ?>" target="_blank"><i class="fab fa-instagram" aria-hidden="true"></i></a></li>
                              <?php
                          }
                          if (get_field('pinterest', 'option')){
                              ?>
                              <li><a href="<?php the_field('pinterest', 'option') ?>" target="_blank"><i class="fab fa-pinterest-p" aria-hidden="true"></i></a></li>
                              <?php
                          }
                          if (get_field('facebook', 'option')){
                              ?>
                              <li><a href="<?php the_field('facebook', 'option') ?>" target="_blank"><i class="fab fa-facebook" aria-hidden="true"></i></a></li>
                              <?php
                          }
                          if (get_field('twitter', 'option')){
                              ?>
                              <li><a href="<?php the_field('twitter', 'option') ?>" target="_blank"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                              <?php
                          }
                          if (get_field('email', 'option')){
                              ?>
                              <li><a href="mailto:<?php the_field('email', 'option') ?>" target="_blank"><i class="fas fa-at" aria-hidden="true"></i></a></li>
                              <?php
                          }
			if (get_field('yt', 'option')){
                                    ?>
                                    <li><a href="<?php the_field('yt', 'option') ?>" target="_blank"><i class="fab fa-youtube" aria-hidden="true"></i></a></li>
                                    <?php
                          }
                          ?>

                      </ul>
                      <form role="search" method="get" class="search-form" action="/">
                          <label>
                              <input class="search-field" placeholder="" value="" name="s" type="search">
                              <input class="search-submit" value="" type="submit">
                          </label>
                      </form>
                  </div><!-- .wrap -->
                  <div id="navbar" class="navbar-collapse collapse">
                      <?php
                      wp_nav_menu(array(
                              'theme_location' => 'menu-1',
                              'menu_id'        => 'primary-menu',
                              'container_class' => '',
                              'menu_class' => 'nav navbar-nav navbar-right',
                              'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
                              'walker' => new wp_bootstrap_navwalker())
                      );
                      ?>
                      <div class="search-wrap rwd">
                          <ul>
                              <?php

                              if (get_field('instagram', 'option')){
                                  ?>
                                  <li><a href="<?php the_field('instagram', 'option') ?>" target="_blank"><i class="fab fa-instagram" aria-hidden="true"></i></a></li>
                                  <?php
                              }
                              if (get_field('pinterest', 'option')){
                                  ?>
                                  <li><a href="<?php the_field('pinterest', 'option') ?>" target="_blank"><i class="fab fa-pinterest-p" aria-hidden="true"></i></a></li>
                                  <?php
                              }
                              if (get_field('facebook', 'option')){
                                  ?>
                                  <li><a href="<?php the_field('facebook', 'option') ?>" target="_blank"><i class="fab fa-facebook" aria-hidden="true"></i></a></li>
                                  <?php
                              }
                              if (get_field('twitter', 'option')){
                                  ?>
                                  <li><a href="<?php the_field('twitter', 'option') ?>" target="_blank"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                  <?php
                              }
                              if (get_field('email', 'option')){
                                  ?>
                                  <li><a href="mailto:<?php the_field('email', 'option') ?>" target="_blank"><i class="fas fa-at" aria-hidden="true"></i></a></li>
                                  <?php
                              }
				if (get_field('yt', 'option')){
                                    ?>
                                    <li><a href="<?php the_field('yt', 'option') ?>" target="_blank"><i class="fab fa-youtube" aria-hidden="true"></i></a></li>
                                    <?php
                          	}
                          	

                              ?>
                          </ul>
                      </div>
                  </div>
              </div>

            </nav>            
	</header><!-- #masthead -->
	<div id="content" class="site-content">

<?php
/*
Template Name: Strona Główna
*/
get_header();
the_post();

$fields = get_fields(get_the_ID());

?>

<div class="main-container">

    <div class="strona-glowna">
        <div class="container-fluid">
            <div class="row">
                <div class="section-main">
                    <div id="myModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <div class="modal-body" id="popupVid">
                                  <iframe id="video" width="560" height="315" src="//www.youtube.com/embed/FKWwdQu6_ok?enablejsapi=1&html5=1" frameborder="0" allowfullscreen></iframe>
                                    <a href="#" id="play-button" class="play-btn-modal" style="left: 40%;"><i class="fa fa-play" aria-hidden="true"></i></a>
                                    <a href="#" id="close-button" class="play-btn-modal close-modal" data-dismiss="modal" style="left: 50%;"><i class="fa fa-times" aria-hidden="true"></i></a>

                                    
                                    <span id="pauza" onclick="pauza()">
                                        pauza
                                    </span>
                                    
                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="mar">
                        <div class="col-md-6">
                            <div class="left-site">
                                <div class="flex-container">
                                    <a href="#" class="play-btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-play" aria-hidden="true"></i></a>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="right-site">
                                <div class="site-wrapper">
                                    <div class="main-title">
                                        
                                        
                                        <?= $fields ['sekcja_pierwsza'] ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="kim-jestesmy">
                <div class="col-md-6">
                    <div class="section-left-site">
                        <div class="section-left-wrapper">
                            <div class="title">
                                <p>Kim Jesteśmy?</p>
                            </div>
                            <?= $fields ['sekcja_kim_jestesmy'] ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="section-right-site">
                        <div class="smart-wrapper">
                            <h4>SMART</h4>
                            <p>AUTOMATION</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="aktualnosci-bg">
        <div class="container">
            <div class="title">
                <p>Aktualności</p>
            </div>
            <div class="row">
                <div class="aktualnosci">
                        <?php


                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                        query_posts(array('posts_per_page' => 3, 'paged' => $paged));

                        if ( have_posts() ) : ?>
                        <?php
                        // Start the Loop.
                        
                        while ( have_posts() ) : the_post();
                    
                        
                        ?>
                        <?php
                        $row = false;
                        ?>
                        <?php if ( has_post_thumbnail() ) : ?>

                        <?php endif; ?>
                        <?php
                        ?>
                    <div class="col-md-4">
                        <div class="one-item">
                            <p class="title-item"><strong><?php the_title(); ?></strong></p>
                            <p class="title-date"><?php the_date(); ?></p>

                            <div class="item-content">
                                <p>
                                    <?php
                                        the_excerpt_max_charlength(180);
                                    ?>
                                </p>
                            </div>

                            <a href="btn" class="btn">Czytaj</a>
                        </div>
                    </div>
                    <?php
                                               
                        endwhile;

                        endif;
                    ?>
                    
                    <a href="#" class="btn read-more text-center">Czytaj wszystkie</a>
                </div>
            </div>
        </div>

    </div>
    <div class="container">
        <div class="slider">
            <div class="owl-carousel">
                <?php
                    $images = get_field('sekcja_sliderfirmy');
                    if($images){

                    foreach ($images as $field) {

                ?>
                        <?php
                        }
                    }

                    ?>
            </div>
        </div>
    </div>


</div>

<?php get_footer(); ?>



<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package idvetmp
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>


<div class="wheather">


	<div class="container">
		<div class="row">
			<div class="pogoda col-md-10 col-md-offset-1">
				<!--                                            <div class="container">-->
				<div class="box row">
					<div class="wheather-one-col">
						<div class="col-md-4">
							<div class="berk-title">
								<p>Faza księżyca</p>
							</div>
							<div class="wheather-box" style="background: url('<?php echo get_template_directory_uri(); ?>/img/tlo_moon.jpg') top center no-repeat">
								<?php dynamic_sidebar( 'sidebar-1' ); ?>
							</div>
						</div>
					</div>
					<div class="wheather-one-col">
						<div class="col-md-4">
							<div class="berk-title">
								<p>Pogoda</p>
							</div>
							<div class="wheather-box">
								<!--                                                            <iframe frameborder="0" marginwidth="0" marginheight="0" width="260" height="220" scrolling="NO" src="http://instytutmeteo.pl/userwidget/sitewidgets?id=9&city_id=756135"></iframe>-->
								<iframe frameborder="0" marginwidth="0" marginheight="0" width="250" height="241" scrolling="NO" src="http://instytutmeteo.pl/userwidget/sitewidgets?id=7&city_id=756135"></iframe>
							</div>
						</div>
					</div>
					<div class="wheather-one-col">
						<div class="col-md-4">
							<div class="berk-title">
								<p>Wschód i zachód</p>
							</div>
							<div class="wheather-box" id="wsch_zach">
								<!--                                                            <a href="https://time.is/Olsztyn" id="time_is_link" rel="nofollow" style="font-size:20px">Aktualny czas w Olsztyn:</a>-->
								<span id="Olsztyn_z733" style="font-size:20px"></span>
								<script src="//widget.time.is/pl.js"></script>
								<script>
									time_is_widget.init({Olsztyn_z733:{template:"SUN", sun_format:"Wschód słońca: <span class='godz'>srhour:srminute</span> <br>Zachód słońca: <span class='godz'>sshour:ssminute</span><br><span class='space'> Długość dnia: <span class='godz'>dlhoursh dlminutesmin</span></span>", coords:"53.7799500,20.4941600"}});
								</script>

							</div>

						</div>
					</div>
					<!--                                            </div>-->
				</div>
			</div>
			<div class="col-md-1"></div>
		</div>
	</div>


</div>




<!--<aside id="secondary" class="widget-area" role="complementary">-->
<!--	--><?php //dynamic_sidebar( 'sidebar-1' ); ?>
<!--</aside><!-- #secondary -->

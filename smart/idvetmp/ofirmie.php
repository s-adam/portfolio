<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package idvetmp
 *
 */
/*
Template Name: O Firmie
*/
get_header();
the_post();
$fields = get_fields(get_the_ID());

?>


    <div class="main-container">

        <div class="o-firmie">
            <div class="container">

                <div class="row">
                    <div class="col-md-5">

                        <div class="o-firmie-info">
                            <div class="firma-photo">
                                <?= $fields ['tekst_po_lewej_stronie'] ?>
                                
                                <img src="<?php echo get_template_directory_uri(); ?>/img/sa.png" alt="">
                            </div>

                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="text-o-firmie">
                            <?= $fields ['tekst_po_prawej_stronie'] ?>
                        </div>
                    </div>

                    </div>
                     
                    <div class="anim" data-vp-add-class="animated fadeIn">
                    <?php
                    $images = get_field('sekcja_liczby');
                    if($images){
                        $i = 0;
                        $j = 0;
                        foreach ($images as $field) {
                            if ($i == 0) {

                                echo '<div class="section-numbers"';
                                echo '<div class="row">';
                                
                            }
                            ?>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6 col-sm-6" >
                                    <div class="centrowy">
                                        <?= $field ['tekst'] ?>
                                        
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6" >
                                    <div class="box-number">
                                        <span class="timer" data-from="0" data-to="<?= $field ['liczba'] ?>" data-speed="1500"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <?php
                            $i++;
                            $j++;
                            if ($i == 2 || count($images) == $j) {


                                echo '</div>';
                                 echo '</div>';

                                $i = 0;
                            }
                        }
                    }

                    ?>
                    
                    </div>
                </div>
                </div>

            </div>

        </div>


 


<?php
get_footer();

<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package idvetmp
 *
 */
/*
Template Name: Wszystkie Usługi
*/
get_header();
the_post();
$fields = get_fields(get_the_ID());

?>

<?php
$args = [
    'post_type' => 'page',
    'nopaging' => true,
    'meta_key' => '_wp_page_template',
    'meta_value' => 'usluga.php',
    'order' => 'ASC'
];
$posts = get_posts( $args );
$field = get_fields();
?>
<div class="main-container">
    <div class="wszystkie-uslugi">
        <div class="container">
        <?php
						$i = 0;
						$j = 0;
					
                        foreach ( $posts as $post ) : setup_postdata( $post );
                            $field = get_fields(get_the_ID());
                            if ($i == 0) {
                                echo '<div class="row">';
                                echo '<div class="col-md-offset-1 col-md-10">';
                                echo '<div class="row">';
                            }
                
                            $bg = 'background:'.$field['tlo'];
                            if(!empty($field ['tlo_bg']) &&  $field['tlo'] == 'grafika'){
                                $bg = 'background: url('.$field ['tlo_bg']['sizes']['usluga-max-bg'].')';
                            }
                            ?>
                                <div class="col-md-4">
                                    <?php
                                        
                                    ?>
                                    <div class="each-usluga" style=" <?= $bg ?>;">

                                        <h3><?php the_title(); ?></h3>
                                        <?php
                                            if ($field['zajawka'] != null){
                                            ?>
                                            <?= $field ['zajawka'] ?>
                                            <?php
                                            }
                                        ?>
                                        
                                        <a href="<?php the_permalink(); ?>" class="btn read-more"><?=pll__("Zobacz");?></a>
                                    </div>
				</div>


                                <?php

                                $i++;
                                $j++;
                                if ($i == 3) {

                                    echo '</div>';
                                    echo '</div>';
                                    echo '</div>';

                                    $i = 0;
                                }
                                endforeach;
                    wp_reset_postdata();
                ?>
        </div>
    </div>
</div>

<?php
get_footer();

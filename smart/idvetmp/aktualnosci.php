<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package idvetmp
 *
 */
/*
Template Name: Aktualnosci
*/
get_header();
the_post();
$fields = get_fields(get_the_ID());

?>
<div class="main-container">
    <div class="kariera">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="special-title">
                        <h3><?php the_title(); ?></h3>
                    </div>
                    <?php


                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                        query_posts(array('posts_per_page' => 4, 'paged' => $paged));

                        if ( have_posts() ) : ?>
                        <?php
                        // Start the Loop.
                        $i = 0;
                        while ( have_posts() ) : the_post();
                        
                        if($i == 0){
                            echo '<div class="row">';
                        }
                        
                        ?>
                        <?php



                    $row = false;
                    ?>
                    <?php if ( has_post_thumbnail() ) : ?>

                    <?php endif; ?>
                    <?php
                    ?>
                        <div class="col-md-6">
                            <div class="kariera-wrapper aktualnosci-wrapper">
                                <div class="kariera-each-wrapper aktualnosc-wrapper">
                                    
                                    <p class="post-title"><strong><?php the_title(); ?></strong></p>
                                    <p class="date"><?php the_time('n.d.Y'); ?></p>
                                    <p>
                                    <?php
                                        the_excerpt_max_charlength(180);
                                    ?>
                                    </p>
                                    <a href="<?php the_permalink(); ?>" class="btn"><?=pll__("Czytaj");?></a>
                                </div>
                            </div>
                        </div>
                  

                    <?php
                        $i++;
                        if($i == 2){
                            echo '</div>';
                            $i = 0;
                        }
                        
                        endwhile;

                        endif;
                    ?>

                </div>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();

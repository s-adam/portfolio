<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package idvetmp
 *
 */
/*
Template Name: Usługa
*/
get_header();
the_post();
$fields = get_fields(get_the_ID());

?>
<div class="main-container">
    <div class="uslugi-bg-wrap">
        <div class="container">
            <div class="uslugi-bg">
                <div class="row">
                    <div class="col-md-5">
                        <div class="usluga-tytul-wrapper">
                            <div class="usluga-tytul">
                                <?= $fields ['nazwa_uslugi'] ?>
                                <div class="photos">
                                     <?php
                                        $images = get_field('zdjecia');
                                        if($images){
                                            $i = 0;
                                            $j = 0;
                                            foreach ($images as $field) {
                                                if ($i == 1) {

                                                   echo '<div class="img-2">';
                                                    
                                                }
                                                ?>
                                            <img src="<?php echo $field['zdjecie']['sizes']['usluga-img'] ?>">
                                            <?php
                                            $i++;
                                            $j++;
                                            if ($i == 2 || count($images) == $j) {


                                                echo '</div>';
                                                echo '</div>';

                                                $i = 0;
                                            }
                                        }
                                    }

                                    ?>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="usluga-text">
                             <?= $fields ['tekst'] ?>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>

<?php
get_footer();

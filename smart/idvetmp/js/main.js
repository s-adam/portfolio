$(document).ready(function () {
        
    $('.section-numbers').viewportChecker({

			callbackFunction: function(elem, action){

				setTimeout(
					function()
					{
						$('.timer').countTo();
					}, 500);
			}

		});


    if (screen.width > 1) {

        $('.anim').viewportChecker(
            
        );
        
        $('.zero').viewportChecker({
            classToAdd: 'nowa'
        });
        $('.margin-wrapp').viewportChecker({
            classToAdd: 'nowa'
        });
    }else{



    }


});

jQuery(function($) {
    $('.usluga-text .btn').addClass('read-more');

});



//MENU SCROLL

$(document).ready(function () {

    jQuery(window).scroll(function () {
        var scrollPos = jQuery(window).scrollTop();
        if (scrollPos > 160){
            $('.navbar').addClass('scrolled-nav');

            
        }
        else
        {
            
            $('.navbar').removeClass('scrolled-nav');

            if (window.width > 50){
                $('#map').removeAttr('style');
            }
        }
    })

});




$( ".play-btn" ).click(function() {
    $( this ).css('height','300vw');
    $( this ).css('width','300vw');
    $( this ).css('border-radius','100%');
    $( this ).css('transition','all 1s ease-in-out');
    $( this ).css('position','absolute');
    $( '.play-btn i' ).css('color','#f8921e');
});

$( ".navbar-toggle" ).click(function() {
    $( '.home nav .container' ).css('padding','0px');
    $( '.home .navbar-brand' ).css('padding-left','15px');
    $( '.play-btn' ).css('z-index','1');
    $( '#navbar' ).css('margin-left','0');
    $( '#navbar' ).css('margin-right','0');
    $( '.strefa' ).css('top','0');
});

$('.btn2').on("click", function (event) {
    $(this).toggleClass('rotate');
    $('.kar').toggleClass('section-kar');
});


$( ".close-modal" ).click(function() {
    $( ".play-btn" ).removeAttr('style');
    $( '.play-btn i' ).css('color','#fff');

});
$( "#myModal" ).click(function() {
    $( ".play-btn" ).removeAttr('style');
    $( '.play-btn i' ).css('color','#fff');

});




$('[data-toggle=modal]').on('click', function (e) {
    var $target = $($(this).data('target'));
    $target.data('triggered',true);
    setTimeout(function() {
        if ($target.data('triggered')) {
            $target.modal('show').data('triggered',false);
        };
    }, 900);
    return false;
});

var player;
function onYouTubePlayerAPIReady() {
    player = new YT.Player('video', {
        events: {
            'onReady': onPlayerReady
        }
    });
}

function onPlayerReady(event) {
    var playButton = document.getElementById("play-button");
    playButton.addEventListener("click", function() {
        player.playVideo();
    });

    player.pauseVideo(
    );
}

// Inject YouTube API script
var tag = document.createElement('script');
tag.src = "//www.youtube.com/player_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);


function pauza(){
    var div = document.getElementById("popupVid");
    var iframe = div.getElementsByTagName("iframe")[0].contentWindow;
    
    
    iframe.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
    
};

$('.navbar-nav a').hover( 
    function() {
        startPosition = $('.navbar-nav .active');
        startPosition.offset();
        p = $(this).offset();

       $('.menu-hover').css('left',p.left);
    }, function() {
        startPosition = $('.navbar-nav .active');
        p = startPosition.offset();
        $('.menu-hover').css('left',p.left);
    }
);

function callbackTS(event) {
}


function callbackTS2(event) {
}


function callbackTS3(event) {
    event.preventDefault();
    setTimeout(
      function() 
      {
      }, 1000);
}

function callbackTS4(event) {
}

$(document).ready(function(){

    var owl = $(".tripe-slider").owlCarousel({
        nav: false,
        loop: true,
        onChange: callbackTS,
        onChanged: callbackTS2,
         onTranslated : callbackTS3,
         onInitialized : callbackTS4,
        rewindNav : true,
        responsive:{
            320:{
                items:1,
                center: false
            },
            480:{
                items:1,
                center: false
            },
            620:{
                items:1,
                center: false
            },
            767:{
                items:1,
                center: false
            },
            992:{
                items:1,
                center: false
            },
            1200:{
                items:1,
                center: false
            }
        }
    });
    
$('#triple-next-slide').click(function(){
   $('.tripe-slider .active .big-box').addClass('visible animated fadeOutLeft');
    setTimeout(function(){
      $('.tripe-slider .active .medium-box-1').addClass('visible animated fadeOutLeft');
    }, 400); 
    setTimeout(function(){
      $('.tripe-slider .active .small-box-1').addClass('visible animated fadeOutLeft');
    }, 800);
    setTimeout(function(){
        $('.tripe-slider .robot-triple').addClass('visible animated fadeOutLeft');
    }, 800);
    
    setTimeout(function(){
      owl.trigger('next.owl.carousel');
      $('.tripe-slider .big-box').removeClass('visible animated fadeOutLeft fadeInRight');
      $('.tripe-slider .medium-box-1').removeClass('visible animated fadeOutLeft fadeInRight');
      $('.tripe-slider .small-box-1').removeClass('visible animated fadeOutLeft fadeInRight');
      $('.tripe-slider .robot-triple').removeClass('visible animated fadeOutLeft fadeInRight');
      $('.tripe-slider .big-box').addClass('hide-element');
      $('.tripe-slider .medium-box-1').addClass('hide-element');
      $('.tripe-slider .small-box-1').addClass('hide-element');
      $('.tripe-slider .robot-triple').addClass('hide-element');
      $('.tripe-slider .active .big-box').addClass('visible animated fadeInRight');
        setTimeout(function(){
          $('.tripe-slider .active .medium-box-1').addClass('visible animated fadeInRight');
        }, 400);  
        setTimeout(function(){
          $('.tripe-slider .active .small-box-1').addClass('visible animated fadeInRight');
        }, 800);
        setTimeout(function(){
            $('.tripe-slider .robot-triple').addClass('visible animated fadeInRight');
        }, 800);
    }, 1100);  
    
});


$('#triple-prev-slide').click(function(){
   $('.tripe-slider .active .medium-box-1').addClass('visible animated fadeOutRight');
    setTimeout(function(){
      
      $('.tripe-slider .active .small-box-1').addClass('visible animated fadeOutRight');
    }, 400);
    setTimeout(function(){

        $('.tripe-slider .robot-triple').addClass('visible animated fadeOutRight');
    }, 400);
    setTimeout(function(){
      
      $('.tripe-slider .active .big-box').addClass('visible animated fadeOutRight');
    }, 800);


    setTimeout(function(){
      owl.trigger('prev.owl.carousel');
      $('.tripe-slider .big-box').removeClass('visible animated fadeOutRight fadeInLeft');
      $('.tripe-slider .medium-box-1').removeClass('visible animated fadeOutRight fadeInLeft');
      $('.tripe-slider .small-box-1').removeClass('visible animated fadeOutRight fadeInLeft');
      $('.tripe-slider .robot-triple').removeClass('visible animated fadeOutRight fadeInLeft');
      $('.tripe-slider .big-box').addClass('hide-element');
      $('.tripe-slider .medium-box-1').addClass('hide-element');
      $('.tripe-slider .small-box-1').addClass('hide-element');
      $('.tripe-slider .robot-triple').addClass('hide-element');
      $('.tripe-slider .active .medium-box-1').addClass('visible animated fadeInLeft');
        setTimeout(function(){
          
          $('.tripe-slider .active .small-box-1').addClass('visible animated fadeInLeft');
        }, 400);
        setTimeout(function(){

            $('.tripe-slider .robot-triple').addClass('visible animated fadeInLeft');
        }, 400);
        setTimeout(function(){
          
          $('.tripe-slider .active .big-box').addClass('visible animated fadeInLeft');
        }, 800);          
    }, 1100);  
    
});



    $(".owl-logo").owlCarousel({
        nav: false,
        rewindNav : true,
        responsive:{
            320:{
                items:1,
                center: false
            },
            480:{
                items:2,
                center: false
            },
            620:{
                items:3,
                center: false
            },
            767:{
                items:5,
                center: false
            },
            992:{
                items:6,
                center: false
            },
            1200:{
                items:7,
                center: false
            }
        }
    });

});



function initMap() {


    var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: lat, lng: lng},
        scrollwheel: false,
        zoom: 16,
        styles:
        [{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#eff2f7"}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#2d446b"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#c0d4f4"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#7b97c4"}]}]
    });

    var marker = new google.maps.Marker({
        position: {lat: lat, lng: lng},
        map: map,
        icon: '/wp-content/themes/idvetmp/img/marker.png'

    });
}

var time = 100;
$('#menu-menu-gorne').find('.menu-item').children().each(function(index){
    var $this  = $(this);
    var count = $('#menu-menu-gorne').find('.menu-item').size();
    function delayed() {
         $this.addClass("animated fadeInUp");
         
            c = index + 1;
            if(c == count){
                startPosition = $('.navbar-nav .active');
                p = startPosition.offset();
                $('.menu-hover').css('left',p.left);

                setTimeout(
                  function() 
                  {
                    $('.menu-hover').css('opacity',1);
                  }, 1000);
            }
     }
    setTimeout( delayed , time );
    time += 100;
})

var didScroll;
var lastScrollTop = 0;
var delta = 5;
var navbarHeight = $('nav').outerHeight();

$(window).scroll(function(event){
    didScroll = true;
});

setInterval(function() {
    if (didScroll) {
        hasScrolled();
        didScroll = false;
    }
}, 250);

function hasScrolled() {
    var st = $(this).scrollTop();
    if(Math.abs(lastScrollTop - st) <= delta)
        return;
    if (st > lastScrollTop && st > navbarHeight){
        $('nav').removeClass('nav-down').addClass('nav-up');
    } else {
        if(st + $(window).height() < $(document).height()) {
            $('nav').removeClass('nav-up').addClass('nav-down');
        }
    }

    lastScrollTop = st;
}

$(document).ready(function(){




    $(".tripe-slider-rwd").owlCarousel({
        nav: true,
        rewindNav : true,
        navText: ["<img src='/wp-content/themes/idvetmp/img/arrow-left.jpg'>","<img src='/wp-content/themes/idvetmp/img/arrow-left.jpg'>"],
        responsive:{
            320:{
                items:1,
                center: false
            },
            480:{
                items:2,
                center: false
            },
            620:{
                items:2,
                center: false
            },
            767:{
                items:2,
                center: false
            },
            992:{
                items:3,
                center: false
            },
            1200:{
                items:3,
                center: false
            }
        }
    });
});
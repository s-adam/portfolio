<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package idvetmp
 *
 */
get_header();
the_post();
$fields = get_fields(get_the_ID());

?>

<div class="main-container empty-site">


                <div class="title-wrapper">
                    <h1 class="text-center"><?php the_title(); ?></h1>
                </div>
                <div class="container default-content">
                    <?php the_content(); ?>
                </div>

    <?php
    get_template_part('menu');
    ?>
    <div class="new-page">

    </div>
</div>
<?php
get_footer();

<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package idvetmp
 *
 */
/*
Template Name: Kontakt
*/
get_header();
the_post();
$fields = get_fields(get_the_ID());

?>
<div class="main-container">
    <div class="kontakt">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="large-text">
                        <?= $fields['informacje_kontaktowe']; ?>
                        
                    </div>
                </div>
                <div class="col-md-6 g-recaptcha custom-form">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="section-map">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="address-info">
                        <div class="wrapper">
                            
                            <?php the_field('adres', 'option'); ?>
                            <a href="mailto:<?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div id="map"></div>
                </div>
            </div>
        </div>
    </div>
</div>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDZlQEbzlmGxq7vNhDs9Wa_SI_SvLwV1As&callback=initMap" async defer></script>
    <script>

        var lat =  <?=$fields ['google_maps']['lat']?>;
        var lng =  <?=$fields ['google_maps']['lng']?>;

    </script>

<?php
get_footer();

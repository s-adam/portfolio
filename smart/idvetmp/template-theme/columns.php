<?php
$columnCount = (int) $content['ilość_kolumn'];
$colMD = 12 / $columnCount;

?>
<section class="<?=$content['acf_fc_layout']?>" style="<?=$content['section_style']?>">

    <div class="<?=!$content['full_width']?'container':'container-fluid'?>">
        <div class="row">
            <?php
            for($i=1;$i<=$columnCount;$i++) {
                ?>
                <div class="col-md-<?=$colMD?> col-sm-6">
                    <?=$content['kolumna_'.$i]?>
                </div>    
                <?php
            }?>
        </div>    
    </div>
</section>   
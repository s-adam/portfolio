<section class="<?=$content['acf_fc_layout']?>" style="<?=$content['section_style']?>">
    <div class="<?=!$content['full_width']?'container':'container-fluid'?>">
        <div class="row">
            <div class="col-md-12">
                <?=$content['tekst'];?>
            </div>
        </div>
    </div>
</section>
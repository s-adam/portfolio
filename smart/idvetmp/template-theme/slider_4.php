
<section class="<?=$content['acf_fc_layout']?>" style="<?=$content['section_style']?>">

    <div class="<?=!$content['full_width']?'container':'container-fluid'?>">
        <div class="row">
            
            <div class="carousel-nav">
                <span class="prev-slide">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/left-2.png" alt="prev">
                </span>
                
                <span class="next-slide">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/right-2.png" alt="next">
                </span> 
            </div>
            
            <div class="owl-carousel-<?=$content['acf_fc_layout']?> owl-carousel ">
               
                <?php
                    if($content['slide']){
                        foreach ($content['slide'] as $item) {
                            ?>
                            <div class="item text-center">
                                <?=wp_get_attachment_image( $item['slider_image'],'gallery-slider-size');?>
                                
                                <img src="<?php echo get_template_directory_uri(); ?>/img/slider-pzd.png" alt="" class="slider-special-pzd">
                                
                                <p><?=$item['slider_image_text']?></p>
                                
                            </div>
                            <?php
                        }
                    }
                ?>
            </div>
        </div>    
    </div> 
</section> 
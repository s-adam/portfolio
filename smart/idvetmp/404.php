<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package idvetmp
 */

get_header(); ?>

	<div class="main-container">
		<!-- The overlay -->
		<?php
		get_template_part('menu');
		?>
		<div class="flex-container">

			<h1 class="text-center"><?=pll__("Nie odnaleziono strony");?></h1>

		</div>






	</div>

<?php
get_footer();

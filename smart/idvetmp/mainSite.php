<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package idvetmp
 *
 */
/*
Template Name: Strona Główna
*/
get_header();
the_post();
$posts = get_posts( $args );
$fields = get_fields(get_the_ID());

?>

<div class="main-container">

    <div class="strona-glowna">
        <div class="container-fluid">
            <div class="row">
                <div class="section-main">
                    <div id="myModal" onclick="pauza()" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <div class="modal-content">

                                <div class="modal-body" id="popupVid">
                                    <iframe id="video" width="1880" height="900" src="//www.youtube.com/embed/FKWwdQu6_ok?enablejsapi=1&html5=1" frameborder="0" allowfullscreen></iframe>
                                    <a href="#" id="pauza" onclick="pauza()" class="play-btn-modal close-modal" data-dismiss="modal" style="right: -1%;top:-1%;"><i class="fa fa-times" aria-hidden="true"></i></a>

                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="mar">
                        <div class="col-md-6">
                            <div class="left-site">
                                <div class="flex-container">

                                    
                                        <div class="wrapper-outer">
                                            <div class="circles c1"></div>
                                            <div class="circles c2"></div>
                                            <div class="circles c3"></div>
                                            <div class="circles c4"></div>
                                        </div>
                                    <a href="#" class="play-btn" id="play-button" data-toggle="modal" data-target="#myModal"><i class="fa fa-play" aria-hidden="true"></i></a>
                                    <span class="robot">
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="right-site">
                                <div class="site-wrapper">
                                    <div class="main-title">
                                        <?= $fields ['sekcja_pierwsza'] ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="container-fluid">
        <div class="row">
            <div class="kim-jestesmy">
                <div class="col-md-6">
                    <div class="section-left-site">
                        <div class="section-left-wrapper">
                            <div class="title">
                                <p><?=pll__("Kim Jesteśmy?");?></p>
                            </div>
                            <?= $fields ['sekcja_kim_jestesmy'] ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="section-right-site">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/bg-kim-jestesmy.jpg" alt="">
                        <div class="smart-wrapper">
                            <h4>SMART</h4>
                            <p>AUTOMATION</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
$args = [
    'post_type' => 'page',
    'nopaging' => true,
    'meta_key' => '_wp_page_template',
    'meta_value' => 'usluga.php',
    'order' => 'ASC'
];
$posts = get_posts( $args );
?>
    <div class="container">
        <div class="row triple-slider-wrapper">

            <span id="triple-prev-slide">
                <img src="<?php echo get_template_directory_uri(); ?>/img/arrow-left.png" alt="">
            </span>

            <span id="triple-next-slide">
                <img src="<?php echo get_template_directory_uri(); ?>/img/arrow-left.png" alt="">
            </span>

            <div class="col-md-12">
                <div class="robot-triple"></div>
        <div class="tripe-slider owl-carousel owl-theme">
                    <?php
                        $i = 0;
                        $j = 0;
                        $m = 1;
                        foreach ( $posts as $post ) : setup_postdata( $post );
						$field = get_fields(get_the_ID());
                            if ($i == 0) {
                                echo '<div class="item">';
                                echo '';
                                echo '<div class="big-box">';
                            }
                            if ($i == 1) {
                                echo '<div class="small-box-1">';

                            }
                            if ($i == 2) {
                                echo '<div class="medium-box-1">';
                            }
                            ?>
                                <div class="content">
                                    <h1><?php the_title(); ?></h1>
                                    <?= $field ['zajawka'] ?>
                                    <a href="<?php the_permalink(); ?>" class="btn read-more"><?=pll__("Zobacz");?></a>
                                </div>
                         </div>
                    <?php
                    $m++;
                    $i++;
                    $j++;
                    if ($i == 3 || count($posts) == $j) {
                        echo ' </div>';
                        $i = 0;
                    }
                    endforeach;
                    wp_reset_postdata();
?>

            </div>
        </div>
    </div>
        <div class="row triple-slider-wrapper-rwd">
            <div class="col-md-12">
                <div class="tripe-slider-rwd text-center owl-carousel owl-theme">
                    <?php
                    foreach ( $posts as $post ) : setup_postdata( $post );
					$field = get_fields(get_the_ID());
                    ?>
                    <div class="item">
                        <div class="content">
                            <h1><?php the_title(); ?></h1>
                            <?= $field ['zajawka'] ?>
                            <a href="<?php the_permalink(); ?>" class="btn read-more"><?=pll__("Zobacz");?></a>
                        </div>
                    </div>

                <?php
                endforeach;
                wp_reset_postdata();
                ?>
                </div>
            </div>
        </div>

<div class="aktualnosci-bg">
    <div class="container">
        <div class="title">
            <p><?=pll__("Aktualności");?></p>
        </div>
        <div class="row">
            <div class="aktualnosci">
                <?php


                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                query_posts(array('posts_per_page' => 3, 'paged' => $paged));

                if ( have_posts() ) : ?>
                    <?php

                    while ( have_posts() ) : the_post();


                        ?>
                        <?php
                        $row = false;
                        ?>
                        <?php if ( has_post_thumbnail() ) : ?>

                        <?php endif; ?>
                        <?php
                        ?>
                        <div class="col-md-4">
                            <div class="one-item">
                                <p class="title-item"><strong><?php the_title(); ?></strong></p>
                                <p class="title-date"><?php the_time('d.m.Y'); ?></p>

                                <div class="item-content">
                                    <p>
                                        <?php
                                        the_excerpt_max_charlength(140);
                                        ?>
                                    </p>
                                </div>

                                <a href="<?php the_permalink();?>" class="btn"><?=pll__("Czytaj");?></a>
                            </div>
                        </div>
                        <?php

                    endwhile;

                endif;
                ?>

                <a href="/aktualnosci" class="btn read-more text-center"><?=pll__("Czytaj wszystkie");?></a>
            </div>
        </div>
    </div>

</div>

    <div class="container">
        <div class="slider">
            <div class="owl-carousel owl-logo">
                <?php
                    $images = $fields['sekcja_sliderfirmy'];
                    if($images){

                    foreach ($images as $field) {

                ?>
                                <a href="<?= $field['sizes']['large']  ?>"><img class="text-center" src="<?= $field['sizes']['gallery-small-img']  ?>" alt=""></a>
                        <?php
                        }
                    }

                    ?>
            </div>
        </div>
    </div>


</div>




<?php get_footer(); ?>



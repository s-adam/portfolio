<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package idvetmp
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/img/favico/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/img/favico/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/img/favico/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/img/favico/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/img/favico/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/img/favico/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/img/favico/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/img/favico/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/img/favico/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri(); ?>/img/favico/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/img/favico/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri(); ?>/img/favico/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/img/favico/favicon-16x16.png">
    <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/img/favico/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff ">
    <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/img/favico/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff ">



<?php wp_head(); ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/magnific-popup.css">
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:400,500,600&amp;subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/animate.min.css">

</head>

<body <?php body_class(); ?>>

<nav class="navbar navbar-default nav-down">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt=""></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <div class="wrapper-menu">
                <ul class="strefa hidden-sm hidden-xs">
                    <li><a href="#" class="strefa-clienta">STREFA KLIENTA</a></li>
                    <li><a href="/" class="<?=pll_current_language('slug')=='pl'?'active':''?>">PL</a></li>
                    <li><a href="/en" class="<?=pll_current_language('slug')=='en'?'active':''?>">EN</a></li>
                    <li><a href="/ru" class="<?=pll_current_language('slug')=='ru'?'active':''?>">RU</a></li>
                </ul>
                <div id="parallelogram"></div>
                
                <?php
					wp_nav_menu(array(
							'menu' => 'primary',
							'theme_location' => 'primary',
							'depth' => 3,
							'container' => false,
							'container_class' => 'collapse navbar-collapse',
							'menu_class' => 'nav navbar-nav',
							'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
							'walker' => new wp_bootstrap_navwalker())
					);
					?>
                <ul class="strefa hidden-md hidden-lg">
                    <li><a href="#" class="strefa-clienta">STREFA KLIENTA</a></li>
                    <li><a href="/" class="<?=pll_current_language('slug')=='pl'?'active':''?>">PL</a></li>
                    <li><a href="/en" class="<?=pll_current_language('slug')=='en'?'active':''?>">EN</a></li>
                    <li><a href="/ru" class="<?=pll_current_language('slug')=='ru'?'active':''?>">RU</a></li>
                </ul>
                
            </div>
            
        </div><!--/.nav-collapse -->
    </div><!--/.container-fluid -->
    <span class="menu-hover"></span>
</nav>

<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package idvetmp
 *
 */
/*
Template Name: Kariera
*/
get_header();
the_post();
$fields = get_fields(get_the_ID());

?>

<div class="main-container">
    <div class="kariera">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="special-title">
                        <h3>Kariera</h3>
                    </div>
                    <div class="kariera-wrapper">
                        <?php
                    $images = get_field('stanowisko');
                        $a = 0;
                    if($images){

                        foreach ($images as $field) {

                            ?>

                        <div class="kariera-each-wrapper">
                            <?= $field ['Widoczny_opis_stanowiska'] ?>
                            <div class="collapse" id="collapseExample<?=$a?>">
                            <?= $field ['ukryty_tekst_stanowiska'] ?>
                                </div>
                                <a class="btn2 rotate" data-toggle="collapse" href="#collapseExample<?=$a?>" aria-expanded="false" aria-controls="collapseExample">
                                    <?=pll__("Zobacz");?>
                                </a>
                            </div>
                            <?php
                            $a++;
                        }
                    }

                    ?>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();

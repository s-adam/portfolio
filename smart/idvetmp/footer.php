<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package idvetmp
 */
?>
<footer class="footer-show">
    <div class="container">
        <div class="row">
           <div class="col-md-4">
               <div class="title">
                   <p><?=pll__("Serwis 24h");?></p>
               </div>
               <div class="phone-info">
                   <a href="tel:<?php the_field('telefon1', 'option'); ?>">+48 <?php the_field('telefon1', 'option'); ?></a>
                   <p><?php the_field('tekst_pod_telefonem_nr_1', 'option'); ?></p>
                   <a href="tel:<?php the_field('telefon2', 'option'); ?>">+48 <?php the_field('telefon2', 'option'); ?></a>
                   <p><?php the_field('tekst_pod_telefonem_nr_2', 'option'); ?></p>
               </div>
           </div>
            <div class="col-md-4">
                <div class="title">
                    <p><?=pll__("Dane firmy");?></p>
                </div>
                <div class="wrapper">
                    <?php the_field('adres', 'option'); ?>
                    <a href="mailto:<?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="logo-footer">
                    <p><?=pll__("Wszelkie prawa zastrzeżone");?> &copy <?php the_date('Y')?></p>
                    <p><?=pll__("Smart Automation Sp. z.o.o");?></p>
                    <img class="smart-logo" src="<?php echo get_template_directory_uri(); ?>/img/footer_logo_2.png" alt=""></br>
                    <a href="https://ideative.pl/" target="_blank"><img class="ideative-logo" src="<?php echo get_template_directory_uri(); ?>/img/footer_logo_ideative.png" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</footer>
<footer class="footer-hide">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img class="smart-logo" src="<?php echo get_template_directory_uri(); ?>/img/footer_logo_2.png" alt="">
                <div class="brand-wrapper">
                    <p><?=pll__("Wszelkie prawa zastrzeżone");?> &copy <?php the_time('Y')?></p>
                    <p><?=pll__("Smart Automation Sp. z.o.o");?></p>
                </div>
            </div>
            <div class="col-md-6">
                <a href="https://ideative.pl/" target="_blank"><img class="ideative-logo" src="<?php echo get_template_directory_uri(); ?>/img/footer_logo_ideative.png" alt=""></a>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

<script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.magnific-popup.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.viewportchecker.js"></script>

<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.min.js"></script>


<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.countTo.js"></script>

</body>
</html>
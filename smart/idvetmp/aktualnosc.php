<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package idvetmp
 *
 */
/*
Template Name: Aktualnosc
*/
get_header();
the_post();
$fields = get_fields(get_the_ID());

?>
<div class="main-container">
    <div class="kariera">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="special-title special-title-archive">
                        
                    </div>
                    <div class="kariera-wrapper aktualnosci-wrapper">
                        <div class="kariera-each-wrapper aktualnosc-wrapper">
                            <p class="post-title"><strong>Lorem ipsum dolor sit.</strong></p>
                            <p class="date">12.12.2017</p>
                            <div class="text-wrapper">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab at cupiditate dignissimos dolores et explicabo facere ipsum iste nostrum odit officiis quam, sint tenetur totam veritatis? Impedit non perferendis sequi!</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab at cupiditate dignissimos dolores et explicabo facere ipsum iste nostrum odit officiis quam, sint tenetur totam veritatis? Impedit non perferendis sequi!</p>
                            
                            </div>
                        </div>
                        

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();

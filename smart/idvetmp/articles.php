<?php
/**
 * Template Name: lista postów
 *
 */
get_header();
the_post();

$fields = get_fields(get_the_ID());

?>
<main id="main" class="site-main" role="main">


    
    <div class="subpage subpage-r">

        <div class="container ">

            <div class="row">
                <div class="col-md-12">
            <?php 

            $posts = (get_query_var('paged')) ? get_query_var('paged') : 1;
            $c = query_posts(array('posts_per_page' => 7, 'paged' => $paged));        

            if (have_posts()) :

                while (have_posts()) : the_post();
                    $fields = get_fields(get_the_ID());

                    echo '<div class="row post-row">';
                    ?>
                    <div class="col-md-12 ">

                        <div class="post-title">
                             <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                         </div>  

                        <div class="post-c">
                            <p><?php the_excerpt() ?></p>
                        </div>    
                        <a href="<?php the_permalink(); ?>" class="btn btn-blue"><span>Czytaj więcej</span></a>
                    </div>
                    <?php
                    echo '</div>';
                endwhile;
                wp_reset_postdata();
            else:
                echo '<div class="alert alert-info">Brak wpisów</div>';
            endif; ?>
                    
                <?php
                the_posts_navigation();
                ?>  
              </div>   

            </div>    
        </div>
    </div>
    
</main>    
    
<?php
get_footer();
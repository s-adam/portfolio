<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 * 
 */

get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>
<div class="main-wrapper aktualnosci">
    <div class="glass-bg anim" data-vp-add-class="animated fadeIn">
        <div class="container">
            <div class="title-section">
                <div class="row">
                    <div class="col-md-6">
                        <div class="ramka-frame" style="background: url(<?= $fields['zdjecie_obok_tytulu_strony']['sizes']['title-img']; ?>) left 1px top no-repeat">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/ramka-white.jpg">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h1>Aktualności</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bigger-wrapper">
        <div class="content-single-area">
            <div class="container">
                <div class="row">
                    <?php
                    //juz
                    global $query_string;
                    query_posts("{$query_string}&posts_per_page=4");
                    $args = array(
                        'posts_per_page' => 4,
                        'post_type' => 'aktualnosci',
                        'paged' => get_query_var('paged'),
                    );
                    $wp_query = new WP_Query($args);
                    while ($wp_query->have_posts()) : $wp_query->the_post();

                    $fields = get_fields(get_the_ID());
                    ?>
                    <div class="col-md-6 col-sm-6">
                        <div class="wrapper-for-half-circle anim" data-vp-add-class="animated fadeIn">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="photo-section">
                                        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('aktualnosci-img'); ?></a>
                                        <div class="wrapper" data-anim="base wrapper">
                                            <div class="circle" data-anim="base right"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="latest-news__single-col">
                                        <span><?php the_time("j F Y"); ?></span>
                                        <div class="wrapper-for-title">
                                            <div class="latest-news__post-title">
                                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    endwhile;

//                    endif;
                    ?>

                    <?php
                    numeric_posts_nav();
                    ?>

                </div>
            </div>
        </div>
    </div>
</div>



<?php
//get_sidebar();
get_footer();

<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 * 
 */
/*
  Template Name: O Firmie
 */
get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>
<div class="main-wrapper o-firmie">
    <div class="glass-bg">
        <div class="container">
            <div class="title-section anim" data-vp-add-class="animated fadeIn">
                <div class="row">
                    <div class="col-md-6">
                        <div class="ramka-frame" style="background: url(<?=$fields['zdjecie_obok_tytulu']['sizes']['title-img']; ?>) left 1px top no-repeat">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/ramka-white.jpg">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h1><?php the_title(); ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bigger-wrapper">
        <div class="content-single-area">
            <div class="container">
                <div class="about-ophthalmologist-adventages anim" data-vp-add-class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="left">
                                <?= $fields['sekcja_pierwsza_-_tekst_po_lewej_stronie']; ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="right">
                                <?= wp_get_attachment_image($fields['sekcja_pierwsza_-_zdjecie_po_prawej_stronie']['ID'], 'wizytowka-img'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="about-ophthalmologist-adventages anim" data-vp-add-class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="left second-left">
                                <?= $fields['sekcja_druga_-_tekst_po_lewej_stronie']; ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="right second-right">
                                <?= $fields['sekcja_druga_-_tekst_po_prawej_stronie']; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="certificates anim" data-vp-add-class="animated fadeIn">
                <div class="container">
                    <div class="main-title-wrapper">
                        <div class="main-title">
                            <?= $fields['tytul_sekcji_certyfikaty']; ?>
                        </div>
                    </div>
                    <div class="certificates-swiper">
                        <div class="row">
                            <div class="col-md-5 text-center">
                                <div class="certyficate-photo">
                                  
                                    <a href="<?= $fields['certyfikaty']['sizes']['lightbox-img']?>" data-toggle="lightbox" data-gallery="example-galelerypp"><?= wp_get_attachment_image($fields['certyfikaty']['ID'], 'duze-zdj'); ?></a>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="certyficate-text">
                                    <?= $fields['tekst_obok_certyfikatu']; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="slider-home class1790 anim" data-vp-add-class="animated fadeIn">
        <div class="container">
            <?php include 'slider.php'; ?>
        </div>
    </div>
</div>



<?php
//get_sidebar();
get_footer();

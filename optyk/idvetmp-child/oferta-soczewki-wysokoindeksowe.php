<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 * 
 */
/*
 * Template Name: Soczewki wysokoindeksowe
 * Template Post Type: oferta
 */

get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>
<div class="main-wrapper oferta-single okulary-korekcyjne okulary-korekcyjne-end wysokoindeksowane">
    <div class="glass-bg anim" data-vp-add-class="animated fadeIn">
        <div class="container">
            <div class="title-section">
                <div class="row">
                    <div class="col-md-6">
                        <div class="ramka-frame" style="background: url(<?= $fields['zdjecie_obok_tytulu_strony']['sizes']['title-img']; ?>) left 1px top no-repeat">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/ramka.png">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h1><?php echo get_the_title(62); ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bigger-wrapper">
        <div class="content-single-area">
            <div class="container">
                <div class="wrapper-for-menu">
                    <ul>
                        <?php
                        
                        
                        $parent_page_id = ( '0' != $post->post_parent ? $post->post_parent : $post->ID );
                        $mypages = get_pages(array('child_of' => $parent_page_id, 'post_type' => 'oferta'));
//                        echo $term;
                        foreach ($mypages as $page) {
                            $content = $page->post_content;
                            $term = get_the_title();
                            ?>
                            <li class="<?php 
                            if($term == $page->post_title){
                                echo "active";
                            }
                            ?>"><a class="<?php 
                            if($term == $page->post_title){
                                echo "active";
                            }
                            ?>" href="<?php echo get_page_link($page->ID); ?>"><?php echo $page->post_title; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="section-first anim" data-vp-add-class="animated fadeIn">
                    <div class="row">
                        <div class="offer-section-title">
                            <!--<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/before-h2.png ?>">-->
                            <h2><?= $fields['sekcja_pierwsza_-_tytul_sekcji'] ?></h2>
                        </div>
                        <div class="col-md-6">
                            <?php while (the_flexible_field("sekcja_pierwsza_-_tekst_po_lewej_stronie")): ?>

                                <?php if (get_row_layout() == "red-label"): // layout: Content  ?>

                                    <div class="red-label">
                                        <?php the_sub_field("red-label"); ?>
                                    </div>

                                <?php elseif (get_row_layout() == "circle-label"): // layout: File  ?>
                                    <div class="circle-label">
                                        <?php the_sub_field("circle_label"); ?>
                                    </div>
                                <?php endif; ?>

                            <?php endwhile; ?>
                        </div>
                        <div class="col-md-6">
                            <div class="right right3">
                                <div class="wrapper wrapper3 wrapper4" data-anim="base wrapper">
                                    <div class="circle" data-anim="base right"></div>
                                </div>
                                <?= wp_get_attachment_image($fields['sekcja_pierwsza_-_zdjecie_po_prawej_stronie']['ID'], 'oferta-zdj-2'); ?>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?php if ($fields['sekcja_pierwsza_-_tekst_na_cala_szerokosc_strony']) { ?>
                                <?= $fields['sekcja_pierwsza_-_tekst_na_cala_szerokosc_strony'] ?>
                            <?php } ?> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bigger-wrapper">
        <div class="content-single-area">
            <div class="container">
                <div class="section-third anim" data-vp-add-class="animated fadeIn">
                    <div class="row">
                        <div class="offer-section-title">
                            <h2><?= $fields['sekcja_trzecia_-_tytul_sekcji'] ?></h2>
                        </div>
                        <div class="col-md-6">
                            <div class="red-label">
                                <?= $fields['sekcja_trzecia_-_tekst_po_lewej_stronie'] ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="right right3 improvement-right">
                                <div class="wrapper wrapper3" data-anim="base wrapper">
                                    <div class="circle" data-anim="base right"></div>
                                </div>
                                <?= $fields['sekcja_trzecia_-_tekst_po_prawej_stronie'] ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section-cooperation">
                    <div class="offer-section-title">
                        <h2><?= $fields['wspolpraca_-_title'] ?></h2>
                    </div>
                    <?php
                    if ($fields['wspolpraca_-_logo_partnera']) {
                        foreach ($fields['wspolpraca_-_logo_partnera'] as $field) {

                            if ($field['adres_url']) {
                                ?>
                    <a target="_blank" href="<?= $field['adres_url'] ?>"><?= wp_get_attachment_image($field['logo']['ID'], 'wizytowka-img'); ?></a>


                                
                                    <?php
                            }else{
                                ?>
                                <?= wp_get_attachment_image($field['logo']['ID'], 'wizytowka-img'); ?>
                                <?php
                            }
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="wrappe2 anim" data-vp-add-class="animated fadeIn">
        <?php include 'cta.php'; ?>
    </div>

</div>



<?php
get_footer();

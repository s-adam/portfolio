<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package idvetmp
 */
?>

</div><!-- #content -->

<footer class="site-footer">
    <div class="container">
        <div class="footer-info">
            <div class="row">
                <div class="col-md-6 col-lg-3">
                    <div class="footer-info__single-col anim" data-vp-add-class="animated fadeIn">
                        <?= the_field('adres', 'option'); ?>


                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="footer-info__single-col">
                        <div class="row">
                            <?php $variable = get_field('dni_otwarcia', 'option'); ?>
                            <?php
                            if ($variable) {
                                foreach ($variable as $field) {
                                    ?>

                                    <div class="col-md-4 col-sm-6 col-xs-6">
                                        <?= $field['dzien'] ?>
                                    </div>
                                    <div class="col-md-8 col-sm-6 col-xs-6">
                                        <?= $field['godzina'] ?>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">
                    <div class="footer-info__single-col">
                        <div class="row">
                            <div class="col-md-4">
                                <img id="duza_rodzina" src="<?php echo get_stylesheet_directory_uri(); ?>/img/karta_duzej_rodziny.png" alt="">
                            </div>
                            <div class="col-md-8">
                                <div class="simple-w">
                                    <?= the_field('stopka_-_karta_duzej_rodziny', 'option'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">
                    <div class="footer-info__single-col">
                        <div class="row">
                            <div class="col-md-4">
                                <img id="karta-visa-mastercard" src="<?php echo get_stylesheet_directory_uri(); ?>/img/visa-mastercard.png" alt="">
                            </div>
                            <div class="col-md-8">
                                <div class="s">
                                    <?= the_field('stopka_-_platnosc_karta', 'option'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center footer-text">
                L.K Bieleccy &copy <?= date('Y') ?> Wszelkie prawa zastrzeżone.
            </div>
        </div>         
        <div class="row">
            <div class="col-md-12 text-center footer-autor">
                <a href="https://ideative.pl">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/by-ideative.svg ?>" alt="ideative.pl">
                </a>
            </div>
        </div>    
    </div>

</footer><!-- #colophon -->
</div><!-- #page -->
<script src='<?php echo get_stylesheet_directory_uri(); ?>/js/lottie.js'></script>
<?php wp_footer(); ?>

<?php if ((get_the_ID() == 30)) {
    ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDT5ONMSnq3pV_L5VlenZYFv4I-UBlVHDo&callback=initMap"
    async defer></script>
    <?php
}
?>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>


<?php if ((get_the_ID() == 56) || (get_the_ID() == 185)) {
    ?>
    <script src='<?php echo get_stylesheet_directory_uri(); ?>/js/lottie.js'></script>
    <script>
        
        var animation = lottie.loadAnimation({
            container: document.getElementById('bm'),
            renderer: 'svg',
            loop: false,
            autoplay: true,
            path: '/wp-content/themes/idvetmp-child/js/data.json'
        });
    </script>
    <?php
}
?>

</body>
</html>

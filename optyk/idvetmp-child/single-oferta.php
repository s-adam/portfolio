<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 * 
 */

get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>
<div class="main-wrapper oferta-single">
    <div class="glass-bg anim" data-vp-add-class="animated fadeIn">
        <div class="container">
            <div class="title-section" style="background: url(<?php echo get_stylesheet_directory_uri(); ?>/img/gabinet-okulistyczny.jpg) left top no-repeat">
                <div class="row">
                    <div class="col-md-6">

                    </div>
                    <div class="col-md-6">
                        <h1><?= the_title(); ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bigger-wrapper">
        <div class="content-single-area">
            <div class="container">
                <div class="section-first anim" data-vp-add-class="animated fadeIn">
                    <div class="row">
                        <div class="offer-section-title">
                            <!--<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/before-h2.png ?>">-->
                            <h2><?= $fields['sekcja_pierwsza_-_tytul_sekcji'] ?></h2>
                        </div>
                        <div class="col-md-6">
                            <?php while (the_flexible_field("sekcja_pierwsza_-_tekst_po_lewej_stronie")): ?>

                                <?php if (get_row_layout() == "red-label"): // layout: Content ?>

                                    <div class="red-label">
                                        <?php the_sub_field("red-label"); ?>
                                    </div>

                                <?php elseif (get_row_layout() == "circle-label"): // layout: File ?>
                                    <div class="circle-label">
                                        <?php the_sub_field("circle_label"); ?>
                                    </div>
                                <?php endif; ?>

                            <?php endwhile; ?>
                        </div>
                        <div class="col-md-6">
                            <?= wp_get_attachment_image($fields['sekcja_pierwsza_-_zdjecie_po_prawej_stronie']['ID'], 'wizytowka-img'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div></div>
    <div class="section-second anim" data-vp-add-class="animated fadeIn">
        <div class="container">
            <div class="row">
                <div class="offer-section-title no-color">
                    <h2><?= $fields['sekcja_druga_-_tytul_sekcji'] ?></h2>
                </div>
                <div class="col-md-6">
                    <div class="left">
                        <div class="red-label">
                            <?= $fields['sekcja_druga_-_tekst_po_lewej_stronie'] ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="right">
                        <?= $fields['sekcja_druga_-_tekst_po_prawej_stronie'] ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bigger-wrapper">
        <div class="content-single-area">
            <div class="container">
                <div class="section-third anim" data-vp-add-class="animated fadeIn">
                    <div class="row">
                        <div class="offer-section-title">
                            <h2><?= $fields['sekcja_pierwsza_-_tytul_sekcji'] ?></h2>
                        </div>
                        <div class="col-md-6">
                            <?php while (the_flexible_field("sekcja_pierwsza_-_tekst_po_lewej_stronie")): ?>

                                <?php if (get_row_layout() == "red-label"): // layout: Content ?>

                                    <div class="red-label">
                                        <?php the_sub_field("red-label"); ?>
                                    </div>

                                <?php elseif (get_row_layout() == "circle-label"): // layout: File ?>
                                    <div class="circle-label">
                                        <?php the_sub_field("circle_label"); ?>
                                    </div>
                                <?php endif; ?>

                            <?php endwhile; ?>
                        </div>
                        <div class="col-md-6">
                            <div class="right">
                                <?= $fields['sekcja_druga_-_tekst_po_prawej_stronie'] ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section-cooperation anim" data-vp-add-class="animated fadeIn">
                    <div class="offer-section-title">
                        <h2><?= $fields['wspolpraca_-_title'] ?></h2>
                    </div>
                    <?php
                    if ($fields['wspolpraca_-_logo_partnera']) {
                        foreach ($fields['wspolpraca_-_logo_partnera'] as $field) {
                            ?>

                            <a href="<?= $field['adres_url'] ?>"><?= wp_get_attachment_image($field['logo']['ID'], 'wizytowka-img'); ?></a>


                        <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>

    <div class="wrappe2 anim" data-vp-add-class="animated fadeIn">
        <?php include 'cta.php'; ?>
    </div>
    

</div>



<?php
get_footer();

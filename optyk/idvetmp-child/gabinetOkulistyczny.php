<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 * 
 */
/*
  Template Name: Gabinet Okulistyczny
 */
get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>
<div class="main-wrapper gabinet">
    <div class="glass-bg anim" data-vp-add-class="animated fadeIn">
        <div class="container">
            <div class="title-section">
                <div class="row">
                    <div class="col-md-6">
                        <div class="ramka-frame" style="background: url(<?= $fields['zdjecie_obok_tytulu']['sizes']['title-img']; ?>) left 1px top no-repeat">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/ramka.png">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h1><?php the_title(); ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bigger-wrapper">
        <div class="content-single-area">
            <div class="container">
                <div class="about-ophthalmologist-adventages anim" data-vp-add-class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="left">
                                <?= $fields['sekcja_pierwsza_-_tekst_po_lewej_stronie']; ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="right">
                                <div class="red-label">
                                    <?= $fields['sekcja_pierwsza_-_tekst_po_prawej_stronie']; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="single-frame anim" data-vp-add-class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="personal-data">
                                <?= $fields['sekcja_druga_-_opis_lekarza'] ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="wrapper-image">
                                
                                <img class="doctor-img" src="<?= $fields['sekcja_druga_-_zdjecie_lekarza']['sizes']['duze-zdj'] ?>">
                                <!--<div class="right"></div>-->
                                <div class="wrapper" data-anim="base wrapper">
                                    <!--<div class="circle" data-anim="base left"></div>-->
                                    <div class="circle" data-anim="base right"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row anim" data-vp-add-class="animated fadeIn">
                    <div class="col-md-6">
                        <div class="single-box">
                            <span class="simple-title">REJESTRACJA</span>
                            <ul>
                                <?php
                                if ($fields['sekcja_trzecia_-_numer']) {
                                    foreach ($fields['sekcja_trzecia_-_numer'] as $field) {
                                        ?>
                                <li><a target="_blank" href="<?= $field['numer_telefonu'] ?>"><i class="fas fa-phone"></i> <?= $field['numer_telefonu'] ?></a></li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                            <?= $fields['sekcja_trzecia_-_rejestracja'] ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="single-box-data">
                            <span class="simple-title">GODZINY PRZYJĘĆ LEKARZA OKULISTY</span>
                            <?= $fields['sekcja_trzecia_-_godziny_przyjec']; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<?php
//get_sidebar();
get_footer();

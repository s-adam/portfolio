<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 * 
 */
/*
 * Template Name: Oprawy Okularowe
 * Template Post Type: oferta
 */
get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>
<div class="main-wrapper oferta-single oprawy-okularowe">
    <div class="glass-bg anim" data-vp-add-class="animated fadeIn">
        <div class="container">
            <div class="title-section">
                <div class="row">
                    <div class="col-md-6">
                        <div class="ramka-frame" style="background: url(<?= $fields['zdjecie_obok_tytulu_strony']['sizes']['title-img']; ?>) left 1px top no-repeat">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/ramka.png">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h1><?php the_title(); ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bigger-wrapper">
        <div class="content-single-area">
            <div class="container">
                <div class="about-ophthalmologist-adventages anim" data-vp-add-class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="left">
                                <?= $fields['sekcja_pierwsza_-_tekst_pierwszy'] ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="right right3">
                                <div class="wrapper wrapper3" data-anim="base wrapper">
                                    <div class="circle" data-anim="base right"></div>
                                </div>
                                <?= wp_get_attachment_image($fields['sekcja_pierwsza_-_zdjecie']['ID'], 'title-img'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div></div>
    <div class="section-second">
        <div class="container">
            <div class="inside-wrapper anim" data-vp-add-class="animated fadeIn">
                <div class="row">
                    <div class="col-md-6">
                        <div class="left">
                            <div class="red-label">
                                <?= $fields['sekcja_druga_-_tekst_po_lewej_stronie'] ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="left">
                            <div class="red-label">
                                <?= $fields['sekcja_druga_-_tekst_po_prawej_stronie'] ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bigger-wrapper">
        <div class="content-single-area">
            <div class="container">
                <div class="section-third anim" data-vp-add-class="animated fadeIn">
                    <div class="inside-wrapper">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="right">
                                    <?= $fields['sekcja_trzecia_-_tekst']; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section-fourth">
                    <div class="row">
                        <div class="offer-section-title">
                            <h2><?= $fields['sekcja_czwarta_-_tytul'] ?></h2>
                        </div>
                        <div class="col-md-12">
                            <div class="simple-text">
                                <?= $fields['sekcja_czwarta_-_tekst'] ?>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wrappe2 anim" data-vp-add-class="animated fadeIn">
    <?php include 'cta.php' ?>
</div>



<?php
get_footer();

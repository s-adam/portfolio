<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 * 
 */
/*
  Template Name: Strona Główna
 */
get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>
<div class="main-wrapper">

    <div class="wrapper-for-glasses" style="background: url(<?php echo get_stylesheet_directory_uri(); ?>/img/okulary-home.png) center center no-repeat">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="main-section">
                        <img class="main-section-img" src="<?php echo get_stylesheet_directory_uri(); ?>/img/main-photo.jpg">
                        <div class="social-section">
                            <?php
                            $s1 = get_field('facebook', 'option');
                            $s2 = get_field('instagram', 'option');
                            
                            
                            if($s1 || $s2){
                                if ($fields['tekst_obok_social_media']) {
                                ?>
                                <p><?= $fields['tekst_obok_social_media'] ?></p>
                                <?php
                            }
                            }
                            ?>



                            <ul>
                                <?php
                                if (get_field('facebook', 'option')) {
                                    ?>
                                    <li><a target="_blank" href="<?php echo the_field('facebook', 'option'); ?>"><i class="fab fa-facebook-f"></i></a></li>
                                    <?php
                                }
                                ?>
                                <?php
                                if (get_field('instagram', 'option')) {
                                    ?>
                                    <li><a target="_blank" href="<?php the_field('instagram', 'option') ?>"><i class="fab fa-instagram"></i></a></li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="promotion anim" data-vp-add-class="animated rotateInUpRight">
                            <?php
                            if ($fields['promocja-napis']) {
                                ?>
                                <span><?= $fields['promocja-napis'] ?></span>
                                <p></p>
                                <?php
                            }
                            ?>
                            <?php
                            if ($fields['promocja_-_tekst_promocji']) {
                                ?>
                                <?= $fields['promocja_-_tekst_promocji'] ?>
                                <?php
                            }
                            ?>

                        </div>
                        <div class="look">
                            <span class="big-one">
                                <div class="Iam">
                                    <b>
                                        <div class="innerIam">
                                            <br /> 
                                            <?= $fields['duzy_napis'] ?><br />
                                        </div>
                                    </b>

                                </div>
                            </span>
                                <span class="big-one2">
                                <div class="Iam">
                                    <b>
                                        <div class="innerIam">
                                            <br /> 
                                            <?= $fields['duzy_napis'] ?><br />
                                        </div>
                                    </b>

                                </div>
                            </span>
                            </br>
                            <!--<span class="red-one"><?= $fields['maly_napis'] ?></span>-->
                            <span class="red-one">    
                                <div class="Iam">
                                    <b>
                                        <div class="innerIam">
                                            <br /> 
                                            <?= $fields['maly_napis'] ?><br />
                                        </div>
                                    </b>

                                </div></span>
                        </div>
                        <div class="wrapper wrapper-for-home" data-anim="base wrapper">
                            <div id="bm"></div>
                            
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--koniec-->


<div class="oferta-big-wrapper class1790">
    <div class="container">
        <div class="oferta">
            <?php
            if ($fields['sekcja_nasza_oferta_-_tytul']) {
                ?>
                <div class="main-title">
                    <?= $fields['sekcja_nasza_oferta_-_tytul'] ?>
                </div>

                <?php
            }
            ?>

            <div class="wrapper-for-oferta">
                <div class="row">
                    <div class="wrapper" data-anim="base wrapper">
                        <div class="circle" data-anim="base right"></div>
                    </div>
                    <div class="wrapper wrapper2" data-anim="base wrapper">
                        <div class="circle" data-anim="base right"></div>
                    </div>
                    <?php
                    //juz
                    global $query_string;
                    query_posts("{$query_string}&posts_per_page=4");
                    $args = array(
                        'posts_per_page' => 4,
                        'post_parent' => 0,
                        'post_type' => 'oferta',
                        'paged' => get_query_var('paged'),
                        'order' => 'DESC'
                    );
                    $wp_query = new WP_Query($args);
                    while ($wp_query->have_posts()) : $wp_query->the_post();

                        $fields = get_fields(get_the_ID());
                        ?>
                        <div class="col-md-3 col-sm-6 anim" data-vp-add-class="animated fadeIn">

                            <figure class="effect-apollo oferta__single-wrapper">
                                <img src="<?php the_post_thumbnail_url('oferta-photo'); ?>" alt="#">
                                <figcaption>

                                    <a href="<?php the_permalink(); ?>"><span><?php the_title(); ?></span></a>
                                </figcaption>			
                            </figure>


                <!--                            <div class="oferta__single-wrapper" style='background: url(<?php the_post_thumbnail_url('oferta-photo'); ?>) no-repeat; '>
                                                <a href="<?php the_permalink(); ?>"><span><?php the_title(); ?></span></a>
                                            </div>-->
                        </div>
                        <?php
                    endwhile;
                    wp_reset_query();
                    ?>

                </div>
            </div>
        </div>
    </div>
</div>


<?php include 'cta.php'; ?>


<div class="latest-news class1790">
    <div class="main-title-wrapper" class="anim" data-vp-add-class="animated fadeIn">
        <?php
        $fieldy = get_fields(get_the_ID());

        if ($fieldy['st']) {
            ?>
            <div class="main-title">
                <?= $fieldy['st'] ?>
            </div>

            <?php
        }
        ?>
    </div>
    <div class="container">
        <div class="row">
            <?php
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

            query_posts(array('posts_per_page' => 2, 'post_type' => 'aktualnosci', 'order' => 'ASC', 'paged' => $paged));

            if (have_posts()) :
                ?>
                <?php
                // Start the Loop.

                while (have_posts()) : the_post();
                    ?>
                    <?php
                    $row = false;
                    ?>
                    <?php if (has_post_thumbnail()) : ?>

                    <?php endif; ?>
                    <div class="col-md-6 anim" data-vp-add-class="animated fadeIn">
                        <div class="latest-news__single-col">
                            <span><?php the_time("j F Y"); ?></span>
                            <div class="wrapper-for-title">
                                <div class="latest-news__post-title">
                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                </div>
                            </div>

                            <div class="latest-news__content">
                                <?php the_excerpt(); ?>
                            </div>
                        </div>
                    </div>
                    <?php
                endwhile;

            endif;
            ?>

        </div>
    </div>
    <div class="read-all">
        <a href="/aktualnosci">CZYTAJ WSZYSTKIE</a>
    </div>
</div>

<div class="slider-home class1790 anim" data-vp-add-class="animated fadeIn">
    <div class="container">
        <?php include 'slider.php'; ?>
    </div>
</div>


</div>



<?php
//get_sidebar();
get_footer();

<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 * 
 */
/*
  Template Name: Kontakt
 */
get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>
<div class="main-wrapper">
    <div class="glass-bg">
        <div class="container">
            <div class="title-section">
                <div class="row">
                    <div class="col-md-6">
                        <div class="ramka-frame" style="background: url(<?php echo get_stylesheet_directory_uri(); ?>/img/title-bg.jpg) left 1px top no-repeat">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/ramka-white.jpg">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h1><?php the_title(); ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bigger-wrapper">
        <div class="content-single-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="single-box">
                            <span class="simple-title">KONTAKT</span>
                            <ul>
                                <?php $variable = get_field('numery_telefonow'); ?>
                                <?php
                                if ($variable) {
                                    foreach ($variable as $field) {
                                        ?>
                                        <li><a href="tel:<?= $field['numer-telefonu'] ?>"><i class="fas fa-phone"></i> <?= $field['numer-telefonu'] ?></a></li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                            <?= the_field('adres', 'option'); ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="single-box-data">
                            <span class="simple-title">GODZINY OTWARCIA</span>
                            <ul>
                                <?php $variable = get_field('godziny_otwarcia'); ?>
                                <?php
                                if ($variable) {
                                    foreach ($variable as $field) {
                                        ?>

                                        <li><?= $field['dzien'] ?><strong> <?= $field['godzina'] ?></strong></li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="map"></div>
    </br>
    </br>
    </br>
    </br>
    </br>

    
</div>

<script>

    var lat = <?= $fields ['google_maps']['lat'] ?>;
    var lng = <?= $fields ['google_maps']['lng'] ?>;
</script>

<?php
//get_sidebar();
get_footer();

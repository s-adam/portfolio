<div class="slider-swiper swiper-container">
    <!--Additional required wrapper--> 
    <div class="swiper-wrapper text-center">
        <!--Slides--> 
        <?php
        $variable = get_field('slider', 'option');
        if ($variable) {
            foreach ($variable as $field) {
                ?>
                <div class="swiper-slide">
                    <?php
                        if($field['adres_url'] != NULL){
                            ?>
                    <a href="<?= $field['adres_url']; ?>"><?= wp_get_attachment_image($field['logo']['ID'], 'slider'); ?></a>
                            <?php
                        }else{
                            ?>
                    <?= wp_get_attachment_image($field['logo']['ID'], 'slider'); ?>
                    <?php
                    
                        }
                    ?>
                    
                </div>
                <?php
            }
        }
        ?>
    </div>
</div>
<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package idvetmp
 */

get_header(); ?>

<div class="main-wrapper aktualnosc">
    <div class="glass-bg">
        <div class="container">
            <div class="title-section">
                <div class="row">
                    <div class="col-md-6">
                        <div class="ramka-frame" style="background: url(<?php echo get_stylesheet_directory_uri(); ?>/img/title-bg.jpg) left 1px top no-repeat">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/ramka-white.jpg">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h1><?php the_title(); ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bigger-wrapper">
        <div class="content-single-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 col-md-offset-3 anim" data-vp-add-class="animated fadeIn">
                        <div class="wrapper-for-half-circle">
                            <div class="row">
                                <div class="col-md-12">
                                    <h1>Nie znaleziono strony.</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>
<?php
get_footer();

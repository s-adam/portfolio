var swiper = new Swiper('.slider-swiper', {
    dots: false,
    slidesPerView: 6,
//    centeredSlides: true,
//    navigation: {
//        nextEl: '.swiper-button-next',
//        prevEl: '.swiper-button-prev',
//    },
    breakpoints: {
        // when window width is <= 320px
        480: {
            slidesPerView: 1,
//            spaceBetween: 10
        },
        // when window width is <= 480px
        640: {
            slidesPerView: 2,
//            spaceBetween: 20
        },
        // when window width is <= 640px
        991: {
            slidesPerView: 3,
//            spaceBetween: 30
        },
        1200: {
            slidesPerView: 4,
//            spaceBetween: 30
        },
        1500: {
            slidesPerView: 5,
//            spaceBetween: 30
        }
    }
});



var swiper2 = new Swiper('.certificates-swiper', {
    slidesPerView: 4,
    navigation: {
        nextEl: '.zn',
        prevEl: '.zp'
    },
    breakpoints: {
        // when window width is <= 320px
        480: {
            slidesPerView: 1,
//            spaceBetween: 10
        },
        // when window width is <= 480px
        640: {
            slidesPerView: 2,
//            spaceBetween: 20
        },
        // when window width is <= 640px
        991: {
            slidesPerView: 1,
//            spaceBetween: 30
        },
        1200: {
            slidesPerView: 3,
//            spaceBetween: 30
        },
        1500: {
            slidesPerView: 4,
//            spaceBetween: 30
        }
    }
});


$(document).ready(function () {


//    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
//        $('.anim').addClass('animated');
//    } else {
//        $('.anim').viewportChecker();
//    }


    //if (screen.width > 1) {

    $('.anim').viewportChecker();
    $('.anim').viewportChecker({
        classToAdd: 'animated'
    });
    $('.margin-wrapp').viewportChecker({
        classToAdd: 'nowa'
    });
    //}else{




    //}

});



function initMap() {
    // Create a map object and specify the DOM element for display.
    var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: lat, lng: lng},
        scrollwheel: false,
        zoom: 14,
        styles:
                [{"featureType": "administrative", "elementType": "labels.text.fill", "stylers": [{"color": "#444444"}]}, {"featureType": "landscape", "elementType": "all", "stylers": [{"color": "#f2f2f2"}]}, {"featureType": "landscape", "elementType": "geometry.fill", "stylers": [{"color": "#c2d3cd"}]}, {"featureType": "poi", "elementType": "all", "stylers": [{"visibility": "off"}]}, {"featureType": "road", "elementType": "all", "stylers": [{"saturation": -100}, {"lightness": 45}]}, {"featureType": "road.highway", "elementType": "all", "stylers": [{"visibility": "simplified"}]}, {"featureType": "road.arterial", "elementType": "labels.icon", "stylers": [{"visibility": "off"}]}, {"featureType": "transit", "elementType": "all", "stylers": [{"visibility": "off"}]}, {"featureType": "water", "elementType": "all", "stylers": [{"color": "#46bcec"}, {"visibility": "on"}]}, {"featureType": "water", "elementType": "geometry.fill", "stylers": [{"color": "#545c6a"}]}, {"featureType": "water", "elementType": "labels.text.fill", "stylers": [{"color": "#ffffff"}]}]
    });

    var marker
//    center: {lat: lat, lng: lng},
    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat, lng),
        map: map,
        icon: '/wp-content/themes/idvetmp-child/img/marker.png'
    });
}


setTimeout(function () {
    $('.promotion').addClass('anim');
}, 1500
        );


var hamburger = document.querySelector(".hamburger");
// On click
hamburger.addEventListener("click", function () {
    hamburger.classList.toggle("is-active");
});

$(document).on('click', '[data-toggle="lightbox"]', function (event) {
    event.preventDefault();
    $(this).ekkoLightbox({
        alwaysShowClose: true
    });
});

var anim = lottie.loadAnimation({
    container: document.getElementById('bm'),
    renderer: 'svg',
    loop: false,
    autoplay: true,
    path: '/wp-content/themes/idvetmp-child/js/data.json'
});
anim.addEventListener("enterFrame", function (animation) {     if (animation.currentTime > (anim.totalFrames - 1)) {        anim.pause();     }});
<?php

function idvetmpchild_scripts() {
    //wp_enqueue_style( 'font-opensans', 'https://fonts.googleapis.com/css?family=Montserrat:400,600,700&amp;subset=latin-ext" rel="stylesheet', array( 'idvetmp-style' ));
    wp_enqueue_style( 'swiper', get_template_directory_uri() . '/library/swiper/swiper.min.css', array( 'idvetmp-style' ));
    wp_enqueue_style( 'idvetmp-style', get_stylesheet_directory_uri() .'/main.css');
}
add_action( 'wp_enqueue_scripts', 'idvetmpchild_scripts' );

function idvetmp_scriptschild() {
    wp_enqueue_script( 'swiper-js', get_template_directory_uri() . '/library/swiper/swiper.min.js', array( 'jquery' ), '20151215', true );
    wp_enqueue_script( 'main-js', get_stylesheet_directory_uri() . '/js/main.js', array( 'jquery','bootstrap-js' ), '20151215', true );
    wp_enqueue_script( 'viewportchecker', get_stylesheet_directory_uri() . '/js/jquery.viewportchecker.js', array( 'jquery','bootstrap-js' ), '20151215', true );
//    wp_enqueue_script( 'lottie', get_stylesheet_directory_uri() . '/js/lottie.js', array( 'jquery','bootstrap-js' ), '20151215', true );
}
add_action( 'wp_enqueue_scripts', 'idvetmp_scriptschild' );

add_image_size('certyfikat-img', 178, 239, false ); //wpisu na blogu
add_image_size('aktualnosci-img', 200, 133, false ); //wpisu na blogu
add_image_size('oferta-photo', 262, 198, false ); //wpisu na blogu
add_image_size('aktualnosci-img-single', 500, 334, false ); //wpisu na blogu
add_image_size('wizytowka-img', 466, 329, false ); //wpisu na blogu
add_image_size('title-img', 493, 264, true ); //wpisu na blogu
add_image_size('oferta-zdj-img', 425, 291, false ); //wpisu na blogu
add_image_size('oferta-zdj-2', 309, 292, false ); //wpisu na blogu
add_image_size('slider', 188, 55, false ); //wpisu na blogu
add_image_size('duze-zdj', 446, 406, false );
add_image_size('lightbox-img', 1200, 800, false); 

function my_acf_init() {
	
	acf_update_setting('google_api_key', 'AIzaSyDT5ONMSnq3pV_L5VlenZYFv4I-UBlVHDo');
}

add_action('acf/init', 'my_acf_init');



function numeric_posts_nav() {

    if( is_singular() )
        return;

    global $wp_query;

    /** Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;

    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );

    /**    Add current page to the array */
    if ( $paged >= 1 )
        $links[] = $paged;

    /**    Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo '<div class="navigation"><ul>' . "\n";

    /**    Previous Post Link */
    if ( get_previous_posts_link() )
        printf( '<li>%s</li>' . "\n", get_previous_posts_link('&laquo;') );

    /**    Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="active"' : '';

        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

        if ( ! in_array( 2, $links ) )
            echo '<li>…</li>';
    }

    /**    Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }

    /**    Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li>…</li>' . "\n";

        $class = $paged == $max ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }

    /**    Next Post Link */
    if ( get_next_posts_link() )
        printf( '<li>%s</li>' . "\n", get_next_posts_link('»') );

    echo '</ul></div>' . "\n";

}
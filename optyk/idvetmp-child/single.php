<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 * 
 */

get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>
<div class="main-wrapper aktualnosc">
    <div class="glass-bg anim" data-vp-add-class="animated fadeIn">
        <div class="container">
            <div class="title-section">
                <div class="row">
                    <div class="col-md-6">
                        <div class="ramka-frame" style="background: url(<?= $fields['zdjecie_obok_tytulu_strony']['sizes']['title-img']; ?>) left 1px top no-repeat">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/ramka.png">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h1><?= the_title(); ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bigger-wrapper">
        <div class="content-single-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 col-md-offset-3 anim" data-vp-add-class="animated fadeIn">
                        <div class="wrapper-for-half-circle">
                            <div class="row">
                                <span class="text-center single-data"><?php the_time("j F Y"); ?></span>
                                <div class="wrapper-for-title text-center">
                                    <div class="latest-news__post-title">
                                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                    </div>
                                </div>
                                <div class="photo-section text-center">
                                    <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('aktualnosci-img-single'); ?></a>
                                    <div class="wrapper" data-anim="base wrapper">
                                        <div class="circle" data-anim="base right"></div>
                                    </div>
                                </div>
                                <div class="latest-news__single-col">

                                    <?php the_content(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>



<?php
get_footer();

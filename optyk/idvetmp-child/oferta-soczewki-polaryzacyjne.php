<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 * 
 */
/*
 * Template Name: Soczewki Polaryzacyjne
 * Template Post Type: oferta
 */
get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>
<div class="main-wrapper oferta-single okulary-przeciwsloneczne okulary-przeciwsloneczne-end soczewki-fotochromowe polaryzacyjne">
    <div class="glass-bg anim" data-vp-add-class="animated fadeIn">
        <div class="container">
            <div class="title-section">
                <div class="row">
                    <div class="col-md-6">
                        <div class="ramka-frame" style="background: url(<?= $fields['zdjecie_obok_tytulu_strony']['sizes']['title-img']; ?>) left 1px top no-repeat">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/ramka.png">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <?php
                        $from = get_the_title(63);
                        $to = sprintf("%s-%s", substr($from, 0, 21), substr($from, -4));
                        ;
                        ?>
                        <h1 class="okulary-przeciwsloneczne-h1"><?= $to ?></h1>
                        <h1 class="okulary-przeciwsloneczne-h1-show"><?= get_the_title(); ?></h1>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bigger-wrapper">
        <div class="content-single-area">
            <div class="container">
                <div class="wrapper-for-menu">
                    <ul>
                        <?php
                        $parent_page_id = ( '0' != $post->post_parent ? $post->post_parent : $post->ID );
                        $mypages = get_pages(array('child_of' => $parent_page_id, 'post_type' => 'oferta'));
//                        echo $term;
                        foreach ($mypages as $page) {
                            $content = $page->post_content;
                            $term = get_the_title();
                            ?>
                            <li class="<?php
                            if ($term == $page->post_title) {
                                echo "active";
                            }
                            ?>"><a class="<?php
                                    if ($term == $page->post_title) {
                                        echo "active";
                                    }
                                    ?>" href="<?php echo get_page_link($page->ID); ?>"><?php echo $page->post_title; ?></a></li>
                            <?php } ?>
                    </ul>
                </div>
                <div class="about-ophthalmologist-adventages anim" data-vp-add-class="animated fadeIn">
                    <div class="row">
                        <?php
                        $fields = get_fields(get_the_ID());
                        ?>
                        <div class="offer-section-title">
                            <h2><?= the_title(); ?></h2>
                        </div>
                        <div class="col-md-7">
                            <div class="left">
                                <?= $fields['sekcja_pierwsza_-_tekst_pierwszy'] ?>
                            </div>
                            <div class="red-label2">
                                <?= $fields['sekcja_pierwsza_-_tekst_drugi'] ?>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="right right3">
                                <div class="wrapper wrapper3" data-anim="base wrapper">
                                    <div class="circle" data-anim="base right"></div>
                                </div>
                                <?= wp_get_attachment_image($fields['sekcja_pierwsza_-_zdjecie']['ID'], 'title-img'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div></div>
    <div class="section-second">
        <div class="container">
            <div class="inside-wrapper anim" data-vp-add-class="animated fadeIn">
                <div class="row">
                    <div class="offer-section-title no-color">
                        <h2><?= $fields['sekcja_druga_-_tytul'] ?></h2>
                    </div>
                    <div class="col-md-6">
                        <div class="left">
                            <div class="red-label">
                                <?= $fields['sekcja_druga_-_tekst_po_lewej_stronie'] ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="left">
                            <div class="red-label">
                                <?= $fields['sekcja_druga_-_tekst_po_prawej_stronie'] ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bigger-wrapper">
        <div class="content-single-area">
            <div class="container">
                <div class="section-third anim" data-vp-add-class="animated fadeIn">
                    <div class="inside-wrapper">
                        <div class="row">
                            <div class="offer-section-title">
                                <h2><?= $fields['sekcja_trzecia_-_tytul'] ?></h2>
                            </div>
                           
                            <div class="col-md-6">
                                <div class="left">
                                    <div class="red-label">
                                        <?= $fields['sekcja_trzecia_-_tekst']; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="left">
                                    <div class="red-label">
                                        <?= $fields['sekcja_trzecia_-_tekst_po_prawej'] ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="certificates anim" data-vp-add-class="animated fadeIn">
                <div class="container">
                    <?php
                    $fields = get_fields(get_the_ID());
                    if ($fields['slider_-_tytul']) {
                        ?>
                        <div class="main-title-wrapper">
                            <div class="main-title">
                                <!--CERTYFIKATY-->
                                <?php
                                echo $fields['slider_-_tytul'];
                                ?>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="certificates-swiper swiper-container">
                        <!--Additional required wrapper--> 
                        <div class="swiper-wrapper text-center">
                            <!--Slides--> 
                            <?php
                            $variable = $fields['slider_-_galeria'];
                            if ($variable) {
                                foreach ($variable as $field) {
                                    ?>
                                    <div class="swiper-slide">
                                        <a href='<?= $field['zdjecie']['sizes']['lightbox-img']; ?>' data-toggle="lightbox" data-gallery="example-gallery"><?= wp_get_attachment_image($field['zdjecie']['ID'], 'certyfikat-img'); ?></a>
                                    </div>
                                    <?php
                                }
                            }
                            ?>

                        </div>
                        <?php
                        if ($variable) {
                            ?>
                            <div class="swiper-button-next zn"></div>
                            <div class="swiper-button-prev zp"></div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<?php
get_footer();

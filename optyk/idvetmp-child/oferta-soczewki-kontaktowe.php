<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 * 
 */
/*
 * Template Name: Soczewki Kontaktowe
 * Template Post Type: oferta
 */
get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>
<div class="main-wrapper oferta-single lens-contact soczewki-kontaktowe-end">
    <div class="glass-bg anim" data-vp-add-class="animated fadeIn">
        <div class="container">
            <div class="title-section">
                <div class="row">
                    <div class="col-md-6">
                        <div class="ramka-frame" style="background: url(<?= $fields['zdjecie_obok_tytulu_strony']['sizes']['title-img']; ?>) left 1px top no-repeat">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/ramka.png">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h1><?php the_title(); ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bigger-wrapper">
        <div class="content-single-area">
            <div class="container">
                <div class="about-ophthalmologist-adventages anim" data-vp-add-class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="left">
                                <?= $fields['sekcja_pierwsza_-_tekst_pierwszy'] ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="right right3 rr">
                                <div class="wrapper wrapper3" data-anim="base wrapper">
                                    <div class="circle" data-anim="base right"></div>
                                </div>
                                <?= wp_get_attachment_image($fields['sekcja_pierwsza_-_zdjecie']['ID'], 'wizytowka-img'); ?>
                            </div>
                        </div>
                    </div>
                    <?php if ($fields['sekcja_pierwsza_-_tekst_na_cala_szerokosc_strony']) {
                        ?>
                        <div class="soczewki-kontaktowe-text-block">
                            <div class="row">
                                <div class="col-md-12">

                                    <?= $fields['sekcja_pierwsza_-_tekst_na_cala_szerokosc_strony']; ?>

                                </div>
                            </div>
                        </div>
                        <?php }
                    ?>
                </div>
                <div class="about-lens anim" data-vp-add-class="animated fadeIn">
                    <div class="row">
                        <?php
                        if (have_rows('rodzaje_soczewek')):

                            // loop through the rows of data
                            while (have_rows('rodzaje_soczewek')) : the_row();

                                // check current row layout
                                if (get_row_layout() == 'lewa-kolumna'):
                                    echo '<div class="col-md-4">';
                                    // check if the nested repeater field has rows of data
                                    if (have_rows('rodzaje-soczewek')):



                                        // loop through the rows of data
                                        while (have_rows('rodzaje-soczewek')) : the_row();

                                            $kolumna = get_sub_field('informacje');
                                            ?>

                                            <div class="red-label">
                                                <div class="single-info-lens">
                                                    <?= $kolumna ?>
                                                </div>
                                            </div>

                                            <?php
                                        endwhile;



                                    endif;
                                    echo '</div>';
                                endif;

                                // check current row layout
                                if (get_row_layout() == 'srodkowa-kolumna'):
                                    echo '<div class="col-md-4">';
                                    // check if the nested repeater field has rows of data
                                    if (have_rows('rodzaje-soczewek')):



                                        // loop through the rows of data
                                        while (have_rows('rodzaje-soczewek')) : the_row();

                                            $kolumna = get_sub_field('informacje');
                                            ?>

                                            <div class="red-label">
                                                <div class="single-info-lens">
                                                    <?= $kolumna ?>
                                                </div>
                                            </div>

                                            <?php
                                        endwhile;



                                    endif;
                                    echo '</div>';
                                endif;

                                // check current row layout
                                if (get_row_layout() == 'prawa-kolumna'):
                                    echo '<div class="col-md-4">';
                                    // check if the nested repeater field has rows of data
                                    if (have_rows('rodzaje-soczewek')):



                                        // loop through the rows of data
                                        while (have_rows('rodzaje-soczewek')) : the_row();

                                            $kolumna = get_sub_field('informacje');
                                            ?>

                                            <div class="red-label">
                                                <div class="single-info-lens">
                                                    <?= $kolumna ?>
                                                </div>
                                            </div>

                                            <?php
                                        endwhile;



                                    endif;
                                    echo '</div>';
                                endif;

                            endwhile;

                        else :

                        // no layouts found

                        endif;
                        ?>
                    </div>

                </div>


                <div class="section-cooperation contact-lens anim" data-vp-add-class="animated fadeIn">
                    <div class="offer-section-title">
                        <h2><?= $fields['wspolpraca-tytul'] ?></h2>
                    </div>
                    <?php
                    if ($fields['wspolpraca_-_logo_partnera']) {
                        foreach ($fields['wspolpraca_-_logo_partnera'] as $field) {

                            if ($field['adres_url']) {
                                ?>
                                <a target="_blank" href="<?= $field['adres_url'] ?>"><?= wp_get_attachment_image($field['logo']['ID'], 'wizytowka-img'); ?></a>



                                <?php
                            } else {
                                ?>
                                <?= wp_get_attachment_image($field['logo']['ID'], 'wizytowka-img'); ?>
                                <?php
                            }
                        }
                    }
                    ?>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="wrappe2 anim" data-vp-add-class="animated fadeIn">
    <?php include 'cta.php'; ?>
</div>

</div>



<?php
get_footer();



<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package idvetmp
 *
 */
/*
Template Name: Realizacje
*/
get_header();
the_post();
$fields = get_fields(get_the_ID());
?>

    <div class="main-container">


        <div class="title-wrapper">
            <h1 class="anim" data-vp-add-class="animated fadeIn"><?php the_title(); ?></h1>
        </div>

        <?php
        get_template_part('menu');
        ?>

        <div class="container">
            <div class="row">
                <div class="realizacje main-wrapper new-page col-md-12">
<?php

if($fields['pojedyncza_sekcja']){
    foreach ($fields['pojedyncza_sekcja'] as $tym) {
        ?>

        <div class="usluga-title">
            <h3><?= $tym['tytul_sekcji']?></h3>
        </div>


        <?php
        if ($tym) {
            $i = 0;
            $j = 0;
            foreach ($tym['dodaj_zdjecia'] as $field) {
                if ($i == 0) {

                    echo '<div class="prace-kursantek row text-center anim" data-vp-add-class="animated fadeIn">';
                    echo '<span><hr></span>';
                    echo '';
                }
                ?>
                <div class="col-md-3 col-xs-12 col-sm-6">
                    <a href="<?= $field['sizes']['large'] ?>"><img src="<?= $field['sizes']['gallery-small-img'] ?>"
                                                                   alt=""></a>
                </div>
                <?php
                $i++;
                $j++;
                if ($i == 4 || count($tym['dodaj_zdjecia']) == $j) {


                    echo '</div>';

                    $i = 0;
                }
            }
        }

        ?>
        <?php
    }}
        ?>

                </div>
            </div>
        </div>
    </div>
<?php
get_footer();

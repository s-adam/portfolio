<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package idvetmp
 *
 */
/*
Template Name: Makijaż
*/
get_header();
the_post();
$fields = get_fields(get_the_ID());

?>


    <div class="main-container">


        <div class="title-wrapper">
            <h1 class="anim" data-vp-add-class="animated fadeIn"><?= get_the_title( $post->post_parent ); ?></h1>
        </div>

        <?php
        get_template_part('menu');
        ?>

        <div class="container">
            <div class="row">
                <div class="main-wrapper makijaz col-md-12" >
                    <div class="row">
                        <div class="col-md-6">
                            <div class="about-author-left">
                                <span><hr class="zero"></span>
                                <span><hr class="aww"></span>
<!--                                                            <img class="author-photo" src="--><?php //echo get_template_directory_uri(); ?><!--/img/oczy.jpg" alt="">-->
                                <div class="author-photo">
                                    <?= wp_get_attachment_image($fields['zdjecie'], 'makijaz-img'); ?>
                                </div>
                            </div>
                            <div class="program program-block anim" data-vp-add-class="animated fadeIn">
                                <div class="lista-szkolenia">
                                    <?= $fields['sekcja_pod_zdjeciem']?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="about-author anim" data-vp-add-class="animated fadeIn">

                                <h2><?= get_the_title(); ?></h2>
                                <?= $fields['tekst_obok_zdjecia']?>
                            </div>

                        </div>
                    </div>
                    <?php
                    if($fields['sekcja_-_ramka'] != null) {
                    ?>

                    <div class="mozliwosci-box after-signs anim" data-vp-add-class="animated fadeInUp">

                        <?= $fields['sekcja_-_ramka']?>
                    </div>

                        <?php
                    }
                        ?>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();

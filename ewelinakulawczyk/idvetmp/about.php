<?php
/*
Template Name: O nas
*/
get_header();
the_post();

$fields = get_fields(get_the_ID());

?>

<div class="main-container">
<div class="liscie-prawo-gora">
    <img class="paralax zdj1" src="<?php echo get_template_directory_uri(); ?>/img/prawe_liscie_gora.png" alt="">
</div>

<div class="liscie-prawo-dol">
    <img class="paralax zdj2" src="<?php echo get_template_directory_uri(); ?>/img/prawe_liscie_dol.png" alt="">
</div>

<div class="liscie-lewe">
    <img class="paralax zdj3" src="<?php echo get_template_directory_uri(); ?>/img/lewe_liscie.png" alt="">
</div>


    <div class="container">
        <div class="row">
            <div class="main-wrapper darken-color col-md-12 ">
                <div class="about-us">

                        <div class="berk-title big-title">
                            <?php the_title(); ?>
                        </div>
                            <div class="little-text">
                                <?= $fields['tekst_pod_tytulem_strony'] ?>

                            </div>
                            <?php

                            if($fields['pojedyncze_kafelki']){
                            $i = 0;
                            $j = 0;
                            foreach ($fields['pojedyncze_kafelki'] as $field) {
                            if ($i == 0) {
                                echo '<div class="row">';
                            }
                            ?>
                            <div class="col-md-4">
                                <div class="one-col">
<!--                                    --><?//= $field ['url'] ?><!--<img src="--><?php //echo get_template_directory_uri(); ?><!--/img/medal.jpg" alt="">-->
<!--                                    --><?php //echo wp_get_attachment_image(get_post_thumbnail_id(), 'max-img-blog'); ?>
                                    <?=wp_get_attachment_image( $field['zdjecie'],'large');?>
                                    <div class="text">
                                        <?= $field ['tekst'] ?>
                                    </div>
                                </div>
                            </div>
                                <?php
                                $i++;
                                $j++;
                                if ($i == 3 || count($fields['pojedyncze_kafelki']) == $j) {


                                    echo '</div>';

                                    $i = 0;
                                }
                            }
                            }

                            ?>


                </div>
            </div>
        </div>
    </div>

</div>

<?php get_footer(); ?>

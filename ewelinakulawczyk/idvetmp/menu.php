<div id="myNav" class="overlay">
	<!-- Overlay content -->
	<div class="overlay-content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-10">

				</div>
				<div class="col-md-2 sliderr">
					<?php
					wp_nav_menu(array(
							'menu' => 'primary',
							'theme_location' => 'primary',
							'depth' => 3,
							'container' => false,
							'container_class' => 'collapse navbar-collapse',
							'menu_class' => 'nav navbar-nav testy',
							'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
							'walker' => new wp_bootstrap_navwalker())
					);
					?>
					<ul class="menu-social">
						<li><a class="social-ref" href="<?= the_field('facebook_link', 'option'); ?>" target="_blank"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
					</ul>
				</div>
			</div>
		</div>

	</div>

</div>
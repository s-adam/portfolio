<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package idvetmp
 *
 */
/*
Template Name: Cennik
*/
get_header();
the_post();
$fields = get_fields(get_the_ID());

?>

    <div class="main-container">


        <div class="title-wrapper">
            <h1 class="anim" data-vp-add-class="animated fadeIn"><?php the_title(); ?></h1>
        </div>

        <?php
        get_template_part('menu');
        ?>

        <div class="container">
            <div class="row">
                <div class="main-wrapper new-page col-md-12">
                    <?php

                    if($fields['pojedyncza_sekcja_cennika']){
                    $i = 0;
                    $j = 0;
                    foreach ($fields['pojedyncza_sekcja_cennika'] as $tym) {
                    if ($i == 0) {
                        echo '<div class="cennik row">';
                    }
                    ?>
<!--                        <div class="cennik row">-->
                        <div class="col-md-9 hidden-now hidden-xs col-sm-7 anim" data-vp-add-class="animated fadeIn">
                            <div class="usluga">
                                <div class="usluga-title">
                                    <h3><?= $tym['tytul'] ?></h3>
                                </div>
                                <?php
                                if ($tym['usluga']) {

                                foreach ($tym['usluga'] as $rym) {
                                    ?>
                                    <p class="title-small">
                                        <?= $rym['nazwa_uslugi'] ?>
                                    </p>
                                    <?php
                                    }
                                }
                                ?>

                            </div>
                        </div>
                        <div class="col-md-3 hidden-now hidden-xs col-sm-5 anim" data-vp-add-class="animated fadeIn">

                                    <span>

                                    </span>
                            <?php

                            if ($tym['usluga']) {

                                foreach ($tym['usluga'] as $rym) {

                                ?>

                                <div class="cena">
                                    <p><?= $rym['cena_uslugi'] ?></p>
                                    <div class="short-line"></div>
                                </div>
                                <?php
                            }}
                            ?>

                        </div>
                            <?php
                            $i++;
                            $j++;
                            if ($i == 3 || count($fields['pojedyncza_sekcja_cennika']) == $j) {

                                echo '</div>';

                                $i = 0;
                            }
                            }
                            }
                            ?>

                    <?php

                    if($fields['pojedyncza_sekcja_cennika']){
                    $i = 0;
                    $j = 0;
                    foreach ($fields['pojedyncza_sekcja_cennika'] as $tym) {

                    ?>

                        <div class="col-md-12 hidden-md hidden-lg">
                            <div class="usluga-title">
                                <p><?= $tym['tytul'] ?></p>
                            </div>
                            <?php
                            if ($tym['usluga']) {

                                foreach ($tym['usluga'] as $rym) {
                                    ?>
                                <div class="row">
                                    <div class="col-md-6 col-sm-7 anim" data-vp-add-class="animated fadeIn">
                                        <p class="title-small">
                                            <?= $rym['nazwa_uslugi'] ?>
                                        </p>
                                    </div>
                                    <div class="col-md-6 col-sm-5 anim" data-vp-add-class="animated fadeIn">
                                        <div class="cena">
                                            <p><?= $rym['cena_uslugi'] ?></p>
                                            <div class="short-line"></div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }}
                            ?>

                        </div>
<!--                    </div>-->
                <?php
                $i++;
                $j++;
                if ($i == 3 || count($fields['pojedyncza_sekcja_cennika']) == $j) {

                    echo '</div>';

                    $i = 0;
                }
                }
                }
                ?>


                    <div class="rabat anim" data-vp-add-class="animated fadeIn">
                        <?= $fields['informacja_o_rabacie'] ?>
                    </div>

                    <?php

                    if($fields['pojedyncza_sekcja_cennika_kopia']){
                        $i = 0;
                        $j = 0;
                        foreach ($fields['pojedyncza_sekcja_cennika_kopia'] as $tym) {
                            if ($i == 0) {
                                echo '<div class="cennik cennik2 row">';
                            }
                            ?>
                            <!--                        <div class="cennik row">-->
                            <div class="col-md-8 hidden-now hidden-xs col-sm-7 anim" data-vp-add-class="animated fadeIn">
                                <div class="usluga">
                                    <div class="usluga-title">
                                        <h3><?= $tym['tytul'] ?></h3>
                                    </div>
                                    <?php
                                    if ($tym['usluga']) {

                                        foreach ($tym['usluga'] as $rym) {
                                            ?>
                                            <p class="title-small">
                                                <?= $rym['nazwa_uslugi'] ?>
                                            </p>
                                            <?php
                                        }
                                    }
                                    ?>

                                </div>
                            </div>
                            <div class="col-md-4 hidden-now hidden-xs col-sm-5 anim" data-vp-add-class="animated fadeIn">

                                    <span>

                                    </span>
                                <?php

                                if ($tym['usluga']) {

                                    foreach ($tym['usluga'] as $rym) {

                                        ?>

                                        <div class="cena">
                                            <p><?= $rym['cena_uslugi'] ?></p>
                                            <div class="short-line"></div>
                                        </div>
                                        <?php
                                    }}
                                ?>

                            </div>
                            <?php
                            $i++;
                            $j++;
                            if ($i == 3 || count($fields['pojedyncza_sekcja_cennika_kopia']) == $j) {

                                echo '</div>';

                                $i = 0;
                            }
                        }
                    }
                    ?>

                    <?php

                    if($fields['pojedyncza_sekcja_cennika_kopia']){
                        $i = 0;
                        $j = 0;
                        foreach ($fields['pojedyncza_sekcja_cennika_kopia'] as $tym) {

                            ?>

                            <div class="col-md-12 cennik2 hidden-md hidden-lg">
                                <div class="usluga-title">
                                    <p><?= $tym['tytul'] ?></p>
                                </div>
                                <?php
                                if ($tym['usluga']) {

                                    foreach ($tym['usluga'] as $rym) {
                                        ?>
                                        <div class="row">
                                            <div class="col-md-6 col-sm-7 anim" data-vp-add-class="animated fadeIn">
                                                <p class="title-small">
                                                    <?= $rym['nazwa_uslugi'] ?>
                                                </p>
                                            </div>
                                            <div class="col-md-6 col-sm-5 anim" data-vp-add-class="animated fadeIn">
                                                <div class="cena">
                                                    <p><?= $rym['cena_uslugi'] ?></p>
                                                    <div class="short-line"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }}
                                ?>

                            </div>
                            <!--                    </div>-->
                            <?php
                            $i++;
                            $j++;
                            if ($i == 3 || count($fields['pojedyncza_sekcja_cennika_kopia']) == $j) {

                                echo '</div>';

                                $i = 0;
                            }
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();


?>

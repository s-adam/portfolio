$( ".hamburger" ).click(function() {
    $('#myNav').toggleClass('open-menu');
    $('.hamburger').toggleClass('openedhamb');
    $('.hamburger').toggleClass('clicked-white');
});
// $('.menu-item-has-children').click(function(){return false;});
$( ".sliderr" ).fadeIn( 5000, function() {

});
// $('.prace-kursantek').magnificPopup({
//     delegate: 'a',
//     type: 'image',
//     gallery: {
//         enabled: true
//     }
// });

$('.prace-kursantek').each(function() {
    $(this).magnificPopup({
        delegate: 'a',
        type:'image',
        gallery: {
            enabled: true
        }
    });
});
$('.owl-carousel').magnificPopup({
    delegate: 'a',
    type: 'image',
    gallery: {
        enabled: true
    }
});
//
// $('.prace-kursantek').each(function() { // the containers for all your galleries
//     $(this).magnificPopup({
//         delegate: 'a', // the selector for gallery item
//         type: 'image',
//         gallery: {
//             enabled:true
//         }
//     });
// });

$(document).ready(function () {


    if (screen.width > 1) {

        $('.anim').viewportChecker();
        $('.zero').viewportChecker({
            classToAdd: 'nowa'
        });
        $('.margin-wrapp').viewportChecker({
            classToAdd: 'nowa'
        });
    }else{




    }

});

//
// var _start = {property: 0};
// var _end = {property: 100};
//
// jQuery(_start).animate(_end, {
//     duration: 99900000,
//     step: function() {
//         // $('.about-author-left .zero').css('width', this.property + "%");
//         $('.about-author-left .zero').addClass('nowa');
//         $('.margin-wrapp').addClass('nowa');
//         //console.log( 'Current percentage is ' + this.property );// You can write this to your bar
//
//     }
// });

function initMap() {


    var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: lat, lng: lng},
    scrollwheel: false,
    zoom: 15,


        styles: [
            {
                "featureType": "all",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "saturation": 36
                    },
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 40
                    }
                ]
            },
            {
                "featureType": "all",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 16
                    }
                ]
            },
            {
                "featureType": "all",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry",
                "stylers": [
                    {
                        "visibility": "off"
                    },
                    {
                        "color": "#000000"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "lightness": 20
                    },
                    {
                        "color": "#000000"
                    },
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 17
                    },
                    {
                        "weight": 1.2
                    },
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#dbdbdb"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#212325"
                    },
                    {
                        "lightness": 20
                    }
                ]
            },
            {
                "featureType": "landscape.man_made",
                "elementType": "geometry",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#6b2f2f"
                    },
                    {
                        "lightness": 21
                    },
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#858585"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 17
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 29
                    },
                    {
                        "weight": 0.2
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 18
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 16
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 19
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 17
                    }
                ]
            }
        ]


    });
    var marker = new google.maps.Marker({
            position: {lat: lat, lng: lng},
    map: map,
        icon: '/wp-content/themes/idvetmp/img/marker.png'

});
}



$(document).ready(function(){




    $(".owl-carousel").owlCarousel({
        nav: true,
        rewindNav : true,
//            center: true,
        navText: ["<img src='/wp-content/themes/idvetmp/img/arrow-left.jpg'>","<img src='/wp-content/themes/idvetmp/img/arrow-right.jpg'>"],
        responsive:{
            320:{
                items:1,
                center: false
            },

            768:{
                items:2,
                center: false
            },

            960:{
                items:3,
                center: false
//                    center: true
            },
            1200:{
                items:4,
                center: false
//                    center: true
            }
        }
    });
});

jQuery(function($) {
    $('.menu-item-has-children').addClass('kkk');

});


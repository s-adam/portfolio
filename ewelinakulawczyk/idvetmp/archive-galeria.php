<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */

get_header(); ?>

	<div class="main-container">


		<div class="title-wrapper">
			<h1><?php the_title(); ?></h1>
		</div>

        <?php
        get_template_part('menu');
        ?>

		<div class="container">
			<div class="row">
				<div class="main-wrapper new-page col-md-12">

<?php
$images = get_field('dodaj_zdjecia');
if($images){
	$i = 0;
	$j = 0;
	foreach ($images as $field) {
		if ($i == 0) {

			echo '<div class="prace-kursantek row text-center">';
			echo '<span><hr></span>';
			echo '';
		}
		?>

					<div class="col-md-3 col-sm-6">
						<a href="<?= $field['sizes']['large']  ?>"><img src="<?= $field['sizes']['gallery-small-img']  ?>" alt=""></a>
					</div>
		<?php
		$i++;
		$j++;
		if ($i == 4 || count($images) == $j) {


//			echo '</div>';
			echo '</div>';

			$i = 0;
		}
	}
}

?>

				</div>
			</div>
		</div>
	</div>
<?php
get_footer();

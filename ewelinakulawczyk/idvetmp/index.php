<?php


/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */

get_header(); 
the_post();
$fields = get_fields(get_the_ID());



?>

    <main id="main" class="site-main" role="main">

        <div class="container">
            <div class="row">
                <div class="ciekawe-linki" style="color: red;">
                    <div class="berk-title">
                        <p>Ciekawe linki</p>
                    </div>
                    <div class="col-md-4">
                        <ul>
                            <li><a href="#">www.link.pl</a></li>
                            <li><a href="#">www.link.pl</a></li>
                        </ul>
                    </div>

                    <div class="col-md-4">
                        <ul>
                            <li><a href="#">www.link.pl</a></li>
                            <li><a href="#">www.link.pl</a></li>
                        </ul>
                    </div>

                    <div class="col-md-4">
                        <ul>
                            <li><a href="#">www.link.pl</a></li>
                            <li><a href="#">www.link.pl</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </main>

<?php
get_footer();

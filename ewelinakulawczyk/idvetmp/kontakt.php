<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package idvetmp
 *
 */
/*
Template Name: Kontakt
*/
get_header();
the_post();
$fields = get_fields(get_the_ID());

?>


    <div class="main-container">


        <?php
        get_template_part('menu');
        ?>

        <div class="container">
            <div class="row">
                <div class="main-wrapper makijaz col-md-12">
                    <div class="row">
                        <div class="col-md-6 hidden-md hidden-lg anim" data-vp-add-class="animated fadeIn" style="margin-bottom: 30px;">
                            <div class="info-contact">
                                <?= $fields ['informacje_kontaktowe'] ?>


                            </div>
                            <div class="phone-number">
                                <a href="tel:<?= the_field('telefon', 'option'); ?>"><?= the_field('telefon', 'option'); ?></a>

                            </div>
                            <div class="phone-number">
                                <a href="mailto:<?php the_field('email', 'option'); ?>" target="_blank"><?php the_field('email', 'option'); ?></a>
                            </div>

                            <div class="fb-ref">
                                <a class="social-ref" href="<?= the_field('facebook_link', 'option'); ?>" target="_blank"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div id="map">

                            </div>
                        </div>
                        <div class="col-md-6 hidden-sm hidden-xs anim" data-vp-add-class="animated fadeIn" >
                            <div class="info-contact">
                                <?= $fields ['informacje_kontaktowe'] ?>


                            </div>
                            <div class="phone-number">
                                <a href="tel:<?= the_field('telefon', 'option'); ?>"><?= the_field('telefon', 'option'); ?></a>

                            </div>
                            <div class="phone-number">
                                <a href="mailto:<?php the_field('email', 'option'); ?>" target="_blank"><?php the_field('email', 'option'); ?></a>
                            </div>

                            <div class="fb-ref">
                                <a class="social-ref" href="<?= the_field('facebook_link', 'option'); ?>" target="_blank"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();
?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAMKKY7mXujoxMREJTx8G548UFES8R15V0&callback=initMap" async defer></script>

<script>

    var lat =  <?=$fields ['google_maps']['lat']?>;
    var lng =  <?=$fields ['google_maps']['lng']?>;



</script>


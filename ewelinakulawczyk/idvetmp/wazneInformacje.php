<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package idvetmp
 *
 */
/*
Template Name: Przeciwskazania
*/
get_header();
the_post();
$fields = get_fields(get_the_ID());

?>

    <div class="main-container">


        <div class="title-wrapper">
            <h1 class="anim" data-vp-add-class="animated fadeIn"><?php the_title(); ?></h1>
        </div>

        <?php
        get_template_part('menu');
        ?>

        <div class="container">
            <div class="row">
                <div class="main-wrapper new-page col-md-12">
                    <div class="zalecenia row section-usluga">
                        <div class="col-md-6 anim" data-vp-add-class="animated fadeInLeft">
                            <div class="usluga important-info lista-szkolenia-black usluga-title">

                                <?= $fields['lewa_kolumna']?>

                            </div>
                        </div>
                        <div class="col-md-6 anim" data-vp-add-class="animated fadeInRight">

                            <div class="usluga important-info lista-szkolenia-black usluga-title">

                                <?= $fields['prawa_kolumna']?>
                            </div>
                        </div>


                </div>
                <div class="full-title anim" data-vp-add-class="animated fadeIn">
                    <?php

                    if($fields['przeciwskazania']) {
                        foreach ($fields['przeciwskazania'] as $field) {


                            if ($field['acf_fc_layout'] == 'tytulprzeciwskazania') {


                                ?>
                                <h3><?= $field['tytul'] ?></h3>
                                <?php
                            }
                        }}
                        ?>
                        <div class="row">
                            <?php
                            if($fields['przeciwskazania']) {
                            foreach ($fields['przeciwskazania'] as $field) {
                            if ($field['acf_fc_layout'] == 'sekcjaprzeciwskazania') {


                            ?>
                            <div class="col-md-6">
                                <div class="small-dots">


                                            <h4><?= $field['tytul'] ?></h4>
                                            <?= $field['lista'] ?>


                                </div>
                            </div>
                                <?php
                            }}}
                            ?>
                        </div>

                </div>
            </div>
        </div>
    </div>
<?php
get_footer();


?>


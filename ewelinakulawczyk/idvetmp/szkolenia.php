<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package idvetmp
 *
 */
/*
Template Name: Szkolenia
*/
get_header();
the_post();
$fields = get_fields(get_the_ID());

?>

    <div class="main-container">


        <div class="title-wrapper">
            <h1 class="anim" data-vp-add-class="animated fadeIn"><?php the_title(); ?></h1>
        </div>

    <?php
    get_template_part('menu');
    ?>

        <div class="container">
            <div class="row">
                <div class="main-wrapper szkolenia col-md-12">
                    <div class="row">
                        <div class="main-wrapper szkolenia col-md-12">
                            <div class="row">
                                <div class="col-md-5 anim" data-vp-add-class="animated fadeIn">
                                    <div class="program mright width-max lista-szkolenia">
                                        <?= $fields['autorski_program_-_czarny_box'] ?>
                                    </div>
                                    <div class="szkolenie-godz mright text-center">
                                        <?= $fields['Prace_kursantek'] ?>
                                    </div>
                                </div>
                                <div class="col-md-7 mleft anim" data-vp-add-class="animated fadeIn">
                                    <div class="about-program">
                                        <?= $fields['sekcja_-_kursy_dla_poczatkujacych'] ?>
                                    </div>
                                    <div class="gwarancja-jakosci lista-szkolenia-black">
                                        <?= $fields['sekcja_gwarancja_jakosci'] ?>
                                    </div>

                                </div>
                            </div>
                            <div class="row szkolenia-wrapper">
                                <div class="col-md-5">


                                </div>
                                <div class="col-md-7 mleft anim" data-vp-add-class="animated fadeIn">

                                </div>
                            </div>
                            <div class="anim" data-vp-add-class="animated fadeInUp">
                                <span class="up-line"></span>
                                <div class="mozliwosci-box">

                                    <?= $fields['box_-_ramka'] ?>
                                </div>
                                <span class="down-line"></span>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
<?php
get_footer();

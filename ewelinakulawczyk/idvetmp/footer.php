<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package idvetmp
 */
?>
<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <p>&copy <?= date('Y') ?> Ewelina Kulawczyk</p>
            </div>
            <div class="col-md-6 text-right">
                <a href="https://ideative.pl/"><img class="logo-change-white" src="<?php echo get_template_directory_uri(); ?>/img/ideative_logo.jpg" alt=""></a>
                <a href="https://ideative.pl/"><img class="logo-change-black" src="<?php echo get_template_directory_uri(); ?>/img/logo_black.png" alt=""></a>
            </div>

        </div>
    </div>
</footer>
<?php wp_footer(); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.1/TweenMax.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.magnific-popup.min.js"></script>

<script src="http://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/animation.gsap.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.viewportchecker.js"></script>


</body>
</html>
<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package idvetmp
 *
 */
/*
Template Name: Zalecenia
*/
get_header();
the_post();
$fields = get_fields(get_the_ID());

?>

    <div class="main-container">


        <div class="title-wrapper">
            <h1 class="anim" data-vp-add-class="animated fadeIn">Zalecenia przed i po zabiegowe</h1>
        </div>

        <?php
        get_template_part('menu');
        ?>

        <div class="container">
            <div class="row">
                <div class="main-wrapper new-page col-md-12">
                    <div class="zalecenia section-usluga small-dots">
                        <?php

                        if($fields['pojedyncza_sekcja']){
                        foreach ($fields['pojedyncza_sekcja'] as $tym) {
                        ?>
                        <div class="row section-usluga">
                            <div class="col-md-6">
                                <div class="usluga">

                                        <div class="usluga-title anim" data-vp-add-class="animated fadeIn">
                                            <h3><?= $tym['glowny_tytul_sekcji'] ?></h3>
                                        </div>
                                        <div class="usluga-under">
                                            <div class="o anim" data-vp-add-class="animated fadeIn">
                                                <?= $tym['tekst_-_lewa_strona'] ?>
                                            </div>
                                            <?php
                                                if($tym['czarny_box_po_lewej_stronie'] != null){

                                            ?>
                                            <div class="black-box-przeciwskazania anim" data-vp-add-class="animated fadeIn">
                                                <?= $tym['czarny_box_po_lewej_stronie'] ?>
                                            </div>
                                                    <?php

                                                }
                                                    ?>
                                        </div>

                                </div>
                            </div>
                            <div class="col-md-6 anim" data-vp-add-class="animated fadeIn">

                             <div class="ml anim" data-vp-add-class="animated fadeIn">
							        <span>

                                    </span>
                                <?= $tym['tekst_-_prawa_strona'] ?>
                            </div>
							 </div>
                                 
                        </div>
                            <?php
                            }
                        }
                        ?>

                    </div>

                </div>
            </div>
        </div>
    </div>
<?php
get_footer();

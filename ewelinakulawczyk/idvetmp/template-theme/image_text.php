<section class="<?=$content['acf_fc_layout']?>" style="<?=$content['section_style']?>">
    <div class="<?=!$content['full_width']?'container':'container-fluid'?>">
        <div class="row">
            <div class="col-md-6 text-center">
                <?=wp_get_attachment_image( $content['box_1_image'],'large');?>
            </div>
            <div class="col-md-6">
                <?= $content['text'];?>
            </div>
        </div>
    </div> 
</section>
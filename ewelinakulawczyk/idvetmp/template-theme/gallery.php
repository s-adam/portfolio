
<section class="<?=$content['acf_fc_layout']?>" style="<?=$content['section_style']?>">

    <div class="<?=!$content['full_width']?'container':'container-fluid'?>">
    
        <div class="row">
            <div class="col-md-12 gallery-content">
            <?php
            
                if (count($content['gallery_row']) > 0) {


                    foreach ($content['gallery_row'] as $gallery) {

                        ?>
                        
                           <?php
                            if($gallery){
                                
                                if(!empty($gallery['gallery_header'])){
                                ?>
                                    
                                <div class="row text-center">
                                    <h2><?=$gallery['gallery_header'];?></h2>
                                </div>    
                                <?php
                                
                                }
                                $i = 0;
                                $j = 0;
                                foreach ($gallery['pliki_graficzne'] as $image) {
                                    if ($i == 0) {
                                        echo '<div class="row">';
                                    }
                                    ?>
                                    <div class="col-md-4 text-center">
                                        <a href="<?= $image['sizes']['large']; ?>" class="mgn-popup">
                                            <img src="<?= $image['sizes']['gallery-thumb']; ?>" alt="">
                                        </a>                            
                                    </div>    
                                    <?php
                                    
                                    $i++;
                                    $j++;
                                    if ($i == 3 || $j == count($gallery['pliki_graficzne'])) {
                                        $i = 0;
                                        echo '</div>';
                                    }
                                    
                                }
                            }
                           
                           ?>
                           
                    <?php

                }
            }
            
            ?>
            
            </div>      
        </div>    
    </div>
</section> 
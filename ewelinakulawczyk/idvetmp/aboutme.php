<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package idvetmp
 *
 */
/*
Template Name: O mnie
*/
get_header();
the_post();
$fields = get_fields(get_the_ID());

?>


    <div class="main-container">


        <div class="title-wrapper">
            <h1 class="anim" data-vp-add-class="animated fadeIn"><?php the_title(); ?></h1>
        </div>


        <?php
        get_template_part('menu');
        ?>

        <div class="container">
            <div class="row">
                <div class="about-me main-wrapper col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="about-author-left">
                                <span><hr class="zero" data-vp-add-class="nowa fadeIn"></span>
                                <span><hr class="aww"></span>
                                <div class="author-photo">
                                    <?= wp_get_attachment_image($fields['duze_zdjecie'], 'author-photo-img'); ?>
                                </div>



                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="about-author anim" data-vp-add-class="animated fadeIn">
                                <?= $fields['glowny_wpis']?>
                                <img src="<?php echo get_template_directory_uri(); ?>/img/podpis.jpg" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">

                        </div>
                        <div class="col-md-6">
                            <hr class="margin-wrapp" data-vp-add-class="nowa fadeIn">
                            <div class="author-specialization">
                                <?= $fields['krotki_opis_pod_linia']?>
                            </div>
                        </div>
                    </div>
                    <div class="slider-about-me anim" data-vp-add-class="animated fadeIn">
                        <hr>
                        <div class="owl-carousel text-center">


                            <?php
                            $images = get_field('dodaj_zdjecia');
                            if($images){

                                foreach ($images as $field) {

                                    ?>

                                        <a href="<?= $field['sizes']['large']  ?>"><img class="text-center" src="<?= $field['sizes']['gallery-small-img']  ?>" alt=""></a>

                                    <?php
                                }
                            }

                            ?>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();

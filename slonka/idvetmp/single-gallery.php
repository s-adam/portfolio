<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package idvetmp
 */
get_header();
the_post();
$fields = get_fields(get_the_ID());
?>

    <div class="main-container">
        <div class="liscie-prawo-gora paralax-up">
            <img class="" src="<?php echo get_template_directory_uri(); ?>/img/prawe_liscie_gora.png" alt="">
        </div>

        <div class="liscie-lewe paralax-middle">
            <img class="" src="<?php echo get_template_directory_uri(); ?>/img/lewe_liscie.png" alt="">
        </div>
        <?php
        if ( is_user_logged_in() ) { ?>

        <div class="container">
            <div class="row">
                <div class="main-wrapper gallery-wrapper darken-color col-md-12 ">
                    <div class="contact text-center">
                        <div class="berk-title big-title">
                            <?php the_title(); ?>
                        </div>
						<div class="text-left row">
							<div class="col-md-12">
								<?=$fields['tekst_nad_galeria'];?>
							</div>
						</div>
                        <div class="new-gallery">
                        <?php
                        $images = get_field('zdjecia');
                        if($images){
                        $i = 0;
                        $j = 0;
                        foreach ($images as $field) {
                        if ($i == 0) {
                                                    echo '<div class="row">';
                        }
                        ?>

                        <div class="galeria-g col-md-4">
                            <a class="img-gallery" href="<?= $field['sizes']['large']  ?>"><img class="pop-up-link" src="<?= $field['sizes']['gallery-small-img'] ?>" alt=""></a>

                        </div>
                        <?php
                        $i++;
                        $j++;
                        if ($i == 3 || count($images) == $j) {


                                    echo '</div>';

                        $i = 0;
                        }
                        }
                        }

                        ?>

                        </div>

                    </div>

                    </div>

                </div>
            </div>
        <?php
        } else {
            ?>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="must-login text-center">
                            <!--                        <h1><a href="/wp-admin">Musisz się zalogować!</a></h1>-->
                            <ul>
                                <li><a href="/wp-admin">Musisz się zalogować!</a></li>
                                <li><a href="/wp-admin"><img src="<?php echo get_template_directory_uri(); ?>/img/logo2.png" alt=""></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>


            <?php
        }
        ?>
        </div>

<!--    </div>-->

<?php
get_footer();

<?php
/*
  Template Name: Historia
 */
get_header();
the_post();

$fields = get_fields(get_the_ID());
?>

<div class="main-container">
    <div class="liscie-prawo-gora paralax-up">
        <img class="paralax" src="<?php echo get_template_directory_uri(); ?>/img/prawe_liscie_gora.png" alt="">
    </div>

    <div class="liscie-prawo-dol paralax-down">
        <img class="paralax" src="<?php echo get_template_directory_uri(); ?>/img/prawe_liscie_dol.png" alt="">
    </div>

    <div class="liscie-lewe paralax-middle">
        <img class="paralax" src="<?php echo get_template_directory_uri(); ?>/img/lewe_liscie.png" alt="">
    </div>


    <div class="container">
        <div class="row">
            <div class="main-wrapper darken-color col-md-12 ">
                <div class="contact">
                    <div class="row col-md-12">
                        <div class="berk-title big-title">
                            <?php the_title(); ?>
                        </div>
                        <div class="historia-wprapper">

                            <?php
                            if ($fields['historia-zawartosc']) {
                                foreach ($fields['historia-zawartosc'] as $field) {


                                    if ($field['acf_fc_layout'] == 'zawartosc') {
                                        echo $field['tekst'];
                                    } elseif ($field['acf_fc_layout'] == 'kolumny') {
                                        ?>

                                        <div class="row">
                                            <div class="col-md-4">
                                        <?= $field['kolumna1'] ?>
                                            </div>
                                            <div class="col-md-4">
                                                <?= $field['kolumna2'] ?>
                                            </div>
                                            <div class="col-md-4">
                                                <?= $field['kolumna3'] ?>
                                            </div>
                                        </div>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>

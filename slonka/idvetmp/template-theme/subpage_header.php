
<section class="<?= $content['acf_fc_layout'] ?>" style="<?=$content['section_style']?>">
    <div class="<?=!$content['full_width']?'container':'container-fluid'?>"> 
    <?php
    if (!empty($content['subpage_header_bg'])) {
        ?>
        <div class="subpage-header-bg" style="background: url(<?= $content['subpage_header_bg']['sizes']['subpage-bg'] ?>)">    
            
                <?php
            }
            ?>

                       

                <div class="row">

                    <div class="col-md-12">
                        <?= $content['subpage_header_text'] ?>
                        <?php
                        if (!empty($content['subpage_header_bg'])) {
                            ?>
                    </div>
                </div>    
                    <?php
                }
                ?>
            

    <?php if (!empty($content['subpage_header_bg'])) {?>            
          
    </div>
    <?php } ?>
    </div>    
</section> 
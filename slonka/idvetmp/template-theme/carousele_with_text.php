
<section class="<?=$content['acf_fc_layout']?>" style="<?=$content['section_style']?>">

    <div class="<?=!$content['full_width']?'container':'container-fluid'?>">
    
        <div class="row">
            <!--
            <div class="carousel-nav">
                <span class="prev-slide">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/left-2.png" alt="prev">
                </span>
                
                <span class="next-slide">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/right-2.png" alt="next">
                </span> 
            </div>
            -->
            <div class="owl-carousel-<?=$content['acf_fc_layout']?> owl-carousel ">
                <?php
                    if($content['box']){
                        foreach ($content['box'] as $item) {
                            ?>
                            <div class="item">
                                <div class="item-text" >
                                    
                                    <?=$item['text']?>
                                    
                                </div>
                            </div>
                            <?php
                        }
                    }
                ?>
            </div>
        </div>    
    </div>
</section> 

<script>

var carousele_with_text_items = '<?=$content['box_count']?>';

</script>
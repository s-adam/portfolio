
<section class="<?=$content['acf_fc_layout']?>" style="<?=$content['section_style']?>" >

    <div class="<?=!$content['full_width']?'container':'container-fluid'?>">
    
        <div class="row">
            
            <div class="carousel-nav">
                <span class="prev-slide">
                    <i class="fa fa-angle-left" aria-hidden="true" ></i>
                </span>
                
                <span class="next-slide">
                    <i class="fa fa-angle-right" aria-hidden="true" ></i>
                </span> 
            </div>
            
            
            <?php
            
            $style = '';
            
            if(!empty($content['section_style'])){
                $style = $content['section_style'];
            }
            
            ?>
            
            <div class="owl-carousel-<?=$content['acf_fc_layout']?> owl-carousel " >
                <?php
                    if($content['slide']){
                        foreach ($content['slide'] as $item) {
                            ?>
                            <div class="item" style="<?=$style;?>background-image: url(<?=$item['background_image']['sizes']['main-slider']?>) ">
                                <div class="item-text" >
                                    <div style="color: <?=$content['text_color']?>; background: rgba(<?=$content['bg_color']?>); font-size: <?=$content['font_size']?>px ">
                                        <?=$item['text']?>
                                    </div>    
                                </div>
                            </div>
                            <?php
                        }
                    }
                ?>
            </div>
        </div>    
    </div>
</section> 
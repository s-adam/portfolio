<section class="<?=$content['acf_fc_layout']?>" style="<?=$content['section_style']?>">
    <div class="<?=!$content['full_width']?'container':'container-fluid'?>">
        <div class="row">
            <div class="col-md-7">
                <?= $content['text'];?>
            </div>
            <div class="col-md-5 text-center">
                <?=wp_get_attachment_image( $content['box_2_image'],'large');?>
            </div>
        </div>
    </div>
</section>
<?php
/*
Template Name: Terminarz
*/
get_header();
the_post();

$fields = get_fields(get_the_ID());
?>

<div class="main-container">
<div class="liscie-prawo-gora paralax-up">
    <img class="" src="<?php echo get_template_directory_uri(); ?>/img/prawe_liscie_gora.png" alt="">
</div>



<div class="liscie-lewe paralax-middle">
    <img class="" src="<?php echo get_template_directory_uri(); ?>/img/lewe_liscie.png" alt="">
</div>

    <?php
    if ( is_user_logged_in() ) { ?>
    <div class="container">
        <div class="row">
            <div class="main-wrapper darken-color col-md-12 ">
                <div class="contact">
                    <div class="row">
                        <div class="berk-title big-title">
                            <?php the_title(); ?>
                        </div>
                        <div class="terminarz-polowan text-center">

                            <table class="table table-hover table-hidden">
                                <thead class="thead-inverse text-center">
                                <tr>
                                    <th class="small-width">L.P.</th>
                                    <th class="data">DATA</th>
                                    <th>GODZINA</th>
                                    <th class="col-zw">ZWIERZYNA DO POZYSKANIA</th>
                                    <th>MIEJSCE ZBIÓRKI</th>
                                    <th>OBWÓD - REJONY POLOWANIA</th>
                                    <th>KIEROWNIK POLOWANIA</th>
      
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                if($fields['terminarz_polowan']) {
                                    $a = 1;
                                    foreach ($fields['terminarz_polowan'] as $field) {


                                    ?>
                                    <tr class="text-center">


                                        <th scope="row"><?= $a++ ?></th>
                                        <td><?= $field ['data'] ?> </td>
                                        <td><?= $field ['godzina'] ?> </td>
                                        <td><p><?= $field ['zwierzyna'] ?></p></td>
                                        <td><p><?= $field ['zbiorka'] ?></p></td>
                                        <td><?= $field ['obwod'] ?></td>
                                        <td class="To-left"><?= $field ['prowadzacy'] ?></td>
                                        

                                    </tr>
                                    <?php

                                }}
                                ?>

                                </tbody>
                            </table>
                            <?php

                            if($fields['terminarz_polowan']) {
                                $a = 1;
                                foreach ($fields['terminarz_polowan'] as $field) {


                                    ?>
                            <table class="table table-hover table-responsive table-visible">

                                <thead class="clicker">
                                <tr>
                                    <th class="white-color lp-bottom">
                                        <span>DATA</span>
                                    </th>
                                    <th class="text-center center-wrapper">
                                         <span class=""><?= $field ['data'] ?></span> <span style="display: none;"><?= $a ?></span>
                                    </th>
                                </tr>
                                </thead>

                                <tbody class="bodybody tooglerek" id="polowanie-<?=$a?>">
                                <tr>
                                    <td class="white-color">
                                        GODZINA
                                    </td>
                                    <td>
                                        <?= $field ['godzina'] ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="white-color">
                                        ZWIERZYNA DO POZYSKANIA
                                    </td>
                                    <td>
                                        <?= $field ['zwierzyna'] ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="white-color">
                                        MIEJSCE ZBIÓRKI
                                    </td>
                                    <td>
                                        <?= $field ['zbiorka'] ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="white-color">
                                        OBWÓD - REJONY POLOWAŃ
                                    </td>
                                    <td>
                                        <?= $field ['obwod'] ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="white-color">
                                        KIEROWNIK POLOWANIA
                                    </td>
                                    <td>
                                        <?= $field ['prowadzacy'] ?>
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                            <?php
                                $a++;
                            }}
                            ?>




                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <?=$fields['text']?>
                        </div>
                    </div>

                </div>

            </div>
        </div>



    </div>
    <?php
    } else {
        ?>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="must-login text-center">
                        <ul>
                            <li><a href="/wp-admin">Musisz się zalogować!</a></li>
                            <li><a href="/wp-admin"><img src="<?php echo get_template_directory_uri(); ?>/img/logo2.png" alt=""></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>


        <?php
    }
    ?>

</div>


<?php get_footer(); ?>
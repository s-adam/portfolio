<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package idvetmp
 */
get_header();
the_post();
$fields = get_fields(get_the_ID());

?>
<main id="main" class="site-main" role="main">
    <div class="container ">
        <div class="row">
            <div class="col-md-12 ">

            </div><!-- #primary -->
        </div>
    </div>
</main>

    <div class="main-container">
        <div class="liscie-prawo-gora paralax-up">
            <img class="" src="<?php echo get_template_directory_uri(); ?>/img/prawe_liscie_gora.png" alt="">
        </div>



        <div class="liscie-lewe paralax-middle">
            <img class="" src="<?php echo get_template_directory_uri(); ?>/img/lewe_liscie.png" alt="">
        </div>


        <div class="container">
            <div class="row">
                <div class="main-wrapper darken-color col-md-12 ">
                    <div class="contact">
                        <div class="row">

                            <div class="light-color light-color-right">
                                <?php echo wp_get_attachment_image(get_post_thumbnail_id(), 'komunikaty'); ?>
<!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium earum laboriosam porro possimus voluptatibus! Architecto corporis cupiditate delectus dolor eius est, facilis fuga illo inventore laudantium magnam necessitatibus, nostrum odio pariatur quasi, quibusdam repellat ullam voluptatibus. Assumenda autem est, ex nostrum perferendis quas. A, ad aspernatur aut deleniti dolor et eum expedita facere in inventore nobis odio officiis omnis quod repellat reprehenderit sapiente sequi totam veniam voluptatum! A, exercitationem, neque.</p>-->
                                <p class="arch-title"><?php the_title(); ?></p>
                                <span class="grey-box-arch"> <?php the_date('d.m.Y'); ?></span>
                                <?php
                                the_content();
                                ?>
                            </div>

                        </div>



                    </div>

                </div>
            </div>
        </div>

    </div>

<?php
get_footer();

<?php
/*
Template Name: Główny Szablon
*/
get_header();
the_post();

$fields = get_fields(get_the_ID());
?>

<div class="main-container">
<div class="liscie-prawo-gora paralax-up">
    <img class="" src="<?php echo get_template_directory_uri(); ?>/img/prawe_liscie_gora.png" alt="">
</div>



<div class="liscie-lewe paralax-middle">
    <img class="" src="<?php echo get_template_directory_uri(); ?>/img/lewe_liscie.png" alt="">
</div>

<?php
if ( is_user_logged_in() ) { ?>
    <div class="container">
        <div class="row">
            <div class="main-wrapper darken-color col-md-12 ">
                <div class="pobierz-height contact">

                        <div class="berk-title big-title">
                            <?php the_title(); ?>
                        </div>
                        <div class="pobierz text-center">

                            <div class="pobierz-plik">
                                <ul>
                                    <?php
                                $pobierz = get_field('plik_do_pobrania');
                                    if ($fields['dodaj_pliki_do_porbrania']){
                                    foreach ($fields['dodaj_pliki_do_porbrania'] as $field) {


                                        ?>
                                        <li><a href="<?=$field['plik_do_pobrania']['url']; ?>"><?= $field['nazwa_pliku'] ?></a> </li>

                                        <?php
                                    }}
                                    ?>

                                </ul>
                            </div>

                        </div>

                </div>

            </div>
        </div>
    </div>
    <?php
} else {
    ?>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="must-login text-center">
                    <ul>
                        <li><a href="/wp-admin">Musisz się zalogować!</a></li>
                        <li><a href="/wp-admin"><img src="<?php echo get_template_directory_uri(); ?>/img/logo2.png" alt=""></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>
</div>


<?php get_footer(); ?>

<?php
/*
Template Name: Komunikaty
*/
get_header();
the_post();

$fields = get_fields(get_the_ID());
?>

<div class="main-container">
    <div class="liscie-prawo-gora paralax-up">
        <img class="" src="<?php echo get_template_directory_uri(); ?>/img/prawe_liscie_gora.png" alt="">
    </div>



    <div class="liscie-lewe paralax-middle">
        <img class="" src="<?php echo get_template_directory_uri(); ?>/img/lewe_liscie.png" alt="">
    </div>
    <?php
    if ( is_user_logged_in() ) { ?>

        <div class="container">
            <div class="row">
                <div class="main-wrapper darken-color col-md-12 ">
                    <div class="contact">
                        <div class="row">
                            <div class="berk-title big-title">
                                <?php the_title(); ?>
                            </div>
                            <div class="komunikaty text-center">

                                <?php


                                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                                query_posts(array('posts_per_page' => 3, 'paged' => $paged));

                                if ( have_posts() ) : ?>
                                    <?php
                                    // Start the Loop.
                                    while ( have_posts() ) : the_post();
                                        ?>
                                        <?php



                                        $row = false;
                                        ?>

                                        <div class="blog-one col-md-12">



                                            <a class="title-blog" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

                                            <span class="grey-box-kom"> <?= get_the_date('d.m.Y'); ?></span>

                                            <div class="area-under-post">
                                                <?php the_excerpt() ?>
                                            </div>


                                        </div>


                                        <?php
                                    endwhile;

                                else :
                                endif;
                                ?>


                                <?php wpbeginner_numeric_posts_nav(); ?>

                            </div>
                        </div>



                    </div>

                </div>
            </div>
        </div>
        <?php
    }
    else {
        ?>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="must-login text-center">
                        <ul>
                            <li><a href="/wp-admin">Musisz się zalogować!</a></li>
                            <li><a href="/wp-admin"><img src="<?php echo get_template_directory_uri(); ?>/img/logo2.png" alt=""></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>


        <?php
    }
    ?>

</div>


<?php get_footer(); ?>

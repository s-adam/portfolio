<?php
/*
Template Name: Obwód łowiecki
*/
get_header();
the_post();

$fields = get_fields(get_the_ID());
?>

<div class="main-container">
<div class="liscie-prawo-gora paralax-up">
    <img class="paralax" src="<?php echo get_template_directory_uri(); ?>/img/prawe_liscie_gora.png" alt="">
</div>

    <div class="liscie-prawo-dol obwod">
        <img class="paralax show-img" src="<?php echo get_template_directory_uri(); ?>/img/prawe_liscie_dol.png" alt="">
    </div>

<div class="liscie-lewe paralax-middle">
    <img class="paralax " src="<?php echo get_template_directory_uri(); ?>/img/lewe_liscie.png" alt="">
</div>


    <div class="container">
        <div class="row">
            <div class="main-wrapper darken-color col-md-12 ">
                <div class="contact">
                    <div class="row">
                        <div class="berk-title big-title">
                            <?php the_title(); ?>
                        </div>
                        <div class="obwody-wprapper">
<!--                            <img src="--><?php //echo get_template_directory_uri(); ?><!--/img/mapka_duza.jp" alt="">-->
                            <?php
                            echo wp_get_attachment_image(get_post_thumbnail_id(), 'blog-thumbnail');
                            ?>

                                <?= $fields ['tekst_pod_mapa'] ?>




                        </div>
                    </div>



                </div>

            </div>
        </div>
    </div>

</div>


<?php get_footer(); ?>

<?php
/*
Template Name: Kontakt
*/
get_header();
the_post();

$fields = get_fields(get_the_ID());
?>

<div class="main-container">
<div class="liscie-prawo-gora paralax-up">
    <img class="paralax" src="<?php echo get_template_directory_uri(); ?>/img/prawe_liscie_gora.png" alt="">
</div>



<div class="liscie-lewe paralax-middle">
    <img class="paralax" src="<?php echo get_template_directory_uri(); ?>/img/lewe_liscie.png" alt="">
</div>


    <div class="container">
        <div class="row">
            <div class="main-wrapper darken-color col-md-12 ">
                <div class="contact">
<div class="row">
                        <div class="berk-title big-title">
                            <?php the_title(); ?>
                        </div>
                        <div class="contact-box">
                            <div class="col-md-5">
                                <div class="left-text">

                                    <?= $fields ['tekst_po_lewej'] ?>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <?php  the_content(); ?>
                            </div>

                        </div>
</div>
                    <div id="map">

                    </div>


                </div>

            </div>
        </div>
    </div>

</div>

<script>
    var map;
    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: <?= $fields ['google_maps']['lat'] ?>, lng: <?= $fields ['google_maps']['lng'] ?>},
            scrollwheel: false,
            zoom: 14,
        });

        var marker = new google.maps.Marker({
            position: {lat: <?= $fields ['google_maps']['lat'] ?>, lng: <?= $fields ['google_maps']['lng'] ?>},
        map: map,
            icon: '/wp-content/themes/idvetmp/img/marker.png'
        });
    }



</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBUrVTSGQntdf5lFcZZnY25nkTQb9OcPtA&callback=initMap" async defer></script>




<?php get_footer(); ?>

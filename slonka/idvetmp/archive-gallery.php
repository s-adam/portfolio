<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */

get_header(); ?>

	<div class="main-container">
		<div class="liscie-prawo-gora paralax-up">
			<img class="" src="<?php echo get_template_directory_uri(); ?>/img/prawe_liscie_gora.png" alt="">
		</div>

		<div class="liscie-lewe paralax-middle">
			<img class="" src="<?php echo get_template_directory_uri(); ?>/img/lewe_liscie.png" alt="">
		</div>

<?php
if ( is_user_logged_in() ) { ?>
		<div class="container">

				<div class="main-wrapper darken-color col-md-12 ">
					<div class="contact galeria-wrapper">
						<div class="row">
							<div class="berk-title big-title">
								Galerie
							</div>
							<?php
							if ( have_posts() ) : 
								while ( have_posts() ) : the_post();?>
									<div class="gallery-one-item col-md-4">

										<a class="text-center" href="<?php the_permalink() ?>"><?=wp_get_attachment_image( get_post_thumbnail_id(),'gallery-small-img');?></a>

										<a class="gallery-button text-center" href="<?php the_permalink() ?>"><h2><?php the_title() ?></h2></a>

									</div>
<?php

								endwhile;

							else :

							endif; ?>
						</div>
					</div>
				</div>
		</div>
    <?php
} else {
    ?>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="must-login text-center">
                    <ul>
                        <li><a href="/wp-admin">Musisz się zalogować!</a></li>
                        <li><a href="/wp-admin"><img src="<?php echo get_template_directory_uri(); ?>/img/logo2.png" alt=""></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>
	</div>
<?php
get_footer();

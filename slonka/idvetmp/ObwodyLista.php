<?php
/*
Template Name: Obwody łowieckie - lista
*/
get_header();
the_post();

$fields = get_fields(get_the_ID());
?>

<div class="main-container">
<div class="liscie-prawo-gora paralax-up">
    <img class="" src="<?php echo get_template_directory_uri(); ?>/img/prawe_liscie_gora.png" alt="">
</div>



<div class="liscie-lewe paralax-middle">
    <img class="" src="<?php echo get_template_directory_uri(); ?>/img/lewe_liscie.png" alt="">
</div>

    <?php
    if ( is_user_logged_in() ) { ?>

    <div class="container" >
        <div class="row" >
            <div class="main-wrapper darken-color col-md-12 " >
                <div class="contact" >
                    <div class="row" >
                        <div class="berk-title big-title" >
                            <?php the_title(); ?>
</div>
    <div class="obwody-Lista text-center">
        <?php

        $args = array(
            'post_type'      => 'page',
            'posts_per_page' => -1,
            'post_parent'    => $post->ID,
            'order'          => 'ASC',
            'orderby'        => 'menu_order'
        );


        $parent = new WP_Query( $args );
        ?>





        <?php
        if ( $parent->have_posts() ) : ?>

            <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
            <div class="col-md-4">
                <div class="pojedynczy-obwod">

<!--                    <a href="#"><img src="--><?php //echo get_template_directory_uri(); ?><!--/img/mapka_mala.jpg" alt=""></a>-->
                    <a href="<?php the_permalink(); ?>"><?php echo wp_get_attachment_image(get_post_thumbnail_id(), 'obwody-img'); ?></a>
                    <!--                                    <img src="' . wp_get_attachment_thumb_url( $attachment->ID )'">-->
                    <div class="btn-obwod">
                        <a href="<?php the_permalink(); ?>"><?=the_title()?></a>


                    </div>
                </div>
            </div>
            <?php endwhile; ?>

        <?php endif; wp_reset_query(); ?>


    </div>
    </div>


    </div>

    </div>
    </div>
    </div>
<?php
    } else {
        ?>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="must-login text-center">
<!--                        <h1><a href="/wp-admin">Musisz się zalogować!</a></h1>-->
                        <ul>
                            <li><a href="/wp-admin">Musisz się zalogować!</a></li>
                            <li><a href="/wp-admin"><img src="<?php echo get_template_directory_uri(); ?>/img/logo2.png" alt=""></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>


    <?php
    }
?>

</div>


<?php get_footer(); ?>

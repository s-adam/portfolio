<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package idvetmp
 */
get_header();
the_post();
$fields = get_fields(get_the_ID());

?>
<div class="main-container">
    <div class="container">
        <div class="row">
            <div class="main-wrapper new-page col-md-12">
                <h1><?php the_title(); ?></h1>
                <?php
                the_content();
                ?>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();

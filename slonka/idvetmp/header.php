<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package idvetmp
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>

    <link rel="apple-touch-icon" sizes="36x36" href="<?php echo get_template_directory_uri(); ?>/img/favico/android-icon-36x36.png">
    <link rel="apple-touch-icon" sizes="48x48" href="<?php echo get_template_directory_uri(); ?>/img/favico/android-icon-48x48.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/img/favico/android-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="96x96" href="<?php echo get_template_directory_uri(); ?>/img/favico/android-icon-96x96.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/img/favico/android-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="192x192" href="<?php echo get_template_directory_uri(); ?>/img/favico/android-icon-192x192.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/img/favico/apple-icon.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/img/favico/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/img/favico/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/img/favico/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/img/favico/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/img/favico/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="144x120" href="<?php echo get_template_directory_uri(); ?>/img/favico/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/img/favico/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/img/favico/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/img/favico/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri(); ?>/img/favico/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/img/favico/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri(); ?>/img/favico/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/img/favico/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff ">
    <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/img/favico/ms-icon-144x144.png">
    <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/img/favico/ms-icon-70x70.png">
    <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/img/favico/ms-icon-150x150.png">
    <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/img/favico/ms-icon-310x310.png">
    <meta name="theme-color" content="#ffffff ">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/magnific-popup.css">
<link href="https://fonts.googleapis.com/css?family=Berkshire+Swash|Open+Sans:400,700&amp;subset=latin-ext" rel="stylesheet">
    <script src='https://www.google.com/recaptcha/api.js'></script>


</head>

<body <?php body_class(); ?>>
     
    
   <nav class="navbar">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
            <a class="navbar-brand text-center" href="/">

                <img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="">

            </a>

            <div class="login pull-right hidden-md hidden-lg">
                <?php
                if ( !is_user_logged_in() ) {
                    echo '<a href="/wp-admin"> <i class="fa fa-user-o" aria-hidden="true"></i></a>';
                } else {
                    ?>
                    <a href="<?php echo wp_logout_url(home_url()); ?>"><i class="fa fa-sign-out" title="Wyloguj" aria-hidden="true"></i></a>
                <?php
                }
?>
            </div>
        </div>
        <div id="navbar" class="collapse navbar-collapse ">
                <div class="row">
                    <div class="col-md-6" >
                        <?php
                        wp_nav_menu(array(
                                'menu' => 'primary',
                                'theme_location' => 'primary',
                                'depth' => 3,
                                'container' => false,
                                'container_class' => 'collapse navbar-collapse',
                                'menu_class' => 'nav navbar-nav',
                                'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
                                'walker' => new wp_bootstrap_navwalker())
                        );
                        ?>

                    </div>

                    <div class="col-md-6 section-right-menu">
                        <div class="section-right">

                            <?php
                            if ( !is_user_logged_in() ) {
                                ?>
                                <ul>
                                    <li><a class="dla-czlonkow" href="/wp-admin">Dla członków</a></li>
                                </ul>
                                <?php

                            } else {
                                ?>
                                <?php
                                wp_nav_menu(array(
                                        'menu' => 'MenuLogin',
                                        'theme_location' => 'MenuLogin',
                                        'depth' => 3,
                                        'container' => false,
                                        'container_class' => 'collapse navbar-collapse',
                                        'menu_class' => 'nav navbar-nav pull-right',
                                        'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
                                        'walker' => new wp_bootstrap_navwalker())
                                );
                                ?>

                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>

        </div><!--/.nav-collapse -->

    </nav>
<?php
/*
Template Name: Zarząd
*/
get_header();
the_post();

$fields = get_fields(get_the_ID());

?>

<div class="main-container">
<div class="liscie-prawo-gora paralax-up">
    <img class="paralax" src="<?php echo get_template_directory_uri(); ?>/img/prawe_liscie_gora.png" alt="">
</div>

<div class="liscie-prawo-dol paralax-down">
    <img class="paralax" src="<?php echo get_template_directory_uri(); ?>/img/prawe_liscie_dol.png" alt="">
</div>

<div class="liscie-lewe paralax-middle">
    <img class="paralax" src="<?php echo get_template_directory_uri(); ?>/img/lewe_liscie.png" alt="">
</div>


    <div class="container">
        <div class="row">
            <div class="main-wrapper darken-color col-md-12 ">
                <div class="about-us">

                        <div class="berk-title big-title">
                            <?php the_title(); ?>
                        </div>

                            <?php

                            if($fields['top_text']){
                                echo $fields['top_text'];
                            }
                            
                            if($fields['czlonkowie_kola']){
                                $i = 0;
                                $j = 0;
                                foreach ($fields['czlonkowie_kola'] as $field) {
                                    if (!empty($field['zdjecie'])){
                                        if ($i == 0) {
                                            echo '<div class="row">';
                                        }
                                ?>
                                <div class="<?=$j==3&&count($fields['czlonkowie_kola'])==5?'col-md-offset-2':''?> col-md-4">
                                    <div class="col-zarzad">
                                        <?=wp_get_attachment_image( $field['zdjecie'],'member');?>
                                        <div class="text-under-photo">
                                            <h5><?= $field ['imie_i_nazwisko'] ?></h5>
                                            <p><?= $field ['stanowisko'] ?></p>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                $i++;
                                $j++;
                                        if ($i == 3 || count($fields['czlonkowie_kola']) == $j) {
                                            echo '</div>';
                                            $i = 0;
                                        }
                                    }
                                }
                            }

                            ?>


                </div>
            </div>
        </div>
    </div>

</div>

<?php get_footer(); ?>

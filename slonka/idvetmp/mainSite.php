<?php
/*
Template Name: Strona Główna
*/
get_header();
the_post();

$fields = get_fields(get_the_ID());

?>

<div class="main-container">
<div class="liscie-prawo-gora">
    <img class="paralax" src="<?php echo get_template_directory_uri(); ?>/img/prawe_liscie_gora.png" alt="">
</div>

<div class="liscie-prawo-dol">
    <img class="paralax" src="<?php echo get_template_directory_uri(); ?>/img/prawe_liscie_dol.png" alt="">
</div>

<div class="liscie-lewe">
    <img class="paralax" src="<?php echo get_template_directory_uri(); ?>/img/lewe_liscie.png" alt="">
</div>

    <div class="container">
        <div class="row">
            <div class="main-wrapper">
                <div class="kolo-photo text-center">
                    <?php
                    if(isset($_GET['loggedout']) && $_GET['loggedout'] == 'true'){
                        ?>
                        <div class="alert alert-success">
                            <strong>Zostałeś Wylogowany!</strong>
                        </div>
                    <?php
                    }
                    ?>
                    <?= $fields ['sekcja_kolo'] ?>
                </div>
                <div class="see">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-5 col-md-offset-1">
                                <div class="see-one-col text-center" style="background: url('<?php echo get_template_directory_uri(); ?>/img/kronika.jpg') top 75px center no-repeat #fff8e6">
                                    <div class="berk-title text-center">
                                        <p>Kronika</p>
                                    </div>
                                    <a class="btn" href="/kronika/">Zobacz</a>
                                </div>
                            </div>
                            <div class="col-md-5 ">
                                <div class="see-one-col text-center" style="background: url('<?php echo get_template_directory_uri(); ?>/img/slonka-dzieci.png') top 75px center no-repeat #fff8e6">
                                    <div class="berk-title text-center">
                                        <p>Gry łowieckie dla dzieci</p>
                                    </div>
                                    <a class="btn" href="https://www.pzlow.pl/palio/html.run?_Instance=www&_PageID=12&_Lang=pl&_CheckSum=-1865952796" target="_BLANK">Zobacz</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
                
                
                <?php if ( is_user_logged_in() ) {?>
                
                <div class="see">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-5 col-md-offset-1">
                                <div class="see-one-col text-center" style="background: url('<?php echo get_template_directory_uri(); ?>/img/kalendarz.jpg') top 75px center no-repeat #fff8e6">
                                    <div class="berk-title text-center">
                                        <p>Kalendarz polowań</p>
                                    </div>
                                    <a class="btn" href="/terminarz-polowan/">Zobacz</a>
                                </div>
                            </div>

                            <div class="col-md-5">
                                <div class="see-one-col text-center" style="background: url('<?php echo get_template_directory_uri(); ?>/img/mapa.jpg') top 75px center no-repeat #fff8e6">
                                    <div class="berk-title text-center">
                                        <p>Obwody łowieckie</p>
                                    </div>
                                    <a class="btn" href="/obwody-lowieckie/">Zobacz</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php } ?>

                <?php get_sidebar(); ?>



                <div class="section-links">
                    <div class="berk-title text-center">
                        <p>Ciekawe linki</p>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="ciekawe-linki col-md-12">
                                <?php

                                if($fields['ciekawe_linki']){
                                foreach ($fields['ciekawe_linki'] as $field) {


                                if($field['acf_fc_layout'] == 'linki'){

                                ?>

                                    <div class="col-md-3 col-md-offset-1 col-sm-6">
                                        <?=$field['tabela1']?>
                                    </div>
                                    <div class="col-md-3 col-md-offset-1 col-sm-6">
                                        <?=$field['tabela2']?>
                                    </div>
                                    <div class="col-md-3  col-sm-6">
                                        <?=$field['tabela3']?>
                                    </div>

                                    <?php

                                }
                                }
                                }

                                ?>


                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>

</div>

<?php get_footer(); ?>

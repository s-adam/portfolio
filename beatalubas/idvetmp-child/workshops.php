<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*
Template Name: Workshop
*/
get_header();
the_post();

$fields = get_fields(get_the_ID());
?>

<div class="main-wrapper workshop">
	<div class="header-post" style="background: url('<?php echo get_stylesheet_directory_uri(); ?>/img/header-photo.jpg') no-repeat">
		<div class="date-and-comments">

		</div>
	</div>
</div>
<?php

get_footer();

?>

<?php

get_header();
the_post();

$fields = get_fields(get_the_ID());
?>

	<div class="main-wrapper workshop">
		<div class="header-post" style="background: url(<?= $fields['header_photo']['sizes']['header_photo-post']?>) center center no-repeat">
			<div class="date-and-comments">

			</div>
		</div>
		<div class="single-content">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="single-title text-center">
							<p><?=the_time('j F Y ');?> /  <span><?php comments_number(); ?></span></p>
							<h2><?php the_title(); ?></h2>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<?php the_content(); ?>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 col-md-offset-2">
						<div class="ingredients">
							<?=$fields['składniki']?>
						</div>
					</div>
					<div class="col-md-4">
						<div class="preparation">
							<?=$fields['przepis']?>
						</div>
					</div>

				</div>
				<div class="last-of-content text-center">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/small-pedzel.jpg" alt="">
				</div>




<div class="hhhh">
	<div class="container">
		<div class="row">
			<div class="col-md-2 col-md-offset-2">
				<div class="print-wrapper">
					<a href="#">Print</a>
				</div>
			</div>
			<div class="col-md-6">
				<div class="share-wrapper">

					<div class="shre">
						<h3>Share it:</h3>
					</div>

					<ul class="share-section">
												<li>
													<a href="https://www.facebook.com/sharer/sharer.php?u=<?= get_permalink()?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
														<img class="fb-icon" src="<?=get_stylesheet_directory_uri()?>/img/twitter.jpg" alt="">
													</a>
												</li>



												<li>
													<a href="https://www.facebook.com/sharer/sharer.php?u=<?= get_permalink()?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
														<img class="fb-icon" src="<?=get_stylesheet_directory_uri()?>/img/pinterest.jpg" alt="">
													</a>
												</li>
						<li>
							<a href="https://www.facebook.com/sharer/sharer.php?u=<?= get_permalink()?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
								<img class="fb-icon" src="<?=get_stylesheet_directory_uri()?>/img/facebook.jpg" alt="">
							</a>
						</li>
					</ul>
				</div>

			</div>
		</div>
	</div>
</div>



			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="comment-wrapper">
						<?php
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php

get_footer();

?>
<?php

get_header();
the_post();

global $wp_query;
$categories = get_the_category();
$category_id = $categories[0]->cat_ID;
$args = array(
    'posts_per_page' => 8,
    'cat' => $category_id,

);

$args = array(
    'posts_per_page'  => 8,
    'orderby'         => 'meta_value',
    'meta_key'    => 'data_rozpoczęcia',
    'order'           => 'DESC',
    'post_type'       => 'post',
    'cat' => $category_id,
    'paged' => ( get_query_var('paged') ? get_query_var('paged') : 1),
    'meta_query' => array(
        array(
            'key' => 'data_zakończenia',
            'value' => date("Y-m-d"),
            'compare' => isset($_GET['archive'])?'<':'>=',
            'type' => 'DATE'
        )
    ),

);



$a = query_posts($args);
$ile_postow = count($a);

if($ile_postow == 0){
    $args = array(
        'posts_per_page'  => 8,
        'orderby'         => 'meta_value',
        'meta_key'    => 'data_rozpoczęcia',
        'order'           => 'DESC',
        'post_type'       => 'post',
        'cat' => $category_id,
        'paged' => ( get_query_var('paged') ? get_query_var('paged') : 1),
        'meta_query' => array(
            array(
                'key' => 'data_zakończenia',
                'value' => date("Y-m-d"),
                'compare' => '<',
                'type' => 'DATE'
            )
        ),

    );
$a = query_posts( $args );
}

?>


<div class="main-wrapper recipes workshops-wrapper">
    <div class="container">
        <div class="row">
            <div class="first-section text-center">
                <div class="col-md-12">
				<?php
					if (get_field('workshop_tytuł_głowny', 'option')){
                                    ?>
							<?php the_field('workshop_tytuł_głowny', 'option') ?>
							<?php
					}
				?>
              
                <div class="sentence-wrapper">
				<?php
					if (get_field('workshop_tekst_pod_tytułem', 'option')){
                                    ?>
							<?php the_field('workshop_tekst_pod_tytułem', 'option') ?>
							<?php
					}
				?>
				
                </div>
            </div>
            </div>
        </div>

        <?php
        $z = get_category_link(3);
        ?>

        <?php
        $actual_link="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $zz = parse_url($actual_link, PHP_URL_QUERY);

        $zz1 = 'archive=1';

        if ($zz == $zz1){
            $bb = 'active';
        }
        else{
            $aa = 'active';
        }
        ?>



        <div class="choose-the-time-of-year">
            <ul class="text-to-left">
                <li><a href="/category/workshops" class="<?=$aa?>"><?=($ile_postow == 0) ? 'PAST': 'UPCOMING'?></a></li>
                <?php if ($ile_postow == 0) {

                }else {

                    ?>
                    <li><a href="?archive=1" class="<?=$bb?>">PAST</a></li>
                    <?php
                }
                ?>

            </ul>
        </div>


        <?php

        if (have_posts()) :
            $i = 0;
            $j = 0;
            while (have_posts()) : the_post();

                $fieldsPost = get_fields(get_the_ID());
                if ($i == 0) {
                    echo '<div class="latest-posts">';
                    echo '<div class="row wrapping">';
                }
                ?>
                <div class="col-md-6">
<!--                    <div class="latest-posts__single-photo photo-height">-->
                        <div class="row">
                            <div class="col-md-4 col-sm-12">
                                <div class="photo-thumbnail-category">
                                    <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('bea-photo-image') ?></a>
                                </div>
                            </div>
                            <div class="col-md-8 col-sm-12">
                                <div class="latest-posts__info">
                                    <?php
                                    $dateFrom = strtotime($fieldsPost['data_rozpoczęcia']);
                                    $dateTo = strtotime($fieldsPost['data_zakończenia']);
                                    ?>
                                    <?php
                                    if($dateFrom != $dateTo){
                                        if(date_i18n('m', $dateFrom) == date_i18n('m', $dateTo)){
                                            echo '<span class="data">'.  date_i18n('j', $dateFrom).'-'.date_i18n( 'j M Y', $dateTo ).'</span>';
                                        }else{
                                            echo '<span class="data">'.  date_i18n('j M Y', $dateFrom).'-'.date_i18n( 'j M Y', $dateTo ).'</span>';
                                        }
                                    }else{
                                        echo '<span class="data">'.date_i18n( 'j M Y', $dateFrom ).'</span>';
                                    }
                                    ?>



                                    <a href="<?php the_permalink(); ?>" class=""><?php the_title(); ?></a>
                                </div>
                        </div>
<!--                        </div>-->
                    </div>
                </div>

                <?php
                $i++;
                $j++;
                if ($i == 2 || $j == count($a)) {


                    echo '</div>';
                    echo '</div>';


                    $i = 0;
                }
                ?>



                <?php
            endwhile; ?>
            <div class="container">
                <div class="row text-right">
                    <div class="col-md-12">
                        <?php the_posts_pagination( array(
                            'mid_size' => 2,
                            'prev_text' => __( '&laquo', 'textdomain' ),
                            'next_text' => __( '&raquo', 'textdomain' ),
                        ) ); ?>
                    </div>
                </div>
            </div>
        <?php endif; wp_reset_query(); ?>

        <?php require ("stay.php"); ?>

        <?php

        get_footer();

        ?>

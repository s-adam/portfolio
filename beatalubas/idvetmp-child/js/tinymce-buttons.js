(function() {
    tinymce.PluginManager.add('gavickpro_tc_button', function( editor, url ) {
        editor.addButton( 'gavickpro_tc_button', {
            //title: 'Dodaj button',
            icon: false,
            //type: 'menubutton',
            //icon: 'icon gavickpro-own-icon',
            
            text: 'Dodaj guzik',
                    onclick: function() {
                        editor.windowManager.open( {
                            title: 'Dodaj guzik',
                            body: [{
                                type: 'textbox',
                                name: 'title',
                                label: 'Nazwa'
                            },
                            {
                                type: 'textbox',
                                name: 'url',
                                label: 'Adres URL'
                            }
                            ],
                            onsubmit: function( e ) {
                                editor.insertContent( '<a href="' + e.data.url + '" class="read-btn">' + e.data.title + '</a>');
                            }
                        });
                    }
                
        });
    });
})();
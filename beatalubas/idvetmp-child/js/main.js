if ( $(".slider2").length >= 1 ) {
    options = {
        autoplay: {
            delay: 3000,
        },
        slidesPerView: 5,
        // loop: true,
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true
        },

        breakpoints: {
	    1200: {
                slidesPerView: 4,
                spaceBetween: 0
            },
	    900: {
                slidesPerView: 3,
                spaceBetween: 0
            },
            700: {
                slidesPerView: 1,
                spaceBetween: 0
            }
        }
    }
} else {
    options = {
        loop: false,
        autoplay: false,
        pagination: false
    }
}
var swiper = new Swiper('.slider2', options);





var swiper = new Swiper('.slider-awards', {
    autoplay: {
        delay: 3000,
    },
    slidesPerView: 1,
    // loop: true,
    pagination: {
        el: '.swiper-paginationsz',
        type: 'fraction',
        clickable: true
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    breakpoints: {
        700: {
            slidesPerView: 1,
            spaceBetween: 0
        }
    }
});

$( ".play-btn" ).click(function() {
    x = $(".YT-Player iframe").attr('src', $(".YT-Player iframe").attr('src')+'&autoplay=1');
    $( ".play-btn" ).hide();
	$( ".yt-thumbnail" ).hide();
    $( ".YT-Player" ).addClass('after');
    f = $(".YT-Player iframe").attr('src');


});


$(".up").click(function() {
    $("html, body").animate({ scrollTop: 0 }, "slow");
    return false;
});


var hamburger = document.querySelector(".hamburger");
var cien = document.querySelector(".nav-wrapper");
// On click
hamburger.addEventListener("click", function() {
    hamburger.classList.toggle("is-active");
    cien.classList.toggle("cien");
});



$(document).ready(function(){
  sizeTheVideo();
  $(window).resize(function(){
    sizeTheVideo();
  });  
});

function sizeTheVideo(){
  // - 1.78 is the aspect ratio of the video
// - This will work if your video is 1920 x 1080
// - To find this value divide the video's native width by the height eg 1920/1080 = 1.78
  var aspectRatio = 1.78;
	var YT = $('.YT-Player::after');
	var ytWrapp = $('.YT-Player');
    var video = $('.YT-Player iframe');
    var ytThumb = $('.yt-thumbnail');
    var videoWidth = video.outerWidth();
	
		var newHeight = videoWidth/aspectRatio;
    
  //Define the new width and centrally align the iframe
  video.css({"width":videoWidth+"px","left":"auto","height":newHeight+"px"});
  YT.css({"width":videoWidth+"px","left":"auto","height":newHeight+"px"});
  ytWrapp.css({"width":videoWidth+"px","left":"auto","height":newHeight+"px"});
  ytThumb.css({"width":videoWidth+"px","left":"auto","height":newHeight+"px"});
}


$(document).ready(function(){
  $('.rgg-imagegrid a').attr('data-toggle','lightbox');
  $('.rgg-imagegrid a').attr('data-gallery','gallery');
  
});

$(document).on('click', '[data-toggle="lightbox"]', function(event) {
                event.preventDefault();
                $(this).ekkoLightbox();
            });

			
			

$(window).scroll(function (event) {
    var scroll = $(window).scrollTop();
	if (scroll> 0){
		$( ".navbar" ).addClass('menu-opacity');
		$( "#primary-menu" ).addClass('menu-transparent');
		$( "#navbar" ).addClass('menu-transparent');
	}else{
		$( ".navbar" ).removeClass('menu-opacity');
	}
});

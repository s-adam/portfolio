<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*
Template Name: Contact
*/
get_header();
the_post();

$fields = get_fields(get_the_ID());
?>

<div class="main-wrapper contact-wrapper" style="background: url(<?= $fields['background_image_contact']['sizes']['slider_home']?>) top center no-repeat; background-size: cover;">
    <div class="overlay-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-wrapper">
                        <?=$fields['first_section']?>
                        <div class="row text-center">
                            <div class="col-md-6 col-xs-6">
                                <div class="location">
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/marker.svg">
                                    <p><?php the_field('location', 'option'); ?></p>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-6">
                                <div class="mail-wrapper">
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/mail-contact.svg">
                                    <p><a href="mailto:<?php the_field('email', 'option'); ?>" target="_blank"><?php the_field('email', 'option'); ?></a></p>
                                </div>
                            </div>
                        </div>
                        <?php echo do_shortcode( "[contact-form-7 id=\"17\" title=\"Contact\"]"); ?>
                    </div>
                </div>



            </div>


        </div>
    </div>
</div>
<div class="contact-content">
        <?php require ("stay.php"); ?>
    <div class="text-under-socials">
    </div>
</div>

<style>
    .wpcf7-response-output{
        color: #fff !important;
    }
</style>
<?php
get_footer();

?>

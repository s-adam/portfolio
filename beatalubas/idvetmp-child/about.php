<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*
Template Name: About
*/
get_header();
the_post();

$fields = get_fields(get_the_ID());
?>

<div class="main-wrapper about">
	<div class="YT-Player">
		<iframe width="100%" height="100%" src="https://www.youtube.com/embed/<?php the_field('YT-Video', 'option'); ?>?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        <img class="yt-thumbnail" src="https://i1.ytimg.com/vi/<?php the_field('YT-Video', 'option'); ?>/maxresdefault.jpg">
		<a class="play-btn"><img src="" alt=""></a>
    </div>
	
	<div class="about-me">
		<div class="container">
			<div class="row">
				<div class="first-section text-center">
					<div class="col-md-12">
						<?=$fields['section_title']?>
						<div class="sentence-wrapper"><?=$fields['section_sentence']?></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="my-awards" style="background: url(<?= $fields['background_image']['sizes']['slider_home']?>); background-size: cover;">
		<div class="h1-my-awards"><h1><?=$fields['section_awards']?></h1></div>
		<div class="slider-awards slider swiper-container">

			<div class="swiper-wrapper text-center">

				<?php

				if($fields['slider_awards']){

					foreach ($fields['slider_awards'] as $field) {

						?>
						<div class="swiper-slide">
							<div class=" slider__item">
								<div class="slider__text text-center">
									<img src="<?= $field['logo']['sizes']['client-logo']?>">
									<p><?=$field['title'];?></p>
								</div>
							</div>
						</div>
						<?php

					}
				}
				?>


			</div>
			<div class="swiper-button-prev"></div>
			<div class="swiper-paginationsz"></div>
			<div class="swiper-button-next"></div>
		</div>
	</div>
	<div class="clients">
		<div class="container-fluid">
			<div class="row">
				<div class="first-section text-center">
					<div class="col-md-12">
						<?=$fields['section_clienta']?>
						<div class="slider2 swiper-container">
							<div class="swiper-wrapper">

								<?php

								if($fields['slider_clients']){

									foreach ($fields['slider_clients'] as $field) {
										?>
										<div class="swiper-slide">
												<a href="<?= $field['link_do_strony_partnera']?>" target="_blank"><img src="<?= $field['zdjęcie']['sizes']['client-logo']?>" alt=""></a>
										</div>
										<?php
									}
								}
								?>


							</div>
							<div class="swiper-pagination"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php require ("stay.php"); ?>

</div>


<?php

get_footer();

?>

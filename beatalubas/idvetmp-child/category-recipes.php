<?php

get_header();
the_post();
global $query_string;
query_posts("{$query_string}&posts_per_page=9");
$fields = get_fields(get_the_ID());
?>

    <div class="main-wrapper recipes mt200">
    <div class="container">
    <div class="row">
        <div class="first-section">
            <div class="col-md-6">
                <h2><?php single_cat_title()?></h2>
            </div>
            <div class="col-md-6">
                <?php
                $term = get_queried_object();

                $image = get_field('kk', $term);
                echo $image;
                ?>
            </div>
        </div>
    </div>

    <div class="choose-the-time-of-year">
        <?php


        wp_nav_menu(array(
                'menu' => 'seasons',
                'theme_location' => 'seasons',
                'depth' => 3,
                'container' => false,
                'container_class' => '',
                'menu_class' => ''
            )
        );

        ?>
    </div>

    <?php
    $categories = get_the_category();
    $category_id = $categories[0]->cat_ID;

    ?>
    <?php

//    $args = array(
//        'post_type'      => 'post',
//        'posts_per_page' => 9,
//        'order'          => 'DESC',
////        'cat' => $category_id
//    );


//    $parent = new WP_Query( $args );
    ?>

    <?php

    if (have_posts()) :
        $i = 0;
        $j = 0;
        while (have_posts()) : the_post();

        if ($i == 0) {
            echo '<div class="latest-posts">';
            echo '<div class="row wrapping">';
        }
        ?>



        <div class="col-md-4">
            <div class="latest-posts__single-photo photo-height">
                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('blog_image_size') ?></a>
                <div class="latest-posts__info">
                    <span class="data"><?php the_time('d F Y'); ?></span>
                    <a href="<?php the_permalink(); ?>" class=""><?php the_title(); ?></a>
                </div>
            </div>
        </div>

        <?php
        $i++;
        if ($i == 3) {


            echo '</div>';
            echo '</div>';


            $i = 0;
        }
        ?>



        <?php
    endwhile; ?>
        <div class="container">
            <div class="row text-right">
                <div class="col-md-12">
                    <?php the_posts_pagination( array(
                        'mid_size' => 2,
                        'prev_text' => __( '&laquo', 'textdomain' ),
                        'next_text' => __( '&raquo', 'textdomain' ),
                    ) ); ?>
                </div>
            </div>
        </div>
    <?php endif; wp_reset_query(); ?>


    <?php require ("slider.php"); ?>
<!--
    <div class="latest-posts">
        <div class="latest-posts__title">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/pedzel_small.png" alt="">
            <h2>Latest Posts</h2>
            <p>What I’ve been up to recently:</p>
        </div>

        <div class="container">
            <div class="row wrapping">
                <?php

                $args = array(
                    'post_type'      => 'post',
                    'posts_per_page' => 3,
                    'order'          => 'DESC'

                );


                $parent = new WP_Query( $args );
                ?>

                <?php
                if ( $parent->have_posts() ) : ?>

                    <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
                        <div class="col-md-4">
                            <div class="latest-posts__single-photo">
                                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('blog_image_size') ?></a>
                                <div class="latest-posts__info">
                                    <span class="data"><?php the_time('d F Y'); ?></span>
                                    <a href="<?php the_permalink(); ?>" class=""><?php the_title(); ?></a>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>

                <?php endif; wp_reset_query(); ?>

            </div>
        </div>
    </div>

-->

<?php

get_footer();

?>
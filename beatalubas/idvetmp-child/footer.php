<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package idvetmp
 */

?>

	</div><!-- #content -->

	<footer class="site-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center footer-text">
                        All rights reserved &copy <?=the_date('Y');?> Bea's cookbook

                    </div>
                </div>         
                <div class="row">
                    <div class="col-md-12 text-center footer-autor">
                        <a href="https://ideative.rocks/">
                            <img src="<?=get_template_directory_uri().'/img/by-ideative.svg'?>" alt="ideative.rocks">
                        </a>
                    </div>
                </div>    
            </div>
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/up.png" alt="" class="up">
	</footer><!-- #colophon -->
	
</div><!-- #page -->

<?php wp_footer(); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
</body>
</html>

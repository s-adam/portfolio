<div class="contact-content">
	<div class="stay-in-touch">
		<h4><?php the_field('title_stay', 'option') ?></h4>
		<ul>

			<?php
			$variable = get_field('opcje', 'option');
			if($variable){

				foreach ($variable as $field) {
					?>
					<?php
					if (strpos($field['link_do_konta_społecznościowego'], '@') == true){
						?>
						<li><a href="mailto:<?=$field['link_do_konta_społecznościowego']; ?>"><img src="<?= $field['ikona']['sizes']['FB_TW'] ?>" alt=""></a> </li>
						<?php
					}else{
						?>
						<li><a href="<?=$field['link_do_konta_społecznościowego']; ?>" target="_blank"><img src="<?= $field['ikona']['sizes']['FB_TW'] ?>" alt=""></a> </li>
						<?php
					}
					?>

					<?php
				}}
			?>
		</ul>
	</div>
</div>
<div class="latest-posts">
	<div class="latest-posts__title" style="margin-bottom: 60px;">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/pedzel_small.png" alt="">
		<h2 style="margin-top: 25px;margin-bottom: 5px;">Related posts</h2>
	</div>

	<div class="container">
		<div class="row wrapping">
			<?php
			$categories = get_the_category();
			$category_id = $categories[0]->cat_ID;

			$args = array(
				'post_type'      => 'post',
				'posts_per_page' => 3,
				'order'          => 'DESC',
				'cat' => 5

			);


			$parent = new WP_Query( $args );
			?>

			<?php
			if ( $parent->have_posts() ) : ?>

				<?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
					<div class="col-md-4">
						<div class="latest-posts__single-photo">
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('blog_image_size') ?></a>
							<div class="latest-posts__info">
								<span class="data"><?php the_time('d F Y'); ?></span>
								<a href="<?php the_permalink(); ?>" class=""><?php the_title(); ?></a>
							</div>
						</div>
					</div>
				<?php endwhile; ?>

			<?php endif; wp_reset_query(); ?>

		</div>
	</div>
</div>
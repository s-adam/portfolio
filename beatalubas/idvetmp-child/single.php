<?php

get_header();
the_post();

$fields = get_fields(get_the_ID());
?>

	<div class="main-single single-content-portfolio">
        <div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="photo-thumbnail">
					
					
					<?php if(($fields['zdjęcie_w_poziomie']['sizes']['blog_image_sizet']) != null) { ?>
						
						<img src="<?=$fields['zdjęcie_w_poziomie']['sizes']['blog_image_sizet']?>">

					<?php } else{
						the_post_thumbnail('blog_image_size'); 
					} ?>

					</div>
					<span class="date">
						
						<?php
									$fieldsPost = get_fields(get_the_ID());
                                    $dateFrom = strtotime($fieldsPost['data_rozpoczęcia']);
                                    $dateTo = strtotime($fieldsPost['data_zakończenia']);
                                    ?>
                                    <?php
                                    if($dateFrom != $dateTo){
                                        if(date_i18n('m', $dateFrom) == date_i18n('m', $dateTo)){
                                            echo '<span class="data">'.  date_i18n('j', $dateFrom).'-'.date_i18n( 'j M Y', $dateTo ).'</span>';
                                        }else{
                                            echo '<span class="data">'.  date_i18n('j M Y', $dateFrom).'-'.date_i18n( 'j M Y', $dateTo ).'</span>';
                                        }
                                    }else{
                                        echo '<span class="data">'.date_i18n( 'j M Y', $dateFrom ).'</span>';
                                    }
                                    ?>
					</span>
					<h2><?php the_title(); ?></h2>

					<?php the_content(); ?>
				</div>
			</div>
		</div>


	</div>
	<div class="contact-content">
		<?php require ("stay.php"); ?>
	</div>
<?php

get_footer();

?>
<?php

function idvetmpchild_scripts() {
    //wp_enqueue_style( 'font-opensans', 'https://fonts.googleapis.com/css?family=Montserrat:400,600,700&amp;subset=latin-ext" rel="stylesheet', array( 'idvetmp-style' ));
    wp_enqueue_style( 'swiper', get_template_directory_uri() . '/library/swiper/swiper.min.css', array( 'idvetmp-style' ));
    wp_enqueue_style( 'idvetmp-style', get_stylesheet_directory_uri() .'/main.css');
}
add_action( 'wp_enqueue_scripts', 'idvetmpchild_scripts' );

function idvetmp_scriptschild() {
    wp_enqueue_script( 'swiper-js', get_template_directory_uri() . '/library/swiper/swiper.min.js', array( 'jquery' ), '20151215', true );
    wp_enqueue_script( 'main-js', get_stylesheet_directory_uri() . '/js/main.js', array( 'jquery','bootstrap-js' ), '20151215', true );
    //wp_enqueue_script( 'lightbox', 'https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js', array( 'lightbox','lightbox-js' ), '20151215', true );
}
add_action( 'wp_enqueue_scripts', 'idvetmp_scriptschild' );

add_filter('jpeg_quality', function($arg){return 100;});

//update_option( 'siteurl', 'http://beatalubas.com' );
//update_option( 'home', 'http://beatalubas.com' );

add_image_size('slider_home', 1920, 922, false );
add_image_size('blog_image_sizet', 740, 1010, true );
add_image_size('blog_image_size', 740, 1010, false );
add_image_size('FB_TW', 66, 70, false );
add_image_size('category_img', 734);
add_image_size('about_me_img', 740, 786, false );
add_image_size('header_photo-post', 1920, 670, false );

set_post_thumbnail_size( 700, 700, false );

add_image_size('bea-photo-image', 428, 580, true );
add_image_size('client-logo', 310, 110, false );



add_action('admin_head', 'add_my_tc_button');

function add_my_tc_button() {
    global $typenow;

    if ( !current_user_can('edit_posts') && !current_user_can('edit_pages') ) {
        return;
    }


    if ( get_user_option('rich_editing') == 'true') {
        add_filter("mce_external_plugins", "add_tinymce_plugin");
        add_filter('mce_buttons', 'register_my_tc_button');
    }
}

function add_tinymce_plugin($plugin_array) {
    $plugin_array['gavickpro_tc_button'] =  '/wp-content/themes/idvetmp-child/js/tinymce-buttons.js';
    return $plugin_array;
}

function register_my_tc_button($buttons) {
    array_push($buttons, "gavickpro_tc_button");
    return $buttons;
}



/* obsługa wyboru kategorii */

function category_change_scripts() {
 
	global $wp_query; 
 
	wp_register_script( 'my_loadmore', get_stylesheet_directory_uri() . '/js/myloadmore.js', array('jquery') );
               
	// now the most interesting part
	// we have to pass parameters to myloadmore.js script but we can get the parameters values only in PHP
	// you can define variables directly in your HTML but I decided that the most proper way is wp_localize_script()
	wp_localize_script( 'my_loadmore', 'categorychange_params', array(
		'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php', // WordPress AJAX
		'posts' => serialize( $wp_query->query_vars ), // everything about your loop is here
		'current_page' => get_query_var( 'paged' ) ? get_query_var('paged') : 1,
		'max_page' => $wp_query->max_num_pages
	) );
 
 	//wp_enqueue_script( 'my_loadmore' );
}
 
add_action( 'wp_enqueue_scripts', 'category_change_scripts' );


function category_change_handler(){
 
	// prepare our arguments for the query
	//$args = unserialize( stripslashes( $_POST['query'] ) );
	$args['paged'] = 1; 
	$args['post_status'] = 'publish';
        $args['cat'] = $_POST['cat'];
    
        //$args['paged'] = 2;
    
	// it is always better to use WP_Query but not here
	query_posts($args);
 
        $i = 0;
        $j = 0;
        if (have_posts()) :
            while (have_posts()) : the_post();
                if($i == 0){
                    echo '<div class="row blog__list-item">';
                }            
                ?>
                <div class="col-md-4 <?=$j>2?'hide':''?>">
                    <?php
                    $btnText = 'Czytać mi się chce';
                        include(locate_template('template-parts/news-single-box.php'));
                    ?>
                </div>    
                <?php

                $i++;
                $j++;
                if ($i == 3) { 
                    echo '</div>';
                    $i = 0;
                }      


            endwhile;
            //wp_reset_postdata();
        endif;
	die; // here we exit the script and even no wp_reset_query() required!
}


add_action('wp_ajax_categorychange', 'category_change_handler'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_categorychange', 'category_change_handler'); // wp_ajax_nopriv_{action}

/* obsługa wyboru kategorii END */


function printthispagebutton_script() {
    wp_register_script('printthispagebutton',get_stylesheet_directory_uri().'/js/print.js',array(),'1.0.0',true);
}
add_action('wp_enqueue_scripts','printthispagebutton_script');


//a print button shortcode
function add_a_simple_print_button_sc($atts, $content = null) {
    wp_enqueue_script('printthispagebutton');
    extract(shortcode_atts(array(
        'buttontext' => 'Print a nice copy of this page for reference',
    ), $atts));
    ob_start(); ?>
    <div class="printthispagebutton">
    <p><img src="http://projekt.dev/wp-content/themes/idvetmp-child/img/drukarka.png" width="32px" height="32px" class="printme"/>
<!--        <?php //echo $buttontext; ?><!--</p>
    </div><?php
    $printmebuttonoutput = ob_get_clean();
    return $printmebuttonoutput;
}
add_shortcode('print-me-button', 'add_a_simple_print_button_sc');




add_filter( 'get_comment_date', 'wpse_comment_date_18350375' );
function wpse_comment_date_18350375( $date ) {
    $date = date("m/d/Y");
    return $date;
}


add_filter( 'single_template', 'my_single_template' );
function my_single_template($single_template)
{
if (in_category(286)) {
    $file = get_stylesheet_directory().'/single-cat-journal.php';
    if ( file_exists($file) ) {
        return $file;
    }
}
if (in_category(285)) {
    $file = get_stylesheet_directory().'/single-cat-workshops-and-events.php';
    if ( file_exists($file) ) {
        return $file;
    }
}
if (in_category(5)) {
    $file = get_stylesheet_directory().'/single-cat-recipes.php';
    if ( file_exists($file) ) {
        return $file;
    }
}
return $single_template;
}

register_nav_menu('seasons',__( 'seasons' ));

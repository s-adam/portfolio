<div class="career__cooperation">
	<h2>Chcesz z nami pracować?</h2>
	<p>Wyślij swoje CV na adres <a href="mailto:<?php the_field('kariera_email', 'option'); ?>" target="_blank"><?php the_field('kariera_email', 'option'); ?></a></p>
</div>
<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*
Template Name: Produkt - AGRO
Template Post Type: agro
*/
get_header('');
the_post();

$fields = get_fields(get_the_ID());
$publishable_lite_single_breadcrumb_section = get_theme_mod('publishable_lite_single_breadcrumb_section', '1');
?>

    <div class="section-oblique" style="background: #a2aab0;padding-top: 250px">
        <img class="skos" src="<?php echo get_stylesheet_directory_uri(); ?>/img/skos.svg" alt="">
    </div>
    <div class="main-wrapper margin-simulate">
        <div class="container">
            <?php
            $parent = get_post(wp_get_post_parent_id( get_the_ID() ));
            $rootParent = get_post(wp_get_post_parent_id( $parent->ID ));

            ?>
            <div class="breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#">
						<span typeof="v:Breadcrumb" class="root">
							<a rel="v:url" property="v:title" href="/">Strona Główna</a>
						</span>
                <span><i class="publishable-icon icon-angle-double-right"></i></span>
                <span typeof="v:Breadcrumb" class="root">
							<a rel="v:url" property="v:title" href="<?=get_permalink($rootParent->ID)?>"><?=$rootParent->post_title?></a>
						</span>
                <span><i class="publishable-icon icon-angle-double-right"></i></span>

                <span typeof="v:Breadcrumb" class="root">
							<a rel="v:url" property="v:title" href="<?=get_permalink($parent->ID)?>"><?=$parent->post_title?></a>
						</span>
                <span><i class="publishable-icon icon-angle-double-right"></i></span>


                <span><span><?php the_title(); ?></span></span>
            </div>

            <div class="row">
                <div class="produkt">
                    <div class="col-md-5">
                        <div class="produkt__photo mb">
                            <?php the_post_thumbnail('lista-produktow'); ?>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="produkt__description">
                            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>



                            <div class="producent"><p><?php echo get_the_title( $post->post_parent ); ?></p></div>


                            <div class="row">
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p>Przeznaczenie</p>
                                        </div>

                                        <div class="col-md-6">
                                            <p><strong><?= $fields['przeznaczenie']; ?></strong></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="padding-bottom: 25px;">
                                            <p>Opakowanie</p>
                                        </div>
                                        <div class="col-md-6">
                                            <p><strong><?= $fields['waga_produktu']; ?></strong></p>
                                        </div>
                                    </div>
                                    <ul id="piktogramy">
                                        <?php

                                        if($fields['piktogramy']){
                                            foreach ($fields['piktogramy'] as $field) {

                                                ?>
                                                <li><img src="<?=$field['piktogram']['sizes']['large']?>"></li>
                                                <?php
                                            }}
                                        ?>
                                    </ul>
                                </div>
                                <div class="col-md-4 produkt__buttons">
                                    <a href="<?= $fields['link_do_zakupu']; ?>" target="_blank" class="btn btn-kup">Kup teraz</a>
                                    <a href="/kontakt" class="btn btn-zapytaj">Zapytaj</a>

                                </div>
                            </div>
                        </div>
                    </div>


                </div>

            </div>
            <div class="tab-container">


                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Opis</a></li>
                    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Skład</a></li>
                    <!--                            --><?php
                    //                            $table = get_field( 'tabela' );
                    //    //						var_dump($fields['wyświetl_dawkowanie_jako_tabela']);
                    //                            if ( $fields['dawkowanie_jako_tekst'] != null || $fields['wyświetl_dawkowanie_jako_tabela']) {
                    //                            ?>
                    <?php
                    $table = get_field( 'tabela' );
                    if ( $table['body'] != null) {
                        ?>
                        <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Analityka</a></li>
                        <?php

                    }

                    ?>
                    <?php
                    $table1 = get_field( 'dawkowanie_-_tabela' );
                    if ( $fields['dawkowanie_jako_tekst'] != null || $table1['body'] != null) {
                        ?>
                        <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Dawkowanie</a></li>
                        <?php

                    }

                    ?>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <?php
                    if($fields['informacje_o_produkcie']){
                        foreach ($fields['informacje_o_produkcie'] as $field) {

                            ?>
                            <?php

                            if($field['zawartość_zakładki']){
                                foreach ($field['zawartość_zakładki'] as $f2) {
                                    ?>
                                    <div role="tabpanel" class="tab-pane active" id="home"><?= $f2['tekst'] ?></div>
                                    <div role="tabpanel" class="tab-pane" id="profile"><?= $f2['tekst1'] ?></div>
                                    <div role="tabpanel" class="tab-pane" id="messages">
                                        <?php
                                        $table = get_field( 'tabela' );

                                        if ( $table ) {

                                            echo '<table border="0">';

                                            if ( $table['header'] ) {

                                                echo '<thead>';

                                                echo '<tr>';

                                                foreach ( $table['header'] as $th ) {

                                                    echo '<th>';
                                                    echo $th['c'];
                                                    echo '</th>';
                                                }

                                                echo '</tr>';

                                                echo '</thead>';
                                            }

                                            echo '<tbody>';

                                            foreach ( $table['body'] as $tr ) {

                                                echo '<tr>';

                                                foreach ( $tr as $td ) {

                                                    echo '<td>';
                                                    echo $td['c'];
                                                    echo '</td>';
                                                }

                                                echo '</tr>';
                                            }

                                            echo '</tbody>';

                                            echo '</table>';
                                        }
                                        ?>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="settings">
                                        <?php
                                        if( $fields['dawkowanie_jako_tekst'] == null){
                                            $table = get_field( 'dawkowanie_-_tabela' );

                                            if ( $table ) {

                                                echo '<table border="0">';

                                                if ( $table['header'] ) {

                                                    echo '<thead>';

                                                    echo '<tr>';

                                                    foreach ( $table['header'] as $th ) {

                                                        echo '<th>';
                                                        echo $th['c'];
                                                        echo '</th>';
                                                    }

                                                    echo '</tr>';

                                                    echo '</thead>';
                                                }

                                                echo '<tbody>';

                                                foreach ( $table['body'] as $tr ) {

                                                    echo '<tr>';

                                                    foreach ( $tr as $td ) {

                                                        echo '<td>';
                                                        echo $td['c'];
                                                        echo '</td>';
                                                    }

                                                    echo '</tr>';
                                                }

                                                echo '</tbody>';

                                                echo '</table>';
                                            }

                                        }
                                        echo $fields['dawkowanie_jako_tekst'];

                                        ?>
                                    </div>
                                    <?php

                                }}
                            ?>

                            <?php
                        }}
                    ?>
                </div>

            </div>
        </div>
    </div>
<?php
$args = array(
    'post_type'      => 'agro',
    'post__not_in' => array(get_the_ID()),
    'posts_per_page' => -1,
    'post_parent'    =>  wp_get_post_parent_id( get_the_ID() ),
    'order'          => 'ASC',
    'orderby'        => 'menu_order'
);


$parent = new WP_Query( $args );

//var_dump($parent);
?>
<?php
if ( $parent->have_posts() ) : ?>
    <div class="similar-products">
        <div class="container">
            <div class="wrap-opinions">
                <div class="title-contact">
                    <h3>Podobne z tej serii</h3>
                </div>

                <div class="similar-products-carousel swiper-container swiper-container-wraps">
                    <div class="row text-center swiper-wrapper">

                            <?php

                            while ( $parent->have_posts() ) : $parent->the_post();?>
                                <div class="swiper-slide">
                                    <div class="similar-products__photo">
                                        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('slide-produktow'); ?></a>
                                    </div>
                                    <div class="similar-products__name">
                                        <h3><a href="<?php the_permalink(); ?>"><?php echo the_title();?></a></h3>
                                    </div>
                                    <div class="similar-products__producent">
                                        <a href="<?php the_permalink($post->post_parent)?>"><?php echo get_the_title( $post->post_parent );?></a>

                                    </div>
                                </div>
                                <?php
                            endwhile;
                        ?>

                    </div>

                </div>
                <div class="container">
                    <div class="swiper-button-prev">
<!--                        <img src="--><?php //echo get_stylesheet_directory_uri(); ?><!--/img/minislider-prev.svg" alt="">-->
                    </div>
                    <div class="swiper-button-next">
<!--                        <img src="--><?php //echo get_stylesheet_directory_uri(); ?><!--/img/minislider-next.svg" alt="">-->
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
endif;
?>
<?php wp_reset_query(); ?>

<?php
if($fields['relacja']!= null) {

    get_template_part('doradcy');
}
?>

<?php
get_footer();

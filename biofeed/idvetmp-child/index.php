<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*
Template Name: Strona Główna
*/
get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>


	<div class="main-wrapper" id="main-wrapper" style="overflow: hidden">
			<div class="slider swiper-container">
				<div class="swiper-wrapper text-center">

					<?php

					if($fields['slider']){

						foreach ($fields['slider'] as $field) {

							?>
										<div class="swiper-slide">
											<div class=" slider__item" style="background: url(<?= $field['zdjęcie']['sizes']['slider_home']?>) center no-repeat;background-size: cover;">
												<div class="slider__text text-center">
													<?= $field['tekst']?>
												</div>
											</div>
										</div>
							<?php

						}
					}
					?>


				</div>
				<ul class="slider__list">
					<li><a href="<?php the_field('facebook_-_biofeed', 'option'); ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i> Biofeed</a></li>
					<li><a href="<?php the_field('facebook_-_biofeed_zoo', 'option'); ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i> Biofeed Zoo</a></li>
					<li><a href="<?php the_field('youtube', 'option'); ?>" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i> Biofeed</a></li>
					<li><a href="<?php the_field('e-sklep', 'option'); ?>" target="_blank"><i class="fa fa-shopping-cart" aria-hidden="true"></i> E-Sklep</a></li>
				</ul>
                <div class="swiper-pagination"></div>
			</div>
	<?php

	if($fields['sekcja_ceo']){

	foreach ($fields['sekcja_ceo'] as $field) {

	?>
		<div class="ceo-wrapper" style="background-image: url(<?= $field['zdjęcie-tło']['sizes']['prezes']?>)">
			<div class="container">
				<div class="row col-md-12">


							<div class="ceo">

								<div class="ceo__text">
									<?= $field ['tekst'] ?>
								</div>
								<div class="ceo__signature">
									<?= $field ['podpis'] ?>
								</div>
								<img class="ceo-img" src="<?= $field['zdjęcie-tło']['sizes']['prezes']?>" alt="">
							</div>

				</div>
			</div>
		</div>
		<?php

	}
	}
	?>
		<div class="skills">
			<div class="container">
			<?php

			if($fields['umiejętnosci']){
				$i = 0;
				$j = 0;
				foreach ($fields['umiejętnosci'] as $field) {
					if (!empty($field['ikona'])){
						if ($i == 0) {
							echo '<div class="row">';
						}
						?>
					<div class="col-md-3">
						<div class="simple-wrapper text-center">
<!--							<img src="--><?php //echo get_stylesheet_directory_uri(); ?><!--/img/ico4.svg" type="image/svg+xml" alt="">-->
							<?=wp_get_attachment_image( $field['ikona'],'large');?>
							<?= $field ['opis'] ?>
						</div>
					</div>
						<?php
						$i++;
						$j++;
						if ($i == 4 || count($fields['umiejętnosci']) == $j) {


							echo '</div>';

							$i = 0;
						}
					}
				}}

			?>

		</div>

	</div>

<div class="our-products__bg" id="oferta">
	<div class="container">
		<div class="our-products">
			<div class="our-products__title main-title">
				<h6>NASZE PRODUKTY</h6>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/small-icons.png" alt="">
			</div>

			<?php
			if($fields['sekcja']){
				$i = 0;
				$j = 0;
				foreach ($fields['sekcja'] as $field) {
					//$fields_product = get_fields($field['sekcja_produkty']->ID);
                                        
					if ($i == 0) {

						echo '<div class="row">';

					}

					?>
							<div class="col-md-3">

								<div class="our-products__category-name"
									 style="background: url(<?= $field['zdjęcie_linii_produktowej']['sizes']['large'] ?>) right bottom no-repeat">

									<div class="our-products__wrapper">
										<div class="our-products__hover">
											<h6><a href="<?php the_permalink($field['sekcja_produkty']->ID); ?>"><?php echo $field['sekcja_produkty']->post_title; ?></a></h6>
											<div class="tt">
												<p><?= $field['opis_linii_produktowej'] ?></p>
												<a class="btn" href="<?php the_permalink($field['sekcja_produkty']->ID); ?>">Zobacz</a>
											</div>
										</div>
									</div>
								</div>




							</div>
					<?php
					$i++;
					$j++;
					if ($i == 4 || count($field['sekcja']) == $j) {

						echo '</div>';

						$i = 0;
					}
				}
			}

			?>
		</div>
	</div>

	</div>

	<div class="news">
		<div class="news__title main-title" id="aktualnosci">
			<h6>Aktualności</h6>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/small-icons.png" alt="">
		</div>
		<div class="container">
			<div class="row">


				<?php
                $args = array(
                    'posts_per_page' => 2,
                    'post_type'      => 'aktualnosci',
                    'paged'          => get_query_var( 'paged' ),
                );
                $wp_query = new WP_Query( $args );
				?>
				<ul>
					<?php
					if ( $wp_query->have_posts() ) : ?>
						<?php
                        while ( $wp_query->have_posts() ) : $wp_query->the_post();

                            $fields = get_fields(get_the_ID());?>

				<div class="col-md-2 col-sm-12">
					<div class="news__photo mar-left">
						<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('medium'); ?></a>
					</div>
				</div>
				<div class="col-md-4 col-sm-12">
					<div class="news__wrapper">
						<div class="news__archive_title">
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						</div>
						<span class="news__archive_date">
						<?php the_time('d F Y'); ?>
					</span>
						<div class="news__archive_text">
							<?php
							the_excerpt_max_charlength(100);
							?>
							<a href="<?php the_permalink(); ?>" class="btn">Zobacz więcej</a>
						</div>
					</div>
				</div>


				<?php
				endwhile;
				endif;

				?>
				<?php wp_reset_query(); ?>
				<div class="col-md-12 text-center news__btn_read_all">
					<a href="/aktualnosci" class="btn text-center">Zobacz wszystkie</a>
				</div>
			</div>
		</div>
	</div>
	<div class="opinions" id="opinie">
		<div class="opinions__title main-title">
			<h6>OPINIE NASZYCH KLIENTÓW</h6>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/small-icons.png" alt="">
		</div>
		<div class="container swiper-container opinie-first">
			<!-- Additional required wrapper -->
			<div class="swiper-wrapper">
				<!-- Slides -->
				<?php $fields = get_fields(get_the_ID()); ?>
				<?php

				if($fields['opinie']){
					foreach ($fields['opinie'] as $field) {

						?>


						<div class="swiper-slide">
							<div class="row">
								<div class="col-md-5">
									<div class="sl-wr text-center">
										<div class="opinions__name">
											<?= $field['pospis']; ?>
										</div>
										<div class="opinions__city">
											<?= $field['miasto']; ?>
										</div>
									</div>
								</div>
								<div class="col-md-7">
									<div class="opinions__text">
										<?= $field['treść']; ?>

									</div>
								</div>
							</div>
						</div>

						<?php
					}}
				?>
			</div>
			<!-- If we need navigation buttons -->
			<div class="swiper-button-prev x2"></div>
			<div class="swiper-button-next x1"></div>
		</div>
	</div>

	<div class="contact">
		<div class="contact__wrapper">
			<div class="contact__title main-title">

									<?= $fields['sekcja_kontakt']; ?>
			</div>
			<div class="container">

			</div>
		</div>
	</div>
	<div class="partners">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="partners__content">
						<h4>PARTNERZY</h4>
						<ul>
<?php

if($fields['partnerzy']){
	foreach ($fields['partnerzy'] as $field) {
		?>
		<li><a href="<?= $field['adres_strony_www']?>" target="_blank"><img src="<?= $field['logotyp']['sizes']['logo_partners']?>" alt=""></a></li>
<?php }}?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script>
		$(".go-to-oferty").click(function() {
			$('html, body').animate({
				scrollTop: $("#oferta").offset().top-130
			}, 1500);
		});

		$(".go-to-archive").click(function() {
			$('html, body').animate({
				scrollTop: $("#aktualnosci").offset().top-130
			}, 1500);
		});

		$(".go-to-opinie").click(function() {
			$('html, body').animate({
				scrollTop: $("#opinie").offset().top-130
			}, 1500);
		});
	</script>


<?php
get_footer();

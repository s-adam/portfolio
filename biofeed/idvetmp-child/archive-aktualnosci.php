<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */

get_header();
the_post();
?>

	<div class="section-oblique-archive-2">
		<h2>Aktualności</h2>
	</div>


	<div class="archive-main-container">
		<div class="container">
<?php //juz

$args = array(
	'posts_per_page' => 5,
	'post_type'      => 'aktualnosci',
	'paged'          => get_query_var( 'paged' ),
);
$wp_query = new WP_Query( $args );
while ( $wp_query->have_posts() ) : $wp_query->the_post();

	$fields = get_fields(get_the_ID());
	?>

					<div class="row">
						<div class="col-md-5">
							<div class="news-post__photo">
								<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('aktualnosci-photo'); ?></a>
							</div>
						</div>
						<div class="col-md-7">
							<div class="news-post">
								<div class="news-post__archive_title">
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								</div>
								<span class="news-post__archive_date"><?php the_time('d F Y'); ?></span>
								<div class="news-post__archive_text">
									<?php
									the_excerpt_max_charlength(300);
									?>
								</div>
								<a href="<?php the_permalink(); ?>" class="btn single-btn-arch">Czytaj więcej</a>
							</div>
						</div>

					</div>
					<img class="signs-after-post" src="<?php echo get_stylesheet_directory_uri(); ?>/img/small-icons.png" alt="">







				<?php
			endwhile;

			?>
			<?php the_posts_pagination( array(
				'mid_size' => 3,
				'prev_text' => __( '&laquo', 'textdomain' ),
				'next_text' => __( '&raquo', 'textdomain' ),
			) ); ?>


		</div>
	</div>

<?php
get_footer();

<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*
Template Name: Linia Produktów Pozostałe
Template Post Type: zoo
*/
get_header('');
the_post();
$publishable_lite_single_breadcrumb_section = get_theme_mod('publishable_lite_single_breadcrumb_section', '1');
$fields = get_fields(get_the_ID());
?>

    <div class="section-oblique" style="background: #a2aab0;padding-top: 250px">

    </div>
    <div class="main-wrapper margin-simulate">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="swiper-container swiper-container-mp">
                        <?php
                        $parent = get_post(wp_get_post_parent_id( get_the_ID() ));
                        $rootParent = get_post(wp_get_post_parent_id( $parent->ID ));

                        ?>
                        <div class="breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#">
						<span typeof="v:Breadcrumb" class="root">
							<a rel="v:url" property="v:title" href="/">Strona Główna</a>
						</span>

                            <span><i class="publishable-icon icon-angle-double-right"></i></span>


                            <span><span><?php the_title(); ?></span></span>
                        </div>
                        <div class="swiper-pagination"></div>
                        <div class="swiper-wrapper">
                            <?php

                            if($fields['slider']){
                                foreach ($fields['slider'] as $field) {

                                    ?>
                                    <div class="one-slide swiper-slide"
                                         style="background: url('<?= $field['zdjęcie_w_tle']['sizes']['sliderPhoto']; ?>') center center;background-size: cover;">
                                    </div>
                                    <?php
                                }}
                            ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
$args = array(
    'post_type'      => 'pozostale',
    'posts_per_page' => 5,
    'post_parent'    => get_the_ID(),
    'order'          => 'ASC',
    'orderby'        => 'menu_order'
);


$parent = new WP_Query( $args );


?>
    <div class="similar-products">
        <div class="container">
            <div class="wrap-opinions">
                <div class="title-contact">
                    <h3>LINIE PRODUKTOWE</h3>
                </div>

                <div class="similar-products-carousel swiper-container">
                    <div class="row text-center swiper-wrapper">



                        <?php
                        if ( $parent->have_posts() ) : ?>

                            <?php

                            while ( $parent->have_posts() ) : $parent->the_post();?>
                                <div class="swiper-slide">
                                    <div class="similar-products__photo">
                                        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('slide-produktow'); ?></a>
                                    </div>
                                    <div class="similar-products__name">
                                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                    </div>

                                </div>

                                <?php

                            endwhile;
                        ?>
                    </div>




                </div>
                <div class="container">
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
            </div>
        </div>
    </div>
    <?php
endif;
?>
<?php wp_reset_query(); ?>
<?php
get_template_part('doradcy');
?>
<?php
get_footer();

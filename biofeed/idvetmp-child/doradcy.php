<?php
the_post();

require_once('class.phpmailer.php');
require_once('class.pop3.php');
require_once('class.phpmaileroauth.php');
require_once('class.smtp.php');


$fields = get_fields(get_the_ID());


$fields_doradcy_form = get_fields(206);
//
//var_dump($fields_doradcy_form['adres_email']);

if (!empty($_POST["name"])) {


	$fields_doradcy_form = get_fields($_POST['person']);


	if (empty($_POST["name"])) {
		$name_field = "Pole wymagane!";
	}
	if (empty($_POST["message"])) {
		$message_field = "Pole wymagane!";
	}

	$email = new PHPMailer();
	$email->IsHTML(true);
	$email->SMTPDebug = 3;

	$email->From = 'kontakt@biofeed.ideative.pl';
	$email->FromName = 'Biofeed';
	$email->CharSet = 'UTF-8';


	$email->Subject = 'Nowe zgłoszenie';
//	$body_content = 'test';
	$body_content = 'Email lub telefon:<br>'.$_POST["name"] .'<br> Wiadomość: <br><br> '. $_POST["message"];
	$email->Body = $body_content;
	$email->AddAddress($fields_doradcy_form['adres_email']);

	if($email->Send()){
		$_POST["name"] = null;
		$_POST["message"] = null;

		$_POST = array();
	}
	$success = '<h4 class="alert alert-success">'.'Wiadomość wysłana poprawnie!'.'</h4>';
}


?>

<div class="contact-info">
	<div class="container">
		<div class="contact-info__wrapper-outer" id="info-error-wrapper">
			<div class="row">
				<div class="col-md-6">
					<div class="title-contact">
						<h3>Skontaktuj się z doradcą</h3>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-6">
							<h3 class="select-state">Wybierz Województwo</h3>
						</div>
						<div class="col-md-6">
							<div class="select-box">
								<select id="select-doradcy" name="carlist" form="carform">
									<option data-value="Wybierz">Wybierz</option>
									<option data-value="Dolnośląskie">Dolnośląskie</option>
									<option data-value="Kujawsko-Pomorskie">Kujawsko-Pomorskie</option>
									<option data-value="Lubelskie">Lubelskie</option>
									<option data-value="Lubuskie">Lubuskie</option>
									<option data-value="Łódzkie">Łódzkie</option>
									<option data-value="Małopolskie">Małopolskie</option>
									<option data-value="Mazowieckie">Mazowieckie</option>
									<option data-value="Opolskie">Opolskie</option>
									<option data-value="Podkarpackie">Podkarpackie</option>
									<option data-value="Podlaskie">Podlaskie</option>
									<option data-value="Pomorskie">Pomorskie</option>
									<option data-value="Śląskie">Śląskie</option>
									<option data-value="Świętokrzyskie">Świętokrzyskie</option>
									<option data-value="Warmińsko-Mazurskie">Warmińsko-Mazurskie</option>
									<option data-value="Wielkopolskie">Wielkopolskie</option>
									<option data-value="Zachodniopomorskie">Zachodniopomorskie</option>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="info-error-wrapper">
				<?php echo $success ?>
			</div>
			<?php

			if($fields['relacja']){
				$a = 0;
				foreach ($fields['relacja'] as $field) {
					$fields_doradcy = get_fields($field->ID);
					?>
					<div class="row rw-wrapper">
						<div class="contact-info__wrapper" data-doradca="<?php echo $fields_doradcy['wojewodztwo']; ?>">
							<div class="col-md-2">
								<div class="contact-info__photo">
									<img src="<?=$fields_doradcy['zdjęcie_doradcy']['sizes']['large']?>" alt="">
								</div>
							</div>
							<div class="col-md-2">
								<div class="contact-info__name">
									<h4><?=$fields_doradcy['imię_i_nazwisko'] ?></h4>
									<?=$fields_doradcy['miasto']?>
								</div>
							</div>
							<div class="col-md-4">
								<div class="contact-info__phone">
									<a href="tel:<?=$fields_doradcy['numer_telefonu']?>"><i class="fa fa-phone" aria-hidden="true"></i><span>+48</span><?=$fields_doradcy['numer_telefonu']?></a>
							<?php
							if($fields_doradcy['adres_email'] != null) {
								?>
								<a href="mailto:<?=$fields_doradcy['adres_email']?>" id="mail"><i class="fa fa-envelope-open" aria-hidden="true"></i><span><?=$fields_doradcy['adres_email']?></span></a>
								<?php
							}
							?>
								</div>
							</div>
							<div class="col-md-4">
								<div class="contact-info__button">
									<?php
										if($fields_doradcy['adres_email'] != null) {
											?>
											<a class="btn" data-toggle="collapse" href="#multiCollapseExample<?= $a ?>"
											   aria-expanded="false" aria-controls="multiCollapseExample1">Napisz</a>
											<?php
										}
											?>
								</div>
							</div>
							<div class="collapse multi-collapse" id="multiCollapseExample<?=$a?>">
								<div class="card card-body">
									<form method="post" action="#info-error-wrapper">
										<div class="form-group ">
											<label class="control-label " for="name">
												Telefon lub Email
											</label>
											<p><input class="form-control" id="name" name="name" type="text" required /></p>

											<p><input type="hidden" value="<?=$field->ID?>" name="person" /></p>

										</div>

										<div class="form-group ">
											<label class="control-label " for="message">
												Wiadomość
												<!--												--><?//=$message_field?>
											</label>
											<p><textarea class="form-control" cols="40" id="message" name="message" required rows="10"></textarea></p>
										</div>

										<div class="form-group">
											<div>
												<p><button class="btn" name="submit" type="submit">
														Wyślij
													</button></p>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>

					</div>

					<?php
					$a++;
				}

			}
			?>
			<div class="row">
				<div class="col-md-12 doradcy-empty" style="display: none;">
					<h5 style="padding: 20px" class="alert alert-danger">Nie znaleziono doradców w tym regionie.</h5>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package idvetmp
 */

get_header();
the_post();
?>

	<div class="section-oblique" style="background: #a2aab0;padding-top: 250px">
		<div class="main-title archive-title">
			<h1><?php the_title(); ?></h1>
			<h6><?php the_time('d F Y'); ?></h6>
		</div>
		<img class="skos" src="<?php echo get_stylesheet_directory_uri(); ?>/img/skos.svg" alt="">
	</div>
	<div class="main-wrapper single-main-wrapper">

		<div class="container">
			<div class="post-archive-wrapper">
				<div class="row">
					<div class="col-md-12">
						<div class="post-archive-wrapper__photo">
							<?php the_post_thumbnail(); ?>
						</div>
						<div class="post-archive-wrapper__desc">
							<?php the_content()?>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

<?php
get_footer();

if ( $(".swiper-container-mp .swiper-slide").length > 1 ) {
    options = {
        autoplay: {
            delay: 3000,
        },
        navigation: {
            nextEl: '.s2',
            prevEl: '.s1'
        },
        loop: true,
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true
        },
        slidesPerView: 1,
        breakpoints: {
            700: {
                slidesPerView: 1,
                spaceBetween: 0
            }
        }
    }
} else {
    options = {
        loop: false,
        autoplay: false,
        pagination: false
    }
}
var swiper = new Swiper('.swiper-container-mp', options);


if ( $(".swiper-container-agro .swiper-slide").length > 1 ) {
    options = {
        autoplay: {
            delay: 3000,
        },
        navigation: {
            nextEl: '.s2',
            prevEl: '.s1'
        },
        loop: true,
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true
        },
        slidesPerView: 1,
        breakpoints: {
            700: {
                slidesPerView: 1,
                spaceBetween: 0
            }
        }
    }
} else {
    options = {
        loop: false,
        autoplay: false,
        pagination: false
    }
}

var swiper = new Swiper('.swiper-container-agro', options);





if ( $(".slider .swiper-slide").length > 1 ) {
    options = {
        autoplay: 5000,
        effect: "slide",
        keyboardControl: "true",
        speed: 600,
        loop: true,
        setWrapperSize: true,
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true
        },
        slidesPerView: 1,
        breakpoints: {
            700: {
                slidesPerView: 1,
                spaceBetween: 0
            }
        }
    }
} else {
    options = {
        loop: false,
        autoplay: false,
        pagination: false
    }
}


var swiper = new Swiper('.slider', options);



var mySwiper3 = new Swiper('.swiper-container-wrap', {
    autoplay: 5000,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
    },
    pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
        clickable: true
    },
    slidesPerView: 1,
    breakpoints: {
        700: {
            slidesPerView: 1,
            spaceBetween: 0
        }
    }
});

var mySwiper4 = new Swiper('.swiper-container-2', {
    autoplay: 5000,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
    },
    pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
        clickable: true
    },
    breakpoints: {
        700: {
            slidesPerView: 1,
            spaceBetween: 0
        }
    }
});




if ( $(".similar-products-carousel .swiper-slide").length > 5 ) {
    options = {
        autoplay: 5000,
        navigation: {
            nextEl: '.swiper-button-prev',
            prevEl: '.swiper-button-next'
        },
        slidesPerView: 5,
        breakpoints: {
            1200: {
                slidesPerView: 3
            },
            900: {
                slidesPerView: 3
            },
            700: {
                slidesPerView: 1
            }
        }
    }
} else {
    options = {
        slidesPerView: 5,
        navigation: {
            nextEl: '.swiper-button-prev',
            prevEl: '.swiper-button-next'
        },
        breakpoints: {
            1200: {
                slidesPerView: 3
            },
            900: {
                slidesPerView: 3
            },
            700: {
                slidesPerView: 1
            }
        }
    }
}
var swiper = new Swiper('.similar-products-carousel', options);




if ( $(".similar-products-pozostale .swiper-slide").length > 5 ) {
    options = {
        autoplay: 5000,
        navigation: {
            nextEl: '.swiper-button-prev',
            prevEl: '.swiper-button-next'
        },
        slidesPerView: 5,
        breakpoints: {
            1200: {
                slidesPerView: 3
            },
            900: {
                slidesPerView: 3
            },
            700: {
                slidesPerView: 1
            }
        }
    }
} else {
    options = {
        slidesPerView: 5,
        navigation: {
            nextEl: '.swiper-button-prev',
            prevEl: '.swiper-button-next'
        },
        breakpoints: {
            1200: {
                slidesPerView: 3
            },
            900: {
                slidesPerView: 3
            },
            700: {
                slidesPerView: 1
            }
        }
    }
}
var swiper = new Swiper('.similar-products-pozostale', options);




var mySwiper6 = new Swiper('.opinie-first', {
    autoplay: 5000,
    // loop: true,
    slidesPerView: 1,
    navigation: {
        nextEl: '.x1',
        prevEl: '.x2'
    },
    preventClicks: false,
    preventClicksPropagation: false
});


$('#tab-pane a').click(function (e) {
    e.preventDefault()
    $(this).tab('show')
})


//Filtrowanie
var doradcy = null;


function show_doradcy(){

    var doradca = $('#select-doradcy').val();

    var i = 0;

    $('.doradcy-empty').fadeOut(400);
    $( ".contact-info__wrapper" ).each(function( index ) {
        if( $(this).attr('data-doradca') == doradca ){
            $(this).fadeIn(400);

            i = i+1;

        }else{
            $(this).fadeOut(400);
        }


    });



    if(i == 0){
        $('.doradcy-empty').fadeIn(400);
    }


}

function sortDoradcy(){
    var doradca = $('#select-doradcy').val();

    j = 0;

    doradcyArray = [];


    $( doradca ).each(function( index, val ) {
        doradcyArray.push(parseInt(val))
    });


    $( ".contact-info__wrapper" ).each(function( index ) {
        showRow = true;


        if(doradcyArray > 0 && showRow){

            if(jQuery.inArray($(this).data('doradca'),doradcyArray) < 0){
                showRow = false;
            }
        }

    });


}




$(function() {
    $('#select-doradcy').change(function() {


        show_doradcy();
    });

    sortDoradcy();

});

$(function() {
    $('.link--mallki span').click(function() {
        $(".navbar-collapse").removeClass("in");

    });

});


var supports3DTransforms =  document.body.style['webkitPerspective'] !== undefined ||
    document.body.style['MozPerspective'] !== undefined;

function linkify( selector ) {
    if( supports3DTransforms ) {

        var nodes = document.querySelectorAll( selector );

        for( var i = 0, len = nodes.length; i < len; i++ ) {
            var node = nodes[i];

            if( !node.className || !node.className.match( /roll/g ) ) {
                node.className += 'link link--mallki';
                node.innerHTML = node.text+'<span data-letters="'+ node.text +'">' + node.innerHTML + '</span>'+'<span data-letters="'+ node.text +'">' + node.innerHTML + '</span>';
            }
        };
    }
}

linkify( '.menu-item a' );

$(document).scroll(function () {

    var $this = $(this);

    if ($this.scrollTop() > 0){

        var ile = $this.scrollTop();

        var a = (ile / 5);
        b = (ile / 2);
        c = ile / 6;
        var szerokosc = $( ".container" ).innerWidth();
        var szerokosc2 = $( ".ceo-wrapper" ).innerWidth();
        // alert(szerokosc2);
        if(szerokosc2 >= 970 && szerokosc2 < 1200){
            $('.ceo-wrapper').css({"background-position": "bottom -" + 0+c + "px right -360px"});
        }
        if(szerokosc2 >= 1200 && szerokosc2 < 1360){
            $('.ceo-wrapper').css({"background-position": "bottom -" + 0+c + "px right -220px"});
        }
        if(szerokosc2 >= 1360 && szerokosc2 < 1500){
            $('.ceo-wrapper').css({"background-position": "bottom -" + 0+c + "px right -450px"});
        }
        if(szerokosc2 >= 1500 && szerokosc2 < 1920){
            $('.ceo-wrapper').css({"background-position": "bottom -" + 0+c + "px right 12%"});
        }
        if(szerokosc2 >= 1920){
            $('.ceo-wrapper').css({"background-position": "bottom -" + 0+c + "px right 30%"});
        }
        if(a >= 0){
            $('.slider__item').css({"background-position": "center bottom -" + 0+b + "px"});
        }
    }

});


var hamburger = document.querySelector(".hamburger");
// On click
hamburger.addEventListener("click", function() {
    // Toggle class "is-active"
    hamburger.classList.toggle("is-active");
    // Do something else, like open/close menu
});


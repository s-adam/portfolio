<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*
Template Name: Produkt
Template Post Type: zoo
*/
get_header();
the_post();

$fields = get_fields(get_the_ID());
$publishable_lite_single_breadcrumb_section = get_theme_mod('publishable_lite_single_breadcrumb_section', '1');
?>


	<div class="section-oblique" style="background: #a2aab0;padding-top: 250px">
		<img class="skos" src="<?php echo get_stylesheet_directory_uri(); ?>/img/skos.svg" alt="">
	</div>
	<div class="main-wrapper" style="overflow: hidden; margin-top: -200px">
		<div class="container">
			<div class="row">
				<div class="produkt">
					<?php
					$parent = get_post(wp_get_post_parent_id( get_the_ID() ));
					$rootParent = get_post(wp_get_post_parent_id( $parent->ID ));

					?>
					<div class="breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#">
						<span typeof="v:Breadcrumb" class="root">
							<a rel="v:url" property="v:title" href="/">Strona Główna</a>
						</span>
						<span><i class="publishable-icon icon-angle-double-right"></i></span>
						<span typeof="v:Breadcrumb" class="root">
							<a rel="v:url" property="v:title" href="<?=get_permalink($rootParent->ID)?>"><?=$rootParent->post_title?></a>
						</span>
						<span><i class="publishable-icon icon-angle-double-right"></i></span>

						<span typeof="v:Breadcrumb" class="root">
							<a rel="v:url" property="v:title" href="<?=get_permalink($parent->ID)?>"><?=$parent->post_title?></a>
						</span>
						<span><i class="publishable-icon icon-angle-double-right"></i></span>


						<span><span><?php the_title(); ?></span></span>
					</div>
					<div class="col-md-5">
						<div class="produkt__photo">
							<?php the_post_thumbnail(); ?>
						</div>
					</div>
					<div class="col-md-7">
							<div class="produkt__description">
								<h3><a href="#"><?php the_title(); ?></a></h3>



									<div class="producent"><?php echo get_the_title( $post->post_parent );?></div>


								<div class="row">
									<div class="col-md-8">
										<div class="row">
											<div class="col-md-6">
												<p>Przeznaczenie</p>
											</div>

											<div class="col-md-6">
												<p><strong><?= $fields['przeznaczenie']; ?></strong></p>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<p>Opakowanie</p>
											</div>
											<div class="col-md-6">
												<p><strong><?= $fields['waga_produktu']; ?></strong></p>
											</div>
										</div>
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/non-gmo.jpg" alt="" class="non-gmo">
									</div>
									<div class="col-md-4 produkt__buttons">
										<a href="<?= $fields['link_do_zakupu']; ?>" target="_blank" class="btn btn-kup">Kup teraz</a>
										<a href="/kontakt" class="btn btn-zapytaj">Zapytaj</a>

									</div>
								</div>
							</div>
					</div>


				</div>

			</div>
			<div class="tab-container">

			<?php
			if($fields['informacje_o_produkcie']){
				foreach ($fields['informacje_o_produkcie'] as $field) {

					?>
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
					<?php

					if($field['tytuł_zakładki']){
						foreach ($field['tytuł_zakładki'] as $f1) {
							$a1 = 0;
							$a2 = 0;
							?>
							<li role="presentation" class="active"><a href="#<?= $a1++?>" aria-controls="home" role="tab" data-toggle="tab"><?= $f1['tytuł']; ?></a>
							</li>
							<?php
							$a1++;
							$a2++;
						}}

							?>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
					<?php

					if($field['zawartość_zakładki']){
						foreach ($field['zawartość_zakładki'] as $f2) {
							$a1 = 0;
							$a2 = 0;
							?>
							<div role="tabpanel" class="tab-pane active" id="<?=$a2?>">
								<?= $f2['tekst'] ?>
							</div>
							<?php
							$a1++;
							$a2++;
						}}
							?>
					</div>
					<?php
				}}
					?>
			</div>
		</div>
	</div>

	<div class="similar-products">
		<div class="container">
			<div class="wrap-opinions">
				<div class="title-contact">
					<h3>OPINIE NASZYCH KLIENTÓW</h3>
				</div>

				<div class="similar-products-carousel swiper-container swiper-container-wrap">
					<div class="row text-center swiper-wrapper">
					<?php
					$args = array(
						'post_type'      => 'zoo',
						'posts_per_page' => -1,
//						'post_parent'    => 0,
						'order'          => 'ASC',
						'orderby'        => 'menu_order'
					);


					$parent = new WP_Query( $args );


					?>
					<?php
					if ( $parent->have_posts() ) : ?>

						<?php

						while ( $parent->have_posts() ) : $parent->the_post();?>
						<div class="swiper-slide">
							<div class="similar-products__photo">
								<?php the_post_thumbnail(); ?>
							</div>
							<div class="similar-products__name">
								<h3><?php echo the_title();?></h3>
							</div>
							<div class="similar-products__producent">
								<p><?php echo get_the_title( $post->post_parent );?></p>

							</div>
						</div>
							<?php
						endwhile;
					endif;
					?>
						<?php wp_reset_query(); ?>
					</div>


					<!-- If we need navigation buttons -->
					<div class="swiper-button-prev"></div>
					<div class="swiper-button-next"></div>


				</div>
			</div>
		</div>
	</div>
<?php
get_template_part('doradcy');
?>

<?php
get_footer();

<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package idvetmp
 */

?>

	</div><!-- #content -->

	<footer class="site-footer">
            <div class="hidden-div-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center footer-text">
                            Wszelkie prawa zastrzeżone &copy Biofeed Sp.z o.o
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center footer-autor">
                            <a href="https://ideative.pl">
                                <img src="<?=get_template_directory_uri().'/img/by-ideative.svg'?>" alt="ideative.pl">
                            </a>
                        </div>
                    </div>
                </div>
            </div>

	</footer><!-- #colophon -->
</div><!-- #page -->

<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/parallax.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/hamburger.js"></script>

<?php wp_footer(); ?>

</body>
</html>

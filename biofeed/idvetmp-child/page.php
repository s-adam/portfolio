<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */

get_header(); ?>

<div class="section-oblique-archive">
	<h2><?php the_title();?></h2>
	<img class="skos" src="<?php echo get_stylesheet_directory_uri(); ?>/img/skos.svg" alt="">
</div>


	<div class="archive-main-container">
		<div class="container">
			<?php
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

			query_posts(array('posts_per_page' => 2, 'paged' => $paged));
			if ( have_posts() ) : ?>
				<?php
				while ( have_posts() ) : the_post();
					?>

					<div class="row">
						<div class="col-md-5">
							<div class="news-post__photo">
								<?php the_post_thumbnail(); ?>
							</div>
						</div>
						<div class="col-md-7">
							<div class="news-post">
								<div class="news-post__archive_title">
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								</div>
								<span class="news-post__archive_date"><?php the_time('d F Y'); ?></span>
								<div class="news-post__archive_text">
									<?php
									the_excerpt_max_charlength(300);
									?>
								</div>
							</div>
						</div>

					</div>
							<img class="signs-after-post" src="<?php echo get_stylesheet_directory_uri(); ?>/img/small-icons.png" alt="">





					<?php
				endwhile;
				?>
				<?php the_posts_pagination( array(
					'mid_size' => 2,
					'prev_text' => __( '&laquo', 'textdomain' ),
					'next_text' => __( '&raquo', 'textdomain' ),
				) ); ?>
				<?php
			else :
			endif;
			?>
			<?php wp_reset_query(); ?>

		</div>
	</div>

<?php
get_footer();

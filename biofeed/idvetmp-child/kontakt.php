<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*
Template Name: Kontakt
*/
get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>

	<div class="section-oblique kontakt-skos">
		<img class="skos" src="<?php echo get_stylesheet_directory_uri(); ?>/img/skos.svg" alt="">
	</div>
	<div class="main-title main-title-contact">
		<h1><?php the_title(); ?></h1>
	</div>
	<div class="main-wrapper kontakt" style="overflow: hidden;">

		<div class="container">
			<div class="row">

				<div class="col-md-6">
					<h3>Bifoeed Sp.z.o.o</h3>

<?php

if($fields['dane_kontaktowe']){
	foreach ($fields['dane_kontaktowe'] as $field) {


		?>

		<?php
		if($field['acf_fc_layout'] == 'tekst_w_2_kolumnach'){
			?>
					<div class="kontakt__single-info-wrapper">
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<?=$field['kol1']?>
							</div>
							<div class="col-md-6 col-sm-6">
								<?php
								$telefon = 0;
								if($field['kol2']){
								foreach ($field['kol2'] as $f1) {
									?>
									<h6><?= $f1['tytuł_-_szary_napis'] ?></h6>
									<a href="tel:<?= $telefon = trim($f1['numer_telefonu']) ?>"
									   class="kontakt__phone"><i class="fa fa-phone" aria-hidden="true"></i><?= $f1['numer_telefonu'] ?></a>
									<a href="mailto:<?= $f1['email'] ?>"
									   class="kontakt__email"><i class="fa fa-envelope-open" aria-hidden="true"></i><?= $f1['email'] ?></a>
									<?php
								}}
									?>
							</div>
						</div>
					</div>
			<?php
		}
		?>

		<?php

	}
}

?>

				</div>
				<div class="col-md-6">
					<div class="contact-form-wrapper">
						<h4>Napisz do nas</h4>
						<?php the_content(); ?>
					</div>
				</div>

			</div>

		</div>
	</div>


<?php
get_footer();

<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*
Template Name: Doradcy
*/
get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>



<?php

if($fields['dane_doradcy']){
	foreach ($fields['dane_doradcy'] as $field) {

		?>
		<div class="row">
			<div class="contact-info__wrapper">
				<div class="col-md-2">
					<div class="contact-info__photo">
						<img src="<?=wp_get_attachment_image( get_post_thumbnail_id(),'large');?>" alt="">
					</div>
				</div>
				<div class="col-md-2">
					<div class="contact-info__name">
						<?= $field['dane']?>
					</div>
				</div>
				<div class="col-md-4">
					<div class="contact-info__phone">
						<a href="<?= $field['numer_telefonu']?>" target="_blank"><i class="fa fa-phone" aria-hidden="true"></i><span>+48</span><?= $field['numer_telefonu']?></a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="contact-info__button">
						<a href="#" class="btn">Napisz</a>
					</div>
				</div>
			</div>
		</div>
		<?php
	}}
		?>

<?php
get_footer();

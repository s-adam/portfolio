<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */

get_header();
the_post();?>



	<div class="section-oblique-archive" style="background: #a2aab0;">
		<div class="main-title">
			<h2>Kariera</h2>
		</div>
		<img class="skos" src="<?php echo get_stylesheet_directory_uri(); ?>/img/skos.svg" alt="">
	</div>
	<div class="main-wrapper" style="overflow: hidden">

		<div class="container">
			<div class="career">
<?php //juz

$args = array(
	'posts_per_page' => 5,
	'post_type'      => 'kariera',
	'paged'          => get_query_var( 'paged' ),
);
$wp_query = new WP_Query( $args );
while ( $wp_query->have_posts() ) : $wp_query->the_post();

	$fields = get_fields(get_the_ID());
	?>
						<div class="row">
							<div class="col-md-2 teczka">
								<?=wp_get_attachment_image( get_post_thumbnail_id(),'medium');?>
							</div>
							<div class="col-md-10">
								<div class="career__desc">
									<a href="<?php the_permalink() ?>"><h3><?php the_title() ?></h3></a>
									<h6><?= $fields['miejsce_pracy']; ?></h6>
									<?= $fields['zajawka']; ?>
									<a href="<?php the_permalink() ?>" class="btn">Zobacz więcej</a>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 text-center">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/small-icons.png" alt="" class="img-bottom">
							</div>
						</div>
	<?php
endwhile;

?>
				<?php the_posts_pagination( array(
					'mid_size' => 3,
					'prev_text' => __( '&laquo', 'textdomain' ),
					'next_text' => __( '&raquo', 'textdomain' ),
				) ); ?>
			</div>

		</div>
	</div>



<?php
get_footer();

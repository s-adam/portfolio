<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*
Template Name: Agro - Produkt
Template Post Type: agro
*/
get_header('');
the_post();

$fields = get_fields(get_the_ID());
?>

    <div class="section-oblique" style="background: #a2aab0;padding-top: 250px">
        <img class="skos" src="<?php echo get_stylesheet_directory_uri(); ?>/img/skos.svg" alt="">
    </div>
    <div class="main-wrapper" style="overflow: hidden; margin-top: -100px">
        <div class="container">
            <div class="slider swiper-container-2">
                <div class="swiper-wrapper text-center">
                    <div class="wrap">
                            <?php
                            if($fields['slider']){
                                foreach ($fields['slider'] as $field) {
                                    ?>
                                    <div class="one-slide" style="background: url('<?= $field['zdjęcie_w_tle']['sizes']['large']; ?>') center center;background-size: cover;">
                                        <div class="flex-container">
                                            <div class="flex-item-default">
                                                <?= $field['treść']; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }}
                            ?>
                    </div>
                </div>
            </div>
            <?php
            if($fields['sekcja_pod_sliderem']){
                foreach ($fields['sekcja_pod_sliderem'] as $field) {

                    ?>
                    <div class="pasze" style="background: #fff;position: relative;">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="pasze__photo">
                                    <img src="<?= $field['zdjęcie_zwierzęcia']['sizes']['large']; ?>" alt="">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="pasza__description">
                                    <?= $field['tekst_obok_zdjęcia']; ?>
                                </div>
                            </div>
                        </div>

                    </div>

                    <?php
                }}
            ?>


        </div>
    </div>


    <?php require 'doradcy.php'; ?>


    <div class="container">
        <div class="kinds-of-food">
            <div class="row text-center">
                <?php

                if($fields['rodzaje_paszy']){
                    foreach ($fields['rodzaje_paszy'] as $field) {

                        ?>
                        <div class="col-md-3 text-center">
                            <a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/pdf.jpg" alt=""></a>
                            <p><?= $field['nazwa'] ?></p>
                            <a href="<?= $field['plik']['url'] ?>" target="_blank" class="btn">POBIERZ OFERTĘ</a>
                        </div>
                        <?php
                    }}
                ?>
            </div>
        </div>
    </div>

    <div class="similar-products">
        <div class="container">
            <div class="wrap-opinions">
                <div class="title-contact">
                    <h3>NASZE PRODUKTY</h3>
                </div>

                <div class="similar-products-carousel swiper-container swiper-container-wrap">
                    <div class="row text-center swiper-wrapper">


                        <?php
                        $args = array(
                            'post_type'      => 'agro',
                            'posts_per_page' => -1,
                            'post_parent'    => get_the_ID(),
                            'order'          => 'ASC',
                            'orderby'        => 'menu_order'
                        );


                        $parent = new WP_Query( $args );


                        ?>
                        <?php
                        if ( $parent->have_posts() ) : ?>

                            <?php

                            while ( $parent->have_posts() ) : $parent->the_post();?>
                                <div class="swiper-slide">
                                    <div class="similar-products__photo">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/prod-slider.jpg" alt="">
                                    </div>
                                    <div class="similar-products__name">
                                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                    </div>
                                    <div class="similar-products__producent">
                                        <p>EUPHORIA</p>
                                    </div>
                                </div>

                                <?php

                            endwhile;
                        else :
                        endif; ?>
                    </div>


                    <!-- If we need navigation buttons -->
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>


                </div>
            </div>
        </div>
    </div>
<?php
get_footer();

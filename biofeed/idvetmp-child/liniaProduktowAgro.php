<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*
Template Name: Linia Produktów AGRO
Template Post Type: agro
*/
get_header('');
the_post();
$publishable_lite_single_breadcrumb_section = get_theme_mod('publishable_lite_single_breadcrumb_section', '1');
$fields = get_fields(get_the_ID());
?>

    <div class="section-oblique" style="background: #a2aab0;padding-top: 250px">
    </div>
    <div class="main-wrapper margin-simulate">

        <div class="container">
            <div class="agro-pagination">
                <div class="swiper-pagination"></div>
            </div>

            <?php if($publishable_lite_single_breadcrumb_section == '1') { ?>
                <div class="breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#"><?php publishable_lite_the_breadcrumb(); ?></div>
            <?php } ?>
            <div class="swiper-container swiper-container-agro">

                <div class="swiper-wrapper">
                    <?php

                    if($fields['slider']){
                        foreach ($fields['slider'] as $field) {

                            ?>
                            <div class="one-slide swiper-slide"
                                 style="background: url('<?= $field['zdjęcie_w_tle']['sizes']['sliderPhoto']; ?>') center center;background-size: cover;">
                            </div>
                            <?php
                        }}
                    ?>

                </div>
            </div>



            <?php

            if($fields['sekcja_pod_sliderem']){
                foreach ($fields['sekcja_pod_sliderem'] as $field) {

                    ?>
                    <div class="pasze" style="background: #fff;position: relative;">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="pasze__photo">
                                    <img src="<?= $field['zdjęcie_zwierzęcia']['sizes']['large']; ?>" alt="">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="pasza__description">
                                    <?= $field['tekst_obok_zdjęcia']; ?>
                                </div>
                            </div>
                        </div>

                    </div>

                    <?php
                }}
            ?>


        </div>
    </div>






    <div class="container">
        <div class="kinds-of-food">
            <div class="row text-center">
                <?php

                if($fields['rodzaje_paszy']){
                    foreach ($fields['rodzaje_paszy'] as $field) {

                        ?>
                        <div class="col-md-3 text-center">
                            <a href="<?= $field['plik']['url'] ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/pdf.jpg" alt=""></a>
                            <p><?= $field['nazwa'] ?></p>
                            <a href="<?= $field['plik']['url'] ?>" target="_blank" class="btn">POBIERZ OFERTĘ</a>
                        </div>
                        <?php
                    }}
                ?>
            </div>
        </div>
    </div>
<?php
$fields = get_fields(get_the_ID());
$args = array(
    'post_type'      => 'agro',
    'posts_per_page' => 5,
    'post_parent'    =>  get_the_ID(),
    'order'          => 'ASC',
    'orderby'        => 'menu_order',
    'depth' => 1
);


$parent = new WP_Query( $args );


?>
<?php
if ( $parent->have_posts() ) : ?>
    <div class="similar-products">
        <div class="container">
            <div class="wrap-opinions">
                <div class="title-contact">
                    <h3>LINIE PRODUKTOWE</h3>
                </div>

                <div class="similar-products-carousel swiper-container">
                    <div class="text-center swiper-wrapper">





                            <?php
                            $i = 0;
                            while ( $parent->have_posts() ) : $parent->the_post();?>
                                <div class="swiper-slide">
                                    <div class="similar-products__photo">
                                        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('slide-produktow'); ?></a>
                                    </div>
                                    <div class="similar-products__name">
                                        <h3><a href="<?php the_permalink($post->post_parent ); ?>"><?php echo get_the_title( ); ?></a></h3>
                                    </div>
                                    <a href="<?php the_permalink(); ?>" class="btn ask">KUP TERAZ</a>
                                </div>

                                <?php

                            endwhile;
                            ?>
                    </div>




                </div>
                <div class="container">
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
            </div>
        </div>
    </div>
<?php
endif;
?>
<?php wp_reset_query(); ?>
<?php
if($fields['relacja']!= null) {

    get_template_part('doradcy');
}
?>
<?php
get_footer();

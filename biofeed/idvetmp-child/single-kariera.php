<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package idvetmp
 */
/*
Template Name: Kariera
*/
get_header();
the_post();
$fields = get_fields(get_the_ID());

?>


    <div class="section-oblique-archive-2 single-archive">
        <div class="main-title archive-title">
            <h1><?php the_title(); ?></h1>
            <h6><?= $fields['miejsce_pracy']; ?></h6>
        </div>
    </div>

    <div class="main-wrapper single-main-wrapper">

        <div class="container">
            <div class="career">
                <div class="row">
                    <div class="col-md-9 col-md-offset-3">
                        <div class="career__desc">
                            <?php the_content(); ?>
                        </div>
                    </div>
                    <?php
                    get_template_part('praca');
                    ?>
                </div>
            </div>

        </div>
    </div>


<?php
get_footer();

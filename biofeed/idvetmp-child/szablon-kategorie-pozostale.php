<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package idvetmp
 */
/*
Template Name: Szablon Kategorii Pozostałe
Template Post Type: pozostale
*/
get_header('');
the_post();
$publishable_lite_single_breadcrumb_section = get_theme_mod('publishable_lite_single_breadcrumb_section', '1');
$fields = get_fields(get_the_ID());
$currentID = get_the_ID();
?>

	<div class="section-oblique" style="background: #a2aab0;padding-top: 250px">
	</div>
	<div class="main-wrapper margin-simulate">
		<div class="container">
			<?php
			$parent = get_post(wp_get_post_parent_id( get_the_ID() ));
			$rootParent = get_post(wp_get_post_parent_id( $parent->ID ));

			?>
			<div class="breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#">
						<span typeof="v:Breadcrumb" class="root">
							<a rel="v:url" property="v:title" href="/">Strona Główna</a>
						</span>
				<span><i class="publishable-icon icon-angle-double-right"></i></span>



				<span><span><?php the_title(); ?></span></span>
			</div>
			<div class="row">

				<div class="col-md-4">

					<div class="category-wrapper">
						<ul>
							<?php wp_reset_query(); ?>
							<?php

							$fields = get_fields(4);
							if($fields['sekcja']){

								foreach ($fields['sekcja'] as $field) {
									?>


										<li><a class="class="<?=get_the_ID()==$currentID?'active':''?>"" href="<?php the_permalink($field['sekcja_produkty']->ID); ?>"><?php echo $field['sekcja_produkty']->post_title; ?></a></li>

									<?php
								}
							}

							?>
						</ul>
					</div>
				</div>
				<div class="col-md-8">
					<div class="single-category-product">
						<?php
						$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
						$args = array(
							'post_type'      => 'pozostale',
							'posts_per_page' => 5,
							'post_parent'    =>  get_the_ID(),
							'order'          => 'ASC',
							'orderby'        => 'menu_order',
							'paged'=>$paged
						);

						$wp_query = null;

						$wp_query = new WP_Query( $args );
						?>
						<?php
						if($wp_query->have_posts() == true){
							if ( $wp_query->have_posts() ) : ?>
								<?php
								while ( $wp_query->have_posts() ) : $wp_query->the_post();?>
									<div class="produkt">
										<?php $fields = get_fields(get_the_ID()); ?>
										<div class="col-md-5">
											<div class="produkt__photo">
												<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('lista-produktow'); ?></a>
											</div>
										</div>
										<div class="col-md-7">
											<div class="produkt__description">
												<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
												<div class="producent"><p><a href="<?php the_permalink($post->post_parent)?>"><?php echo get_the_title( $post->post_parent ); ?></a></p></div>
												<div class="col-md-4 sidebar">
												</div>
												<div class="row single-category-">
													<div class="col-md-12">
														<div class="row">
															<div class="col-md-6">
																<p>Przeznaczenie</p>
															</div>

															<div class="col-md-6">
																<p><strong><?= $fields['przeznaczenie']?></strong></p>
															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
																<p>Opakowanie</p>
															</div>
															<div class="col-md-6">
																<p><strong><?= $fields['waga_produktu']?></strong></p>
															</div>
															<a href="<?php the_permalink(); ?>" class="btn btn-zapytaj">Zobacz</a>
														</div>
													</div>

												</div>
											</div>
										</div>
									</div>

									<?php
								endwhile;
								the_posts_pagination( array(
									'mid_size' => 3,
									'prev_text' => __( '&laquo', 'textdomain' ),
									'next_text' => __( '&raquo', 'textdomain' ),
								) );
							endif;
						}else{
							?>
							<div class="no-products">
								<h1>BRAK PRODUKTÓW</h1>
							</div>
							<?php
						}
						?>


						<!--                        <div class="nav-previous alignleft">--><?php //next_posts_link( 'Older posts' ); ?><!--</div>-->
						<!--                        <div class="nav-next alignright">--><?php //previous_posts_link( 'Newer posts' ); ?><!--</div>-->

						<?php wp_reset_query(); ?>


					</div>
				</div>
			</div>
		</div>
	</div>


<?php
get_footer();
